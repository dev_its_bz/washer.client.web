<!DOCTYPE html>
<!-- saved from url=(0032)http://jonathanbird.com.au/lead/ -->
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>ПОМОЙ МЕНЯ! - Единый сервис записи на автомойки</title>
    <!-- google font -->
    <link href="./Lead_files/css" rel="stylesheet" type="text/css">
    <!-- bootstrap 3 & countdown -->
    <link rel="stylesheet" href="./Lead_files/bootstrap.min.css">
    <link rel="stylesheet" href="./Lead_files/jquery.countdown.css">
    <!-- overall styles -->
    <link rel="stylesheet" href="./Lead_files/style.css">
    <link rel="shortcut icon" href="./Lead_files/favicon.png">
    <script src="./Lead_files/jquery-1.7.1.min.js"></script>
    <style type="text/css"></style>
    <script src="./Lead_files/functions.js"></script>
</head>
<body id="home" style="opacity:0;" onLoad='$("#home").fadeTo("slow" , 1, function() {});'>
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div id="header">
                <h2 id="logo">ПОМОЙ-МЕНЯ.РФ <br/>
                    <small>washmeup.com</small>
                </h2>
            </div>
            <div id="intro">
                <h2 style="font-size:14pt">Единый сервис записи на автомойки</h2>
                До запуска сервиса осталось:
            </div>
            <div id="countdown-wrap">
                <div id="countdown" class="countdown">
                </div>
            </div>
            <div id="signup">
                <form id="easy" name="contact" method="post" class="validate" action="#" onsubmit="return checkform()">
                    <div id="error">Ошибка! ((</div>
                    <div id="success">Спасибо!</div>
                    <input type="email" value="" id="email" name="email" placeholder="Ваш Email *"
                           class="required email">
                    <input type="submit" value="Подписаться" name="subscribe" class="button">
                </form>
            </div>
            <br><br>
        </div>
        <div class="col-md-7">
            <div id="laptop">
                <img src="./Lead_files/promo_client.png" alt="Washer Screenshot">
            </div>
        </div>
    </div>
</div>
<div class="white-bg clients">
    <div class="container">
        <div class="row">
            <h3>Наша команда</h3>

            <div class="col-md-3">
                <img class="client-img" src="./Lead_files/no_avatar.png" alt="person one">
                <h4>Андрей</h4>

                <p class="italic justify">"Руководитель проекта"</p>
            </div>

            <div class="col-md-3">
                <img class="client-img" src="./Lead_files/no_avatar.png" alt="person two">
                <h4>Константин</h4>

                <p class="italic justify">"Ведущий программист"</p>
            </div>

            <div class="col-md-3">
                <img class="client-img" src="./Lead_files/no_avatar.png" alt="person three">
                <h4>Михаил</h4>

                <p class="italic justify">"Аналитик"</p>
            </div>

            <div class="col-md-3">
                <img class="client-img" src="./Lead_files/no_avatar.png" alt="person four">
                <h4>Алексей</h4>

                <p class="italic justify">"Автор и вдохновитель проекта"</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="grey-bg">
    <div class="container">
        <div class="row">
            <center>
                <h3>Наши цели</h3>
            </center>

            <div class="col-md-4 services">
                <span class="webdesign"><img style="height: 150px; opacity: 0.8" src="./Lead_files/wallet.png"
                                             alt="Web Design"></span>

                <h2 class="service-heading">Автовладелец</h2>

                <p class="justify">
                <ul class="list-group" style="text-align: left">
                    <li class="list-group-item" style="text-align: left">Получает возможность быстро найти место, где
                        помоют его машину. Без ожидания, точно зная цену и назначенное время.
                    </li>
                    <li class="list-group-item" style="text-align: left">Сможет поделиться с другими своими
                        впечатлениями о качестве работ на выбранной мойке, а также отсортировать мойки по рейтингу и
                        прочитать отзывы других посетителей.
                    </li>
                    <li class="list-group-item" style="text-align: left">Получать скидки на мойках, участвующих в
                        системе.
                    </li>
                </ul>
            </div>
            <div class="col-md-4 services">
                <span class="branding"><img style="height: 150px; opacity: 0.8" src="./Lead_files/shop1.png"
                                            alt="Branding"></span>

                <h2 class="service-heading">Автомойки</h2>
                <ul class="list-group" style="text-align: left">
                    <li class="list-group-item" style="text-align: left">Получают систему автоматизации управления
                        заказами, работой по заказам, расчёт зарплаты рабочих, аналитическую информацию.
                    </li>
                    <li class="list-group-item" style="text-align: left">Систематизированные и упорядоченные по времени,
                        боксам и назначенным рабочим заказы, тем самым минимизируя простои и исключая коллизии.
                    </li>
                    <li class="list-group-item" style="text-align: left">Дополнительный приток клиентов, привлечение
                        постоянных клиентов.
                    </li>
                </ul>
            </div>
            <div class="col-md-4 services">
                <span class="seo"><img style="height: 150px; opacity: 0.8" src="./Lead_files/banknote.png"
                                       alt="SEO"></span>
                <nobr><h2 class="service-heading">Смежный бизнес</h2></nobr>
                <ul class="list-group" style="text-align: left">
                    <li class="list-group-item" style="text-align: left">Получит возможность рекламировать свою
                        продукцию на страницах сервиса.
                    </li>
                    <li class="list-group-item" style="text-align: left">Работать с автомойками напрямую, используя
                        систему электронного заказа расходных материалов и автохимии.
                    </li>
                    <li class="list-group-item" style="text-align: left">Прогнозировать объём закупок мойками продукции,
                        используя инструментарий аналитики.
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="white-bg clients">
    <div class="container">
        <div class="row">
            <center>
                <h3>Поддержите нас</h3>
            </center>
            Мы верим, что наш сервис будет полезен людям и сделает жизнь чуточку лучше. Мы любим наш проект и трудимся
            над ним, движимые своим искренним энтузиазмом.<br/>У нас нет спонсоров, нет инвесторов - мы считаем, это
            испортит проект. Но для
            работы нам необходима Ваша поддержка - моральная или материальная. <br/>
            <br/>
            <i>
                Если вам интересен наш проект - поделитесь ссылкой на него - пусть о нём узнают Ваши друзья и знакомые
                или поддержите нас финансво, используя форму ниже.<br/><br/>
            </i>
            <b>
                Спасибо!
            </b>
            <br/>
            <iframe frameborder="0" allowtransparency="true" scrolling="no"
                    src="https://money.yandex.ru/embed/donate.xml?account=41001107366728&quickpay=donate&payment-type-choice=on&default-sum=100&targets=%D0%9D%D0%B0+%D1%80%D0%B0%D0%B7%D0%B2%D0%B8%D1%82%D0%B8%D0%B5+%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0+%D0%9F%D0%9E%D0%9C%D0%9E%D0%99-%D0%9C%D0%95%D0%9D%D0%AF.%D0%A0%D0%A4&target-visibility=on&project-name=%D0%9F%D0%9E%D0%9C%D0%9E%D0%99-%D0%9C%D0%95%D0%9D%D0%AF.%D0%A0%D0%A4&project-site=washmeup.com&button-text=05"
                    width="507" height="133"></iframe>
        </div>
    </div>
</div>
<footer id="footer-container">
    <div class="container">
        <h2>Подписывайтесь!</h2>

        <div class="social-snippet">
            <a href="#"><img src="./Lead_files/social-fb.png"></a>
            <a href="#"><img src="./Lead_files/social-tw.png"></a>
            <a href="#"><img src="./Lead_files/social-li.png"></a>
            <a href="#"><img src="./Lead_files/social-gp.png"></a>
        </div>
        <br>
        <a href="mailto:info@its.bz" title="info@its.bz" rel="noreferrer">info@washmeup.com&nbsp;&nbsp;&nbsp;&nbsp;</a>|<a
            href="http://washmeup.com">&nbsp;&nbsp;&nbsp;&nbsp;+7 910 737 3125</a>
        <!-- the usual copyright crap -->
        <div class="copyright">АйТиЭс © 2014. Все права защищены.<br><a href="#">^ Наверх ^</a></div>
        <div class="clearfix"></div>
    </div>
</footer>

<script src="./Lead_files/jquery.countdown.js"></script>
<script>
    // countdown setup
    // set date, note: months are 0-based ---- numbers are: year, month, day, hour, minute, second, milliseconds
    var ts = (new Date(2014, 5, 1, 0, 0, 0, 0));
    $('#countdown').countdown({timestamp: ts, movement: 60});
</script>
</body>
</html>