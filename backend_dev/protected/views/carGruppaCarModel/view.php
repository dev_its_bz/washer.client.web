<?php
$this->breadcrumbs=array(
	'Car Gruppa Car Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CarGruppaCarModel','url'=>array('index')),
	array('label'=>'Create CarGruppaCarModel','url'=>array('create')),
	array('label'=>'Update CarGruppaCarModel','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete CarGruppaCarModel','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CarGruppaCarModel','url'=>array('admin')),
);
?>

<h1>View CarGruppaCarModel #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'car_gruppa_id',
		'car_model',
		'data_sozdaniya',
		'avtor_zapisi_id',
		'data_zapisi',
	),
)); ?>
