<?php
$this->breadcrumbs=array(
	'Car Gruppa Car Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CarGruppaCarModel','url'=>array('index')),
	array('label'=>'Manage CarGruppaCarModel','url'=>array('admin')),
);
?>

<h1>Create CarGruppaCarModel</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>