<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'car-gruppa-car-model-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'car_gruppa_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'car_model',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'data_sozdaniya',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'avtor_zapisi_id',array('class'=>'span5','maxlength'=>11)); ?>

	<?php echo $form->textFieldRow($model,'data_zapisi',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
