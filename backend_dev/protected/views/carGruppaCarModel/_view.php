<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('car_gruppa_id')); ?>:</b>
	<?php echo CHtml::encode($data->car_gruppa_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('car_model_id')); ?>:</b>
	<?php echo CHtml::encode($data->car_model_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_sozdaniya')); ?>:</b>
	<?php echo CHtml::encode($data->data_sozdaniya); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('avtor_zapisi_id')); ?>:</b>
	<?php echo CHtml::encode($data->avtor_zapisi_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_zapisi')); ?>:</b>
	<?php echo CHtml::encode($data->data_zapisi); ?>
	<br />


</div>