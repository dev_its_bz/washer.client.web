<?php
$this->breadcrumbs=array(
	'Car Gruppa Car Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CarGruppaCarModel','url'=>array('index')),
	array('label'=>'Create CarGruppaCarModel','url'=>array('create')),
	array('label'=>'View CarGruppaCarModel','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage CarGruppaCarModel','url'=>array('admin')),
);
?>

<h1>Update CarGruppaCarModel <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>