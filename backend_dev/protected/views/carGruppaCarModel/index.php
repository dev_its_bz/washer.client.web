<?php
$this->breadcrumbs = array(
    'Car Gruppa Car Models' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List CarGruppaCarModel', 'url' => array('index')),
    array('label' => 'Create CarGruppaCarModel', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('car-gruppa-car-model-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage <?php echo $groupname ?></h1>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'car-gruppa-car-model-grid',
    'dataProvider' => $model->search($group_id),
    'filter' => $model,
    'columns' => array(
        array(
            'type' => 'raw',
            'name' => 'columMarka',
            'filter' => CHtml::listData(CarMarka::model()->findAll(), 'id', 'nazvanie'),
            'value' => '$data->carmodels->carmarks->nazvanie',
        ),
        array(
            'name' => 'searchModel',
            'type' => 'raw',
            'value' => '$data->carmodels->nazvanie',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array(
                'delete' => array(
                    'lable' => 'Удалить',
                    'icon' => 'trash',
                    'url' => 'Yii::app()->createUrl("CarGruppaCarModel/Delete", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
));
?>
