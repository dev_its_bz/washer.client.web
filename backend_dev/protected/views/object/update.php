<?php
$this->breadcrumbs = array(
    'Объекты' => array('admin'),
    $model->nazvanie => array('view', 'id' => $model->id),
    'Редактор',
);
?>

<div class="page-header">
    <h1>Редактор <?php echo $model->nazvanie; ?></h1>
</div>


<?php echo $this->renderPartial('_form', array('model' => $model, 'grafik' => $grafik, 'gorod' => $gorod)); ?>