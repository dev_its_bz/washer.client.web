<?php
$this->widget('TbEditableDetailView', array(
    'data' => $model,
    'url' => $this->createUrl('/object/UpdateZnachenie'),
    'attributes' => array(
        'id',
        'nazvanie',
        'adres',
        'telefon',
        array(
            'name' => 'grafik_raboty_id',
            'editable' => array(
                'type' => 'select',
                'source' => CHtml::listData(GrafikRaboty::model()->findAllByAttributes(array("kontragent_id" => $model->kontragent_id)), 'id', 'nazvanie')
            )
        ),
        array(
            'name' => 'gorod_id',
            'editable' => array(
                'type' => 'select',
                'source' => CHtml::listData(Gorod::model()->findAll(), 'id', 'nazvanie')
            )
        ),
        'zarplata',
    ),
));
?>