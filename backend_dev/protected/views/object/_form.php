<div class="alert alert-info glyphicon-info-sign">Поля, отмеченные <span class="required">*</span> обязательны к
    заполнению.
</div>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'object-form',
    'htmlOptions' => array('class' => 'well'),
    'enableAjaxValidation' => false,
    'focus'=>array($model,'nazvanie'),
));
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'nazvanie', array('class' => 'span5', 'maxlength' => 100)); ?>

<?php echo $form->textAreaRow($model, 'adres', array('rows' => 2, 'class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'telefon', array('maxlength' => 30)); ?>

<div class="row2">
    <?php echo $form->labelEx($model, 'Город'); ?>
    <?php echo $form->dropDownList($model, 'gorod_id', $gorod, array('prompt' => '(Укажите город)'));
    ?>
    <?php echo $form->error($model, 'gorod_id'); ?>
</div>

<div class="row2">
    <?php echo $form->labelEx($model, 'График работы'); ?>
    <?php echo $form->dropDownList($model, 'grafik_raboty_id', $grafik, array('prompt' => '(Укажите график)'));
    ?>
    <?php echo $form->error($model, 'grafik_raboty_id'); ?>
</div>

<?php echo $form->textFieldRow($model, 'zarplata'); ?>

<hr/>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'icon' => 'check white',
    'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
));
?>

<?
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'danger',
    'icon' => 'remove white',
    'label' => 'Отмена',
    'htmlOptions' => array(
        'onClick' => "window.location.href = '/kontragent/view/" . @$_GET['id'] . "?tab=3'",
    )
));
?>

<?php $this->endWidget(); ?>
