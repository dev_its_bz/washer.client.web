<?php
$this->breadcrumbs = array(
    'Контрагенты' => array('/kontragent/admin/'),
    $kontragent->nazvanie => array('/kontragent/view/' . $kontragent->id),
    'Объекты' => array('/kontragent/view/' . $kontragent->id . "?tab=3"),
    $model->nazvanie,
);

//Yii::app()->clientScript->registerScriptFile('history.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/object/assets/history.js'));
?>

<div class="page-header">
    <h1>
        <?php
        echo "Объект &laquo;" . $model->nazvanie . "&raquo; ";
        ?>
    </h1>
</div>


<?php
list($object) = Yii::app()->createController('Object');
list($carGruppa) = Yii::app()->createController('CarGruppa');
//list($grafikRaboty) = Yii::app()->createController('GrafikRaboty');
list($polzovatel) = Yii::app()->createController('Polzovatel');
list($opciiSelect) = Yii::app()->createController('OpciiSelect');
list($usluga) = Yii::app()->createController('UslugaObject');
list($box) = Yii::app()->createController('Box');
list($objectBoxes) = Yii::app()->createController('ZapisUsluga');
list($udobstvoObject) = Yii::app()->createController('Udobstvo');

$this->beginWidget('system.web.widgets.CClipWidget', array('id' => 'Карта'));
//$gMap->renderMap();
$this->endWidget();

$this->beginWidget('system.web.widgets.CClipWidget', array('id' => 'Опции записи'));
$opciiSelect->viewOpcii($model->id);
$this->endWidget();

$tabParameters = array();
foreach ($this->clips as $key => $clip)
    $tabParameters['tab' . (count($tabParameters) + 1)] = array('title' => $key, 'content' => $clip);


//$this->widget('system.web.widgets.CTabView', array('tabs' => $tabParameters));
?>
<script>
    $(document).ready(function () {
        setInterval(function () {
            google.maps.event.trigger(<?php echo($gMap->getContainerId()) ?>, 'resize');
        }, 100);
    });
</script>



<?php
Yii::import('bootstrap.widgets.TbTabs');

class ajaxTabs extends TbTabs
{

    /**
     * @var bool Reload the tab content each time the tab is clicked. Default: load only once
     */
    public $reload = false;

    public function run()
    {
        $id = $this->id;

        /** @var CClientScript $cs */
        $cs = Yii::app()->getClientScript();
        $cs->registerScript(
            __CLASS__ . '#' . $id . '_ajax', '
                function TbTabsAjax(e) {

                    e.preventDefault();

                    var $eTarget = $(e.target);
                    var $tab = $($eTarget.attr("href"));
                    var ctUrl = $eTarget.data("tab-url");

                    if (ctUrl != "" && ctUrl !== undefined) {
                        $.ajax({
                            url: ctUrl,
                            type: "POST",
                            dataType: "html",
                            cache: false,
                            success: function (html) {
                                $tab.html(html);
                            },
                            error: function () {
                                alert("Request failed");
                            }
                        });
                    }
                    if(' . ($this->reload ? 0 : 1) . '){
                        $eTarget.data("tab-url", "");
                    }
                    return false;
                }'
        );
        $this->events['shown'] = 'js:TbTabsAjax';

        parent::run();
    }

}

?>
<?
$polzovatelModel = new Polzovatel('search');
$polzovatelModel->unsetAttributes(); // clear any default values
if (isset($_GET['Polzovatel']))
    $polzovatelModel->attributes = $_GET['Polzovatel'];

$dpuser = $polzovatelModel->search();
$criteriaU=new CDbCriteria;
$criteriaU->condition = "object_id = ".$model->id;
$dpuser->criteria->mergeWith($criteriaU);

/*
$grafikRabotyModel = new GrafikRaboty('search');
$grafikRabotyModel->unsetAttributes(); // clear any default values
if (isset($_GET['GrafikRaboty']))
    $grafikRabotyModel->attributes = $_GET['GrafikRaboty'];
$grafikRabotyModel->kontragent_id = $model->kontragent_id;
*/

$opciiZapisiModel = new OpciiSelect('search');
$opciiZapisiModel->unsetAttributes(); // clear any default values
if (isset($_GET['OpciiZapisi']))
    $opciiZapisiModel->attributes = $_GET['OpciiZapisi'];
$opciiZapisiModel->object_id = $model->id;

$uslugaModel = new UslugaObject('search');
$uslugaModel->unsetAttributes(); // clear any default values
if (isset($_GET['UslugaObject']))
    $uslugaModel->attributes = $_GET['UslugaObject'];
$uslugaModel->id_object = $model->id;

$boxModel = new Box('search');
$boxModel->unsetAttributes(); // clear any default values
if (isset($_GET['Box']))
    $boxModel->attributes = $_GET['Box'];
$boxModel->object_id = $model->id;

$objectBoxesModel = ZapisUsluga::model()->findAll();//ByAttributes(array("zapis->object_id"=>$model->id));
//$objectBoxesModel->unsetAttributes(); // clear any default values
//$objectBoxesModel->zapis->object_id = $model->id;

$Ud_model = new Udobstvo('search');
$Ud_model->unsetAttributes(); // clear any default values
if (isset($_GET['Udobstvo']))
    $Ud_model->attributes = $_GET['Udobstvo'];
$Ud_dp = $Ud_model->getByObj($model->id);
//var_dump($Ud_dp);
//exit;

$this->widget('bootstrap.widgets.TbTabs', array(
        "id" => "tabs",
        "type" => "tabs",
        'tabs' => array(
            array(
                'active' => !isset($_GET['tab']) || @$_GET['tab'] == '1',
                'label' => 'Общая информация',
                'content' => $object->renderPartial('info', array('model' => $model,), true)
            ),
            /*
            array(
                'active' => @$_GET['tab']=='2',
                'label' => 'Графики работы',
                'content' => $grafikRaboty->renderPartial('admin', array('model' => $grafikRabotyModel, 'AcessScript' => Yii::app()->params[Yii::app()->user->role]), true),
            ),
            */
            array(
                'active' => @$_GET['tab'] == '3',
                'label' => 'Пользователи',
                'content' => $polzovatel->renderPartial('admin', array('model' => $polzovatelModel, 'dp'=> $dpuser, 'AcessScript' => Yii::app()->params[Yii::app()->user->role]), true),
            ),
            array(
                'label' => 'Услуги и цены',
                'active' => @$_GET['tab'] >= 17 && @$_GET['tab'] <= 22,
                'items' => array(
                    array(
                        'active' => @$_GET['tab'] == '17',
                        'label' => 'Микролитражка, мопед',
                        $uslugaModel->car_gruppa_id = '17',
                        'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel, 'grid_id' => 'mikrolitrazhka_grid'), true),
                    ),
                    array(
                        'active' => @$_GET['tab'] == '18',
                        'label' => 'Легковая',
                        $uslugaModel->car_gruppa_id = '18',
                        'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel, 'grid_id' => 'legkovie_grid'), true),
                    ),
                    array(
                        'active' => @$_GET['tab'] == '19',
                        'label' => 'Минивен',
                        $uslugaModel->car_gruppa_id = '19',
                        'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel, 'grid_id' => 'miniven_grid'), true),
                    ),
                    array(
                        'active' => @$_GET['tab'] == '20',
                        'label' => 'Джип',
                        $uslugaModel->car_gruppa_id = '20',
                        'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel, 'grid_id' => 'djip_grid'), true),
                    ),
                    array(
                        'active' => @$_GET['tab'] == '21',
                        'label' => 'Микроавтобус',
                        $uslugaModel->car_gruppa_id = '21',
                        'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel, 'grid_id' => 'avtobus_grid'), true),
                    ),
                    array(
                        'active' => @$_GET['tab'] == '22',
                        'label' => 'Газель',
                        $uslugaModel->car_gruppa_id = '22',
                        'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel, 'grid_id' => 'gazel_grid'), true),
                    )),
            ),
            array(
                'active' => @$_GET['tab'] == '5',
                'label' => 'Настройки записи',
                'content' => $opciiSelect->renderPartial('index', array('model' => $opciiZapisiModel), true),
            ),
            array(
                'active' => @$_GET['tab'] == '6',
                'label' => 'Боксы',
                'content' => $box->renderPartial('admin', array('model' => $boxModel), true),
            ),
            /*
            array(
                'active' => @$_GET['tab'] == '6',
                'label' => 'Заявки',
                'content' => $objectBoxes->renderPartial('boxes_view', array('model' => $objectBoxesModel), true),
            ),*/
            array(
                'active' => @$_GET['tab'] == '7',
                'id' => 'tabMap',
                'class' => 'tabMap',
                'htmlOptions' => array('class' => 'mapTab'),
                'label' => 'Расположение на карте',
                'content' => $object->renderPartial('map', array('model' => $model, 'gMap' => $gMap), true)
            ),
            array(
                'active' => @$_GET['tab'] == '8',
                'label' => 'Удобства',
                'content' => $udobstvoObject->renderPartial('addToObj', array('object_id' => $model->id,'model' => $Ud_model,'dp' => $Ud_dp), true)
            ),
        ),
    )
);
?>


<?php if (Yii::app()->request->getParam("tab") != null): ?>

    <script>
        window.history.pushState(null, null, '<?php echo Yii::app()->request->getParam("id"); ?>');
    </script>

<?php endif; ?>

