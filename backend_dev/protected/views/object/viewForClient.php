<div class="page-header">
    <h1>
        <?php
        echo "&laquo;" . $model->nazvanie . "&raquo;";
        ?>
    </h1>
</div>
<style type="text/css">
    .activeTime {
        background-color: #a5df41;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#a5df41), to(#4ca916));
        background-image: -webkit-linear-gradient(top, #a5df41, #4ca916);
        background-image: -moz-linear-gradient(top, #a5df41, #4ca916);
        background-image: -ms-linear-gradient(top, #a5df41, #4ca916);
        background-image: -o-linear-gradient(top, #a5df41, #4ca916);
        background-image: linear-gradient(top, #a5df41, #4ca916);
    }

    .inactiveTime {
        background-color: #fecf23;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#fecf23), to(#fd9215));
        background-image: -webkit-linear-gradient(top, #fecf23, #fd9215);
        background-image: -moz-linear-gradient(top, #fecf23, #fd9215);
        background-image: -ms-linear-gradient(top, #fecf23, #fd9215);
        background-image: -o-linear-gradient(top, #fecf23, #fd9215);
        background-image: linear-gradient(top, #fecf23, #fd9215);
        transition: width .4s ease-in-out;
    }

    .tab-content {
        overflow: visible;
        padding: 0;
    }
</style>

<?
$day = $data;
if ($data == null)
    $data = 'CURRENT_DATE()';
else
    $data = "CURRENT_DATE() + INTERVAL $data DAY";

if ($uslugi == null)
    $uslugi = "-1";

if ($car_id == null) {
    echo "Car ID undefined";
    return;
}

$polzovatelCar = PolzovatelCar::model()->findByPk($car_id);
$car_gruppa_id = $polzovatelCar->carModel->car_gruppa_id;

$sql = "
        SELECT SUM(dlitelnost) AS dlitelnost
        FROM usluga_object
        WHERE id_usluga IN ($uslugi) AND car_gruppa_id = $car_gruppa_id
        ";
$services = UslugaObject::model()->findBySql($sql);
if ($services->dlitelnost == null) {
    $object_id = $model->id;
    $dlitelnost = @OpciiSelect::model()->findByAttributes(array('object_id' => $object_id, 'opcii_id' => 2))->znachenie;
    if ($dlitelnost == null)
        $dlitelnost = '30';
} else
    $dlitelnost = $services->dlitelnost;

$timeContent = $this->widget(
    'bootstrap.widgets.TbButtonGroup',
    array(
        'type' => 'success',
        'htmlOptions' => array('style' => 'display:inline-flex; width:100%'),
        'toggle' => 'radio',
        'buttons' => array(
            array(
                'label' => 'Сегодня',
                'buttonType' => 'ajaxLink',
                'active' => $day == 0 ? true : false,
                'htmlOptions' => array(
                    'style' => 'width:33%',
                    'onClick' => "
                    $.ajax({
                    url: '/object/viewForClient/?object_id={$model->id}&uslugi={$uslugi}&car_id={$car_id}&data=0&currentTab=1',
                    }).done(function (data) {
                        $('#objectInfo').html(data);
                    });
                    "
                ),
            ),
            array(
                'label' => 'Завтра',
                'buttonType' => 'ajaxLink',
                'active' => $day == 1 ? true : false,
                'htmlOptions' => array(
                    'style' => 'width:33%',
                    'onClick' => "
                    $.ajax({
                    url: '/object/viewForClient/?object_id={$model->id}&uslugi={$uslugi}&car_id={$car_id}&data=1&currentTab=1',
                    }).done(function (data) {
                        $('#objectInfo').html(data);
                    });
                    "
                ),
            ),
            array(
                'label' => 'Послезавтра',
                'buttonType' => 'ajaxLink',
                'active' => $day == 2 ? true : false,
                'htmlOptions' => array(
                    'style' => 'width:33%',
                    'onClick' => "
                    $.ajax({
                    url: '/object/viewForClient/?object_id={$model->id}&uslugi={$uslugi}&car_id={$car_id}&data=2&currentTab=1',
                    }).done(function (data) {
                        $('#objectInfo').html(data);
                    });
                    "
                ),
            )
        ),
    ),
    true
);

$timeContent .= "<br/>";

$sql = "
SELECT id, box_id, box_nazvanie, SUBSTR(vremya,1,5) AS vremya, `data`
FROM object_resource_view
WHERE object_id={$model->id} AND `data`=$data AND resource>=$dlitelnost
";
if ($day == 0) $sql .= " AND vremya > IF(TIME(NOW() + INTERVAL 30 MINUTE)<TIME(NOW()), '23:59:59', TIME(NOW() + INTERVAL 30 MINUTE))";

if ($day == 0)
    $dayText = "СЕГОДНЯ";
else
    if ($day == 1)
        $dayText = "ЗАВТРА";
    else
        if ($day == 2)
            $dayText = "ПОСЛЕЗАВТРА";
        else

            $timeContent .= "<br/>";
$mdl = ObjectResourceView::model()->findAllBySql($sql);
$timeline = "<div class='row-fluid'>";
$h = $m = 0;
for ($h = 0; $h < 24; $h++)
    for ($m = 0; $m < 60; $m += 10) {
        if (strlen($h) < 2) $h = '0' . $h;
        if (strlen($m) < 2) $m = '0' . $m;
        $time = "$h:$m";

        $timeline .= "<div ";

        $times = array_values(CHtml::listData($mdl, 'id', 'vremya'));
        $keys = array_keys(CHtml::listData($mdl, 'id', 'vremya'));
        if (in_array($time, $times)) {
            $key = array_search($time, $times);
            $color = "#00ff00";
            $cursor = 'pointer';
            $class = "activeTime";
            $timeline .= "onClick='selectTime(\"$keys[$key]\")' ";
        } else {
            $class = "inactiveTime";
            $cursor = 'not-allowed';
            $color = "#ff0000";

        }
        $timeline .= "style='cursor:$cursor; float:left; width:0.695%;' data-toggle='tooltip' title='$time' data-placement='bottom' class='$class'>&nbsp;</div>";
    }
$timeline .= "</div>";
$timeContent .= "<br/>Записаться на <strong>$dayText</strong> в ";
$timeContent .= $this->widget(
    'bootstrap.widgets.TbSelect2',
    array(
        'id' => 'vremya',
        'model' => $mdl,
        'name' => 'vremya',
        'data' => CHtml::listData($mdl, 'id', 'vremya'),
        'htmlOptions' => array(
            'placeholder' => 'Время записи',
            'style' => 'width:auto',
        ),
    ),
    true
);
$timeContent .= "<br/><br/>$timeline<br/>";
$currentTab = @$_GET['currentTab'];
$currentDay = @$_GET['data'];
?>

<script>
    var vremya = "";
    var vremyaId = "";
    var summaryTime = "";
    $("#vremya").on("change", function () {
        vremya = $("#vremya :selected").text();
        vremyaId = $("#vremya").val();
        summaryTime = "<?=date('d.m.Y', strtotime(($mdl[0]->data)))." в "?>" + vremya;
    });
    $('#vremya').select2();
    $('#vremya').select2().select2('val', $('#vremya option:eq(1)').val()).trigger("change");

    function selectTime(time) {
        $('#vremya').select2().select2('val', time).trigger("change");
    }
</script>

<?
list($usluga) = Yii::app()->createController("usluga");
$uslugaModel = new UslugaListView();
$uslugaModel->unsetAttributes();
if (isset($_GET['UslugaListView']))
    $uslugaModel->attributes = $_GET['UslugaListView'];
$uslugaModel->id_object = $model->id;
$uslugaModel->car_gruppa_id = $car_gruppa_id;

list($udobstvoObject) = Yii::app()->createController("/udobstvoObject");
$udobstvoObjectModel = new UdobstvoObject();
$udobstvoObjectModel->unsetAttributes();
$udobstvoObjectModel->object_id = $model->id;

$this->widget(
    'bootstrap.widgets.TbTabs',
    array(
        'type' => 'tabs',
        'tabs' => array(
            array(
                'label' => 'Время записи',
                'content' => $timeContent,
                'active' => !isset($currentTab) ? true : $currentTab == 1,
                'linkOptions' => array('onClick' => '$("#currentTab").val(1);')
            ),
            array(
                'label' => 'Услуги',
                'content' => $usluga->renderPartial("/uslugaObject/adminForClient", array('model' => $uslugaModel), true),
                'active' => $currentTab == 2,
                'linkOptions' => array('onClick' => '$("#currentTab").val(2);')
            ),
            array(
                'label' => 'Удобства',
                'content' => $udobstvoObject->renderPartial("/udobstvoObject/adminForClient", array('model' => $udobstvoObjectModel), true),
                'active' => $currentTab == 3,
                'linkOptions' => array('onClick' => '$("#currentTab").val(3);')
            ),
            array(
                'label' => 'Карта',
                'content' => '<div class="span12" id="map" style="height: 400px"></div>',
                'active' => $currentTab == 4,
                'linkOptions' => array('onClick' => '$("#currentTab").val(4);')
            )
        ),
    )
);
?>
<input type="hidden" id="currentTab" value="<?= $currentTab ?>">
<input type="hidden" id="currentDay" value="<?= $currentDay ?>">
<script>
    function init(adres) {
        var myMap = new ymaps.Map('map', {
            center: adres,
            zoom: 13,
            behaviors: ['default', 'scrollZoom'],

        });
        myPlacemark = new ymaps.Placemark(adres, {
            balloonContentHeader: "<?=$model->nazvanie?>",
            balloonContentBody: "<?=$model->adres?><br/><?=$model->telefon?>",
            hintContent: "<?=$model->nazvanie?>"
        });
        myMap.geoObjects.add(myPlacemark);

        myMap.controls
            // Список типов карты
            .add('typeSelector', { right: 0, top: 0 });

        // Также в метод add можно передать экземпляр класса элемента управления.
        // Например, панель управления пробками.
        var trafficControl = new ymaps.control.TrafficControl();
        myMap.controls
            .add(trafficControl, { left: 0, top: 0 })
    }
    init(<?="[{$model->lat},{$model->lon}]"?>);
</script>