<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>

<?php echo $form->textFieldRow($model, 'id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'nazvanie', array('class' => 'span5', 'maxlength' => 100)); ?>

<?php echo $form->textAreaRow($model, 'adres', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->textFieldRow($model, 'telefon', array('class' => 'span5', 'maxlength' => 30)); ?>

<?php echo $form->textFieldRow($model, 'grafik_raboty_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'zarplata', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'data_sozdaniya', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'avtor_zapisi_id', array('class' => 'span5')); ?>

    <?php echo $form->textFieldRow($model, 'data_zapisi', array('class' => 'span5')); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => 'Найти',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
