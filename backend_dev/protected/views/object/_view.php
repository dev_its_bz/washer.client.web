<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('nazvanie')); ?>:</b>
    <?php echo CHtml::encode($data->nazvanie); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('adres')); ?>:</b>
    <?php echo CHtml::encode($data->adres); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('telefon')); ?>:</b>
    <?php echo CHtml::encode($data->telefon); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('grafik_raboty_id')); ?>:</b>
    <?php echo CHtml::encode($data->grafik_raboty_id); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('zarplata')); ?>:</b>
    <?php echo CHtml::encode($data->zarplata); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('data_sozdaniya')); ?>:</b>
    <?php echo CHtml::encode($data->data_sozdaniya); ?>
    <br />

    <?php /*
      <b><?php echo CHtml::encode($data->getAttributeLabel('avtor_zapisi_id')); ?>:</b>
      <?php echo CHtml::encode($data->avtor_zapisi_id); ?>
      <br />

      <b><?php echo CHtml::encode($data->getAttributeLabel('data_zapisi')); ?>:</b>
      <?php echo CHtml::encode($data->data_zapisi); ?>
      <br />

     */ ?>

</div>