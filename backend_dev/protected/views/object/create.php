<?php
$this->breadcrumbs = array(
    'Контрагенты' => array('/kontragent/admin/'),
    $kontragent->nazvanie => array('/kontragent/view/' . $kontragent->id),
    'Объекты' => array('/kontragent/view/' . $kontragent->id."?tab=3"),
    'Новый',
);
?>
<div class="page-header">
    <h1>Новый объект</h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model, 'grafik' => $grafik, 'gorod' => $gorod)); ?>