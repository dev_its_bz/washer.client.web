<?php
$this->breadcrumbs = array(
    'Объекты'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('object-grid', {
		data: $(this).serialize()
	});
	return false;
}); 
");
?>
    <script>
        $(document).ready(function () {
            $('#object-grid tbody tr').live('dblclick', function () {
                var id = $.fn.yiiGridView.getKey(
                    'object-grid',
                    $(this).prevAll().length
                );
                document.location.href = '/object/' + id;
            });
        });
    </script>
    <div class="page-header">
        <h1>Объекты
            <?php
              Yii::app()->user->checkAccess('owner')?
            $this->widget('bootstrap.widgets.TbButtonGroup', array(              
                'buttons' => array(
                    array('buttonType' => 'link', 'type' => 'success', 'icon' => 'plus white', 'url' => '/object/create/' . $model->kontragent_id),
                    array('buttonType' => 'link', 'type' => 'primary', 'icon' => 'search white', 'url' => '#', 'htmlOptions' => array('class' => 'search-button')),
                ))):
                 $this->widget('bootstrap.widgets.TbButtonGroup', array(              
                'buttons' => array(               
                    array('buttonType' => 'link', 'type' => 'primary', 'icon' => 'search white', 'url' => '#', 'htmlOptions' => array('class' => 'search-button')),
                )));  
            ?>
        </h1>
    </div>

    <div class="search-form" style="display:none">
        <div class="alert alert-info glyphicon-info-sign">
            В фильтрах можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
            или равенства <b>=</b>), указав их перед выражением.
        </div>
        <?php
        $this->renderPartial('_search', array(
            'model' => $model,
        ));
        ?>
    </div><!-- search-form -->
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'object-grid',
    'dataProvider' => $dp,
    'filter' => $model,
    'columns' => array(
      //  array('name' => 'id', 'htmlOptions' => array('style' => 'width:20px; text-align:center')),
        'id',
        array('name' => 'nazvanie', 'type'=>'raw', 'value'=>'$data->nazvanie'),
        //'nazvanie',
        
        'adres',
        'telefon',
        'gorod.nazvanie',
        //'grafik_raboty_id',
        //'zarplata',
        /*
          'data_sozdaniya',
          'avtor_zapisi_id',
          'data_zapisi',
         */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete}',
            'buttons' => array
            (
                'view' => array
                (
                    'url' => 'Yii::app()->createUrl("/object/view", array("id"=>$data->id))',
                ),
                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("/object/delete", array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn-column',
                    ),
                ),
            ),
        ),
    ),
));
?>