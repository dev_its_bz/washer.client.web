<div class="span4">
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'object-grid',
        'dataProvider' => $dataProvider,
        'filter' => $model,
        'hideHeader' => true,
        'type' => 'striped',
        'columns' => array(
            array(
                'name' => 'nazvanie',
                'type' => 'raw',
                'value' => '"<strong><nobr>".$data->nazvanie."</nobr></strong><br/>".$data->adres'
            )
        ),
    ));
    ?>
</div>
<div class="span8 well" style="height: 100%;" id="objectInfo">
    <br/>
    <h4 class="icon-arrow-left"> Для поиска мойки по названию используйте фильтр</h4>
    <br/><br/><br/>
    <h4 class="icon-arrow-left"> Выберите мойку из списка</h4>
</div>