<?php
$this->breadcrumbs=array(
	'Car Gruppas'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List CarGruppa','url'=>array('index')),
array('label'=>'Manage CarGruppa','url'=>array('admin')),
);
?>

<h1>Create CarGruppa</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>