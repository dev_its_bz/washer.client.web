<?php
$this->breadcrumbs=array(
	'Car Gruppas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List CarGruppa','url'=>array('index')),
	array('label'=>'Create CarGruppa','url'=>array('create')),
	array('label'=>'View CarGruppa','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage CarGruppa','url'=>array('admin')),
	);
	?>

	<h1>Update CarGruppa <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>