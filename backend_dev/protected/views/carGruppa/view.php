<?php
$this->breadcrumbs=array(
	'Car Gruppas'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List CarGruppa','url'=>array('index')),
array('label'=>'Create CarGruppa','url'=>array('create')),
array('label'=>'Update CarGruppa','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete CarGruppa','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage CarGruppa','url'=>array('admin')),
);
?>

<h1>View CarGruppa #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nazvanie',
		'data_sozdaniya',
		'avtor_zapisi_id',
		'data_zapisi',
),
)); ?>
