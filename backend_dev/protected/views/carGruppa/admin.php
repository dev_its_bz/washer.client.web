<?php
$this->breadcrumbs = array(
    'Группы автомобилей'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('car-gruppa-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<script>
    $(document).ready(function () {
        $('#car-gruppa-grid tbody tr').live('dblclick', function () {
            var id = $.fn.yiiGridView.getKey(
                'car-gruppa-grid',
                $(this).prevAll().length
            );
            document.location.href = '/CarModel/ModelByGroup/' + id;
        });
    });
</script>
<div class="page-header">
    <h1>Группы автомобилей
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons' => array(
                array('buttonType' => 'link', 'type' => 'success', 'icon' => 'plus white', 'url' => '/CarGruppa/create/'),
                array('buttonType' => 'link', 'type' => 'primary', 'icon' => 'search white', 'url' => '#', 'htmlOptions' => array('class' => 'search-button')),
            )));
        ?>
    </h1>
</div>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'car-gruppa-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('name' => 'id', 'htmlOptions' => array('style' => 'width:20px; text-align:center')),
        array(
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'name' => 'nazvanie',
            'editable' => array(
                'type' => 'text',
                'url' => '/carGruppa/UpdateZnachenie',
            )
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array(
                'update' => array(
                    'lable' => 'Редактировать',
                    'icon' => 'pencil',
                    'url' => 'Yii::app()->createUrl("/CarModel/ModelByGroup", array("id" => $data->id))',
                ),
                'delete' => array(
                    'lable' => 'Удалить',
                    'icon' => 'trash',
                    'url' => 'Yii::app()->createUrl("CarGruppa/delete", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
)); ?>
