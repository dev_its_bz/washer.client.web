<?php
/* @var $this ServicesController */
/* @var $model services */


?>

<h1>Manage Services</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>


</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'services-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'nazvanie',
		'opisanie',
		//'moika.nazvanie',
		//'gruppa.nazvanie',
		
            
       
                
                 array(
                        'name'=>'type_name',
                       'type' => 'raw',
                        'value'=>'$data->uslugaTip->nazvanie',
                       
                        'filter'=> CHtml::listData(UslugaGruppa::model()->findAll(), 'id', 'nazvanie'),
                            //UslugaGruppa::model()->getForFilter(),
                         
                ),
                
             
		/*
		'data_sozdaniya',
		'avtor_zapisi_id',
		'data_zapisi',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
