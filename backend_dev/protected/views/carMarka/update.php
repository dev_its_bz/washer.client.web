<?php
$this->breadcrumbs = array(
    'Марки автомобилей' => array('/carMarka/admin'),
    $model->nazvanie
);

?>

<div class="page-header">
    <h1>Редактор марки &laquo;<?php echo $model->nazvanie; ?>&raquo;</h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model, 'carModel' => $carModel)); ?>

