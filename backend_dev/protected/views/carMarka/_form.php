<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'car-marka-form',
    'htmlOptions' => array('class' => 'well'),
    'focus' => array($model, 'nazvanie'),
    'enableAjaxValidation' => false,
));
?>

<?php echo $form->errorSummary($model); ?>
Название марки:
<?
if ($model->isNewRecord) {
    echo $form->textFieldRow($model, 'nazvanie', array('class' => 'span5', 'maxlength' => 30));
} else
    $this->widget('TbEditableField', array(
        'type' => 'text',
        'model' => $model,
        'attribute' => 'nazvanie',
        'url' => $this->createUrl('/carMarka/updateZnachenie'),
        'placement' => 'right',
    ));
?>

<?php if (!$model->isNewRecord) : ?>

    <?php $dataProvider = $carModel->search($model->id); ?>

    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'car-model-grid',
        'dataProvider' => $dataProvider,
        'filter' => $carModel,
        'columns' => array(
            array('name' => 'id', 'htmlOptions' => array('style' => 'width:20px; text-align:center')),
            array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'nazvanie',
                'sortable' => true,
                'editable' => array(
                    'url' => '/carModel/UpdateZnachenie',
                )
            ),
            array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'car_gruppa_id',
                'editable' => array(
                    'type' => 'select',
                    'url' => '/carModel/UpdateZnachenie',
                    'source' => CHtml::listData(CarGruppa::model()->findAll(), 'id', 'nazvanie')
                )
            ),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{delete}',
                'buttons' => array(
                    'delete' => array(
                        'lable' => 'Удалить',
                        'icon' => 'trash',
                        'url' => 'Yii::app()->createUrl("CarModel/delete", array("id"=>$data->id))',
                    ),
                ),
            ),
        ),
    ));
    ?>

<?php endif; ?>
<hr/>
<?php
if ($model->isNewRecord) {
    echo CHtml::button('Создать новую марку и добавить к ней модели автомобилей', array('submit' => array('SaveAndAdd'), 'class' => 'btn btn-success'));
} else {
    echo CHtml::link('Добавить модель', "/CarModel/Create?id={$model->id}", array('class' => 'btn btn-success'));
}
?>

<?php $this->endWidget(); ?>
