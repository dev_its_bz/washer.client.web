<?php
$this->breadcrumbs = array(
    'Car Markas' => array('index'),
    $model->id,
);
?>

<h1>View CarMarka #<?php echo $model->id; ?></h1>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'car-marka-form',
    'enableAjaxValidation' => false,
));
?>
<?php echo $form->labelEx($model, 'Марка машины'); ?>
<?php echo $form->textField($model, 'nazvanie', array('class' => 'span5', 'maxlength' => 30, 'readonly' => 'true')); ?>

<?php if (!$model->isNewRecord) : ?>


    <?php $dataProvider = $carModel->search($model->id); ?>

    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'car-marka-grid',
        'dataProvider' => $dataProvider,
        'filter' => $carModel,
        'columns' => array(
            'nazvanie',
        ),
    ));
    ?>

<?php endif; ?>

<?php $this->endWidget(); ?>

