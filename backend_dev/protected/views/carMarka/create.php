<?php
$this->breadcrumbs = array(
    'Марки автомобилей' => array('admin'),
    'Новая марка',
);
?>

    <div class="page-header">
        <h1>Новая марка</h1>
    </div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>