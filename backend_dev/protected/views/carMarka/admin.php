<?php
$this->breadcrumbs = array(
    'Марки автомобилей'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('car-marka-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
    $(document).ready(function () {
        $('#car-marka-grid tbody tr').live('dblclick', function () {
            var id = $.fn.yiiGridView.getKey(
                'car-marka-grid',
                $(this).prevAll().length
            );
            document.location.href = '/carMarka/update/' + id;
        });
    });
</script>

<div class="page-header">
    <h1>Марки автомобилей
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons' => array(
                array('buttonType' => 'link', 'type' => 'success', 'icon' => 'plus white', 'url' => '/carMarka/create/'),
                array('buttonType' => 'link', 'type' => 'primary', 'icon' => 'search white', 'url' => '#', 'htmlOptions' => array('class' => 'search-button')),
            )));
        ?>
    </h1>
</div>

<div class="search-form" style="display:none">
    <div class="alert alert-info glyphicon-info-sign">
        В фильтрах можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
            &lt;&gt;</b>
        или равенства <b>=</b>), указав их перед выражением.
    </div>
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'car-marka-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('name' => 'id', 'htmlOptions' => array('style' => 'width:20px; text-align:center')),
        array('name' => 'nazvanie', 'htmlOptions' => array('style' => 'width:150px;')),
        array('name' => 'carMarka.nazvanie', 'header' => 'Модели', 'value' => '$data->getModelNames()'),

        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete}',
            'buttons' => array
            (
                'view' => array
                (
                    'url' => 'Yii::app()->createUrl("/carMarka/update", array("id"=>$data->id))',
                ),
                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("/carMarka/delete", array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn-column',
                    ),
                ),
            ),
        ),
    ),
)); ?>
