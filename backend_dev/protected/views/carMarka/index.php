<?php
$this->breadcrumbs=array(
	'Car Markas',
);

$this->menu=array(
	array('label'=>'Create CarMarka','url'=>array('create')),
	array('label'=>'Manage CarMarka','url'=>array('admin')),
);
?>

<h1>Car Markas</h1>
  <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'car-marka-grid',
        'dataProvider' => $model->search(),
        'filter'=>$model,
        'columns' => array(
            'nazvanie',
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template'=>'{view}',
                'buttons'=>array(
                    'view'=>array(
                        'lable'=>'просмотр модели',
                        'icon'=>'eye-open',
                        'url'=>'Yii::app()->createUrl("CarMarka/view", array("id"=>$data->id))',
                    ),
                ),
            ),
        ),
    ));
    ?>
