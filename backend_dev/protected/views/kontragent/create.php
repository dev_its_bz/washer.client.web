<?php
$this->breadcrumbs = array(
    'Контрагенты' => array('admin'),
    'Новый',
);
?>

<div class="page-header">
    <h1>Новый контрагент</h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model, 'status_data' => $status_data)); ?>