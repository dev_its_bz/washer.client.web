<?php
$this->breadcrumbs = array(
    'Контрагенты' => Yii::app()->user->gruppa_id == '2' ? array('admin') : null,
    $model->nazvanie => array('view', 'id' => $model->id),
    'Редактор',
);
?>

<div class="page-header">
    <h1>Редактор <?php echo $model->nazvanie; ?></h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model, 'status_data' => $status_data)); ?>