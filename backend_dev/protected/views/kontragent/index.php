<?php
$this->breadcrumbs=array(
	'Kontragents',
);

$this->menu=array(
	array('label'=>'Create Kontragent','url'=>array('create')),
	array('label'=>'Manage Kontragent','url'=>array('admin')),
);
?>

<h1>Kontragents</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
