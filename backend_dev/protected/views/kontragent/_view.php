<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nazvanie')); ?>:</b>
	<?php echo CHtml::encode($data->nazvanie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nazvanie_ur')); ?>:</b>
	<?php echo CHtml::encode($data->nazvanie_ur); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nazvanie_ur_kr')); ?>:</b>
	<?php echo CHtml::encode($data->nazvanie_ur_kr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inn')); ?>:</b>
	<?php echo CHtml::encode($data->inn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kpp')); ?>:</b>
	<?php echo CHtml::encode($data->kpp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('okpo')); ?>:</b>
	<?php echo CHtml::encode($data->okpo); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ogrn')); ?>:</b>
	<?php echo CHtml::encode($data->ogrn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bank')); ?>:</b>
	<?php echo CHtml::encode($data->bank); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bik')); ?>:</b>
	<?php echo CHtml::encode($data->bik); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ks')); ?>:</b>
	<?php echo CHtml::encode($data->ks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rs')); ?>:</b>
	<?php echo CHtml::encode($data->rs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_id')); ?>:</b>
	<?php echo CHtml::encode($data->status_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_sozdaniya')); ?>:</b>
	<?php echo CHtml::encode($data->data_sozdaniya); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('avtor_zapisi')); ?>:</b>
	<?php echo CHtml::encode($data->avtor_zapisi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_zapisi')); ?>:</b>
	<?php echo CHtml::encode($data->data_zapisi); ?>
	<br />

	*/ ?>

</div>