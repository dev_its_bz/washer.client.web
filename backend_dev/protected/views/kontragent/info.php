<?php

$this->widget('TbEditableDetailView', array(
    'data' => $model,
    'url' => $this->createUrl('/kontragent/UpdateZnachenie'),
    'attributes' => array(
        'id',
        array('name' => 'nazvanie',
            'editable' => array(
                'type' => 'text',
                'inputclass' => 'input-large',
                'emptytext' => 'special emptytext',
                'validate' => 'function(value) {
                    if(!value) return "Название не может быть пустым!"
                }'
            )
        ),
        'nazvanie_ur',
        'nazvanie_ur_kr',
        'inn',
        'kpp',
        'okpo',
        'ogrn',
        'bank',
        'bik',
        'ks',
        'rs',
        'site',
        array( //select loaded from database
            'name' => 'status_id',
            'editable' => array(
                'type' => 'select',
                'source' => CHtml::listData(KontragentStatus::model()->findAll(), 'id', 'nazvanie')
            )
        ),
    ),
));
?>
