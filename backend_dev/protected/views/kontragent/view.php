<?php
$this->breadcrumbs = array(
    'Контрагенты' => Yii::app()->user->gruppa_id == '2' ? array('/kontragent/admin') : null,
    $model->nazvanie,
);
?>

<div class="page-header">
    <h1>
        <?php
        echo $model->nazvanie;
        ?>
    </h1>
</div>

<?php
list($object) = Yii::app()->createController('Object');
list($polzovatel) = Yii::app()->createController('Polzovatel');
list($kontragent) = Yii::app()->createController('Kontragent');
list($grafikRaboty) = Yii::app()->createController('GrafikRaboty');

$polzovatelModel = new Polzovatel('search');
$polzovatelModel->unsetAttributes();
if (isset($_GET['Polzovatel']))
    $polzovatelModel->attributes = $_GET['Polzovatel'];

$dpuser = $polzovatelModel->search();
$criteriaU = new CDbCriteria;
$criteriaU->condition = "t.kontragent_id = " . $model->id;
$dpuser->criteria->mergeWith($criteriaU);


$objectModel = new Object('search');
$objectModel->unsetAttributes();
if (isset($_GET['Object']))
    $objectModel->attributes = $_GET['Object'];

$dp = $objectModel->search();
$criteria1 = new CDbCriteria;
$criteria1->condition = "kontragent_id = " . $model->id;
$dp->criteria->mergeWith($criteria1);


$grafikRabotyModel = new GrafikRaboty('search');
$grafikRabotyModel->unsetAttributes();
if (isset($_GET['GrafikRaboty']))
    $grafikRabotyModel->attributes = $_GET['GrafikRaboty'];
$grafikRabotyModel->kontragent_id = $model->id;

$this->widget('bootstrap.widgets.TbTabs', array(
        'tabs' => array(
            array(
                'active' => !isset($_GET['tab']) || @$_GET['tab'] == '1',
                'label' => 'Общая информация',
                'icon' => 'info-sign',
                'content' => $kontragent->renderPartial('info', array('model' => $model), true)
            ),
            array(
                'active' => @$_GET['tab'] == '2',
                'label' => 'Пользователи',
                'icon' => 'user',
                'content' => $polzovatel->renderPartial('admin', array('model' => $polzovatelModel, 'dp' => $dpuser, 'AcessScript' => Yii::app()->params[Yii::app()->user->role]), true),
            ),
            array(
                'active' => @$_GET['tab'] == '3',
                'label' => 'Объекты',
                'icon' => 'home',
                'content' => $object->renderPartial('admin', array('model' => $objectModel, 'dp' => $dp), true),
            ),
            array(
                'active' => @$_GET['tab'] == '4',
                'label' => 'Графики работы',
                'icon' => 'time',
                'content' => $grafikRaboty->renderPartial('admin', array('model' => $grafikRabotyModel), true),
            ),
            array(
                'active' => @$_GET['tab'] == '5',
                'label' => 'Показатели',
                'icon' => 'signal',
                'content' => $kontragent->renderPartial('statistika', array('model' => $model), true),
            ),
        )
    )
);
?>
