<?php
$this->breadcrumbs = array(
    'Контрагенты'
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('kontragent-grid', {
data: $(this).serialize()
});
return false;
});
$(document).ready(function () {
    $('#kontragent-grid tbody tr').live('dblclick', function () {
        var id = $.fn.yiiGridView.getKey(
            'kontragent-grid',
            $(this).prevAll().length
        );
        document.location.href = '/kontragent/' + id;
    });
});
");
?>


<div class="page-header">
    <h1>Контрагенты
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'size'=>'mini',
            'buttons' => array(
                array('buttonType' => 'link', 'type' => 'success', 'icon' => 'glyphicon glyphicon-plus', 'url' => 'create'),
                array('buttonType' => 'link', 'type' => 'primary', 'icon' => 'glyphicon glyphicon-search', 'url' => '#', 'htmlOptions' => array('class' => 'search-button')),
            )));
        ?>
    </h1>
</div>

<div class="search-form" style="display:none">
    <div class="alert alert-info glyphicon-info-sign">
        В фильтрах можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
            &lt;&gt;</b>
        или равенства <b>=</b>), указав их перед выражением.
    </div>
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'kontragent-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('name' => 'id', 'htmlOptions' => array('style' => 'width:20px; text-align:center')),
        'nazvanie',
        array(
            'name' => 'id_stat',
            'type' => 'raw',
            'value' => '$data->status->nazvanie',
            'htmlOptions' => array('style' => 'width:100px;'),
            'filter' => CHtml::listData(KontragentStatus::model()->findAll(), "id", "nazvanie"),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete}'
        ),
    ),
));
?>
