<?php
$this->breadcrumbs=array(
	'Opcii Selects'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OpciiSelect','url'=>array('index')),
	array('label'=>'Create OpciiSelect','url'=>array('create')),
	array('label'=>'Update OpciiSelect','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete OpciiSelect','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OpciiSelect','url'=>array('admin')),
);
?>

<h1>View OpciiSelect #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'object_id',
		'opcii_id',
		'znachenie',
	),
)); ?>
