<?php
$this->breadcrumbs=array(
	'Opcii Selects'=>array('index'),
	'Управление',
);

$this->menu=array(
	array('label'=>'List OpciiSelect','url'=>array('index')),
	array('label'=>'Create OpciiSelect','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('opcii-select-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Opcii Selects</h1>

<p>
В фильтрах  можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или равенства <b>=</b>), указав их перед выражением.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'opcii-select-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                'object_id',
		'opcii_id',
		'znachenie',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
