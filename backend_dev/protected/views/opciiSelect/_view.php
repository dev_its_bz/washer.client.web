<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('object_id')); ?>:</b>
	<?php echo CHtml::encode($data->object_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('opcii_id')); ?>:</b>
	<?php echo CHtml::encode($data->opcii_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('znachenie')); ?>:</b>
	<?php echo CHtml::encode($data->znachenie); ?>
	<br />


</div>