<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'opcii-select-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Поля, помеченные <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'object_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'opcii_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'znachenie',array('class'=>'span5','maxlength'=>50)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
