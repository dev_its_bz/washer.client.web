<?php
$this->breadcrumbs=array(
	'Opcii Selects'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OpciiSelect','url'=>array('index')),
	array('label'=>'Create OpciiSelect','url'=>array('create')),
	array('label'=>'View OpciiSelect','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage OpciiSelect','url'=>array('admin')),
);
?>

<h1>Update OpciiSelect <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>