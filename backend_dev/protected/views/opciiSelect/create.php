<?php
$this->breadcrumbs=array(
	'Opcii Selects'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OpciiSelect','url'=>array('index')),
	array('label'=>'Manage OpciiSelect','url'=>array('admin')),
);
?>

<h1>Create OpciiSelect</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>