<?php
$this->breadcrumbs = array(
    'Настройки записи',
);
?>
<div class="page-header">
    <h1>Настройки записи</h1>
</div>

<?php
$url = $this->createUrl('/opciiSelect/updateStatus');
Yii::app()->clientScript->registerScript('initStatus', "$('select.status').live('change',function() {
        el = $(this);        
        $.post(
                '$url',
               { status: el.val(), id: el.data('id'), object_id:'$model->object_id)',opcii_id:el.data('opcii')},
                function(){ 
                     $.fn.yiiGridView.update('opcii-select-grid');
                });
    });", CClientScript::POS_READY
);


$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'opcii-select-grid',
    'dataProvider' => $model->search(),
    //'filter' => $model,
    'columns' => array(
        array(
            'htmlOptions' => array('style' => 'width:20px'),
            'name' => 'inObject',
            'type' => 'raw',
            'value' => '$data->statusDropdown',
        ),
        'opciiname',
        array(
            'class' => 'TbEditableColumn',
            'name' => 'znachenie',
            'headerHtmlOptions' => array('style' => 'width: 110px'),
            'editable' => array( //editable section
                'apply' => '$data->inObject != 0', //can't edit deleted users
                'title' => 'Введите значение',
                'url' => $this->createUrl('OpciiSelect/UpdateZnachenie'),
                'placement' => 'right',
                'success' => 'js: function() {
                    $.fn.yiiGridView.update("opcii-select-grid");
                 }',
            )
        ),
    ),
));
?>
