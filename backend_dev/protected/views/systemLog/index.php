<?php
/* @var $this SystemLogController */

$this->breadcrumbs = array(
    'Системный журнал',
);
?>
    <div class="page-header">
        <h1>Системный журнал</h1>
    </div>
<?
if ($this->action->id == 'clear') {
    Yii::app()->user->setFlash('success', '<strong>Готово!</strong> Вы только что очистили весь системный журнал. Не стыдно?!');
    $this->widget('bootstrap.widgets.TbAlert', array(
        'block' => true,
        'fade' => true,
        'alerts' => array( // configurations per alert type
            'success' => array('block' => true, 'fade' => true), // success, info, warning, error or danger
        ),
    ));
}
?>
    <fieldset>
        <textarea style="width: 100%; border: none; height: 100%"
                  rows="30"><?= file_get_contents(Yii::app()->basePath . '/runtime/application.log'); ?></textarea>
    </fieldset>

    <hr/>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'type' => 'primary',
    'label' => 'Обновить',
    'url' => '/systemLog/index',
));
?>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'label' => 'Очистить журнал',
    'type' => 'danger',
    'htmlOptions' => array(
        'data-toggle' => 'modal',
        'data-target' => '#clearModal',
    ),
));
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'clearModal'));
?>

    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Очистка журнала</h4>
    </div>

    <div class="modal-body">
        <p>Системный журнал - это важный файл, а не глупость какая. Вы уверены, что нужно чистить журнал?</p>
    </div>

    <div class="modal-footer">
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'type' => 'primary',
            'label' => 'Да, я уверен',
            'url' => '/systemLog/clear',
        ));
        ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'label' => 'Ладно, не буду',
            'url' => '#',
            'htmlOptions' => array('data-dismiss' => 'modal'),
        ));
        ?>
    </div>

<?php $this->endWidget(); ?>