<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'usluga-object-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Поля, помеченные <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'id_usluga',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'id_object',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'car_gruppa_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'cena',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'dlitelnost',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'zarplata',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
