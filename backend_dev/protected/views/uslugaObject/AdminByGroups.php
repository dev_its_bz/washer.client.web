<?php
//$model->car_gruppa_id=$groups;
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'usluga-object-grid',

    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'id_usluga',

        'car_gruppa_id',
        'cena',
        'dlitelnost',
        /*
        'zarplata',
        */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>
<?
list($usluga) = Yii::app()->createController('UslugaObject');

$uslugaModel = new UslugaObject('search');
$uslugaModel->unsetAttributes(); // clear any default values
if (isset($_GET['UslugaObject']))
    $uslugaModel->attributes = $_GET['UslugaObject'];
//$uslugaModel->id_object = $id;


$this->widget('bootstrap.widgets.TbTabs', array(
        'type' => 'pills',
        'tabs' => array(
            array(
                'active' => true,
                'label' => 'Легковые автомобили',
                // 'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel), true),
            ),
            array(
                'active' => false,
                'label' => 'Минивэны',
                // 'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel), true),
            ),
            array(
                'active' => false,
                'label' => 'Джипы',
                //  'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel), true),
            ),
            array(
                'active' => false,
                'label' => 'Автобусы',
                //   'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel), true),
            ),
            array(
                'active' => false,
                'label' => 'Газели, грузовики',
                //   'content' => $usluga->renderPartial('admin', array('model' => $uslugaModel), true),
            ),


        ),
    )
);
?>