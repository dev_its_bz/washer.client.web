<?php
$this->breadcrumbs=array(
	'Usluga Objects'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UslugaObject','url'=>array('index')),
	array('label'=>'Create UslugaObject','url'=>array('create')),
	array('label'=>'Update UslugaObject','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete UslugaObject','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UslugaObject','url'=>array('admin')),
);
?>

<h1>View UslugaObject #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_usluga',
		'id_object',
		'car_gruppa_id',
		'cena',
		'dlitelnost',
		'zarplata',
	),
)); ?>
