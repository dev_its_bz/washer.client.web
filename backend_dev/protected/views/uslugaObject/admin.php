<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('usluga-object-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$carGruppaModel = CarGruppa::model()->find('id=?', array($model->car_gruppa_id));
?>

<div class="page-header">
    <h1>Услуги группы &laquo;<?= $carGruppaModel->nazvanie ?>&raquo;
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons' => array(
                array('buttonType' => 'link', 'type' => 'success', 'icon' => 'pencil white', 'url' => $this->createUrl("/UslugaObject/create/", array('obj' => $model->id_object, 'group' => $model->car_gruppa_id))),
                array('buttonType' => 'link', 'type' => 'primary', 'icon' => 'search white', 'url' => '#', 'htmlOptions' => array('class' => 'search-button')),
            )));
        ?>
    </h1>
</div>


<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => $grid_id,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'usl_type',
            'filter' => CHtml::listData(UslugaTip::model()->findAll(), 'id', 'nazvanie'),
            'type' => 'raw',
            'value' => '$data->uslugi->uslugaTip->nazvanie'
        ),
        array('name' => 'usl_name', 'value' => '$data->uslugi->nazvanie'),
        array(
            'class' => 'TbEditableColumn',
            'name' => 'cena',
            'value' => '($data->cena=="1")?"0":($data->cena)',
            'headerHtmlOptions' => array('style' => 'width: 110px'),
            'editable' => array( //editable section
                //  'apply' => '$data->inObject != 0', //can't edit deleted users
                'apply' => 'true',
                'title' => 'Введите значение',
                'url' => $this->createUrl('UslugaObject/UpdateVar'),
                'placement' => 'right',
                'validate' => 'js: function(value) {             
                    if (!isFinite($.trim(value))) return "Введите число";
                }',
                'success' => 'js: function() {
                            $.fn.yiiGridView.update("' . $grid_id . '");
                         }',
                'display' => 'js: function(value, sourceData) {
                     if (value>1) _color="blue"; else _color="red";                 
                      $(this).css("color", _color);    
                 }',
            )
        ),
        array(
            'class' => 'TbEditableColumn',
            'name' => 'dlitelnost',
            'value' => '($data->dlitelnost=="0")?"30":($data->dlitelnost)',
            'headerHtmlOptions' => array('style' => 'width: 110px'),
            //  'HtmlOptions' => array('style' => 'width: 400px'),
            'editable' => array( //editable section
                //  'apply' => '$data->inObject != 0', //can't edit deleted users
                'title' => 'Введите значение',
                'url' => $this->createUrl('UslugaObject/UpdateVar'),
                'placement' => 'right',
                'validate' => 'js: function(value) {             
                    if (!isFinite($.trim(value))) return "Введите число";
                }',
                'display' => 'js: function(value, sourceData) {
                     if (value>0) _color="blue"; else _color="red";                 
                      $(this).css("color", _color);    
                 }',
                'success' => 'js: function() {
                            $.fn.yiiGridView.update("' . $grid_id . '");
                         }',
            )
        ),
        array(
            'class' => 'TbEditableColumn',
            'name' => 'zarplata',
            'value' => '($data->zarplata=="0")?"0":($data->zarplata)',
            'headerHtmlOptions' => array('style' => 'width: 110px'),
            'editable' => array( //editable section

                'title' => 'Введите значение',
                'url' => $this->createUrl('UslugaObject/UpdateVar'),
                'placement' => 'right',
                'success' => 'js: function() {
                            $.fn.yiiGridView.update("' . $grid_id . '");
                         }',
                'validate' => 'js: function(value) {             
                    if (!isFinite($.trim(value))) return "Введите число";
                }',
                'display' => 'js: function(value, sourceData) {
                     if (value>0) _color="blue"; else _color="red";                 
                      $(this).css("color", _color);    
                 }',
            )
        ),
    ),
));
?>
