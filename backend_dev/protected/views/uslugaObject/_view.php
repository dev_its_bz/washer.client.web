<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_usluga')); ?>:</b>
	<?php echo CHtml::encode($data->id_usluga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_object')); ?>:</b>
	<?php echo CHtml::encode($data->id_object); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('car_gruppa_id')); ?>:</b>
	<?php echo CHtml::encode($data->car_gruppa_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cena')); ?>:</b>
	<?php echo CHtml::encode($data->cena); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dlitelnost')); ?>:</b>
	<?php echo CHtml::encode($data->dlitelnost); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zarplata')); ?>:</b>
	<?php echo CHtml::encode($data->zarplata); ?>
	<br />


</div>