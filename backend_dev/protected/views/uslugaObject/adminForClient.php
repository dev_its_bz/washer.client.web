<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
        'id' => 'services-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'responsiveTable' => true,
        'hideHeader' => true,
        'headerOffset' => 40,
        'type' => 'striped',
        'ajaxUrl' => '/uslugaObject/admin',
        'columns' => array(
            'nazvanie',
            array(
                'name' => 'cena',
                'htmlOptions' => array('style' => 'width:50px')
            ),
            array(
                'name' => 'dlitelnost',
                'htmlOptions' => array('style' => 'width:50px')
            ),
        ),
    )
);
?>
