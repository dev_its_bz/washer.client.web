<?php
$this->breadcrumbs=array(
	'Usluga Objects'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UslugaObject','url'=>array('index')),
	array('label'=>'Manage UslugaObject','url'=>array('admin')),
);
?>

<h1>Create UslugaObject</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>