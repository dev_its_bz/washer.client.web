<?php

$objectModel = Object::model()->find('id=?', array($obj));
$carGruppaModel = CarGruppa::model()->find('id=?', array($group));

$this->breadcrumbs = array(
    'Контрагенты' => '/kontragent/',
    $objectModel->kontragenty->nazvanie => array('/kontragent/' . $objectModel->kontragenty->id),
    'Объекты' => array("/kontragent/{$objectModel->kontragenty->id}?tab=3"),
    $objectModel->nazvanie => array("/object/{$objectModel->id}"),
    "Услуги группы {$carGruppaModel->nazvanie}" => array("/object/{$objectModel->id}?tab={$group}"),
    'Редактор'
);
?>

    <div class="page-header">
        <h1>Выбор услуг группы &laquo;<?=$carGruppaModel->nazvanie?>&raquo;</h1>
    </div>

<?
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'polzovateli-objects-form',
    'action' => "/Usluga/SaveUslugiAndAdd?id_obj=$obj&id_group=$group",
    'enableAjaxValidation' => false,
));


$dp = $model->getForCheckBox($obj, $group);
$dp->pagination = false;
?>

<?php
    if (Yii::app()->user->gruppa_id == 2)
    $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'usluga-grid',
    'dataProvider' => $dp,
    'filter' => $model,
    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'model_to_insert',
            'checked' => '$data->inObj',
            'value' => '$data->id',
            'header' => 'Добавить',
            'selectableRows' => 2,
            'name' => 'model_to_insert',
        ),
        'nazvanie',
        'opisanie',
        array(
            'name' => 'usluga_tip_id',
            'filter' => CHtml::listData(UslugaTip::model()->findAll(), 'id', 'nazvanie'),
            'type' => 'raw',
            'value' => '$data->uslugaTip->nazvanie'
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'model_temp',
            'checked' => '$data->inObj',
            'value' => '$data->id',
            'htmlOptions' => array('style' => 'display:none'),
            'headerHtmlOptions' => array('style' => 'display:none'),
            'selectableRows' => 2,
            'id' => 'model_temp',
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
));
else
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'usluga-grid',
        'dataProvider' => $dp,
        'filter' => $model,
        'columns' => array(
            array(
                'class' => 'CCheckBoxColumn',
                'id' => 'model_to_insert',
                'checked' => '$data->inObj',
                'value' => '$data->id',
                'header' => 'Добавить',
                'selectableRows' => 2,
                'name' => 'model_to_insert',
            ),
            'nazvanie',
            'opisanie',
            array(
                'name' => 'usluga_tip_id',
                'filter' => CHtml::listData(UslugaTip::model()->findAll(), 'id', 'nazvanie'),
                'type' => 'raw',
                'value' => '$data->uslugaTip->nazvanie'
            ),
            array(
                'class' => 'CCheckBoxColumn',
                'id' => 'model_temp',
                'checked' => '$data->inObj',
                'value' => '$data->id',
                'htmlOptions' => array('style' => 'display:none'),
                'headerHtmlOptions' => array('style' => 'display:none'),
                'selectableRows' => 2,
                'id' => 'model_temp',
            )
        ),
    ));

?>

<hr/>

<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'icon' => 'check white',
    'type' => 'primary',
    'label' => 'Сохранить',
));
?>

<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'danger',
    'icon' => 'remove white',
    'label' => 'Отмена',
    'htmlOptions' => array(
        'onClick' => "window.location.href = '/object/view/$obj?tab={$group}'",
    )
));
?>


<?php $this->endWidget(); ?>