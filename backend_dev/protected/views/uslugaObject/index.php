<?php
$this->breadcrumbs=array(
	'Usluga Objects',
);

$this->menu=array(
	array('label'=>'Create UslugaObject','url'=>array('create')),
	array('label'=>'Manage UslugaObject','url'=>array('admin')),
);
?>

<h1>Usluga Objects</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
