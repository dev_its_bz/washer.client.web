<?php
$this->breadcrumbs=array(
	'Usluga Objects'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UslugaObject','url'=>array('index')),
	array('label'=>'Create UslugaObject','url'=>array('create')),
	array('label'=>'View UslugaObject','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage UslugaObject','url'=>array('admin')),
);
?>

<h1>Update UslugaObject <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>