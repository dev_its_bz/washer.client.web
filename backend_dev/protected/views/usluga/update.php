<?php
/* @var $this UslugaController */
/* @var $model Usluga */

$this->breadcrumbs=array(
	'Uslugas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Usluga', 'url'=>array('index')),
	array('label'=>'Create Usluga', 'url'=>array('create')),
	array('label'=>'View Usluga', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Usluga', 'url'=>array('admin')),
);
?>

<h1>Update Usluga <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>