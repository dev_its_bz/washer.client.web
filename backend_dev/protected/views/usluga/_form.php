<?php
/* @var $this UslugaController */
/* @var $model Usluga */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'usluga-form',
    'htmlOptions' => array('class' => 'well'),
    'enableAjaxValidation' => false,
    'focus'=>array($model,'nazvanie'),
));
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'nazvanie', array('class' => 'span5', 'maxlength' => 100)); ?>

<?php echo $form->textAreaRow($model, 'opisanie', array('rows' => 5, 'class' => 'span5')); ?>

<?php echo $form->hiddenField($model, 'usluga_tip_id'); ?>

    <hr/>
<?
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'icon' => 'check white',
    'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
));
?>

<?
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'danger',
    'icon' => 'remove white',
    'label' => 'Отмена',
    'htmlOptions' => array(
        'onClick' => "window.location.href = '/Usluga/ViewByTip/{$model->usluga_tip_id}'",
    )
));
?>

<?php $this->endWidget(); ?>