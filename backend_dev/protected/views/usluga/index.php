<?php
/* @var $this UslugaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Uslugas',
);

$this->menu=array(
	array('label'=>'Create Usluga', 'url'=>array('create')),
	array('label'=>'Manage Usluga', 'url'=>array('admin')),
);
?>

<h1>Uslugas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
