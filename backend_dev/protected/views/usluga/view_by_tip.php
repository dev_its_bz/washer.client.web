<?php

/* @var $this UslugaController */
/* @var $model Usluga */
?>

<?
$this->breadcrumbs = array(
    'Виды услуг' => array('/uslugaTip/admin'),
    $model->uslugaTip->nazvanie
);
?>

<div class="page-header">
    <h1>&laquo;<?= $tip_name ?>&raquo;
        <?php
        $this->widget('bootstrap.widgets.TbButton',
            array('buttonType' => 'link', 'type' => 'success', 'icon' => 'plus white', 'url' => '/Usluga/create/' . $tip_id)
        )
        ?>
    </h1>
</div>


<?php

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'services-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        // 'id',
        array(
            'name' => 'id',
            'headerHtmlOptions' => array('style' => 'width: 30px'),
        ),
        array(
            'class' => 'TbEditableColumn',
            'name' => 'nazvanie',
            'value' => '$data->nazvanie',

            'editable' => array(
                'apply' => 'true',
                'title' => 'Введите значение',
                'url' => $this->createUrl('Usluga/UpdateVar'),

            )
        ),
        array(
            'class' => 'TbEditableColumn',
            'name' => 'opisanie',
            'value' => '$data->opisanie',

            'editable' => array(
                'apply' => 'true',
                'title' => 'Введите значение',
                'url' => $this->createUrl('Usluga/UpdateVar'),

            )
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',

        ),
    ),
));
?>
