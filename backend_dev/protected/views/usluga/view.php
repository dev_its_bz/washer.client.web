<?php
/* @var $this UslugaController */
/* @var $model Usluga */

$this->breadcrumbs=array(
	'Uslugas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Usluga', 'url'=>array('index')),
	array('label'=>'Create Usluga', 'url'=>array('create')),
	array('label'=>'Update Usluga', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Usluga', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Usluga', 'url'=>array('admin')),
);
?>

<h1>View Usluga #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nazvanie',
		'opisanie',
		'usluga_tip_id',
		'data_sozdaniya',
		'avtor_zapisi_id',
		'data_zapisi',
	),
)); ?>
