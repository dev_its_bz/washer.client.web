<?php
/* @var $this UslugaController */
/* @var $model Usluga */
?>

<?

$model->usluga_tip_id = $tip;

$this->breadcrumbs = array(
    'Виды услуг' => array('/uslugaTip/admin'),
    $model->uslugaTip->nazvanie => array('/Usluga/ViewByTip/' . $tip),
    'Новая услуга'
);
?>
    <div class="page-header">
        <h1>Новая услуга</h1>
    </div>

<?php
$model->usluga_tip_id = $tip;
$this->renderPartial('_form', array('model' => $model));
?>