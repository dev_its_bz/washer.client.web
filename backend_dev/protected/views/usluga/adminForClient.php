<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
        'id' => 'services-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'responsiveTable' => true,
        'fixedHeader' => true,
        'headerOffset' => 40,
        'type' => 'striped',
        'ajaxUrl' => '/object/index',
        'columns' => array(
            'nazvanie',
            array(
                'name' => 'usluga_tip_id',
                'type' => 'raw',
                'value' => '$data->uslugaTip->nazvanie',
                'filter' => CHtml::listData(UslugaTip::model()->findAll(), 'id', 'nazvanie'),
            ),
        ),
        'extendedSummaryOptions' => array(
            'class' => 'well pull-right',
            'style' => 'width:300px'
        ),
    )
);
?>
