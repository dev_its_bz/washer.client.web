<?php
/* @var $this UslugaController */
/* @var $model Usluga */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nazvanie'); ?>
		<?php echo $form->textField($model,'nazvanie',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'opisanie'); ?>
		<?php echo $form->textArea($model,'opisanie',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usluga_tip_id'); ?>
		<?php echo $form->textField($model,'usluga_tip_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_sozdaniya'); ?>
		<?php echo $form->textField($model,'data_sozdaniya'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'avtor_zapisi_id'); ?>
		<?php echo $form->textField($model,'avtor_zapisi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'data_zapisi'); ?>
		<?php echo $form->textField($model,'data_zapisi'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->