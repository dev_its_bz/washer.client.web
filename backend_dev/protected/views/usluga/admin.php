<?php
/* @var $this UslugaController */
/* @var $model Usluga */

$this->breadcrumbs = array(
    'Uslugas' => array('index'),
    'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#usluga-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Uslugas</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'services-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id',
        'nazvanie',
        'opisanie',
        //'moika.nazvanie',
        //'gruppa.nazvanie',
        array(
            'name' => 'type_name',
            'type' => 'raw',
            'value' => '$data->uslugaTip->nazvanie',

            'filter' => CHtml::listData(UslugaTip::model()->findAll(), 'id', 'nazvanie'),
            //UslugaGruppa::model()->getForFilter(),

        ),


        /*
        'data_sozdaniya',
        'avtor_zapisi_id',
        'data_zapisi',
        */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>
