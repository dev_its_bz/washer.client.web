<?php
$this->breadcrumbs=array(
	'Udobstvo Objects'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UdobstvoObject','url'=>array('index')),
	array('label'=>'Manage UdobstvoObject','url'=>array('admin')),
);
?>

<h1>Create UdobstvoObject</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>