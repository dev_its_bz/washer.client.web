<?php
$this->breadcrumbs=array(
	'Udobstvo Objects'=>array('index'),
	'Управление',
);

$this->menu=array(
	array('label'=>'List UdobstvoObject','url'=>array('index')),
	array('label'=>'Create UdobstvoObject','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('udobstvo-object-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Udobstvo Objects</h1>

<p>
В фильтрах  можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или равенства <b>=</b>), указав их перед выражением.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'udobstvo-object-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'udobstvo_id',
		'object_id',
        'udobstvo.nazvanie',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
