<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'udobstvo-object-grid',
        'dataProvider' => $model->search(),
        'hideHeader' => true,
        'template' => '{items}',
        'type' => 'striped',
        'columns' => array(
            array(
                'type' => 'raw',
                'value' => '"<img src=/images/udobstvo/".$data->udobstvo->drawable.".png align=middle> ".$data->udobstvo->nazvanie',
                'htmlOptions' => array('style' => 'vertical-align: middle')
            )
        ),
    )
);
?>
