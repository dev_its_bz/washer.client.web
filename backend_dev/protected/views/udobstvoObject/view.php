<?php
$this->breadcrumbs=array(
	'Udobstvo Objects'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UdobstvoObject','url'=>array('index')),
	array('label'=>'Create UdobstvoObject','url'=>array('create')),
	array('label'=>'Update UdobstvoObject','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete UdobstvoObject','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UdobstvoObject','url'=>array('admin')),
);
?>

<h1>View UdobstvoObject #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'udobstvo_id',
		'object_id',
		'avtor_zapisi_id',
		'data_sozdaniya',
		'data_zapisi',
	),
)); ?>
