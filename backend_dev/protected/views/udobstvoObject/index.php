<?php
$this->breadcrumbs=array(
	'Udobstvo Objects',
);

$this->menu=array(
	array('label'=>'Create UdobstvoObject','url'=>array('create')),
	array('label'=>'Manage UdobstvoObject','url'=>array('admin')),
);
?>

<h1>Udobstvo Objects</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
