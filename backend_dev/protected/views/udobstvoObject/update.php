<?php
$this->breadcrumbs=array(
	'Udobstvo Objects'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UdobstvoObject','url'=>array('index')),
	array('label'=>'Create UdobstvoObject','url'=>array('create')),
	array('label'=>'View UdobstvoObject','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage UdobstvoObject','url'=>array('admin')),
);
?>

<h1>Update UdobstvoObject <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>