<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'zapis-usluga-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Поля, помеченные <span class="required">*</span> обязательны.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'zapis_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'usluga_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'data_zapisi',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'avtor_zapisi',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
