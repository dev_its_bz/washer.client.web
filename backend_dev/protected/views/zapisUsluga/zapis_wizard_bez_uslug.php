<?php
Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/SmartWizard/js/jquery.smartWizard-2.0.js'));
Yii::app()->clientScript->registerCssFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/SmartWizard/styles/smart_wizard.css'));
?>

<script>
    function seterror($err) {
        //  $('#wizard').smartWizard('setError', {stepnum: 1, iserror: true});
        //  $('#wizard').smartWizard('showMessage', 'ошибка');
        alert($err);
    }

    $(document).ready(function() {

        $('#wizard').smartWizard({onFinish: finishRecCallBack, contentURL: 'ProcessStepsNoServices',
            onLeaveStep: leaveAStepCallback, contentCache: false,
            labelNext: 'Далее', labelPrevious: 'Назад', labelFinish: 'Закончить'});

        function finishRecCallBack(obj) {
            var recordIsAdd;
            $.post(
                    'SaveM',
                    {index: index, begin_time: time, zap_date: $("#date_zapis").val()},
            function(data) {
                recordIsAdd = data.msg;
                initDivs(data.data);
                url = '/zapisUsluga/finnal?rec_id=' + data.id_rec;
            }, 'json');
            setTimeout(function() {
                if (!recordIsAdd) {
                    isStepValid = false;
                    seterror('На выбранное время уже , выберите другое время');
                } else
                    window.location.replace(url);
            }, 1000);
        }

        function leaveAStepCallback(obj) {
            var step_num = obj.attr('rel'); // get the current step number
            return validateSteps(step_num, obj); // return false to stay on step and true to continue navigation 
        }

        function validateSteps(stepnumber, obj) {
            var isStepValid = true;

            if (stepnumber == 1) {
                if (!step1_valid) {
                    isStepValid = false;
                    seterror('Выберите авто или группу авто');
                }
                //       if ($("#group_id").prop("selectedIndex") == -1 && $("#group_id").prop("selectedIndex") == -1);
            }
            if (stepnumber == 3) {


            }
       

            return isStepValid;
        }

        function onFinishCallback() {
            $('#wizard').smartWizard('showMessage', 'Finish Clicked');
        }
    });
</script>


<table align="center" border="0" cellpadding="10" cellspacing="0" style="width: 100%; height: 100%">
    <tr>
        <td>
            <!-- Smart Wizard -->
            <div id="wizard" class="swMain" style="width: 100%; height: 100%">
                <ul>
                    <li><a href="#step-1">
                            <span class="stepNumber">1</span>
                            <span class="stepDesc">
                                Автомобиль<br/>
                                <small>Тип или модель авто</small>
                            </span>
                        </a></li>

                    <li><a href="#step-3">
                            <span class="stepNumber">3</span>
                            <span class="stepDesc">
                                Время<br/>
                                <small>Выбор времени записи</small>
                            </span>
                        </a></li>

                </ul>
                <div id="step-1" style="width: 98%">
                </div>

                <div id="step-3" style="width: 98%">

                </div>

            </div>
            <!-- End SmartWizard Content -->

        </td>
    </tr>
</table>
