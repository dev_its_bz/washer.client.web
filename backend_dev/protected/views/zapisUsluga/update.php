<?php
$this->breadcrumbs=array(
	'Zapis Uslugas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ZapisUsluga','url'=>array('index')),
	array('label'=>'Create ZapisUsluga','url'=>array('create')),
	array('label'=>'View ZapisUsluga','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ZapisUsluga','url'=>array('admin')),
);
?>

<h1>Update ZapisUsluga <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>