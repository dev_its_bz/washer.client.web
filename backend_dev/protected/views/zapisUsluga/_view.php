<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zapis_id')); ?>:</b>
	<?php echo CHtml::encode($data->zapis_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usluga_id')); ?>:</b>
	<?php echo CHtml::encode($data->usluga_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_zapisi')); ?>:</b>
	<?php echo CHtml::encode($data->data_zapisi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('avtor_zapisi')); ?>:</b>
	<?php echo CHtml::encode($data->avtor_zapisi); ?>
	<br />


</div>