<?php
$this->breadcrumbs=array(
	'Zapis Uslugas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ZapisUsluga','url'=>array('index')),
	array('label'=>'Manage ZapisUsluga','url'=>array('admin')),
);
?>

<h1>Create ZapisUsluga</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>