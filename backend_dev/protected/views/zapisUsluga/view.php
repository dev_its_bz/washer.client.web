<?php
$this->breadcrumbs=array(
	'Zapis Uslugas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ZapisUsluga','url'=>array('index')),
	array('label'=>'Create ZapisUsluga','url'=>array('create')),
	array('label'=>'Update ZapisUsluga','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete ZapisUsluga','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ZapisUsluga','url'=>array('admin')),
);
?>

<h1>View ZapisUsluga #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'zapis_id',
		'usluga_id',
		'data_zapisi',
		'avtor_zapisi',
	),
)); ?>
