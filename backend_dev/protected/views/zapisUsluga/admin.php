<?php
//Yii::app()->clientscript->scriptMap['jquery.js'] = false;
?>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('usluga-object-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h1>Предоставляемые услуги</h1>


<div id="f" style="float:left; display:inline; width: 400px; height:450px; overflow: auto; scrollbar-base-color:#ffeaff">


    <?php
    $dp = $model->search();
    $dp->pagination = false;
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'zapis-usluga-grid',
        'dataProvider' => $dp,
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'id',
                'value' => '$data->uslugi->id',
                'filter' => '',
            ),
            array(
                'name' => 'usl_type',
                'filter' => CHtml::listData(UslugaTip::model()->findAll(), 'id', 'nazvanie'),
                'type' => 'raw',
                'value' => '$data->uslugi->uslugaTip->nazvanie',
                'headerHtmlOptions' => array('style' => ' width: 80px'),
                'htmlOptions' => array('style' => ' width: 80px'),
            ),
            array(
                'name' => 'usl_name',
                'value' => '$data->uslugi->nazvanie',
                'headerHtmlOptions' => array('style' => 'width: 80px'),
                'htmlOptions' => array('style' => ' width: 80px'),
            ),
            array(
                'name' => 'cena',
                'value' => '$data->cena',
                'headerHtmlOptions' => array('style' => ' width: 80px'),
                'htmlOptions' => array('style' => ' width: 80px'),
            ),
            array(
                'name' => 'dlitelnost',
                'value' => '$data->dlitelnost',
                'headerHtmlOptions' => array('style' => ' width: 80px'),
                'htmlOptions' => array('style' => ' width: 80px'),
            ),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{offbut}',
                'buttons' => array
                    (
                    'offbut' => array
                        (
                        'label' => 'перенести',
                        'icon' => 'icon-list',
                        'url' => 'Yii::app()->controller->createUrl("addService")',
                        'options' => array('class' => 'butof',
                            'ajax' => array('type' => 'POST', 'url' => 'js:$(this).attr("href")',
                                'data' => array('serv' => 'js: services_id'),
                                'success' => 'js:function(data) {}')),
                    ),
                ),
            ),
        ),
    ));
    ?>
</div>

<script>
    jQuery.noConflict();
    var services_id = [];

</script>>

<script>
    $(document).ready(function() {
        //   $('#choosen_services').yiiGridView.getRow(1);
        //   alert($.fn.yiiGridView.yiiGridView.getRow('choosen_services', 1)); 
        //    alert('fdsfs')

        $('body').on('click', '.butof', function() {

            $(this).parent().parent().hide();
            services_id.push($(this).parent().parent().children(":nth-child(1)").text());
            return false;
        });

        grid = $('#zapis-usluga-grid');
        $('tr', grid).each(function() {
            $('td:eq(0), th:eq(0)', this).hide();
        });

    });



</script>


<div id="s" style="float:left; display:inline; width: 400px; height:450px; overflow: auto; scrollbar-base-color:#ffeaff">

<?php
$dp = $model->search();
$dp->pagination = false;
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'choosen_services',
    'dataProvider' => $dp,
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'usl_type',
            'filter' => CHtml::listData(UslugaTip::model()->findAll(), 'id', 'nazvanie'),
            'type' => 'raw',
            'value' => '$data->uslugi->uslugaTip->nazvanie',
            'headerHtmlOptions' => array('style' => 'text-align: center; width: 80px'),
            'htmlOptions' => array('style' => 'text-align: center; width: 80px'),
        ),
        array(
            'name' => 'usl_name',
            'value' => '$data->uslugi->nazvanie',
            'headerHtmlOptions' => array('style' => 'text-align: center; width: 80px'),
            'htmlOptions' => array('style' => 'text-align: center; width: 80px'),
        ),
        array(
            'name' => 'cena',
            'value' => '$data->cena',
            'headerHtmlOptions' => array('style' => 'text-align: center; width: 80px'),
            'htmlOptions' => array('style' => 'text-align: center; width: 80px'),
        ),
        array(
            'name' => 'dlitelnost',
            'value' => '$data->dlitelnost',
        // 'headerHtmlOptions' => array('style' => 'text-align: center; width: 80px'),
        // 'htmlOptions' => array('style' => 'text-align: center; width: 80px'),
        ),
    ),
));
?>
</div>
