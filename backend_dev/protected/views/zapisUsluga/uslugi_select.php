<?php
Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/JsHttpRequest/JsHttpRequest.js'));

Yii::app()->getClientScript()->registerCoreScript('jquery.ui');;
//Yii::app()->clientScript->registerCssFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/multiselect2/css/common.css'));
Yii::app()->clientScript->registerCssFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/multiselect2/css/jquery.uix.multiselect.css'));

Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl() . '/jui/css/base/jquery-ui.css');
Yii::app()->clientScript->registerCssFile(
    Yii::app()->assetManager->publish(Yii::app()->basePath . '/vendor/jquery.ui/sunny/') . '/jquery-ui.css', 'screen'
);


Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/multiselect2/js/jquery.uix.multiselect.js'));
Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/multiselect2/js/locales/jquery.uix.multiselect_ru.js'));
?>
<div>
    <select id="services" class="multiselect" style="height:500px; width: 700px;" multiple="multiple" name="countries[]"
            style="width:700">
        <? var_dump($tags) ?>
    </select>
</div>
<br/>
<div>
    <span id="cost"></span><br/>
    <span id="duration"></span>
</div>
<script type="text/javascript">

    var select_elements = [];
    var cost = 0;
    var duration = 0;
</script>

<br/>
<div>
    <span id="cost"></span><br/>
    <span id="duration"></span>
</div>

<script>
    $(document).ready(function () {

        $('#cars_id').hide();

        $("#services").multiselect({locale: 'ru'});

        $("#services").bind('multiselectChange', function (evt, ui) {
            cost = 0;
            duration = 0;
            ui.optionElements.forEach(function (service) {
                if (jQuery.inArray(service, select_elements) == -1)
                    select_elements.push(service);
                else
                    select_elements.splice(jQuery.inArray(service, select_elements), 1);
            });
            select_elements.forEach(function (service) {
                cost += parseInt(service.dataset.cost);
                duration += parseInt(service.dataset.duration);
            });

            $("#cost").text('Общая цена=' + cost + ' рублей');
            $("#duration").text('Общая длительность=' + duration + ' минут');

            $.post(
                'AddUslugi',
                {cost: cost, duration: duration},
                'json');
        });


    });
</script>
