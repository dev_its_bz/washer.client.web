<?php
$this->widget(
    'bootstrap.widgets.TbWizard',
    array(
        'type' => 'tabs', // 'tabs' or 'pills'
        'tabs' => array(
            array(
                'label' => 'Home',
                'content' => 'Home Content',
                'active' => true
            ),
            array('label' => 'Profile', 'content' => 'Profile Content'),
            array('label' => 'Messages', 'content' => 'Messages Content'),
        ),
    )
);?>
