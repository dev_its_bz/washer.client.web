<?php
$tags = array(
    array('id' => 1, 'text' => 'php'),
    array('id' => 2, 'text' => 'html'),
    array('id' => 3, 'text' => 'css'),
    array('id' => 4, 'text' => 'javascript'),
);


$this->widget('zii.widgets.CMenu', array(
    'items' => array(
        array('label' => 'Заказ N ' . $record['id']),
        array('label' => 'Время начала: ' . $record['time_begin']),
        array('label' => 'Дата начала: ' . $record['zapis_date']),
        array('label' => 'Тип автомобиля: ' . $record->avto_type->nazvanie),
        array('label' => 'Примерная длительность (округлено): ' . $record->full_time_temp),
        array('label' => 'Сумма: ' . $record->summa),
    ),
));
?>
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap-editable/js/bootstrap-editable.min.js"></script>
<script>
    helpers = new Array();
    services = new Array();

    function Helper(id, service) {
            this.id = id;
            this.service = service;
        }
    function Service(id) {
            this.id = id;
            this.cost = 0;
            this.duration = 0;
        }

    var rec = <?= $services ?>;
  
    var executors_all = <?= json_encode($executors_all) ?>;
    function addTableRow(data) {
        var tds = '<tr  id="' + data['id'] + '">';
        tds += '<td class="service_name">' + data['name'] + '</td>';
        tds += '<td class="service_executor">' + '<span class = "main_executor">'
                + $('#username').text() + '</span>' + '<br>Помошники:<span class = "helpers"></span>' + '<button class="helper_add">+</button>' +
                '<select class="help_sel" ></select>' + '</td>';
        tds += '<td class="service_duration"><a class="duration">' + data['duration'] + '</a></td>';
        tds += '<td class="service_cost"><a class="cost">' + data['cost'] + '</a></td>';
        tds += '<td class="service_manage">' + '<button class="delete_service">-</button>' + '</td>';
        tds += '</tr>';
        
        $('tbody', '#main_table').append(tds);
        
        service = new Service(data.id);
        services.push(service);
        
        $('#' + data['id'] + ' .help_sel').hide();
        $('#' + data['id'] + ' .cost').editable({
            type: 'text',
            mode: 'inline',
            title: 'Введите цену',
        });
        $('#' + data['id'] + ' .duration').editable({
            type: 'text',
            mode: 'inline',
            title: 'Введите время (мин)',
        });
        $('.duration').on('save', function(e, params) {
            $(this).text(params.newValue);
            addLastTr();
        });
        $('.cost').on('save', function(e, params) {
            $(this).text(params.newValue);
            addLastTr();
        });

    }
    ;

    $(".delete_service").live('click', function() {
        service_id = $(this).parent().parent().attr('id');
        $("tr#" + service_id).remove();
        services.forEach(function (service){
            if (service.id === service_id) services.splice(services.indexOf(service),1);
        });
        var fordel = [];
        helpers.forEach(function (helper){        
            if (helper.service === service_id) {
                fordel.push (helper);                    
            }
        });
        
        fordel.forEach(function (helper){
            helpers.splice(helpers.indexOf(helper),1);
        });
    });
    
    $(".add_service").live('click', function() {
        $("#Service_select").show();
    });

    $("#Service_select").live('change', function() {
        $.post(
                'addNewService',
                {service_id: $("#Service_select :selected").val(), avto_type: <?= $record['car_group_id'] ?>},
        function(data) {           
            addTableRow(data);
        
          setNewSelect(data['id']);
            addLastTr();
        }, 'json');
        $(this).hide();
            
    });

    function updateSelects() {
        $('.help_sel').empty();
        seloption = "";
        $.each(executors_all, function(i) {
            seloption += '<option value="' + i + '">' + executors_all[i] + '</option>';
        });
        $('.help_sel').append(seloption);
    } 
    function setNewSelect(id) {
        x_edit_val = $('#username').editable('getValue');
        executors_id = x_edit_val["username"];
       
        seloption = "";
        $.each(executors_all, function(i) {
            if ($.inArray(i, executors_id) == -1)
           
            seloption += '<option value="' + i + '">' + executors_all[i] + '</option>';
        
        });
        $('#'+ id +' .help_sel').append(seloption);
    } 

    function reloadTable() {

        rec.forEach(function(serv) {
            addTableRow(serv);
        });

        updateSelects();
    }
    

    $(".helper_add").live('click', function() {

        delfromSelect($(this).parent().parent().attr('id'));
        $('#' + $(this).parent().parent().attr('id') + ' .help_sel').show();
        $(this).hide();

    });



    function addLastTr() {
        $(".lastTr").empty();
        var full_duration = 0;
        $(".duration").each(function() {
            full_duration += parseInt($(this).text());
        });

        var full_cost = 0;
        $(".cost").each(function() {
            full_cost += parseInt($(this).text());
        });

        var tds = '<tr  class="lastTr">';
        tds += '<td class="service_name"></td>';
        tds += '<td class="service_executor">Итого:</td>';
        tds += '<td class="service_duration">' + full_duration + '</td>';
        tds += '<td class="service_cost">' + full_cost + '</td>';
        tds += '<td class="service_manage"></td>';
        tds += '</tr>';

        $('tbody', '#main_table').append(tds);
    }
    ;
    function delfromSelect(id) {
        $('#' + id + ' .helper').each(function(i) { //перебераем всех помошников

            $('#' + id + " .help_sel :contains('" + $(this).text() + "')").remove(); // удаляем помошников из списка
        });
    }
    ;

    $(".help_sel").live('change', function() {
        $('#' + $(this).parent().parent().attr('id') + ' .helper_add').show();
        $(this).hide();
        AddHelper($(this).parent().parent().attr('id'), $('option:selected', this).val(), $('option:selected', this).text())
       
    });

    function AddHelper(id_service, id, name) {      
        $('#' + id_service + ' .helpers').append('<br><span class="helper">' + name + '</span>');
        helper = new Helper(id, id_service);
        helpers.push(helper);

    }
    

    $("#save_all").live('click', function() {
        x_edit_val = $('#username').editable('getValue');
        executors_id = x_edit_val["username"]; //рабочие
            
        services.forEach(function (service){ // услуги
            service.cost = $('#' + service.id +' .cost').text();
            service.duration = $('#' + service.id +' .duration').text();
        });
                // помошники лежат в helpers
       
        $.post(
                'saveRecordData',
                {executors: executors_id, helpers: helpers, services: services, record_id: <?=$record['id']?>},
        function(data) {
          
        }, 'json');
    });
    
    $("#paid").live('click', function() {      
        $.post(
                'recordPaid',
                {record_id: <?=$record['id']?>},
        function(data) {
          
        }, 'json');
    });


    $("#delete_all").live('click', function() {      
        $.post(
                'deleteRecord',
                {record_id: <?=$record['id']?>},
        function(data) {
          
        }, 'json');
    });

    $(document).ready(function() {

        executors = <?= json_encode($executor_index)  ?>;
   
        $('#username').editable({
            type: 'checklist',
            mode: 'popup',
            pk: 1,
            title: 'Выберите сотрудника',
            value: executors,
            source: executors_all
        });


        $('#username').on('save', function(e, params) {
            updateSelects();
            $('.main_executor').empty();
            params.newValue.forEach(function(i) {
                $('.main_executor').append(executors_all[i] + ', ');
                $('.helper').each(function() {
                    if ($(this).text() == executors_all[i])
                        $(this).remove();
                });
                $(".help_sel option[value='" + i + "']").remove();
            });

        });


        reloadTable();
        addLastTr();
        executors.forEach(function (ex){
             $(".help_sel option[value='" + ex + "']").remove();
        });
       
       this_helpers = <?=json_encode($helpers)?>;
       this_helpers.forEach(function(helper){
         
           AddHelper(helper[0], helper[1], helper[2]);
       });
   //     alert (helpers[0]);


    });
</script>
<h3>Выбранныe услуги: </h3>

<table id="main_table">
    <tr>
        <th>Услуги</th>
        <th> <a href="#" id="username" ></a>  </th>
        <th>Время</th>
        <th>Цена</th>
        <th>Управление</th>

    </tr>

</table>
<button class="add_service">Добавить услугу</button>
<?=
CHtml::dropDownList('Service_select', '', $services_all, array('style' => 'display: none'));
?>
<br>
<button id="paid">Оплатить</button>
<button id="save_all">Сохранить</button>
<button id="delete_all">Удалить</button>

