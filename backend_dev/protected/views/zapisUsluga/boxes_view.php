<?php
Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/datepick/jquery.datepick.js'));

Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/datepick/jquery.datepick-ru.js'));

Yii::app()->clientScript->registerCssFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/datepick/jquery.datepick.css'));
?>
    <div class="page-header">
        <h1> Боксы </h1>
    </div>
    <style>
        .cell {
            width: 8px;
            height: 100%;
            border: 1px solid #000;
            float: left;
            font-size: 7px;
        }

        .row {
            float: left;
            width: auto;
            height: 10px;
        }

        .red {
            background-color: red;
        }

        .green {
            background: #aea;
        }

        .black {
            background: #04c;
        }

        .pink {
            background: #CA0EE3;
        }

        .yellow {
            background: #ad6704
        }

        .full-border {
            border: 1px solid #000;
        }

        .left-border {
            border-left: 1px solid #000;
        }

        .cell2 {
            width: 40px;
            height: 40px;
            border: 1px solid #333;
            float: left;
            font-size: 12px;
            text-align: center;
            vertical-align: middle;
        }

        .row2 {
            width: auto;
            height: 40px;
            text-align: center;
            vertical-align: middle;
        }

        .hidden {
            display: none;
        }

        .totop {
            vertical-align: top;
            width: 40px;
            height: 7px;
            display: none;
        }

        .tobot {
            vertical-align: bottom;
            width: 40px;
            height: 7px;
            display: none;
        }

        .rec_id {
            vertical-align: middle;
            position: absolute;
        }

    </style>
    <script>


        $(".red").live({
            mouseenter: function () {
                id = $(this).attr('id');
                $("#" + id + " .totop").show();
                $("#" + id + " .tobot").show();
            },
            mouseleave: function () {
                id = $(this).attr('id');
                $("#" + id + " .totop").hide();
                $("#" + id + " .tobot").hide();
            }
        });

        $(".totop").live("click", function () {
            $.post(
                'Changebox',
                {box: $(this).data("box"), block: $(this).data("block"), type: 'up'},
                function (data) {
                    initDivs(data.data);
                },
                'json');
        });

        $(".tobot").live("click", function () {
            $.post(
                'Changebox',
                {box: $(this).data("box"), block: $(this).data("block"), type: 'down'},
                function (data) {
                    initDivs(data.data);
                },
                'json');
        });

        $(".red").live("dblclick", function () {
            url = '/zapisUsluga/ViewRecord?rec_id=' + $(this).data("rec_id");
            window.location.replace(url);
        });

    </script>

    <script>
        var index;
        var time;

        Date.prototype.yyyymmdd = function () {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth() + 1).toString();
            var dd = this.getDate().toString();
            return (dd[1] ? dd : "0" + dd[0]) + '.' + (mm[1] ? mm : "0" + mm[0]) + '.' + yyyy;
            return yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]); // padding
        };

        var mas1 =<?php echo $boxes ?>;

        time = new Date();
        h = time.getHours();
        m = time.getMinutes();
        var strtcll = parseInt((h * 60 + m) / <?= $block_time ?>);
        var endcll = strtcll + 21;

        $('#prev').live("click", function () {

            if (strtcll != 0) {//начальная граница показа времени записи
                strtcll--;
                endcll--;
            }
            goToMassM(mas1);
        });

        $('#sled').live("click", function () {
            if (endcll != 96) {//конечная граница показа времени записи
                strtcll++;
                endcll++;
            }
            goToMassM(mas1);
        });

        function initializeClass() {
            $('.cell2').removeClass('green');
            $('.cell2').removeClass('red');
            $('.cell2').removeClass('black');
            $('.cell2').removeClass('pink');
            $('.cell2').removeClass('hidden');
            $('.cell2').removeClass('yellow');
            $('.cell').removeClass('green');
            $('.cell').removeClass('red');
            $('.cell').removeClass('black');
            $('.cell').removeClass('pink');
            $('.cell').removeClass('hidden');
            $('.cell').removeClass('yellow');
            $('.totop').remove();
            $('.tobot').remove();
        }

        function loadTimeBoxM(mas1, classtype) {
            //     alert(mas1);
            for (var box in mas1) {
                for (j = 0; j < <?= $block_count ?>; j++) {
                    if (mas1[box][j] > 0) {
                        $('#' + box + classtype + (j)).addClass('red');

                        $('#' + box + classtype + (j)).append("<button class='totop' data-box='" +
                            box + "' data-block='" + j + "'>вверх</button>");
                        //      $('#' + box + classtype + (j)).append("<span class='rec_id'>"+mas1[box][j]+"</span>");
                        $('#' + box + classtype + (j)).attr("data-rec_id", mas1[box][j]);
                        $('#' + box + classtype + (j)).append("<button class='tobot' data-box='" +
                            box + "' data-block='" + j + "'>вверх</button>");
                    } else if (mas1[box][j] == 0) {
                        $('#' + box + classtype + (j)).addClass('green');
                        //     $('#' + classtype + (j)).width(40);
                    } else if (mas1[box][j] == -1) {
                        $('#' + box + classtype + (j)).addClass('pink');

                    }
                    //   alert($('#' + parseInt(box) + classtype + (j)).html());
                }
            }
            return mas1;
        }
        ;

        function goToMassM(mas1) {
            for (var box in mas1) {
                for (j = 0; j < <?= $block_count ?>; j++) {
                    if (j < strtcll || j > endcll) {
                        $('#' + box + 'mm' + (j)).addClass('hidden');
                        $('#' + box + 'time' + j).addClass('hidden');
                    } else {
                        $('#' + box + 'mm' + (j)).removeClass('hidden');
                        $('#' + box + 'time' + j).removeClass('hidden');
                    }
                }
            }
        }
        ;

        function initDivs(arr) {
            initializeClass();

            m = loadTimeBoxM(arr, 'mm');
            goToMassM(m);
        }

        $(document).ready(function () {
            initDivs(mas1);
            d = new Date();
            $("#date_zapis").datepick({renderer: $.extend({}, $.datepick.defaultRenderer,
                {picker: $.datepick.defaultRenderer.picker.
                    replace(/\{link:clear\}/, '')}), changeYear: false, changeMonth: false,
                onClose: function (date) {
                    $.post(
                        'updateBoxesManager',
                        {date: $("#date_zapis").val()},
                        function (data) {
                            initDivs(data.data);
                        }, 'json');
                },
            });

            $("#date_zapis").val(d.yyyymmdd());
        });


    </script>

    <input type="text" size="40" id="date_zapis"/>

<?php foreach ($boxes_id as $box) : ?>
    <div id="<?= $box ?>" style="width:100%;height:auto; float: left">
        <table cellPadding=0 CellSpacing=0 Border=0>
            <tr>
                <td>
                    <button id="prev" style="height:84px;">&Lleftarrow;</button>
                </td>
                <td style="text-align: center">
                    <div class='row2' id='time'>
                        <?php for ($j = 0; $j < $block_count; $j++) : ?>
                            <div class='cell2'
                                 id="<?= $box ?>time<?php echo($j) ?>"> <?php echo (integer)(($block_time * $j) / 60) ?>:<?php echo (($block_time * $j) % 60) < 10 ? '0' : '' ?><?php echo(($block_time * $j) % 60) ?></div>
                        <?php endfor; ?>
                    </div>
                    <div class='row2' id='5'>
                        <?php for ($j = 0; $j < $block_count; $j++) : ?>
                            <div class='cell2' col="<?php echo($j); ?>" row="1" id='<?= $box ?>mm<?php echo($j); ?>'></div>
                        <?php endfor; ?>
                    </div>
                </td>
                <td>
                    <button id="sled" style="float: left; height:84px;">&Rrightarrow;</button>
                </td>
            </tr>
        </table>
    </div>
<?php endforeach; ?>