<?php
Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/datepick/jquery.datepick.js'));

Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/datepick/jquery.datepick-ru.js'));

Yii::app()->clientScript->registerCssFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/datepick/jquery.datepick.css'));


?>
<h1>Zapis Uslugas</h1>
<style>
    .cell {
        width: 8px;
        height: 100%;
        border: 1px solid #000;
        float: left;
        font-size: 7px;
    }

    .row {
        float: left;
        width: 100%;
        height: 10px;
    }

    .red {
        background-color: red;
    }

    .green {
        background: #aea;
    }

    .black {
        background: #04c;
    }

    .pink {
        background: #CA0EE3;
    }

    .yellow {
        background: #ad6704
    }

    .cell2 {
        width: 40px;
        height: 40px;
        border: 1px solid #000;
        float: left;
        font-size: 12px;
        text-align: center;
    }

    .row2 {
        float: left;
        width: 100%;
        height: 40px;
    }

    .hidden {
        display: none;
    }

</style>
<?php $url = $this->createUrl('/zapisUsluga/loadBox'); ?>
<?php $urlUpdate = $this->createUrl('/zapisUsluga/UpdateTimeLine'); ?>
<?php $urlSaveM = $this->createUrl('/zapisUsluga/SaveM'); ?>
<script>
    var index;
    var time;
    Date.prototype.yyyymmdd = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
        var dd = this.getDate().toString();
        return (dd[1] ? dd : "0" + dd[0]) + '.' + (mm[1] ? mm : "0" + mm[0]) + '.' + yyyy;
        return yyyy + (mm[1] ? mm : "0" + mm[0]) + (dd[1] ? dd : "0" + dd[0]); // padding
    };

    d = new Date();


    $("#date_zapis").datepick({renderer: $.extend({}, $.datepick.defaultRenderer,
        {picker: $.datepick.defaultRenderer.picker.
            replace(/\{link:clear\}/, '')}), changeYear: false, changeMonth: false,
        minDate: new Date(), maxDate: "+<?= $day_count ?>D",
        onClose: function (date) {
            $.post(
                '<?php echo $urlUpdate ?>',
                {date: $("#date_zapis").val()},
                function (data) {
                    initDivs(data.data);
                }, 'json');
        }
    });
    $("#date_zapis").val(d.yyyymmdd());

    var mas1 =<?php echo $farr ?>;

    time = new Date();
    h = time.getHours();
    m = time.getMinutes();
    var strtcll;
    var endcll;

    $('#prev').click(function () {
        if (strtcll != parseInt((h * 60 + m) / <?= $block_time ?>)) {//начальная граница показа времени записи
            strtcll--;
            endcll--;
        }
        goToMassM();
    });

    $('#sled').click(function () {
        if (endcll != 96) {//конечная граница показа времени записи
            strtcll++;
            endcll++;
        }
        goToMassM();
    });

    function initializeClass() {
        $('.cell2').removeClass('green');
        $('.cell2').removeClass('red');
        $('.cell2').removeClass('black');
        $('.cell2').removeClass('pink');
        $('.cell2').removeClass('hidden');
        $('.cell2').removeClass('yellow');
        $('.cell').removeClass('green');
        $('.cell').removeClass('red');
        $('.cell').removeClass('black');
        $('.cell').removeClass('pink');
        $('.cell').removeClass('hidden');
        $('.cell').removeClass('yellow');
    }

    function loadTimeBoxM(mas1, classtype) {
        //     alert(mas1);
        for (j = 0; j < <?= $block_count ?>; j++) {

            if (mas1[j] == 2) {
                $('#' + classtype + (j + 1)).addClass('black');
            } else if (mas1[j] == 1) {

                $('#' + classtype + (j + 1)).addClass('red');
            }
            else if (mas1[j] == 0) {
                $('#' + classtype + (j + 1)).addClass('green');
            }
            if (mas1[j] == 3) {
                $('#' + classtype + (j + 1)).addClass('pink');
            }

            if (mas1[j] == 4) {
                $('#' + classtype + (j + 1)).addClass('yellow');
            }

        }
    }
    ;

    function goToMassM() {
        for (j = 0; j < <?= $block_count ?>; j++) {
            if (j < strtcll || j > endcll) {
                $('#' + 'mm' + (j + 1)).addClass('hidden');
                $('#time' + j).addClass('hidden');
            } else {
                $('#' + 'mm' + (j + 1)).removeClass('hidden');
                $('#time' + j).removeClass('hidden');
            }

        }
    }
    ;

    function initDivs(arr) {
        initializeClass();

        if ($("#date_zapis").val() == d.yyyymmdd())
            strtcll = parseInt((h * 60 + m) / <?= $block_time ?>);
        else
            strtcll = 0;
        endcll = strtcll + 22;

        loadTimeBoxM(arr, 'm');
        loadTimeBoxM(arr, 'mm');
        goToMassM();
    }
    $(document).ready(function () {


        initDivs(mas1);

        $('.green').live("click", (function () {
            $('.black').addClass('green');
            $('.black').removeClass('black');
            index = parseInt($('#' + this.id).attr('col'));
            time = $('#time' + index).text() + ':00';
            $('#' + 'mm' + (index + 1)).removeClass('green');
            $('#' + 'mm' + (index + 1)).addClass('black');
        }));


    });
    function upd() {
        $.fn.yiiGridView.update('zapis-usluga-grid');
    }


</script>
<input type="text" size="40" id="date_zapis"/>


<div id="main" style="width:100%;height:auto;">
    <div class='row' id='4'>
        <?php for ($j = 0; $j < $block_count; $j++) : ?>
            <div class='cell' col="<?php echo($j); ?>" row="1" id='m<?php echo($j + 1); ?>'></div>
        <?php endfor; ?>
    </div>


</div>
<br>
<br>

<br>
<br>


<div id="main2" style="width:100%;height:auto;">
    <div class='row2' id='time'>
        <?php for ($j = 0; $j < $block_count; $j++) : ?>
            <div class='cell2' id="time<?php echo($j) ?>"> <?php echo (integer)(($block_time * $j) / 60) ?>
                :<?php echo (($block_time * $j) % 60) < 10 ? '0' : '' ?><?php echo(($block_time * $j) % 60) ?></div>
        <?php endfor; ?>
    </div>

    <div class='row2' id='5'>
        <?php for ($j = 0; $j < $block_count; $j++) : ?>
            <div class='cell2' col="<?php echo($j); ?>" row="1" id='mm<?php echo($j + 1); ?>'></div>
        <?php endfor; ?>
    </div>

</div>
<button id="prev">Назад</button>
<button id="sled">Вперед</button>


<?php
/* $this->widget('bootstrap.widgets.TbGridView', array(
  'id' => 'zapis-usluga-grid',
  'dataProvider' => $model->search(),
  'filter' => $model,
  'columns' => array(
  'id',
  'car_group_id',
  'full_time_temp',
  'time_begin',
  'time_end',
  'box_id',
  // 'zapis_id',
  // 'usluga_id',
  //   'data_zapisi',
  //   'avtor_zapisi',
  array(
  'class' => 'bootstrap.widgets.TbButtonColumn',
  ),
  ),
  )); */
?>

