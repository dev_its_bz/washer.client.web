<?php
$this->breadcrumbs = array(
    'Удобства' => array('/Udobstvo/admin'),
    'Новое удобство',
);
?>
    <div class="page-header">
        <h1>Новое удобство</h1>
    </div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>