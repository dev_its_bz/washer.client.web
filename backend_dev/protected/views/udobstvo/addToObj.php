<?php
$this->breadcrumbs = array(
    'Удобства'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('udobstvo-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<?php
      $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'polzovateli-objects-form',
    'htmlOptions' => array('class' => 'well'),
    'action' => "/Udobstvo/SaveAndAdd?id=$object_id",
    'enableAjaxValidation' => false,
));
        ?>
<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'udobstvo-grid',
    'dataProvider' => $dp,
    //'filter' => $model,
    'columns' => array(
        
   
        'nazvanie',
       
  
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'model_to_insert',
            'checked' => '$data->inObj',
            'value' => '$data->id',
            'header' => 'Добавить',
            'selectableRows' => 2,
            'name' => 'model_to_insert',
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'model_temp',
            'checked' => '$data->inObj',
            'value' => '$data->id',
            'htmlOptions' => array('style' => 'display:none'),
            'headerHtmlOptions' => array('style' => 'display:none'),
            'selectableRows' => 2,
            'id' => 'model_temp',
        ),
       
    ),
)); ?>

 <?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'icon' => 'check white',
    'type' => 'primary',
    'label' => 'Сохранить',
));
?>
<?php $this->endWidget(); ?>