<?php
$this->breadcrumbs=array(
	'Udobstvos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Udobstvo','url'=>array('index')),
	array('label'=>'Create Udobstvo','url'=>array('create')),
	array('label'=>'View Udobstvo','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Udobstvo','url'=>array('admin')),
);
?>

<h1>Update Udobstvo <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>