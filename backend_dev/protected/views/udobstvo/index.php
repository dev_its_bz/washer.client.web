<?php
$this->breadcrumbs=array(
	'Udobstvos',
);

$this->menu=array(
	array('label'=>'Create Udobstvo','url'=>array('create')),
	array('label'=>'Manage Udobstvo','url'=>array('admin')),
);
?>

<h1>Udobstvos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
