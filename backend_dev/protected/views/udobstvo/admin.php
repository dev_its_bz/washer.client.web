<?php
$this->breadcrumbs = array(
    'Удобства'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('udobstvo-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="page-header">
    <h1>Удобства
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons' => array(
                array('buttonType' => 'link', 'type' => 'success', 'icon' => 'plus white', 'url' => '/udobstvo/create/'),
                array('buttonType' => 'link', 'type' => 'primary', 'icon' => 'search white', 'url' => '#', 'htmlOptions' => array('class' => 'search-button')),
            )));
        ?>
    </h1>
</div>


<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'udobstvo-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('name' => 'id', 'htmlOptions' => array('style' => 'width:20px; text-align:center')),
        array(
            'name' => 'nazvanie',
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'editable' => array(
                'type' => 'text',
                'url' => '/Udobstvo/updateZnachenie'
            )
        ),
        array(
            'name' => 'drawable',
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'editable' => array(
                'type' => 'text',
                'url' => '/Udobstvo/updateZnachenie'
            )
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array
            (
                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("/Udobstvo/delete", array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn-column',
                    ),
                ),
            ),
        ),
    ),
)); ?>
