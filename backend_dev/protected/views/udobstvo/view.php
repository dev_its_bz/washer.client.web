<?php
$this->breadcrumbs=array(
	'Udobstvos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Udobstvo','url'=>array('index')),
	array('label'=>'Create Udobstvo','url'=>array('create')),
	array('label'=>'Update Udobstvo','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Udobstvo','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Udobstvo','url'=>array('admin')),
);
?>

<h1>View Udobstvo #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nazvanie',
		'drawable',
	),
)); ?>
