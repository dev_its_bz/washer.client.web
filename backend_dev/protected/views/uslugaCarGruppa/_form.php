<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'usluga-car-gruppa-form',
    'enableAjaxValidation' => false,
));
?>

<p class="help-block">Поля, помеченные <span class="required">*</span> обязательны.</p>

<?php echo $form->errorSummary($model); ?>

<div class="row2">
    <?php echo $form->labelEx($model, 'Группа авто'); ?>
    <?php echo $form->dropDownList($model, 'car_gruppa_id', $carGruppa, array('prompt' => '(Укажите тип автомобиля)'));
    ?>
    <?php echo $form->error($model, 'car_gruppa_id'); ?>
</div>

<?php echo $form->textFieldRow($model, 'cena', array('class' => 'span5', 'maxlength' => 30)); ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
    )); ?>
</div>

<?php $this->endWidget(); ?>
