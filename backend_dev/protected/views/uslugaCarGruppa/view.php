<?php
$this->breadcrumbs=array(
	'Usluga Car Gruppas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UslugaCarGruppa','url'=>array('index')),
	array('label'=>'Create UslugaCarGruppa','url'=>array('create')),
	array('label'=>'Update UslugaCarGruppa','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete UslugaCarGruppa','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UslugaCarGruppa','url'=>array('admin')),
);
?>

<h1>View UslugaCarGruppa #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'object_id',
		'usluga_id',
		'car_gruppa_id',
		'cena',
		'data_sozdaniya',
		'avtor_zapisi_id',
		'data_zapisi',
	),
)); ?>
