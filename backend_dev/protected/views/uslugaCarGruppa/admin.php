<?php
$this->breadcrumbs=array(
	'Usluga Car Gruppas'=>array('index'),
	'Управление',
);

$this->menu=array(
	array('label'=>'List UslugaCarGruppa','url'=>array('index')),
	array('label'=>'Create UslugaCarGruppa','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('usluga-car-gruppa-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Usluga Car Gruppas</h1>

<p>
В фильтрах  можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или равенства <b>=</b>), указав их перед выражением.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'usluga-car-gruppa-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'object_id',
		'usluga_id',
		'car_gruppa_id',
		'cena',
		'data_sozdaniya',
		/*
		'avtor_zapisi_id',
		'data_zapisi',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
