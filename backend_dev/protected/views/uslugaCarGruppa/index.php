<?php
$this->breadcrumbs=array(
	'Usluga Car Gruppas',
);

$this->menu=array(
	array('label'=>'Create UslugaCarGruppa','url'=>array('create')),
	array('label'=>'Manage UslugaCarGruppa','url'=>array('admin')),
);
?>

<h1>Usluga Car Gruppas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
