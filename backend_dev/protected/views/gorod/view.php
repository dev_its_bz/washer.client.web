<?php
$this->breadcrumbs = array(
    'Города' => array('/gorod/admin'),
    $model->nazvanie,
);
?>

<div class="page-header">
    <h1>Город &laquo;<?php echo $model->nazvanie; ?>&raquo;</h1>
</div>

<?php
$this->widget('bootstrap.widgets.TbEditableDetailView', array(
    'data' => $model,
    'url' => $this->createUrl('/gorod/UpdateZnachenie'),
    'attributes' => array(
        'id',
        'lat',
        'lon',
        'nazvanie_r',
        'nazvanie_pr',
        'nazvanie',
    ),
));
?>
