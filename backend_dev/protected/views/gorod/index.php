<?php
$this->breadcrumbs=array(
	'Gorods',
);

$this->menu=array(
	array('label'=>'Create Gorod','url'=>array('create')),
	array('label'=>'Manage Gorod','url'=>array('admin')),
);
?>

<h1>Gorods</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
