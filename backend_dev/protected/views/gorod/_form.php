<div class="alert alert-info glyphicon-info-sign">Поля, отмеченные <span class="required">*</span> обязательны к
    заполнению.
</div>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'gorod-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'well'),
    'focus' => array($model, 'nazvanie'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'nazvanie', array('class' => 'span5', 'maxlength' => 50)); ?>

<?php echo $form->textFieldRow($model, 'lat', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'lon', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'nazvanie_r', array('class' => 'span5', 'maxlength' => 50)); ?>

<?php echo $form->textFieldRow($model, 'nazvanie_pr', array('class' => 'span5', 'maxlength' => 50)); ?>

<hr/>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'icon' => 'check white',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
)); ?>

<?php $this->endWidget(); ?>
