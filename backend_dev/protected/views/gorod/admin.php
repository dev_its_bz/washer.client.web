<?php
$this->breadcrumbs = array(
    'Города'
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gorod-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
    $(document).ready(function() {
        $('#gorod-grid tbody tr').live('dblclick', function() {
            var id = $.fn.yiiGridView.getKey(
                    'gorod-grid',
                    $(this).prevAll().length
                    );
            document.location.href = '/gorod/' + id;
        });
    });
</script>
<div class="page-header">
    <h1>Города
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons' => array(
                array('buttonType' => 'link', 'type' => 'success', 'icon' => 'plus white', 'url' => 'create'),
                array('buttonType' => 'link', 'type' => 'primary', 'icon' => 'search white', 'url' => '#', 'htmlOptions' => array('class' => 'search-button')),
        )));
        ?>
    </h1>
</div>

<div class="search-form" style="display:none">
    <div class="alert alert-info glyphicon-info-sign">
        В фильтрах  можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
        или равенства <b>=</b>), указав их перед выражением.
    </div>
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'gorod-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('name' => 'id', 'htmlOptions' => array('style' => 'width:20px; text-align:center')),
        'nazvanie',
        array('name' => 'lat', 'htmlOptions' => array('style' => 'width:100px;')),
        array('name' => 'lon', 'htmlOptions' => array('style' => 'width:100px;')),
        array('name' => 'object_count', 'htmlOptions' => array('style' => 'width:50px;')),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete}',
            'buttons' => array
            (
                'view' => array
                (
                    'url' => 'Yii::app()->createUrl("/gorod/view", array("id"=>$data->id))',
                ),
                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("delete", array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn-column',
                    ),
                ),
            ),
        ),
    ),
));
?>
