<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lat')); ?>:</b>
	<?php echo CHtml::encode($data->lat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lon')); ?>:</b>
	<?php echo CHtml::encode($data->lon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nazvanie_r')); ?>:</b>
	<?php echo CHtml::encode($data->nazvanie_r); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nazvanie_pr')); ?>:</b>
	<?php echo CHtml::encode($data->nazvanie_pr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nazvanie')); ?>:</b>
	<?php echo CHtml::encode($data->nazvanie); ?>
	<br />


</div>