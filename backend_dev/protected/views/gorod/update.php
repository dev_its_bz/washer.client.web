<?php
$this->breadcrumbs=array(
	'Gorods'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Gorod','url'=>array('index')),
	array('label'=>'Create Gorod','url'=>array('create')),
	array('label'=>'View Gorod','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Gorod','url'=>array('admin')),
);
?>

<h1>Update Gorod <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>