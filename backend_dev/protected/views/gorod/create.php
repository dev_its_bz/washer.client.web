<?php
$this->breadcrumbs = array(
    'Города' => array('admin'),
    'Новый город',
);
?>

    <div class="page-header">
        <h1>Новый город</h1>
    </div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>