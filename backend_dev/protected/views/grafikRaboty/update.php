<?php
$this->breadcrumbs=array(
	'Grafik Raboties'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GrafikRaboty','url'=>array('index')),
	array('label'=>'Create GrafikRaboty','url'=>array('create')),
	array('label'=>'View GrafikRaboty','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage GrafikRaboty','url'=>array('admin')),
);
?>

<h1>Update GrafikRaboty <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>