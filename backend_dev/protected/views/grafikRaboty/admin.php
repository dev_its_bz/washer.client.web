<?php
$this->breadcrumbs = array(
    'Графики работы'
);

Yii::app()->clientScript->registerScript('search', "
$(document).ready(function() {
    $('#grafik-raboty-grid tbody tr').live('dblclick', function() {
        var id = $.fn.yiiGridView.getKey(
                'grafik-raboty-grid',
                $(this).prevAll().length
                );
        document.location.href = '/grafikRaboty/' + id;
    });
});
");
if (!isset($AccessScript)) $AccessScript = Yii::app()->params[Yii::app()->user->role];
Yii::app()->clientScript->registerScript('CheckAccess', $AccessScript);
?>
<div class="page-header">
    <h1>Графики работы
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'size' => 'mini',
                'buttons' => array(
                    array(
                        'buttonType' => 'link', 'htmlOptions' => array('class' => 'owner_button'), 'type' => 'success', 'icon' => 'glyphicon glyphicon-plus white', 'url' => '/grafikRaboty/create/' . $model->kontragent_id,),
                )
            )
        );
        ?>
    </h1>
</div>

<div class="search-form" style="display:none">
    <div class="alert alert-info glyphicon-info-sign">
        В фильтрах можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
            &lt;&gt;</b>
        или равенства <b>=</b>), указав их перед выражением.
    </div>
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'grafik-raboty-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('name' => 'id', 'htmlOptions' => array('style' => 'width:20px; text-align:center')),
        'nazvanie',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete}',
            'buttons' => array
            (
                'view' => array
                (
                    'url' => 'Yii::app()->createUrl("/grafikRaboty/view", array("id"=>$data->id))',

                ),
                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("/grafikRaboty/delete", array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn-column',
                        'class' => 'owner_button'
                    ),
                ),
            ),
        ),
    ),
));
?>
