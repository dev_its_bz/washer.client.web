<?php
$this->breadcrumbs = array(
    'Контрагенты' => array('/kontragent/admin/'),
    $model->kontragent->nazvanie => array('/kontragent/view/' . $model->kontragent_id),
    'Графики работы' => array('/kontragent/view/' . $model->kontragent_id."?tab=4"),
    'Новый',
);
?>

<div class="page-header">
    <h1>Новый график работы</h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>