<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nazvanie',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'kontragent_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pn_start',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pn_stop',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'vt_start',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'vt_stop',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'sr_start',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'sr_stop',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'cht_start',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'cht_stop',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pt_start',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pt_stop',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'sb_start',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'sb_stop',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'vs_start',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'vs_stop',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'data_sozdaniya',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'avtor_zapisi_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'data_zapisi',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Найти',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
