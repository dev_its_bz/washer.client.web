<?php
$this->breadcrumbs = array(
    'Контрагенты' => array('/kontragent/admin/'),
    $model->kontragent->nazvanie => array('/kontragent/view/' . $model->kontragent_id),
    'Графики работы' => array('/kontragent/view/' . $model->kontragent_id."?tab=4"),
    $model->nazvanie,
);
?>

<div class="page-header">
    <h1>График работы &laquo;<?php echo $model->nazvanie; ?>&raquo;</h1>
</div>

<?php
$this->widget('TbEditableDetailView', array(
    'data' => $model,
    'url' => $this->createUrl('/grafikRaboty/UpdateZnachenie'),
    'attributes' => array(
        'nazvanie',
        array(
            'name' => 'pn_start',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'pn_stop',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'vt_start',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'vt_stop',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'sr_start',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'sr_stop',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'cht_start',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'cht_stop',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'pt_start',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'pt_stop',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'sb_start',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'sb_stop',
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'vs_start', 
            'editable' => array(
                'type' => 'time'
            )
        ),
        array(
            'name' => 'vs_stop',
            'editable' => array(
                'type' => 'time'
            )
        ),
    ),
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'primary',
    'icon' => 'arrow-left white',
    'label' => 'Назад',
    'htmlOptions' => array(
        'onClick' => "window.location.href = '/kontragent/view/{$model->kontragent_id}?tab=4'",
    )
));
?>
