<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nazvanie')); ?>:</b>
	<?php echo CHtml::encode($data->nazvanie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kontragent_id')); ?>:</b>
	<?php echo CHtml::encode($data->kontragent_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pn_start')); ?>:</b>
	<?php echo CHtml::encode($data->pn_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pn_stop')); ?>:</b>
	<?php echo CHtml::encode($data->pn_stop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vt_start')); ?>:</b>
	<?php echo CHtml::encode($data->vt_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vt_stop')); ?>:</b>
	<?php echo CHtml::encode($data->vt_stop); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('sr_start')); ?>:</b>
	<?php echo CHtml::encode($data->sr_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sr_stop')); ?>:</b>
	<?php echo CHtml::encode($data->sr_stop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cht_start')); ?>:</b>
	<?php echo CHtml::encode($data->cht_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cht_stop')); ?>:</b>
	<?php echo CHtml::encode($data->cht_stop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_start')); ?>:</b>
	<?php echo CHtml::encode($data->pt_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pt_stop')); ?>:</b>
	<?php echo CHtml::encode($data->pt_stop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sb_start')); ?>:</b>
	<?php echo CHtml::encode($data->sb_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sb_stop')); ?>:</b>
	<?php echo CHtml::encode($data->sb_stop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vs_start')); ?>:</b>
	<?php echo CHtml::encode($data->vs_start); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vs_stop')); ?>:</b>
	<?php echo CHtml::encode($data->vs_stop); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_sozdaniya')); ?>:</b>
	<?php echo CHtml::encode($data->data_sozdaniya); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('avtor_zapisi_id')); ?>:</b>
	<?php echo CHtml::encode($data->avtor_zapisi_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_zapisi')); ?>:</b>
	<?php echo CHtml::encode($data->data_zapisi); ?>
	<br />

	*/ ?>

</div>