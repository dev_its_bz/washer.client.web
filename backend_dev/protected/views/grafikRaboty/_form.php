<div class="alert alert-info glyphicon-info-sign">
    Поля, отмеченные <span class="required">*</span> обязательны к заполнению.<br/>
    Если Вы хотите сделать день выходным, то оставьте начало и конец рабочего дня пустым.
</div>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'grafik-raboty-form',
    'htmlOptions' => array('class' => 'well'),
    'focus'=>array($model,'nazvanie'),
    'enableAjaxValidation' => false,
));
?>

<?php echo $form->errorSummary($model); ?>

<div class="row2">
    <?php echo $form->labelEx($model, 'nazvanie'); ?>
    <?php echo $form->textField($model, 'nazvanie', array('class' => 'span5', 'maxlength' => 100)); ?>
    <?php echo $form->error($model, 'nazvanie'); ?>
</div>

<div class="row2">
    <?php echo $form->labelEx($model, 'pn_start'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'pn_start',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'pn_stop'); ?>
</div>
<div class="row2">
    <?php echo $form->labelEx($model, 'pn_stop'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'pn_stop',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'pn_stop'); ?>
</div>

<div class="row2">
    <?php echo $form->labelEx($model, 'vt_start'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'vt_start',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'vt_start'); ?>
</div>
<div class="row2">
    <?php echo $form->labelEx($model, 'vt_stop'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'vt_stop',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'vt_stop'); ?>
</div>

<div class="row2">
    <?php echo $form->labelEx($model, 'sr_start'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'sr_start',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'sr_start'); ?>
</div>
<div class="row2">
    <?php echo $form->labelEx($model, 'sr_stop'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'sr_stop',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'sr_stop'); ?>
</div>

<div class="row2">
    <?php echo $form->labelEx($model, 'cht_start'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'cht_start',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'cht_start'); ?>
</div>
<div class="row2">
    <?php echo $form->labelEx($model, 'cht_stop'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'cht_stop',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'cht_stop'); ?>
</div>

<div class="row2">
    <?php echo $form->labelEx($model, 'pt_start'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'pt_start',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'pt_start'); ?>
</div>
<div class="row2">
    <?php echo $form->labelEx($model, 'pt_stop'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'pt_stop',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'pt_stop'); ?>
</div>

<div class="row2">
    <?php echo $form->labelEx($model, 'sb_start'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'sb_start',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'sb_start'); ?>
</div>
<div class="row2">
    <?php echo $form->labelEx($model, 'sb_stop'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'sb_stop',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'sb_stop'); ?>
</div>

<div class="row2">
    <?php echo $form->labelEx($model, 'vs_start'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'vs_start',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'vs_start'); ?>
</div>

<div class="row2">
    <?php echo $form->labelEx($model, 'vs_stop'); ?>
    <?php
    $this->widget(
        'ext.timepicker.timepicker', array(
            'name' => 'vs_stop',
            'model' => $model,
            'options' => array(
                'timeFormat' => 'hh:mm',
                'hourGrid' => 4,
                'minuteGrid' => 10,
                'timeOnly' => true,
            ),)
    );
    ?>
    <?php echo $form->error($model, 'vs_stop'); ?>
</div>

<hr/>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'icon' => 'check white',
    'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
));
?>

<?
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'danger',
    'icon' => 'remove white',
    'label' => 'Отмена',
    'htmlOptions' => array(
        'onClick' => "window.location.href = '/kontragent/view/" . @$_GET['id'] . "?tab=4'",
    )
));
?>

<?php $this->endWidget(); ?>
