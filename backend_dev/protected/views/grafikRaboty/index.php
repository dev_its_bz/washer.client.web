<?php
$this->breadcrumbs = array(
    'Grafik Raboties',
);
?>


<h1>Grafik Raboties</h1>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'grafik-raboty-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'nazvanie',
        /* 	'kontragent_id',
         * 		'id',
          'pn_start',
          'pn_stop',
          'vt_start',

          'vt_stop',
          'sr_start',
          'sr_stop',
          'cht_start',
          'cht_stop',
          'pt_start',
          'pt_stop',
          'sb_start',
          'sb_stop',
          'vs_start',
          'vs_stop',
          'data_sozdaniya',
          'avtor_zapisi_id',
          'data_zapisi',
         */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view},{edit},{delete}',
            'buttons' => array(
                'view' => array(
                    'lable' => 'Просмотр',
                    'url' => 'Yii::app()->createUrl("GrafikRaboty/view", array("id"=>$data->id))',
                ),
                'edit' => array(
                    'lable' => 'Редактировать',
                    'icon' => 'pencil',
                    'url' => 'Yii::app()->createUrl("GrafikRaboty/Update", array("id"=>$data->id))',
                ),
                'delete' => array(
                    'lable' => 'Удалить',
                    'url' => 'Yii::app()->createUrl("GrafikRaboty/delete", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
));
?>
<div class="form-actions">

    <?php echo CHtml::button('Добавить График', array('submit' => Yii::app()->createUrl("GrafikRaboty/create/" . $kontragent_id), 'class' => 'btn btn-success')); ?>
</div>
