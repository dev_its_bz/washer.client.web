<?php
$this->breadcrumbs = array(
    'Опции' => array('/opcii/admin'),
    'Новая',
);
?>

<div class="page-header">
    <h1>Новая опция</h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>