<?php
$this->breadcrumbs=array(
	'Opciis',
);

$this->menu=array(
	array('label'=>'Create Opcii','url'=>array('create')),
	array('label'=>'Manage Opcii','url'=>array('admin')),
);
?>

<h1>Opciis</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
