<?php
$this->breadcrumbs = array(
    'Опции' => array('/opcii/admin'),
    $model->nazvanie,
);
?>

<div class="page-header">
    <h1>Опция &laquo;<?php echo $model->nazvanie; ?>&raquo;</h1>
</div>

<?php
$this->widget('bootstrap.widgets.TbEditableDetailView', array(
    'data' => $model,
    'url' => $this->createUrl('/opcii/UpdateZnachenie'),
    'attributes' => array(
        'id',
        'nazvanie',
        'opisanie'
    ),
));
?>
