<?php
$this->breadcrumbs=array(
	'Opciis'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Opcii','url'=>array('index')),
	array('label'=>'Create Opcii','url'=>array('create')),
	array('label'=>'View Opcii','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Opcii','url'=>array('admin')),
);
?>

<h1>Update Opcii <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>