<script>
    $("#forgot").live("click", function () {
        $('#container').arcticmodal();
        // $('#restoreModal2').arcticmodal();
    });
    $("#getPass").live("click", function () {
        if ($('#username2').val()) {
            $.ajax({
                    type: "POST",
                    url: 'sendPass',
                    data: {username: $('#username2').val(), recaptcha_challenge_field: $("input[name='recaptcha_challenge_field'").val(), recaptcha_response_field: $("input[name='recaptcha_response_field'").val()},
                    success: function (data) {
                        if (data != 'ERROR') {

                            $('#restoreModal').hide();
                            anichange('#restoreModal2');
                        } else
                            alert('Неверный код');

                    }, dataType: 'json'}
            );
        } else
            alert('Введите телефон');
    });

    $("#ChangePass").live("click", function () {
            $.ajax({
                    type: "POST",
                    url: 'CheckCode',
                    data: {username: $('#username2').val(), code: $('#change_code').val()},
                    success: function (data) {
                        if (data == 'NO_ERROR') {
                            $('#restoreModal2').hide();
                            anichange('#restoreModal3');
                        } else
                            alert('Неверный код');
                    }, dataType: 'json'}
            );
        }
    );

    $("#New_pass_confirm").live("click", function () {
            $.ajax({
                    type: "POST",
                    url: 'New_pass',
                    data: {username: $('#username2').val(), new_pass: $('#new_pass').val()},
                    success: function (data) {
                        $.arcticmodal('close');
                    }, dataType: 'json'}
            );
        }
    );

</script>
<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'login-form',
    'type' => 'vertical',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
));
?>

<?php
echo $form->maskedTextFieldRow($model, 'username', array('id' => 'username', 'placeholder' => 'X', 'mask' => '+9-999-999-99-99', 'htmlOptions' => array('placeholder' => 'Номер телефона')), array('labelOptions' => array('label' => false)));
echo $form->passwordFieldRow($model, 'password', array('placeholder' => 'Пароль'), array('labelOptions' => array('label' => false)));
?>
<input type="hidden" name="tabLogin" value="1"/>

<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'size' => 'large',
    'type' => 'success',
    'htmlOptions' => array('style' => 'width:85%;'),
    'label' => 'ВОЙТИ',
));
?>
<div class="forgot" id="forgot" style="text-align: left; padding-left: 25px;">
    <a href="#">Забыли пароль?</a>
</div>
<?php $this->endWidget(); ?>


<div style="display: none;">
    <div id="container">
        <?
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'login-form',
            'type' => 'vertical',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>
        <div class="box-modal well form" id="restoreModal">
            <h4>Восстановление пароля</h4>
            <?php
            $model->unsetAttributes();
            echo $form->maskedTextFieldRow(new Polzovatel(), 'login', array('placeholder' => 'X', 'mask' => '+9-999-999-99-99', 'htmlOptions' => array('id' => 'username2', 'placeholder' => 'Номер телефона')), array('labelOptions' => array('label' => false)));
            echo "<br/>";
            //require('recaptchalib.php');
            $publickey = "6LfNWvESAAAAAH9Iy02tHbOtG7M-I0GzctMzn8UV";
            echo recaptcha_get_html($publickey);
            $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => "success", 'size' => 'large', 'label' => 'ПОЛУЧИТЬ КОД СМЕНЫ ПАРОЛЯ', 'htmlOptions' => array('id' => 'getPass', 'style' => 'width:85%; margin-bottom:-5px'))
            );
            ?>
        </div>
        <?php $this->endWidget(); ?>


        <div class="box-modal well form" id="restoreModal2" style="display: none">
            <?php
            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id' => 'change_code_form',
                'type' => 'vertical',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            echo $form->textFieldRow(new Polzovatel(), 'login', array('placeholder' => 'КОД', 'id' => 'change_code', 'maxlength' => 30), array('labelOptions' => array('label' => false)));

            echo "<br/>";
            $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => "success", 'size' => 'large', 'label' => 'СМЕНИТЬ ПАРОЛЬ', 'htmlOptions' => array('id' => 'ChangePass', 'style' => 'width:85%; margin-bottom:-5px')));
            ?>
            <?php $this->endWidget(); ?>
        </div>

        <div class="box-modal well form" id="restoreModal3" style="display: none">
            <?php
            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'id' => 'change_pass_form',
                'type' => 'vertical',
                'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            echo $form->passwordFieldRow($model, 'password', array('placeholder' => 'Пароль', 'id' => 'new_pass',), array('labelOptions' => array('label' => false)));
            echo "<br/>";
            $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => "success", 'size' => 'large', 'label' => 'ПОДТВЕРДИТЬ', 'htmlOptions' => array('id' => 'New_pass_confirm', 'style' => 'width:85%; margin-bottom:-5px')));
            ?>
            <?php $this->endWidget(); ?>
        </div>

    </div>
</div>

<script>
    function anichange(objName) {
        if ($(objName).css('display') == 'none') {
            $(objName).animate({height: 'show'}, 400);
        } else {
            $(objName).animate({height: 'hide'}, 200);
        }
    }
</script>