<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>
<div id="gMap" style="height: 50%">
    <iframe style="width:100%; height:100%; border:none" id="map"
            src="<?php echo $this->createUrl('site/showMap'); ?>"></iframe>
</div>
<?php
$url = Yii::app()->createUrl('site/showMap');
//echo CHtml::dropDownList('city_id', '', $gorod, array('prompt' => '(Выбирите город)',));
?>
<script>
    $(document).ready(function () {
        $('#city_id').change(function () {
            $('#map').attr('src', "<?php echo $url ?>/" + this.value);
        });

        $("select[name='object[columGorod]']").live('change', function () {
            $('#map').attr('src', "<?php echo $url ?>/" + this.value);
        });
    });
</script>
<div style="height: 50%; overflow-y: scroll">
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'object-grid',
        'dataProvider' => $model->search(),
        'enablePagination' => true,
        'type' => 'striped condensed',
        'template' => '{items}',
        'htmlOptions' => array('style' => 'padding-top:0px;'),
        'filter' => $model,
        'hideHeader' => true,
        'columns' => array(
            array(
                'type' => 'raw',
                'name' => 'columGorod',
                'filter' => CHtml::listData(Gorod::model()->findAll(), 'id', 'nazvanie'),
                'value' => '$data->gorod->nazvanie',
            ),
            'nazvanie',
            'adres',
            'telefon',
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{zapis}',
                'buttons' => array(
                    'zapis' => array
                    (
                        'label' => 'Записаться',
                        'icon' => 'icon-list',
                        'url' => '"/zapisUsluga/fastWash?id=".$data->id',
                        'options' => array(
                            'class' => 'btn-column',
                        ),
                        'visible' => '$data->kontragenty->status_id == 2',
                    ),
                ),
            ),
        ),
    ));
    ?>
</div>