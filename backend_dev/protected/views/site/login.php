<style type="text/css">
    @font-face {
        font-family: 'Lobster';
        src: url(<?php echo Yii::app()->theme->baseUrl; ?>/font/lobster.ttf);
        font-weight:400;
    }
</style>

<?php
Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/site/assets/modal/jquery.arcticmodal-0.3.min.js'));
Yii::app()->clientScript->registerCssFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/site/assets/modal/jquery.arcticmodal-0.3.css'));

/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm */
$this->layout = "//layouts/login_layout";
$this->pageTitle = Yii::app()->name . ' - Авторизация';
?>
<div>
    <div style="margin-left: 200px; margin-right: auto; width: 400px; float: left; margin-top: 100px;">
        <a href="/">
            <img src="/images/washer_512.png" style="width: 128px; margin-bottom: 40px;">
        </a>

        <p style="font-family: 'Lobster'; font-size: 42px; text-transform: uppercase; text-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
           text-align: left; color: white; font-weight: 700; line-height: 1.2;">
            <strong>Помой</strong> меня!
        </p>

        <p style="font: 20px 'Source Sans Pro', sans-serif; font-weight: 300; max-width: 400px; margin-top: 30px; margin-bottom: 70px; text-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
           text-align: left; color: white;">
            Уникальный общедоступный сервис записи на автомойки.<br/>Мы объединяем автовладельцев и автомойки в единую счастливую систему.
        </p>

        <p>
            <a href="https://play.google.com/store/apps/details?id=bz.its.washer.client">
                <img alt="Загрузить на Apple Store" style="height: 48px;"
                     src="/images/ios_app_button.png"/>
            </a>
            <a href="https://play.google.com/store/apps/details?id=bz.its.washer.client">
                <img alt="Загрузить на Google Play" style="height: 48px"
                     src="/images/android_app_button.png"/>
            </a>
        </p>
    </div>
    <div style="margin-left: auto; margin-right: 200px; width: 400px; float: right">
        <div class="form">
            <?
            require('recaptchalib.php');
            $this->widget(
                    'bootstrap.widgets.TbTabs', array(
                'type' => 'tabs', // 'tabs' or 'pills'
                'tabs' => array(
                    array(
                        'label' => 'ЗАРЕГИСТРИРОВАТЬСЯ',
                        'content' => $this->renderPartial('registrationForm', array('model' => new Polzovatel()), true),
                        'active' => @$_POST['tabReg'] ? true : @$_POST['tabLogin'] ? false : true
                    ),
                    array(
                        'label' => 'ВОЙТИ',
                        'active' => @$_POST['tabLogin'] ? true : false,
                        'content' => $this->renderPartial('loginForm', array('model' => $model), true),
                    ),
                ),
                    )
            );
            ?>
        </div>
        <!-- form -->
    </div>
</div>
