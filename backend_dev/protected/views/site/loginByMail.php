<?php
  $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'login-form-mail',
            'type' => 'vertical',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>

        <?php echo $form->textFieldRow($model, 'email', array('placeholder' => 'Email пользователя'), array('labelOptions'=>array('label'=>false))); ?>

        <?php
        echo $form->passwordFieldRow($model, 'password', array('placeholder' => 'Пароль'), array('labelOptions'=>array('label'=>false)));
        ?>

        <a href="#" class="forgot">Потерялся пароль?</a>

        <div class="remember">
<?php echo $form->checkBoxRow($model, 'rememberMe'); ?>
        </div>

        <div>
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'success',
                'label' => 'Войти, используя email и пароль',
            ));
            ?>      
        </div>
            <?php $this->endWidget(); ?>