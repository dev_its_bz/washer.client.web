<?
$this->breadcrumbs = array(
    'О компании'
);
?>
<br/>
<div class="content">
    <div class="headline">Быстрая запись на автомойку!</div>
    <div class="leftColumn">
        <p><strong>Помой-Меня.РФ</strong>&nbsp;— это уникальный сервис экспресс записи на автомойку, который позволяет
            каждому автовладельцу найти и записаться на свободную автомойку, максимально экономя время на её поиски.</p>
        <br>

        <p>Добавляйте неограниченное количество своих автомобилей.</p>

        <p>Выбирайте "ближайшую" или скорейшую автомойку, а может быть
            "ближайшая"+"скорейшая"+"лучшая"? Можно и не такое!</p>

        <p>Читайте и оставляйте отзывы о качестве работ автомоек.
            Выберите (не обязательно) список необходимых Вам услуг и сразу узнаете стомость!</p>

        <p><strong>И всё это совешенно бесплатно!</strong></p>
        <br><br></div>
    <!-- image -->
    <div class="rightColumn">
        <p><strong>Звоните</strong>: <br> <span class="address_data">+7(4722) 37-31-25</span></p>

        <p>По общим вопросам:<br> <span class="address_data"><a
                    href="mailto:info@washmeup.com">info@washmeup.com</a></span></p>

        <p>По вопросам поддержки:<br> <span class="address_data"><a
                    href="mailto:support@washmeup.com">support@washmeup.com</a></span></p>
        </p>
        <p>По вопросам сотрудничества:<br> <span class="address_data"><a
                    href="mailto:sales@washmeup.com">sales@washmeup.com</a></span></p>
        </p>
    </div>
    <div class="headline"><hr/></div>
    <div style="text-align: center">
        <a href="/site/page?view=dummy">
            <img alt="Загрузить на AppStore" src="/images/ios_app_button.png" style="width: 30%">
        </a>
        <a href="https://play.google.com/store/apps/details?id=bz.its.washer.client">
            <img alt="Загрузить на Google Play" src="/images/android_app_button.png" style="width: 30%">
        </a>
        <a href="/site/page?view=dummy">
            <img alt="Загрузить на Windows Store" src="/images/windows_app_button.png" style="width: 30%">
        </a>
    </div>
</div>