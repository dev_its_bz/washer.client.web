<?
$this->breadcrumbs = array(
    'Ошибка 404'
);
?>
<div class="span12">
    <!--PAGE CONTENT BEGINS-->

    <div class="error-container">
        <div class="well">
            <h1 class="grey lighter smaller">
                <span class="blue bigger-125">
                <i class="icon-sitemap"></i>
                404
                </span>Страница не найдена
            </h1>
            <hr>
            <h3 class="lighter smaller">Страница, которую Вы ищете куда-то потерялась..</h3>

            <div>
                <form class="form-search">
                    <span class="input-icon">
                        <i class="icon-search"></i>
                        <input type="text" class="input-medium search-query"
                               placeholder="Поискать на сайте..">
                    </span>
                    <button class="btn btn-small" onclick="return false;">Go!</button>
                </form>

                <div class="space"></div>
                <h4 class="smaller">Попробуйте следующее:</h4>

                <ul class="unstyled spaced inline bigger-110">
                    <li>
                        <i class="icon-hand-right blue"></i>
                        Проверьте введённый адрес страницы
                    </li>

                    <li>
                        <i class="icon-hand-right blue"></i>
                        Прочитайте справку
                    </li>

                    <li>
                        <i class="icon-hand-right blue"></i>
                        Сообщите об ошибке нам
                    </li>
                </ul>
            </div>

            <hr>
            <div class="space"></div>

            <div class="row-fluid">
                <div class="center">
                    <a href="javascript:history.back(1);" class="btn btn-grey">
                        <i class="icon-arrow-left"></i>
                        Назад
                    </a>

                    <a href="/" class="btn btn-primary">
                        <i class="icon-home"></i>
                        На главную
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!--PAGE CONTENT ENDS-->
</div>