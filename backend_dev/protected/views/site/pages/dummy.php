<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name . ' - Страница в разработке';
$this->breadcrumbs = array(
    'В разработке',
);
?>
<div class="page-header">
    <h1>Страница в разработке</h1>
</div>

<p>
    Эта страница ещё не готова к показу. Но мы стараемся её подготовить. ;-)
</p>
