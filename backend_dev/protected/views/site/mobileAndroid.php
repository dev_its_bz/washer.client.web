<?php
/* @var $this SiteController */
/**
 * Created by PhpStorm.
 * User: zolotarev
 * Date: 06.04.14
 * Time: 21:46
 */
$this->pageTitle = Yii::app()->name . " - Android";
$this->layout = "//layouts/mobile_layout";
?>
<a href="https://play.google.com/store/apps/details?id=bz.its.washer.client">
    <img alt="Загрузить на Google Play" style="width: 85%;"
         src="/images/android_app_button.png"/>
</a>