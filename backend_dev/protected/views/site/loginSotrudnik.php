<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'login-form-mail',
    'type' => 'vertical',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'clientOptions'=>array(
        'hideErrorMessage' => true
    ),
    'focus' => 'input[type="text"]:first',
));
?>

<?
$this->widget('CMaskedTextField', array(
    "id" => "login",
    'name' => "login",
    'model' => new Polzovatel(),
    'attribute' => 'login',
    'value' => '',
    'mask' => '+9-999-999-99-99',
    'htmlOptions' => array('placeholder' => '+X-XXX-XXX-XX-XX')
));

?>

<?php
echo $form->passwordFieldRow($model, 'password', array('placeholder' => 'Пароль'));
?>

<a href="#" id="forgot" class="forgot">Забыли пароль?</a>


<div class="remember">
    <?php echo $form->checkBoxRow($model, 'rememberMe'); ?>
</div>


<div>
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'success',
        'label' => 'Войти в систему',
    ));
    ?>
</div>
<?php $this->endWidget(); ?>
<br/>
Хотите участвовать?
<a href="/polzovatel/Registration?asKontragent">Присоединяйтесь!</a>