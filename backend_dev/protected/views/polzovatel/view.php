<?php
$this->breadcrumbs = array(
    'Контрагенты' => '/kontragent/',
    $model->kontragent->nazvanie => array('/kontragent/' . $model->kontragent->id),
    'Пользователи' => "/kontragent/{$model->kontragent->id}?tab=2",
    $model->fio
);
?>

<div class="page-header">
    <h1><?php
        echo "Пользователь &laquo;" . $model->fio . "&raquo; ";
        ?>
    </h1>
</div>

<?php
$this->widget('TbEditableDetailView', array(
    'data' => $model,
    'url' => $this->createUrl('/polzovatel/UpdateZnachenie'),
    'attributes' => array(
        'id',
        'login',
        array(
            'name' => 'parol',
            'editable' => array(
                'type' => 'password',
            )
        ),
        'fio',
        'dolzhnost',
        'telefon',
        'email',
        array(
            'name' => 'gruppa_id',
            'editable' => array(
                'type' => 'select',
                'source' => CHtml::listData(PolzovatelGruppa::model()->findAll("id=1 OR id >= " . Yii::app()->user->gruppa_id), 'id', 'nazvanie')
            )
        ),
        array(
            'name' => 'status_id',
            'visible' => ($model->id != Yii::app()->user->id),
            'editable' => array(
                'type' => 'select',
                'source' => CHtml::listData(PolzovatelStatus::model()->findAll(), 'id', 'nazvanie'),
            )
        ),
    ),
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'primary',
    'icon' => 'arrow-left white',
    'label' => 'Назад',
    'htmlOptions' => array(
        'onClick' => "window.location.href = '/kontragent/view/{$model->kontragent_id}?tab=2'",
    )
));
?>
