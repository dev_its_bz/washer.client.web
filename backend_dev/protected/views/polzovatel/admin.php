<?php
$this->breadcrumbs = array(
    'Пользователи'
);

Yii::app()->clientScript->registerScript('search', "
$(document).ready(function () {
            $('#polzovatel-grid tbody tr').live('dblclick', function () {
                var id = $.fn.yiiGridView.getKey(
                    'polzovatel-grid',
                    $(this).prevAll().length
                );
                document.location.href = '/polzovatel/view/' + id;
            });
        });");

Yii::app()->clientScript->registerScript('CheckAcess', $AcessScript);
?>

    <div class="page-header">
        <h1>Пользователи
            <?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
                    'size' => 'mini',
                    'buttons' => array(
                        $model->object_id != null ?
                            array('buttonType' => 'link', 'htmlOptions' => array('class' => 'owner_button'), 'type' => 'success', 'icon' => 'user white', 'url' => '/polzovatel/linkToObject/' . $model->object_id) :
                            array('buttonType' => 'link', 'htmlOptions' => array('class' => 'owner_button'), 'type' => 'success', 'icon' => 'plus white', 'url' => '/polzovatel/create/' . $model->kontragent_id),
                    )
                )
            );
            ?>
        </h1>
    </div>

    <div class="search-form" style="display:none">
        <div class="alert alert-info glyphicon-info-sign">
            В фильтрах можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
            или равенства <b>=</b>), указав их перед выражением.
        </div>
        <?php
        $this->renderPartial('_search', array(
            'model' => $model,
        ));
        ?>
    </div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'polzovatel-grid',
    'dataProvider' => $dp,
    'filter' => $model,
    'columns' => array(
        array('name' => 'id', 'htmlOptions' => array('style' => 'width:20px; text-align:center')),
        'login',
        'parol',
        'fio',
        'dolzhnost',
        array(
            'name' => 'gruppa.nazvanie',
            'header' => 'Группа прав',
            'filter' => CHtml::listData(PolzovatelGruppa::model()->findAll(), "id", "nazvanie"),
        ),
        'telefon',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete}',
            'buttons' => array
            (
                'view' => array
                (
                    'url' => 'Yii::app()->createUrl("/polzovatel/view", array("id"=>$data->id))',
                ),
                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("/polzovatel/delete", array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn-column',
                        'class' => 'owner_button'
                    ),
                ),
            ),
        ),
    ),
));
?>