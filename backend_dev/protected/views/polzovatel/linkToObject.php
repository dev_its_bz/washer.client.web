<?php
$kontragentModel = Kontragent::model()->find('id=:id', array('id' => $kontragent_id));
$objectModel = Object::model()->find('id=:id', array('id' => $object_id));

$this->breadcrumbs = array(
    'Контрагенты' => array('/kontragent/admin/'),
    $kontragentModel->nazvanie => array('/kontragent/view/' . $kontragentModel->id),
    'Объекты' => array('/kontragent/view/' . $kontragentModel->id . "?tab=3"),
    $objectModel->nazvanie => array('/object/view/' . $object_id),
    'Привязка пользователей'
);
?>


    <div class="page-header">
        <h1>
            <?php
            echo "Пользователи &laquo;" . $objectModel->nazvanie . "&raquo; ";
            ?>
        </h1>
    </div>

<?php

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'polzovateli-objects-form',
    'htmlOptions' => array('class' => 'well'),
    'action' => "/polzovatel/SaveUsersAndAdd?idK=$kontragent_id&id=$object_id",
    'enableAjaxValidation' => false,
));

$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'polzovatel-grid',
    'dataProvider' => Polzovatel::model()->getForCheckBox($object_id, $kontragent_id),
    //'filter'=>$model,

    'columns' => array(
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'model_to_insert',
            'checked' => '$data->inObj',
            'value' => '$data->id',
            'header' => 'Добавить',
            'selectableRows' => 2,
            'name' => 'model_to_insert',
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'model_temp',
            'checked' => '$data->inObj',
            'value' => '$data->id',
            'htmlOptions' => array('style' => 'display:none'),
            'headerHtmlOptions' => array('style' => 'display:none'),
            'selectableRows' => 2,
            'id' => 'model_temp',
        ),
        'id',
        'login',
        'parol',
        'fio',
        'dolzhnost',
        //   'objects.id',
        /*
        'email',
        'gruppa_id',

        'status_id',
        'data_sozdaniya',
        'avtor_zapisi_id',
        'data_zapisi',
        */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
        ),
    ),
)); ?>
    <hr/>

<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'icon' => 'check white',
    'type' => 'primary',
    'label' => 'Сохранить',
));
?>

<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'danger',
    'icon' => 'remove white',
    'label' => 'Отмена',
    'htmlOptions' => array(
        'onClick' => "window.location.href = '/object/view/$object_id?tab=3'",
    )
));
?>
<?php $this->endWidget(); ?>