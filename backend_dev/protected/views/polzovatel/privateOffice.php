<?php
$this->breadcrumbs = array(
    'Мои автомобили',
);
?>
    <div class="page-header">
        <h1> Мои автомобили </h1>
    </div>

<?
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'polzovatel-car-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'carModel.carMarka.nazvanie',
        'carModel.nazvanie',
        'gos_nomer',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{delete}',
            'buttons' => array
            (
                'update' => array
                (
                    'url' => 'Yii::app()->createUrl("/polzovatelCar/update", array("id"=>$data->id))',
                ),
                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("/polzovatel/deleteCar", array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn-column',
                        'class' => 'owner_button'
                    ),
                ),
            ),
        ),
    ),
));
?>

<?
/*
?>
<table id="main_table">
    <tr>
        <th>Марка</th>
        <th>Модель</th>
        <th>Гос. номер</th>
        <th>Фото</th>
        <th>Функции</th>
    </tr>

</table>
<br>
<a href="#" onclick="anichange('#divId');
        return false">Добавить авто</a>
<div id="divId" style="display: none">
    <?php
    echo CHtml::dropDownList('marka_id', null, $marka_list, array(
        'prompt' => 'Не выбранно',
        'ajax' => array(
            'type' => 'POST',
            'url' => CController::createUrl('Polzovatel/GetCars'),
            'data' => array('marka_id' => 'js:this.value'),
            'update' => '#cars_id',
        ),
        'onChange' => "js: $('#cars_id').show();"
    ));

    echo CHtml::dropDownList('cars_id', '', array(), array(
        'onChange' => 'js: $("#uslugi").empty();',
        'ajax' => array(
            'type' => 'POST',
            'data' => array('car_id' => 'js:this.value'),
            'url' => CController::createUrl('polzovatel/AddCar'),
            'dataType' => "json",
            'success' => "js: function(data){ 
            $('#main_table td').parent().remove();
            data.forEach(function(obj) {
            addTableRow(obj);            
            });
            anichange('#divId');
            }"

        )
    ));
    ?>
</div>

<script>


    var id = null;

    function addTableRow(data) {
        //   alert(data);
        var tds = '<tr id="' + data['id'] + '">';
        tds += '<td class="marka">' + data['marka'] + '</td>';
        tds += '<td class="model">' + data['nazvanie'] + '</td>';
        tds += '<td class="nomer"><a class="gos_nomer">' + data['gos_nomer'] + '</a></td>';
        tds += '<td class="photo">' + data['foto'] + '</td>';
        tds += '<td> <button class="delete" id="' + data['id'] + '">Удалить</button> </td>';
        tds += '</tr>';


        $('tbody', '#main_table').append(tds);


        $('#' + data['id'] + ' .gos_nomer').editable({
            type: 'text',
            mode: 'inline',
            title: 'Введите номер',
        });

        $('.gos_nomer').on('save', function (e, params) {
            //     alert($(this).parent().parent().attr('id'));
            $.post(
                'addNomer',
                {nomer: params.newValue, id: $(this).parent().parent().attr('id')},
                'json');
        });
    }


    $(document).ready(function () {
        //   $("#object-grid tr").unbind("click");
        var data = <?= json_encode($query) ?>;
        //   alert(data[0]['nazvanie']);
        // addTableRow(data[0]);
        data.forEach(function (obj) {
            addTableRow(obj);

        });

        $(".delete").click(function () {
            id_rec = $(this).attr('id');
            $.post(
                'deleteCar',
                {id: id_rec},
                function (data) {
                    $('#main_table #' + id_rec).remove();

                }, 'json');
        });

        $("#marka_id").prop("selectedIndex", -1);
        $('#cars_id').hide();
    });

    function anichange(objName) {
        if ($(objName).css('display') == 'none') {
            $(objName).animate({height: 'show'}, 400);
        } else {
            $(objName).animate({height: 'hide'}, 200);
        }
    }

</script>
*/
?>