<?php
/**
 * Created by PhpStorm.
 * User: zolotarev
 * Date: 10.04.14
 * Time: 1:14
 */
?>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/fuelux.wizard.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->theme->baseUrl; ?>/css/jquery.datetimepicker.css"/ >
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.datetimepicker.js"></script>
<script src="//api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
    <div class="widget-main">
        <div id="washWizard" class="row-fluid hide wizard" style="display: block;">
            <ul class="steps wizard-steps">
                <li data-target="#step1" class="active" style="min-width: 25%; max-width: 25%;">
                    <span class="step">1</span>
                    <span class="title">Выбор автомобиля</span>
                </li>
                <li data-target="#step2" style="min-width: 25%; max-width: 25%;">
                    <span class="step">2</span>
                    <span class="title">Выбор услуг</span>
                </li>
                <li data-target="#step3" style="min-width: 25%; max-width: 25%;">
                    <span class="step">3</span>
                    <span class="title">Выбор автомойки</span>
                </li>
                <li data-target="#step4" style="min-width: 25%; max-width: 25%;">
                    <span class="step">4</span>
                    <span class="title">Готово</span>
                </li>
            </ul>
            <div class="actions row-fluid">
                <hr>
            </div>
            <div class="actions row-fluid">
                <div class="span2">
                    <button type="button" class="btn btn-success btn-prev row-fluid">
                        <i class="icon-arrow-left"></i>Назад
                    </button>
                </div>
                <div class="span8">
                    <blockquote>
                        <p id="hintBody">Выберите из списка ниже автомобиль, который Вы хотите помыть</p>
                        <footer id="hintFooter">Управлять автомобилями можно в разделе <em>
                                <a
                                    href="/polzovatelCar/index">Мои автомобили
                                </a>
                            </em>
                        </footer>
                    </blockquote>
                </div>
                <div class="span2">
                    <button type="button" class="btn btn-success btn-next row-fluid" data-last="Готово">
                        Далее<i class="icon-arrow-right" style="padding-left: 5px;"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="step-content row-fluid position-relative">
            <div class="step-pane active" id="step1">
                <?
                list($polzovatelCar) = Yii::app()->createController("polzovatelCar");
                $polzovatelCarModel = new PolzovatelCar();
                $polzovatelCarModel->polzovatel_id = Yii::app()->user->id;
                $polzovatelCarModel->deleted = '0';
                $polzovatelCar->renderPartial('index', array('model' => $polzovatelCarModel, 'onlyBody' => true));
                ?>
            </div>
            <div class="step-pane" id="step2">
                <?
                $this->widget(
                    'bootstrap.widgets.TbSelect2',
                    array(
                        'id' => 'uslugiList',
                        'asDropDownList' => false,
                        'name' => 'uslugiList',
                        'options' => array(
                            'tags' => array(),
                            'placeholder' => 'Выберите услуги из списка',
                            'width' => '100%',
                            'tokenSeparators' => array(',', ' ')
                        )
                    )
                );
                $this->widget(
                    'bootstrap.widgets.TbButtonGroup',
                    array(
                        'type' => 'primary',
                        'toggle' => 'radio',
                        'htmlOptions' => array('style' => 'display:inline-flex'),
                        'buttons' => array(
                            array('label' => 'Отбой с пеной + коврики', 'htmlOptions' => array('id' => 'simpleButton')),
                            array('label' => 'Комплекс', 'htmlOptions' => array('id' => 'baseButton')),
                            array('label' => 'Комплекс с полировкой', 'htmlOptions' => array('id' => 'complexButton')),
                            array('label' => 'Выбрать вручную', 'htmlOptions' => array('id' => 'manualButton')),
                        ),
                    )
                );
                $this->widget(
                    'bootstrap.widgets.TbButton',
                    array('label' => 'Очистить', 'type' => 'danger', 'htmlOptions' => array('id' => 'clearButton'))
                );
                ?>
                <div id="uslugi" class="hide">
                    <?
                    list($usluga) = Yii::app()->createController("Usluga");
                    $uslugaModel = new Usluga();
                    $uslugaModel->unsetAttributes();
                    if (isset($_GET['Usluga']))
                        $uslugaModel->attributes = $_GET['Usluga'];
                    $usluga->renderPartial('adminForClient', array('model' => $uslugaModel, 'onlyBody' => true));
                    ?>
                </div>
            </div>
            <div class="step-pane" id="step3">
                <?
                //TODO: сделать показ объектов, только у которых есть выбранные услуги и есть время/цена
                list($object) = Yii::app()->createController("Object");
                $objectModel = new ObjectWithEarliest();
                $objectModel->unsetAttributes();
                if (isset($_GET['ObjectWithEarliest']))
                    $objectModel->attributes = $_GET['ObjectWithEarliest'];

                $object->renderPartial('adminForClient', array('model' => $objectModel, 'dataProvider' => $objectModel->search(), 'onlyBody' => true));
                ?>
            </div>
            <div class="step-pane well" id="step4">
                <div class="page-header">
                    <h1>Данные заявки:</h1>
                </div>
                <dl>
                    <dt>Объект:</dt>
                    <dd id="summaryObject"></dd>
                    <dt>Дата и время визита:</dt>
                    <dd id="summaryTime"></dd>
                    <dt>Автомобиль:</dt>
                    <dd id="summaryCar"></dd>
                    <dt>Выбранные услуги:</dt>
                    <dd id="summaryUslugi"></dd>
                </dl>
            </div>
        </div>
    </div>


<?
Yii::app()->clientScript->registerScript('scripts', '
    $("#washWizard").on("change", function (e, data) {
        $("#s2id_autogen1").prop("readonly", "readonly");
        if (data.step === 1 && data.direction === "next") {
            if (polzovatelCarId == -1) {
                alert("Выберите автомобиль");
                return e.preventDefault();
            }
        }
        if (data.step === 3 && data.direction === "next") {
            if (vremyaId == "")
            {
                alert("Укажите время записи");
                return e.preventDefault();
            }
            showSummary();
        }

        //Hints
        if (data.step === 0){
            $("#hintBody").html("Выберите из списка ниже автомобиль, который Вы хотите помыть");
            $("#hintFooter").html("Управлять автомобилями можно в разделе <em><a href=\"/polzovatelCar/index\">Мои автомобили</a></em>");
        }
        else
        if (data.step === 1){
            $("#hintBody").html("Выберите услуги, которыми Вы хотите воспользоваться");
            $("#hintFooter").html("Можно пропустить этот шаг, в этом случае длительность работ учитыватья не будет");
        }
        else
        if (data.step === 2){
            $("#hintBody").html("Выберите понравившуюся автомойку и желаемое время записи");
            $("#hintFooter").html("Автомойки отсортированы по времени ближайшей возможной записи");
        }
        else
        if (data.step === 3){
            $("#hintBody").html("Проверьте корректность указанных вами данных");
            $("#hintFooter").html("Если все данные указаны верно, то для подачи заявки нажмите на кнопку <em>Готово</em> <i class=\"icon-arrow-right\"></i>");
        }
    });

    $("#washWizard").on("finished", function (e, data) {
        var params = "polzovatel_id=' . Yii::app()->user->id . '&car_id="+polzovatelCarId+"&services="+$("#uslugiList").select2("val")+"&box_resource_id=" + vremyaId;
            $.ajax({
            type: "GET",
            url: "/zapis/NewZapis/?"+params
        })
        .success(function(msg) {
            msg = msg.split("|");
            if (msg[0] == "OK")
            {
                alert( msg[1]);
                window.location.href = "/zapis/index";
            }
            else
                alert( "Ошибка: " + msg[1]);
        })
        .fail(function(msg) {
            alert( "Ошибка: " + msg );
        })
    });

    var polzovatelCarId = -1;
    var polzovatelCar = "";
    $("#polzovatel-car-grid table tbody tr").live("click", function() {
        polzovatelCarId = $.fn.yiiGridView.getKey(
                "polzovatel-car-grid",
                $(this).prevAll().length
            );
        $this = $(this);
        polzovatelCar = $this.find("td:nth-child(1)").text() + " (" + $this.find("td:nth-child(2)").text() + ")";
        $("#washWizard").wizard("next");
    });

    $("#simpleButton").live("click", function () {
        $("#uslugi").hide();
        selectedItems = [];
        selectedItems.push({id: "12", text: "Мойка кузова с пеной (бесконтактная)"});
        selectedItems.push({id: "10", text: "Мойка резиновых ковриков, пылесос текстильных (1 комплект)"});
        $("#uslugiList").select2("data", selectedItems);
    });

    $("#baseButton").live("click", function () {
        $("#uslugi").hide();
        selectedItems = [];
        selectedItems.push({id: "12", text: "Мойка кузова с пеной (бесконтактная)"});
        selectedItems.push({id: "10", text: "Мойка резиновых ковриков, пылесос текстильных (1 комплект)"});
        selectedItems.push({id: "25", text: "Пылесос кресел (за 4шт)"});
        $("#uslugiList").select2("data", selectedItems);
    });

    $("#complexButton").live("click", function () {
        $("#uslugi").hide();
        selectedItems = [];
        selectedItems.push({id: "29", text: "Полироль кузова"});
        selectedItems.push({id: "12", text: "Мойка кузова с пеной (бесконтактная)"});
        selectedItems.push({id: "10", text: "Мойка резиновых ковриков, пылесос текстильных (1 комплект)"});
        selectedItems.push({id: "25", text: "Пылесос кресел (за 4шт)"});
        $("#uslugiList").select2("data", selectedItems);
    });

    $("#manualButton").live("click", function () {
        $("#uslugi").show();
    });

    $("#clearButton").live("click", function () {
        $("#uslugiList").select2("data", []);
    });

    $("#services-grid table tbody tr").live("click", function () {
        var uslugaId = $.fn.yiiGridView.getKey(
            "services-grid",
            $(this).prevAll().length
        );

        $this = $(this);
        var uslugaText = $this.find("td:nth-child(1)").text();

        var selectedItems = $("#uslugiList").select2("data");
        selectedItems.push({id: uslugaId, text: uslugaText});
        $("#uslugiList").select2("data", selectedItems);
    });

    var objectId = -1;
    var object = "";
    var objectAdres = "";
    $("#object-grid table tbody tr").live("click", function () {
        objectId = $.fn.yiiGridView.getKey(
            "object-grid",
            $(this).prevAll().length
        );
        $this = $(this);
        object = $this.find("td:nth-child(1)").text();

        var currentTab = $("#currentTab").val();
        if (currentTab == null)
            currentTab = 1;

        var currentDay = $("#currentDay").val();
        if (currentDay == null)
            currentDay = 0;

        $.ajax({
            url: "/object/viewForClient/?object_id=" + objectId + "&uslugi=" + $("#uslugiList").select2("val") + "&car_id="+polzovatelCarId + "&currentTab=" + currentTab + "&data=" + currentDay,
        }).done(function (data) {
            $("#objectInfo").html(data);
        });
    });

    function showSummary(){
        $("#summaryCar").html(polzovatelCar);
        var uslugiListJson =$("#uslugiList").select2("data");
        console.log(JSON.stringify(uslugiListJson));
        var uslugiList = "<ul>";

        if (uslugiListJson == [])
            uslugiList = "Будут выбраны на месте";
        else
        {
            uslugiListJson.forEach(
                function(usl) {
                    uslugiList +="<li>"+usl.text+"</li>";
                }
            );
            uslugiList += "</ul>";
        }
        $("#summaryUslugi").html(uslugiList);
        $("#summaryObject").html(object);
        $("#summaryTime").html(summaryTime);
    }

    var currentTab = 2;
    ');
?>