<?php
$this->breadcrumbs = array(
    'Редактор профиля'
);
?>

<div class="page-header">
    <h1><?php
        echo "Редактор профиля";
        ?>
    </h1>
</div>

<?php
$this->widget('TbEditableDetailView', array(
    'data' => $model,
    'url' => $this->createUrl('/polzovatel/UpdateZnachenie'),
    'attributes' => array(
        'id',
        'fio',
        'email',
    ),
));

$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'primary',
    'icon' => 'arrow-left white',
    'label' => 'Назад',
    'htmlOptions' => array(
        'onClick' => "history.back(1);",
    )
));
?>
