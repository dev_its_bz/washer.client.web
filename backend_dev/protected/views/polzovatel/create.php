<?php

$model->kontragent_id = $_GET['id'];

$this->breadcrumbs = array(
    'Контрагенты' => '/kontragent/',
    $model->kontragent->nazvanie => array('/kontragent/' . $model->kontragent->id),
    'Пользователи' => array("/kontragent/{$model->kontragent->id}?tab=2"),
    'Новый'
);
?>

<div class="page-header">
    <h1>Новый пользователь</h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model
    , 'status' => $status, 'groups' => $groups));
?>