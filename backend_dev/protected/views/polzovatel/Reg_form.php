<?php
Yii::app()->clientScript->registerScriptFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/SmartWizard/js/jquery.smartWizard-2.0.js'));
Yii::app()->clientScript->registerCssFile(CHtml::asset(Yii::getPathOfAlias('application.views') . '/zapisUsluga/assets/SmartWizard/styles/smart_wizard.css'));
?>

<script>
    function seterror($err) {
        //  $('#wizard').smartWizard('setError', {stepnum: 1, iserror: true});
        //  $('#wizard').smartWizard('showMessage', 'ошибка');
        alert($err);
    }

    $(document).ready(function () {

        $('#wizard').smartWizard({onFinish: finishRecCallBack, onLeaveStep: leaveAStepCallback});


        function leaveAStepCallback(obj) {
            var step_num = obj.attr('rel'); // get the current step number
            return validateSteps(step_num, obj); // return false to stay on step and true to continue navigation 
        }

        function validateSteps(stepnumber, obj) {
            var isStepValid = false;


            if (stepnumber == 1) {

                console.log(isStepValid);

                function changeS(bool) {
                    isStepValid = bool;
                    console.log(isStepValid);
                }

                if ($("#telefon").val() == '')
                    alert('Телефон не заполнен');
                else if ($("#parol").val() == '')
                    alert('Пароль не может быть пустым.');
                else
                    $.ajax({
                            type: "POST",
                            url: '/polzovatel/CheckUniqByTelefon',
                            async: false,
                            data: {telefon: $("#telefon").val()},
                            success: function (data) {
                                if (data == '1') {
                                    alert('Пользователь с таким номером уже существует!');
                                    changeS(false);
                                }
                                else {
                                    changeS(true);
                                }
                            }, dataType: 'json'}
                    );
            }

            return isStepValid;
        }

        function finishRecCallBack(obj) {
            $.ajax({
                    type: "POST",
                    url: 'checkCode',
                    async: false,
                    data: {code: $("#code").val(), telefon: $("#Polzovatel_reg_telefon").val()},
                    success: function (data) {
                        if (data == true) {
                            $.ajax({
                                    type: "POST",
                                    url: 'AddToDb',
                                    async: false,
                                    data: $("#polzovatel-form").serialize(),
                                    success: function (data) {
                                        window.location.replace('/Polzovatel/PrivateOffice'); //
                                    }, dataType: 'json'}
                            );
                        }
                    }, dataType: 'json'}
            );
        }
    });
</script>


<table align="center" border="0" cellpadding="10" cellspacing="0" style="width: 100%; height: 100%">
    <tr>
        <td>
            <!-- Smart Wizard -->
            <div id="wizard" class="swMain" style="width: 100%; height: 100%">
                <ul>
                    <li><a href="#step-1">
                            <span class="stepNumber">1</span>
                            <span class="stepDesc">
                                Отправка<br/>
                                <small>Введите данные</small>
                            </span>
                        </a></li>
                    <li><a href="#step-2">
                            <span class="stepNumber">2</span>
                            <span class="stepDesc">
                                Проверка<br/>
                                <small>Введите код</small>
                            </span>
                        </a></li>
                </ul>
                <div id="step-1" style="width: 98%">
                    <div class="container-fluid">
                        <label for="Polzovatel_reg_login" class="required">Номер телефона <span
                                class="required">*</span></label>
                        <?php
                        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                            'id' => 'polzovatel-form',
                            'focus' => array($model, 'login'),
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                                'validateOnChange' => true,
                                'validateOnType' => true,
                            ),
                        ));
                        ?>
                        <?
                        $this->widget('CMaskedTextField', array(
                            "id" => "telefon",
                            'name' => "telefon",
                            'model' => $model,
                            'attribute' => 'telefon',
                            'value' => '',
                            'mask' => '+9-999-999-99-99',
                            'htmlOptions' => array('placeholder' => '+X-XXX-XXX-XX-XX', 'class' => 'span5', 'maxlength' => 30)
                        ));
                        ?>
                        <?php echo $form->passwordFieldRow($model, 'parol', array('id' => 'parol', 'class' => 'span5', 'maxlength' => 30)); ?>
                        <?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 30)); ?>

                        <?php echo $form->errorSummary($model); ?>

                        <?php $this->endWidget(); ?>
                    </div>
                </div>
                <div id="step-2" style="width: 98%">

                    <?php echo CHtml::textField('code'); ?>
                    <?php
                    echo CHtml::ajaxSubmitButton('Мне не пришел код', 'CodeAgain', array(
                        'type' => 'POST',
                        'data' => 'js: $("#Polzovatel_reg_telefon").serialize()'
                    ));
                    ?>

                </div>
                <!-- End SmartWizard Content -->

        </td>
    </tr>
</table>