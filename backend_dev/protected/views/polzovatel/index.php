<?php
$this->breadcrumbs = array(
    'Контрагенты' => Yii::app()->user->gruppa_id == '2' ? array('/kontragent/admin') : null,
    Yii::app()->user->gruppa_id != 2 ? $model->kontragent->nazvanie : 'Все контрагенты' => $kontragent != null ? array('/kontragent/view/', 'id' => $kontragent->id) : null,
    $object != null ? $object->nazvanie : 'Все объекты' => $object != null ? array('/object/view/', 'id' => $object->id) : null,
    'Пользователи'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('polzovatel-grid', {
		data: $(this).serialize()
	});
	return false;
});
$(document).ready(function () {
        $('#polzovatel-grid tbody tr').live('dblclick', function () {
            var id = $.fn.yiiGridView.getKey(
                'polzovatel-grid',
                $(this).prevAll().length
            );
            document.location.href = '/polzovatel/' + id;
        });
    });
");
?>

<div class="page-header position-relative">
    <h1>Пользователи
        <?
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'size' => 'mini',
                'buttons' => array(
                    array(
                        'buttonType' => 'link', 'htmlOptions' => array('class' => 'owner_button'), 'type' => 'success', 'icon' => 'glyphicon glyphicon-plus white', 'url' => '/Polzovatel/create/' . $model->kontragent_id,),
                    array(
                        'buttonType' => 'link', 'type' => 'primary', 'icon' => 'glyphicon glyphicon-search white', 'url' => '#', 'htmlOptions' => array('class' => 'search-button')
                    ),
                )
            )
        );
        ?>
    </h1>
</div>

<div class="search-form" style="display:none">
    <div class="alert alert-info glyphicon-info-sign">
        В фильтрах можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
            &lt;&gt;</b>
        или равенства <b>=</b>), указав их перед выражением.
    </div>
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'polzovatel-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('name' => 'id', 'htmlOptions' => array('style' => 'width:20px; text-align:center')),
        'login',
        'fio',
        'dolzhnost',
        'telefon',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete}'
        ),
    ),
));
?>
