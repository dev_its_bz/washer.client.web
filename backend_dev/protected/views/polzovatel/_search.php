<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>

<?php echo $form->textFieldRow($model, 'id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'login', array('class' => 'span5', 'maxlength' => 30)); ?>

<?php echo $form->textFieldRow($model, 'parol', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'fio', array('class' => 'span5', 'maxlength' => 100)); ?>

<?php echo $form->textFieldRow($model, 'dolzhnost', array('class' => 'span5', 'maxlength' => 100)); ?>

<?php echo $form->textFieldRow($model, 'telefon', array('class' => 'span5', 'maxlength' => 30)); ?>

<?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 30)); ?>

<?php echo $form->textFieldRow($model, 'gruppa_id', array('class' => 'span5')); ?>



<?php echo $form->textFieldRow($model, 'status_id', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'data_sozdaniya', array('class' => 'span5')); ?>

<?php echo $form->textFieldRow($model, 'avtor_zapisi_id', array('class' => 'span5')); ?>

    <?php echo $form->textFieldRow($model, 'data_zapisi', array('class' => 'span5')); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => 'Найти',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
