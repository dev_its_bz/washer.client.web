<script>
    var tip = 0;
    $("#activate").live("click", function () {
        if ($('#activationCode').val()) {
            $.ajax({
                    type: "POST",
                    url: '/polzovatel/checkCode',
                    data: {activationCode: $('#activationCode').val(), login: $('#login').val(), parol: $('#parol').val(), tip: tip},
                    success: function (data, textStatus) {
                        if (data == '1') {

                            alert("Номер подтверждён.");
                            $.arcticmodal('close');
                            window.location.replace("/");
                        }
                        else
                            alert(data);
                    },
                    dataType: 'text'}
            );
        } else
            alert('Введите проверочный код');
    });
    $("#register").live("click", function () {
        if ($('#login').val() && $('#parol').val()) {
            $.ajax({
                    type: "POST",
                    url: '/polzovatel/checkUnique',
                    data: {login: $('#login').val()},
                    success: function (data, textStatus) {
                        if (data == '1')
                            $('#activateModal').arcticmodal();
                        else
                            alert(data);
                    },
                    dataType: 'text'}
            );
        } else
            alert('Введите телефон и желаемый пароль');
    });
</script>

<?php
$isKontragent = isset($_GET['asKontragent']);

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'polzovatel-form',
    'enableClientValidation' => true,
    'action' => '/polzovatel/registration',
    'clientOptions' => array(
        'validateOnSubmit' => true,
    )
));

$this->widget(
    'bootstrap.widgets.TbButtonGroup',
    array(
        'type' => 'secondary',
        'toggle' => 'radio',
        'encodeLabel' => false,
        'buttons' => array(
            array(
                'label' => '<img src="/images/car.png"><br/>Владелец автомобиля',
                'htmlOptions' => array('onClick' => 'tip=0;'),
                'active' => true,
            ),
            array(
                'label' => '<img src="/images/carwash.png"><br/>Владелец автомойки',
                'htmlOptions' => array('onClick' => 'tip=1;'),
            )
        ),
    )
);

echo "<br/><br/>";
?>
<?php echo $form->maskedTextFieldRow($model, 'login', array('placeholder' => 'X', 'mask' => '+9-999-999-99-99', 'htmlOptions' => array('id' => 'login', 'placeholder' => 'Номер телефона')), array('labelOptions' => array('label' => false))); ?>
<?php echo $form->passwordFieldRow($model, 'parol', array('placeholder' => 'Пароль (от 6 символов)', 'id' => 'parol', 'class' => 'span5', 'maxlength' => 30, 'minlength' => 6), array('labelOptions' => array('label' => false))); ?>
<?php echo $form->textFieldRow($model, 'email', array('placeholder' => 'E-mail (не обязательно)', 'class' => 'span5', 'maxlength' => 30), array('labelOptions' => array('label' => false))); ?>
<input type="hidden" name="tabLogin" value="1"/>
<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'link',
    'size' => 'large',
    'type' => 'success',
    'htmlOptions' => array('style' => 'width:85%; margin-bottom:15px', 'id' => 'register'),
    'label' => 'ЗАРЕГИСТРИРОВАТЬСЯ',
));
?>

<?php $this->endWidget(); ?>

<div style="display: none;">
    <?
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'login-form',
        'type' => 'vertical',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>
    <div class="box-modal well form" id="activateModal">
        <h4>Активация номера телефона</h4>
        <input name="activationCode" id="activationCode" type="text" maxlength="4"
               style="width: 100px; text-align: center; font-size: xx-large; height: 40px;">
        <?php
        echo "<br/>";
        $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'link', 'type' => "success", 'size' => 'large', 'label' => 'ПОДТВЕРДИТЬ', 'htmlOptions' => array('id' => 'activate', 'style' => 'width:85%; margin-bottom:-5px'))
        );
        ?>
    </div>
    <?php $this->endWidget(); ?>
</div>