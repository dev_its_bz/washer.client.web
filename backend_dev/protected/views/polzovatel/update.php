<?php
$this->breadcrumbs = array(
    'Контрагенты' => '/kontragent/',
    $model->kontragent->nazvanie => array('/kontragent/' . $model->kontragent->id),
    'Пользователи' => "/kontragent/{$model->kontragent->id}?tab=2",
    $model->fio => array('update', 'id' => $model->id)
);
?>

    <h1><?php echo $model->fio; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model, 'status' => $status, 'groups' => $groups)); ?>