<div class="alert alert-info glyphicon-info-sign">Поля, отмеченные <span class="required">*</span> обязательны к
    заполнению.
</div>

<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'polzovatel-form',
    'htmlOptions' => array('class' => 'well'),
    'focus'=>array($model,'login'),
    'enableAjaxValidation' => false,
));
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'login', array('class' => 'span5', 'maxlength' => 30)); ?>

<?php echo $form->textFieldRow($model, 'parol', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'fio', array('class' => 'span5', 'maxlength' => 100)); ?>

<?php echo $form->textFieldRow($model, 'dolzhnost', array('class' => 'span5', 'maxlength' => 100)); ?>

<?php echo $form->textFieldRow($model, 'telefon', array('class' => 'span5', 'maxlength' => 30)); ?>

<?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 30)); ?>


<div class="row2">
    <?php echo $form->labelEx($model, 'Группа'); ?>

    <?php echo $form->dropDownList($model, 'gruppa_id', $groups, array('prompt' => '(Укажите группу)'));
    ?>
    <?php echo $form->error($model, 'gruppa_id'); ?>
</div>


<div class="row2">
    <?php echo $form->labelEx($model, 'Статус'); ?>

    <?php echo $form->dropDownList($model, 'status_id', $status, array('prompt' => '(Укажите статус)'));
    ?>

    <?php echo $form->error($model, 'status_id'); ?>
</div>

<hr>


<?php
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'icon' => 'check white',
    'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
));
?>

<?
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'danger',
    'icon' => 'remove white',
    'label' => 'Отмена',
    'htmlOptions' => array(
        'onClick' => "window.location.href = '/kontragent/view/{$model->kontragent_id}?tab=2'",
    )
));
?>

<?php $this->endWidget(); ?>
