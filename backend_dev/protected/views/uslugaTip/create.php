<?php
$this->breadcrumbs = array(
    'Виды услуг' => array('/uslugaTip/admin/'),
    'Новая',
);
?>

    <div class="page-header">
        <h1>Новый вид услуг</h1>
    </div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>