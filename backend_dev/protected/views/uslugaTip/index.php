<?php
$this->breadcrumbs=array(
	'Usluga Tips',
);

$this->menu=array(
	array('label'=>'Create UslugaTip','url'=>array('create')),
	array('label'=>'Manage UslugaTip','url'=>array('admin')),
);
?>

<h1>Usluga Tips</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
