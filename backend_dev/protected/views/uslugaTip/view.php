<?php
$this->breadcrumbs=array(
	'Usluga Tips'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UslugaTip','url'=>array('index')),
	array('label'=>'Create UslugaTip','url'=>array('create')),
	array('label'=>'Update UslugaTip','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete UslugaTip','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UslugaTip','url'=>array('admin')),
);
?>

<h1>View UslugaTip #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nazvanie',
		'data_sozdaniya',
		'avtor_zapisi_id',
		'data_zapisi',
	),
)); ?>
