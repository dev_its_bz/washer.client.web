<div class="alert alert-info glyphicon-info-sign">Поля, отмеченные <span class="required">*</span> обязательны к
    заполнению.
</div>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'usluga-tip-form',
    'htmlOptions' => array('class' => 'well'),
    'enableAjaxValidation' => false,
    'focus'=>array($model,'nazvanie'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'nazvanie', array('class' => 'span5', 'maxlength' => 100)); ?>

<hr/>
<?
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'icon' => 'check white',
    'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
));
?>

<?
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'danger',
    'icon' => 'remove white',
    'label' => 'Отмена',
    'htmlOptions' => array(
        'onClick' => "window.location.href = '/uslugaTip/admin/'",
    )
));
?>

<?php $this->endWidget(); ?>