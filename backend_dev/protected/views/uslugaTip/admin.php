<?php
$this->breadcrumbs = array(
    'Виды услуг'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('usluga-tip-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
    $(document).ready(function () {
        $('#usluga-tip-grid tbody tr').live('dblclick', function () {
            var id = $.fn.yiiGridView.getKey(
                'usluga-tip-grid',
                $(this).prevAll().length
            );
            document.location.href = '/Usluga/ViewByTip/' + id;
        });
    });
</script>

<div class="page-header">
    <h1>Виды услуг
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons' => array(
                array('buttonType' => 'link', 'type' => 'success', 'icon' => 'plus white', 'url' => '/UslugaTip/create/'),
            )))
        ?>
    </h1>
</div>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'usluga-tip-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'class' => 'TbEditableColumn',
            'name' => 'nazvanie',
            'value' => '$data->nazvanie',
            'editable' => array(
                'apply' => 'true',
                'title' => 'Введите значение',
                'url' => $this->createUrl('/UslugaTip/UpdateVar'),
                'placement' => 'right',


            )
        ),


        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}',
            'buttons' => array
            (
                'update' => array
                (
                    'url' => 'Yii::app()->createUrl("/Usluga/ViewByTip", array("id"=>$data->id))',
                ),
                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("/UslugaTip/delete", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
));
?>
