<?php
$this->breadcrumbs=array(
	'Usluga Tips'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UslugaTip','url'=>array('index')),
	array('label'=>'Create UslugaTip','url'=>array('create')),
	array('label'=>'View UslugaTip','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage UslugaTip','url'=>array('admin')),
);
?>

<h1>Update UslugaTip <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>