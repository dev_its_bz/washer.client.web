<?php
$this->breadcrumbs=array(
	'Zapises'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Zapis','url'=>array('index')),
array('label'=>'Manage Zapis','url'=>array('admin')),
);
?>

<h1>Create Zapis</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>