<?php
$this->breadcrumbs=array(
	'Zapises'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Zapis','url'=>array('index')),
array('label'=>'Create Zapis','url'=>array('create')),
array('label'=>'Update Zapis','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Zapis','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Zapis','url'=>array('admin')),
);
?>

<h1>View Zapis #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'polzovatel_id',
		'car_id',
		'object_id',
		'summa',
		'full_time_temp',
		'full_time_fakt',
		'box_id',
		'time_begin',
		'time_end',
		'data_sozdaniya',
		'data_zapisi',
		'status_id',
		'oplacheno',
		'avtor_zapisi_id',
		'zapis_date',
),
)); ?>
