<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'polzovatel_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'car_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'object_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'summa',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'full_time_temp',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'full_time_fakt',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'box_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'time_begin',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'time_end',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'data_sozdaniya',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'data_zapisi',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'status_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'oplacheno',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'avtor_zapisi_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'zapis_date',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
