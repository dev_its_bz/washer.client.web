<?php
$this->breadcrumbs=array(
	'Zapises'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Zapis','url'=>array('index')),
	array('label'=>'Create Zapis','url'=>array('create')),
	array('label'=>'View Zapis','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Zapis','url'=>array('admin')),
	);
	?>

	<h1>Update Zapis <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>