<?php
$this->breadcrumbs = array(
    'Записи',
);
?>

<div class="page-header">
    <h1>Записи
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'link',
                'type' => 'success',
                'icon' => 'plus',
                'size' => 'mini',
                'url' => '/polzovatel/dashboard',
                'htmlOptions' => array(
                    'data-toggle' => "tooltip",
                    'data-original-title' => "Записаться на автомойку"
                )
            )
        );
        ?>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'zapis-grid',
        'dataProvider' => $model->search(),
        'columns' => array(
            array(
                'value' => '$data->id',
                'header' => 'ID',
                'htmlOptions' => array('style' => 'width:20px; vertical-align:middle')
            ),
            array(
                'header' => 'Автомобиль',
                'type' => 'raw',
                'value' => '$data->polzovatelCar->carModel->carMarka->nazvanie." ".$data->polzovatelCar->carModel->nazvanie. "<br/>".$data->polzovatelCar->gos_nomer'
            ),
            array(
                'header' => 'Автомойка, бокс',
                'type' => 'raw',
                'value' => '$data->object->nazvanie." (".$data->box->nazvanie.")<br/>".$data->object->adres'
            ),
            array(
                'header' => 'Время и длительность записи',
                'type' => 'raw',
                'value' => 'date("d.m.Y", strtotime($data->zapis_date))."<br/>с ".$data->time_begin." по ".$data->time_end." (".$data->full_time_temp." мин.)"',
            ),
            array(
                'name' => 'summa',
                'header' => 'Сумма, руб.',
                'htmlOptions' => array('style' => 'vertical-align:middle'),
                'value' => '$data->summa',
            ),
            array(
                'name' => 'status.nazvanie',
                'htmlOptions' => array('style' => 'vertical-align:middle'),
            ),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'htmlOptions' => array('style' => 'vertical-align:middle'),
                'template' => "{delete}"
            ),
        ),
    )
);
?>
