<?php
$this->breadcrumbs=array(
	'Zapises'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Zapis','url'=>array('index')),
array('label'=>'Create Zapis','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('zapis-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Zapises</h1>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'zapis-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'polzovatel_id',
		'car_id',
		'object_id',
		'summa',
		'full_time_temp',
		/*
		'full_time_fakt',
		'box_id',
		'time_begin',
		'time_end',
		'data_sozdaniya',
		'data_zapisi',
		'status_id',
		'oplacheno',
		'avtor_zapisi_id',
		'zapis_date',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
