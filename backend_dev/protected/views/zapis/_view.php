<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('polzovatel_id')); ?>:</b>
	<?php echo CHtml::encode($data->polzovatel_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('car_id')); ?>:</b>
	<?php echo CHtml::encode($data->car_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('object_id')); ?>:</b>
	<?php echo CHtml::encode($data->object_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('summa')); ?>:</b>
	<?php echo CHtml::encode($data->summa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('full_time_temp')); ?>:</b>
	<?php echo CHtml::encode($data->full_time_temp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('full_time_fakt')); ?>:</b>
	<?php echo CHtml::encode($data->full_time_fakt); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('box_id')); ?>:</b>
	<?php echo CHtml::encode($data->box_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_begin')); ?>:</b>
	<?php echo CHtml::encode($data->time_begin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('time_end')); ?>:</b>
	<?php echo CHtml::encode($data->time_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_sozdaniya')); ?>:</b>
	<?php echo CHtml::encode($data->data_sozdaniya); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('data_zapisi')); ?>:</b>
	<?php echo CHtml::encode($data->data_zapisi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_id')); ?>:</b>
	<?php echo CHtml::encode($data->status_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('oplacheno')); ?>:</b>
	<?php echo CHtml::encode($data->oplacheno); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('avtor_zapisi_id')); ?>:</b>
	<?php echo CHtml::encode($data->avtor_zapisi_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zapis_date')); ?>:</b>
	<?php echo CHtml::encode($data->zapis_date); ?>
	<br />

	*/ ?>

</div>