<?php
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'cars-groups-form',
    'enableAjaxValidation' => false,
));


$dp = $model->getForCheckBox($group);
$dp->pagination = false;

?>

    <h1>Автомобили в выбранной группе</h1>

<?php if ($model->isNewRecord) : ?>
    <?php
    if (Yii::app()->user->role == 'administrator') echo CHtml::button('Сохранить изменения', array('submit' => array('CarModel/SaveGruppaAndAdd', 'group' => $group), 'class' => 'btn btn-success'));

    ?>

    <?php echo CHtml::button('Назад', array('submit' => Yii::app()->createUrl("/carGruppa/admin"), 'confirm' => 'Изменения не сохранятся! Продолжить?', 'class' => 'btn btn-danger')); ?>
<?php endif; ?>

<?php $this->widget('bootstrap.widgets.TbGroupGridView', array(
    'id' => 'car-model-grid',
    'dataProvider' => $dp,
    'filter' => $model,
    'extraRowColumns' => array('car_marka_id'),
    'extraRowExpression' => '"<b style=\"font-size: x-large; color: #333;\">".$data->carMarka->nazvanie."</b>"',
    'columns' => array(
        'nazvanie',
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'model_to_insert',
            'checked' => '$data->inObj',
            'value' => '$data->id',
            'header' => 'Добавить',
            'selectableRows' => 2,
            'name' => 'model_to_insert',
        ),
        array(
            'class' => 'CCheckBoxColumn',
            'id' => 'model_temp',
            'checked' => '$data->inObj',
            'value' => '$data->id',
            'htmlOptions' => array('style' => 'display:none'),
            'headerHtmlOptions' => array('style' => 'display:none'),
            'selectableRows' => 2,
        ),
    ),
)); ?>

<?php $this->endWidget(); ?>