<div class="alert alert-info glyphicon-info-sign">Поля, отмеченные <span class="required">*</span> обязательны к
    заполнению.
</div>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'car-model-form',
    'focus' => array($model, 'nazvanie'),
    'htmlOptions' => array('class' => 'well'),
    'enableAjaxValidation' => false,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo CHtml::hiddenField('car_marka_id', $id); ?>

<?php echo $form->textFieldRow($model, 'nazvanie', array('class' => 'span5', 'maxlength' => 30)); ?>

<?php echo "<br/>Группа авто<br>".$form->dropDownList($model, "car_gruppa_id", CHtml::listData(CarGruppa::model()->findAll(), 'id', 'nazvanie')); ?>

<?php echo $form->hiddenField($model, 'data_sozdaniya', array('value' => date("Y-m-d h:i"))); ?>

<?php echo $form->hiddenField($model, 'avtor_zapisi_id', array('value' => Yii::app()->user->id)); ?>

<?php echo $form->hiddenField($model, 'data_zapisi', array('value' => date("Y-m-d h:i"))); ?>

<?php echo $form->hiddenField($model, 'car_marka_id', array('value' => $id)); ?>

<hr/>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Создать модель' : 'Сохранить',
)); ?>
</div>

<?php $this->endWidget(); ?>
