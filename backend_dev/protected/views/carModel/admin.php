<?php
$this->breadcrumbs=array(
	'Car Models'=>array('index'),
	'Управление',
);

$this->menu=array(
	array('label'=>'List CarModel','url'=>array('index')),
	array('label'=>'Create CarModel','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('car-model-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Car Models</h1>

<p>
В фильтрах  можно использовать знаки сравнения (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или равенства <b>=</b>), указав их перед выражением.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'car-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'car_marka_id',
		'nazvanie',
		'car_gruppa_id',
		'data_sozdaniya',
		'avtor_zapisi_id',
		/*
		'data_zapisi',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
