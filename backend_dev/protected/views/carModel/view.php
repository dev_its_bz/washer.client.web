<?php
$this->breadcrumbs=array(
	'Car Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CarModel','url'=>array('index')),
	array('label'=>'Create CarModel','url'=>array('create')),
	array('label'=>'Update CarModel','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete CarModel','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CarModel','url'=>array('admin')),
);
?>

<h1>View CarModel #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'car_marka_id',
		'nazvanie',
		'car_gruppa_id',
		'data_sozdaniya',
		'avtor_zapisi_id',
		'data_zapisi',
	),
)); ?>
