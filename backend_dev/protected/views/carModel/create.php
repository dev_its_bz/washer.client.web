<?php
$carMarka = CarMarka::model()->findByPk($id);

$this->breadcrumbs = array(
    'Марки автомобилей' => '/CarMarka/admin',
    $carMarka->nazvanie => "/CarMarka/update/" . $id,
    'Новая',
);
?>
    <div class="page-header">
        <h1>Новая модель</h1>
    </div>

<?php echo $this->renderPartial('_form', array('model' => $model, 'id' => $id)); ?>