<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php //Yii::app()->bootstrap->register(); ?>
</head>

<body style="border: 0; padding:0; margin: 0;">
<table style="width: 100%; height: 100%; margin: 0; padding: 0;" border="0" cellPadding="0" cellSpacing="0">
    <tr>
        <td style="padding: 0; margin: 0">
            <?
            $this->widget('bootstrap.widgets.TbNavbar', array(
                    'type' => 'inverse', // null or 'inverse'
                    'fixed' => 'false',
                    'brand' => Yii::app()->name,
                    'brandOptions' => array('style' => 'background:url(/images/washer_32.png) left no-repeat; padding-left:42px; margin-left: -15px'),
                    'collapse' => true, // requires bootstrap-responsive.css
                    'items' => array(
                        array(
                            'class' => 'bootstrap.widgets.TbMenu',
                            'htmlOptions' => array('class' => 'pull-right'),
                            'items' => array(
                                array('label' => 'Как это работает?', 'url' => array('/site/page', 'view' => 'kak_eto_rabotaet_mobile')),
                                array('label' => 'О компании', 'url' => array('/site/page', 'view' => 'o_kompanii_mobile')),
                                array('label' => 'Рекламодателям', 'url' => array('/site/page', 'view' => 'reklamodatelyam_mobile')),
                                array('label' => 'Вопросы?', 'url' => array('/site/page', 'view' => 'voprosy_mobile')),
                            ),
                        ),
                    ),
                )
            );
            ?>
        </td>
    </tr>
    <tr>
        <td valign="middle" align="left" style="padding: 0; margin: 0">
            <div class="flatBar" style="height: 100%; padding-top: 1px;">
                <?
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                    'separator' => '&raquo;',
                    'links' => $this->breadcrumbs));
                ?>
            </div>
        </td>
    </tr>
    <tr>
        <td style="height: 100%; padding: 0; margin: 0; text-align: center; vertical-align: <?= isset($_GET['view']) ? 'top' : 'middle' ?>">
            <?php echo $content; ?>
        </td>
    </tr>
    <tr>
        <td style="padding: 0; margin: 0; padding-top: 2px; border-top: 1px solid #e5e5e5; text-align: center">
            &copy; <?php echo date('Y'); ?><a href="http://its.bz" target="_blank">
                АйТиЭс</a>&trade;<br/>
            <?php echo Yii::powered(); ?>
        </td>
    </tr>
</table>
</body>
</html>