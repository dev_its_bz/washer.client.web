<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace-responsive.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace-skins.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace-ie.min.css"/>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/glyphicons.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css"/>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300"/>
</head>
<body>
<?
$this->widget('bootstrap.widgets.TbNavbar', array(
    'type' => '', // null or 'inverse'
    'fixed' => 'true',
    'brand' => " <small>&nbsp;<i class='glyphicon glyphicon-tint' ></i > " . Yii::app()->name . ' <small>&raquo; Сервис записи на автомойки</small></small>',
    'collapse' => true, // requires bootstrap-responsive.css
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'htmlOptions' => array('class' => 'pull-right'),
            'encodeLabel' => false,
            'items' => array(
                array('label' => 'Как это работает?', 'url' => array('/site/page', 'view' => 'kak_eto_rabotaet')),
                array('label' => 'О сервисе', 'url' => array('/site/page', 'view' => 'o_kompanii')),
                array('label' => 'Автомойкам', 'url' => array('/site/page', 'view' => 'avtomoikam')),
                array('label' => 'Рекламодателям', 'url' => array('/site/page', 'view' => 'reklamodatelyam')),
                array('label' => 'Вопросы?', 'url' => array('/site/page', 'view' => 'voprosy')),

                array('label' => 'Войти', 'url' => array('/site/login'), 'icon' => 'glyphicon glyphicon-log-in', 'visible' => Yii::app()->user->isGuest),
                array('label' => 'Выйти', 'url' => array('/site/logout'), 'icon' => 'glyphicon glyphicon-log-out', 'visible' => !Yii::app()->user->isGuest),
            ),
        ),
    ),
));
?>
<div class="main-container container-fluid">
    <div class="main-content" style="margin: 0;">
        <? if ($this->breadcrumbs != null) { ?>
            <div class="breadcrumbs" id="breadcrumbs">
                <?
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                    'separator' => '&raquo;',
                    'links' => $this->breadcrumbs));
                ?>
            </div>
        <? } ?>
        <div class="page-content" style="padding: 0">
            <?php echo $content; ?>
        </div>
    </div>
</div>
</body>
</html>