<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php //Yii::app()->bootstrap->register(); ?>
</head>

<body>
<?php
$this->widget('bootstrap.widgets.TbNavbar', array(
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'items' => array(
                array('label' => 'Главная', 'url' => array('/site/index')),
                array('label' => 'О проекте', 'url' => array('/site/page', 'view' => 'about')),
                array('label' => 'Контакты', 'url' => array('/site/contact')),
                array('label' => 'Вход', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                array('label' => 'Выход (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'РАЗРАБОТКА', 'visible' => !Yii::app()->user->isGuest, 'items' => array(
                    array('label' => 'Пользователи', 'url' => array('/polzovatel/admin'), 'visible' => !Yii::app()->user->isGuest),
                    array('label' => 'Контрагенты', 'url' => array('/kontragent/admin'), 'visible' => !Yii::app()->user->isGuest),
                    array('label' => 'Услуги', 'url' => array('/services/admin'), 'visible' => !Yii::app()->user->isGuest),
                    array('label' => 'Марки и модели avto', 'url' => array('/carMarka/admin'), 'visible' => !Yii::app()->user->isGuest),
                )
                )
            ),
        ),
    ),
));
?>

<div class="container" id="page">
    <div>
        <?php echo $content; ?>
        <div class="clear"></div>
    </div>

    <div id="footer">
        Разработано &copy; <?php echo date('Y'); ?><a href="http://its.bz" target="_blank"> АйТиЭс</a>&trade;<br/>
        <?php echo Yii::powered(); ?>
    </div>
    <!-- footer -->
    <!-- Main -->
</div>
<!-- page -->
</body>
</html>
