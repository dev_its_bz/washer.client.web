<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace-responsive.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace-skins.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace-ie.min.css"/>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/glyphicons.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css"/>
    <style type="text/css">
        @font-face {
            font-family: 'Open Sans1';
            src: url(<?php echo Yii::app()->theme->baseUrl; ?>/font/OpenSans-Regular.ttf);
            font-weight:normal;
        }
    </style>
</head>
<body>
<?
$fio = Yii::app()->user->fio == '---' ? "Пользоваатель" : Yii::app()->user->fio;
$time = "";
$now = date("H");
if ($now <= 5 || $now >= 23) $time = "Доброй ночи";
else
    if ($now < 12) $time = "Доброе утро";
    else
        if ($now < 23) $time = "Добрый вечер";
$profileLabel = "
<img class='nav-user-photo' src='/images/washer_48.png' alt='Фото'><span class='user-info'><small>$time,</small>$fio</span></i>";

$this->widget('bootstrap.widgets.TbNavbar', array(
    'type' => '', // null or 'inverse'
    'fixed' => 'true',
    'brand' => " <small>&nbsp;<i class='glyphicon glyphicon-tint' ></i > " . Yii::app()->name . ' <small>(Автовладелец)</small></small>',
    'collapse' => true, // requires bootstrap-responsive.css
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'htmlOptions' => array('class' => 'nav ace-nav pull-right'),
            'encodeLabel' => false,
            'items' => array(
                array('label' => '<span class="badge badge-primary">4</span>', 'url' => array('/site/page', 'view' => 'dummy'), 'icon' => 'glyphicon glyphicon-tasks', 'itemOptions' => array('class' => 'grey'),),
                array('label' => '<span class="badge badge-important">21</span>', 'url' => array('/site/page', 'view' => 'dummy'), 'icon' => 'glyphicon glyphicon-ok', 'itemOptions' => array('class' => 'purple'),),
                array('label' => '<span class="badge badge-success">6</span>', 'url' => array('/site/page', 'view' => 'dummy'), 'icon' => 'glyphicon glyphicon-road', 'itemOptions' => array('class' => 'green'),),
                array('label' => $profileLabel, 'itemOptions' => array('class' => 'light-blue'), 'items' => array(
                    array('label' => 'Настройки системы', 'url' => array('/site/logout'), 'icon' => 'glyphicon glyphicon-cog'),
                    array('label' => 'Профиль пользователя', 'url' => array('/polzovatel/view/'), 'icon' => 'glyphicon glyphicon-user'),
                    '---',
                    array('label' => 'Выйти', 'url' => array('/site/logout'), 'icon' => 'glyphicon glyphicon-log-out', 'visible' => !Yii::app()->user->isGuest),
                )),
            ),
        ),
    ),
));
?>
<div class="main-container container-fluid">
    <div class="sidebar">
        <div class="sidebar-shortcuts" id="sidebar-shortcuts" style="padding: 2px 0 3px 0">
            <?
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'size' => 'small',
                'buttons' => array(
                    array('label' => '', 'icon' => 'glyphicon glyphicon-user', 'url' => '#', 'type' => 'success', 'htmlOptions' => array('style' => 'padding: 2px 8px 2px 11px; margin-right:3px;'),),
                    array('label' => '', 'icon' => 'glyphicon glyphicon-wrench', 'url' => '#', 'type' => 'info', 'htmlOptions' => array('style' => 'padding: 2px 8px 2px 11px; margin-right:3px;'),),
                    array('label' => '', 'icon' => 'glyphicon glyphicon-shopping-cart', 'url' => '#', 'type' => 'warning', 'htmlOptions' => array('style' => 'padding: 2px 8px 2px 11px; margin-right:3px;'),),
                    array('label' => '', 'icon' => 'glyphicon glyphicon-search', 'url' => '#', 'type' => 'danger', 'htmlOptions' => array('style' => 'padding: 2px 8px 2px 11px; margin-right:3px;'),),
                ),
            ));
            ?>
        </div>
        <?php
        $this->widget('bootstrap.widgets.TbMenu', array(
                'type' => 'list',
                'htmlOptions' => array(),
                'items' => array(
                    array('label' => 'Рабочий стол', 'icon' => 'dashboard', 'url' => array('/polzovatel/dashboard')),
                    array('label' => 'Справочники'),

                    array('label' => 'Мои автомобили', 'icon' => 'briefcase', 'url' => array('/polzovatelCar/index')),
                    array('label' => 'Автомойки', 'icon' => 'glyphicon glyphicon-home', 'url' => array('/object/index')),
                    array('label' => 'Избранное', 'icon' => 'heart', 'url' => array('/polzovatelFavorite/index')),
                    array('label' => 'Записи', 'icon' => 'star', 'url' => array('/Zapis/index')),
                    array('label' => 'Отзывы', 'icon' => 'star', 'url' => array('/PolzovatelReply/index')),

                    array('label' => 'Инструменты'),
                    array('label' => 'Сообщить об ошибке', 'icon' => 'glyphicon glyphicon-fire', 'url' => array('/site/page', 'view' => 'dummy')),
                    array('label' => 'Справка по системе', 'icon' => 'glyphicon glyphicon-question-sign', 'url' => array('/site/page', 'view' => 'dummy')),
                )
            )
        );
        ?>
    </div>
    <div class="main-content">
        <? if ($this->breadcrumbs != null) { ?>
            <div class="breadcrumbs" id="breadcrumbs">
                <?
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                    'separator' => '&raquo;',
                    'links' => $this->breadcrumbs));
                ?>
            </div>
        <? } ?>
        <div class="page-content" style="<?= $this->breadcrumbs == null ? 'padding:0' : '' ?>">
            <?php echo $content; ?>
        </div>
    </div>
</div>
</body>
</html>