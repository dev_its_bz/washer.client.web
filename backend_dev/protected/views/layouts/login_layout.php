<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru" class="login-bg">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/login_layout.css"/>
</head>

<body class="bg_img">
<?php echo $content; ?>
<div class="clear"></div>
</body>


<div
    style="position: absolute; bottom: 0px; width: 100%; background: rgba(255, 255, 255, 0.65); padding-bottom: 10px; padding-top: 10px; text-align: center; font-size: 10pt;">
    Разработано в <a href="http://its.bz" target="_blank">
        АйТиЭс</a>&trade; <?php echo Yii::powered(); ?> &copy; <?php echo date('Y'); ?>
</div>
</html>
