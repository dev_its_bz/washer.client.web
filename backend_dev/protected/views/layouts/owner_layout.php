<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace-responsive.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace-skins.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ace-ie.min.css"/>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/glyphicons.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css"/>
    <style type="text/css">
        @font-face {
            font-family: 'Open Sans1';
            src: url(<?php echo Yii::app()->theme->baseUrl; ?>/font/OpenSans-Regular.ttf);
            font-weight:normal;
        }
    </style>
</head>
<body>
<?
$fio = Yii::app()->user->fio == '---' ? "Пользоваатель" : Yii::app()->user->fio;
$time = "";
$now = date("H");
if ($now <= 5 || $now >= 23) $time = "Доброй ночи";
else
    if ($now < 12) $time = "Доброе утро";
    else
        if ($now < 23) $time = "Добрый вечер";
$profileLabel = "
<img class='nav-user-photo' src='/images/washer_48.png' alt='Фото'><span class='user-info'><small>$time,</small>$fio</span></i>";

$this->widget('bootstrap.widgets.TbNavbar', array(
    'type' => '', // null or 'inverse'
    'fixed' => 'true',
    'brand' => " <small>&nbsp;<i class='glyphicon glyphicon-tint' ></i > " . Yii::app()->name . ' <small>(Владелец автомойки)</small></small>',
    'collapse' => true, // requires bootstrap-responsive.css
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbMenu',
            'htmlOptions' => array('class' => 'nav ace-nav pull-right'),
            'encodeLabel' => false,
            'items' => array(
                array('label' => '<span class="badge badge-primary">4</span>', 'url' => array('/site/page', 'view' => 'dummy'), 'icon' => 'glyphicon glyphicon-tasks', 'itemOptions' => array('class' => 'grey'),),
                array('label' => '<span class="badge badge-important">21</span>', 'url' => array('/site/page', 'view' => 'dummy'), 'icon' => 'glyphicon glyphicon-ok', 'itemOptions' => array('class' => 'purple'),),
                array('label' => '<span class="badge badge-success">6</span>', 'url' => array('/site/page', 'view' => 'dummy'), 'icon' => 'glyphicon glyphicon-road', 'itemOptions' => array('class' => 'green'),),
                array('label' => '<span class="badge badge-warning">2</span>', 'url' => array('/site/page', 'view' => 'dummy'), 'icon' => 'glyphicon glyphicon-envelope', 'itemOptions' => array('class' => 'blue'),),
                array('label' => $profileLabel, 'itemOptions' => array('class' => 'light-blue'), 'items' =>
                    array(
                        array('label' => 'Настройки системы', 'url' => array('/site/logout'), 'icon' => 'glyphicon glyphicon-cog'),
                        array('label' => 'Профиль пользователя', 'url' => array('/polzovatel/view/' . Yii::app()->user->id), 'icon' => 'glyphicon glyphicon-user'),
                        '---',
                        array('label' => 'Выйти', 'url' => array('/site/logout'), 'icon' => 'glyphicon glyphicon-log-out', 'visible' => !Yii::app()->user->isGuest),
                    ),
                ),
            ),
        ),
    ),
));
?>
<div class="main-container container-fluid">
    <div class="sidebar">
        <div class="sidebar-shortcuts" id="sidebar-shortcuts" style="padding: 3px 0 3px 0">
            <?
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'size' => 'small',
                'buttons' => array(
                    array('label' => '', 'icon' => 'glyphicon glyphicon-user', 'url' => '#', 'type' => 'success', 'htmlOptions' => array('style' => 'padding: 2px 8px 2px 11px; margin-right:3px;'),),
                    array('label' => '', 'icon' => 'glyphicon glyphicon-wrench', 'url' => '#', 'type' => 'info', 'htmlOptions' => array('style' => 'padding: 2px 8px 2px 11px; margin-right:3px;'),),
                    array('label' => '', 'icon' => 'glyphicon glyphicon-shopping-cart', 'url' => '#', 'type' => 'warning', 'htmlOptions' => array('style' => 'padding: 2px 8px 2px 11px; margin-right:3px;'),),
                    array('label' => '', 'icon' => 'glyphicon glyphicon-search', 'url' => '#', 'type' => 'danger', 'htmlOptions' => array('style' => 'padding: 2px 8px 2px 11px; margin-right:3px;'),),
                ),
            ));
            ?>
        </div>
        <?php
        $this->widget('bootstrap.widgets.TbMenu', array(
                'type' => 'list',
                'htmlOptions' => array(),
                'items' => array(
                    array('label' => 'Рабочий стол', 'icon' => 'glyphicon glyphicon-dashboard', 'url' => array('/site/index')),
                    array('label' => 'Справочники'),

                    array('label' => 'Контрагент', 'icon' => 'glyphicon glyphicon-briefcase', 'url' => array('/kontragent/view/' . Yii::app()->user->kontragent_id)),
                    array('label' => 'Пользователи', 'icon' => 'glyphicon glyphicon-user', 'url' => array('/polzovatel/index')),
                    array('label' => 'Объекты', 'icon' => 'glyphicon glyphicon-home', 'url' => array('/object/index')),
                    array('label' => 'Графики работы', 'icon' => 'glyphicon glyphicon-calendar', 'url' => array('/grafikRaboty/admin/' . Yii::app()->user->kontragent_id)),

                    array('label' => 'Отчёты'),
                    array('label' => 'Приходы с сайта', 'icon' => 'glyphicon glyphicon-stats', 'url' => array('/site/page', 'view' => 'dummy')),
                    array('label' => 'Общая доходность', 'icon' => 'glyphicon glyphicon-stats', 'url' => array('/site/page', 'view' => 'dummy')),
                    array('label' => 'Доходы по объектам', 'icon' => 'glyphicon glyphicon-stats', 'url' => array('/site/page', 'view' => 'dummy')),
                    array('label' => 'Проходимость', 'icon' => 'glyphicon glyphicon-stats', 'url' => array('/site/page', 'view' => 'dummy')),

                    array('label' => 'Инструменты'),
                    array('label' => 'Сообщить об ошибке', 'icon' => 'glyphicon glyphicon-fire', 'url' => array('/site/page', 'view' => 'dummy')),
                    array('label' => 'Справка по системе', 'icon' => 'glyphicon glyphicon-question-sign', 'url' => array('/site/page', 'view' => 'dummy')),
                )
            )
        );
        ?>
    </div>
    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <?
            $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                'separator' => '&raquo;',
                'links' => $this->breadcrumbs));
            ?>
        </div>
        <div class="nav-search" id="nav-search">
            <form class="form-search">
							<span class="input-icon">
								<input type="text" placeholder="Найти.." class="input-small nav-search-input"
                                       id="nav-search-input" autocomplete="off">
								<i class="icon-search nav-search-icon"></i>
							</span>
            </form>
        </div>
        <div class="page-content">
            <?php echo $content; ?>
        </div>
    </div>
</div>
</body>
</html>