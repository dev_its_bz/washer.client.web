<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css"/>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<table style="padding: 0; margin: 0; width: 100%; height: 100%" cellpadding="0" cellspacing="0" border="0"
       class="adminTable">>
    <tr height="10">
        <td colspan="2">
            <?
            $this->widget('bootstrap.widgets.TbNavbar', array(
                'type' => 'inverse', // null or 'inverse'
                'fixed' => 'false',
                'brand' => Yii::app()->name . ' - Управляющий автомойкой',
                'brandOptions' => array('style' => 'background:url(/images/washer_32.png) left no-repeat; background-position: 15px; padding-left:58px; margin-left 20px'),
                'collapse' => false, // requires bootstrap-responsive.css
                'items' => array(
                    array(
                        'class' => 'bootstrap.widgets.TbMenu',
                        'htmlOptions' => array('class' => 'pull-right'),
                        'items' => array(
                            array('label' => 'Сообщить об ошибке', 'url' => array('/site/page', 'view' => 'dummy')),
                            '---',
                            array('label' => 'Документация', 'url' => '#', 'items' => array(
                                array('label' => 'Руководство Администратора системы', 'url' => array('/site/page', 'view' => 'dummy')),
                                array('label' => 'Руководство Владельца бизнеса', 'url' => array('/site/page', 'view' => 'dummy')),
                                array('label' => 'Руководство Управляющего', 'url' => array('/site/page', 'view' => 'dummy')),
                                array('label' => 'Руководство Бухгалтера', 'url' => array('/site/page', 'view' => 'dummy')),
                                array('label' => 'Руководство Работника', 'url' => array('/site/page', 'view' => 'dummy')),
                                array('label' => 'Руководство Клиента', 'url' => array('/site/page', 'view' => 'dummy')),
                            )),
                            array('label' => 'О нас', 'url' => '#', 'items' => array(
                                array('label' => 'Наша команда', 'url' => array('/site/page', 'view' => 'dummy')),
                                array('label' => 'Наш проект', 'url' => array('/site/page', 'view' => 'dummy')),
                                array('label' => 'Наши цели', 'url' => array('/site/page', 'view' => 'dummy')),
                                '---',
                                array('label' => 'Контакты', 'url' => array('/site/page', 'view' => 'dummy')),
                            )),
                            array('label' => 'Вход', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                            array('label' => 'Выход (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest),
                        ),
                    ),
                ),
            ));
            ?>
        </td>
    </tr>
    <tr height="40">
        <td style="text-align: center;">
            <div class="flatBar" style="height: 100%; padding-top: 3px;">                <?
                $this->widget('bootstrap.widgets.TbButtonGroup', array(
                    'buttons' => array(
                        array('label' => '', 'icon' => 'user', 'url' => '#', 'type' => 'success'),
                        array('label' => '', 'icon' => 'wrench', 'url' => '#', 'type' => 'info'),
                        array('label' => '', 'icon' => 'shopping-cart', 'url' => '#', 'type' => 'danger'),
                        array('label' => '', 'icon' => 'search', 'url' => '#', 'type' => 'primary'),
                    ),
                ));
                ?>
            </div>
        </td>
        <?php if (@$this->breadcrumbs != null) { ?>
            <td valign="middle" align="left">
                <div class="flatBar" style="height: 100%; padding-top: 1px;">
                    <?
                    $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                        'separator' => '&raquo;',
                        'links' => $this->breadcrumbs));
                    ?>
                </div>
            </td>
        <?
        } else {
            ?>
            <td valign="top" align="left" style="padding-left: 20px; border: none;" rowspan="2">
                <?php echo $content; ?>
            </td>
        <?
        }
        ?>
    </tr>
    <tr>
        <td width="10" valign="top">
            <div class="flatBar" style="height: 100%; border-top: none; padding-top: 10px; border-bottom: none">
                <nobr>
                    <?php
                    $this->widget('bootstrap.widgets.TbMenu', array(
                            'type' => 'list',
                            'items' => array(
                                array('label' => 'Рабочий стол', 'icon' => 'home', 'url' => array('/site/index')),
                                '---',
                                array('label' => 'Справочники'),

                                array('label' => 'Главная', 'icon' => 'briefcase', 'url' => array('/object/sampleOfObj')),
                                //array('label' => 'Объекты', 'icon' => 'home', 'url' => array('/object/index')),
                                //array('label' => 'Пользователи', 'icon' => 'user', 'url' => array('/polzovatel/index')),
                                //array('label' => 'Услуги', 'icon' => 'tasks', 'url' => array('/services/admin')),
                                //array('label' => 'Группы авто', 'icon' => 'camera', 'url' => array('/carGruppa/index/2')),
                                //array('label' => 'График работы', 'icon' => 'calendar', 'url' => array('/grafikRaboty/index/1')),
                                //   array('label' => 'Администрирование'),
                                //   array('label' => 'Города', 'icon' => 'calendar', 'url' => array('/gorod/admin')),
                                //        array('label' => 'Опции', 'icon' => 'calendar', 'url' => array('/opcii/admin')),
                                //   array('label' => 'Марки и модели авто', 'icon' => 'bullhorn', 'url' => array('/carMarka/admin')),
                                //      array('label' => 'Группы авто', 'icon' => 'bullhorn', 'url' => array('/carGruppa/index/18')),
                                '---',
                                array('label' => 'Статистика '),
                                array('label' => 'Приходы с сайта', 'icon' => 'question-sign', 'url' => array('/site/page', 'view' => 'dummy')),
                                array('label' => 'Общая доходность', 'icon' => 'question-sign', 'url' => array('/site/page', 'view' => 'dummy')),
                                array('label' => 'Доходы по объектам', 'icon' => 'question-sign', 'url' => array('/site/page', 'view' => 'dummy')),
                                array('label' => 'Проходимость', 'icon' => 'question-sign', 'url' => array('/site/page', 'view' => 'dummy')),
                                '---',
                                array('label' => 'Администрирование'),
                                array('label' => 'Системный журнал', 'icon' => 'book', 'url' => array('/systemLog/index')),
                            )
                        )
                    );
                    ?>
                </nobr>
            </div>
        </td>
        <? if (@$this->breadcrumbs != null) { ?>
            <td valign="top" align="left" style="padding-left: 20px; padding-right: 20px;">
                <?php echo $content; ?>
            </td>
        <?
        }
        ?>
    </tr>
    <tr height="10">
        <td colspan="2"
            style=" text-align: right; padding-right: 10px;  padding-top: 2px; border-top: 1px solid #e5e5e5;">
            <?= "Разработано &copy; " . date('Y') . "<a href='http://its.bz' target='_blank'> АйТиЭс</a>&trade; " . Yii::powered() ?>
        </td>
    </tr>
</table>
</body>
</html>