<?php
$this->breadcrumbs = array(
    'Мои автомобили' => array('/polzovatel/privateOffice'),
    $model->carModel->carMarka->nazvanie . " " . $model->carModel->nazvanie,
);
?>
    <div class="page-header">
        <h1><?= $model->carModel->carMarka->nazvanie . " " . $model->carModel->nazvanie ?></h1>
    </div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>