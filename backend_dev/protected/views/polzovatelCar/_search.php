<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'car_model_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'polzovatel_id',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'gos_nomer',array('class'=>'span5','maxlength'=>30)); ?>

		<?php echo $form->textFieldRow($model,'foto',array('class'=>'span5','maxlength'=>100)); ?>

		<?php echo $form->textFieldRow($model,'data_moiki',array('class'=>'span5')); ?>

		<?php echo $form->textFieldRow($model,'deleted',array('class'=>'span5')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
