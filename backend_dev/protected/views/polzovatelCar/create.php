<?php
$this->breadcrumbs = array(
    'Мои автомобили' => array('/polzovatelCar/index'),
    'Новый автомобиль',
);
?>
    <div class="page-header">
        <h1>Новый автомобиль</h1>
    </div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>