<?php
$this->breadcrumbs=array(
	'Polzovatel Cars'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List PolzovatelCar','url'=>array('index')),
array('label'=>'Create PolzovatelCar','url'=>array('create')),
array('label'=>'Update PolzovatelCar','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete PolzovatelCar','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage PolzovatelCar','url'=>array('admin')),
);
?>

<h1>View PolzovatelCar #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'car_model_id',
		'polzovatel_id',
		'gos_nomer',
		'foto',
		'data_moiki',
		'deleted',
),
)); ?>
