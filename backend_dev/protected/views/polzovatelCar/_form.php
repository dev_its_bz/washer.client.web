<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'polzovatel-car-form',
    'enableAjaxValidation' => false,
)); ?>

<?php echo $form->errorSummary($model); ?>


<?
echo $form->dropDownList($model->carModel, 'car_marka_id', CHtml::listData(CarMarka::model()->findAll(array('order'=>'nazvanie ASC')), 'id', 'nazvanie'),
    array(
        'prompt' => 'Марка автомобиля',
        'ajax' => array(
            'type' => 'POST',
            'url' => '/CarModel/ComboModelByMarka',
            'data' => array('car_marka_id' => 'js:this.value'),
            'update' => '#PolzovatelCar_car_model_id',),)
);

echo $form->dropDownList($model, 'car_model_id', CHtml::listData(CarModel::model()->findAllByAttributes(array('car_marka_id' => $model->carModel->car_marka_id), array('order'=>'nazvanie ASC')), 'id', 'nazvanie'),
    array('prompt' => 'Модель автомобиля'));
/*
$this->widget('TbEditableField', array(
    'type' => 'select',
    'model' => $model,
    'attribute' => 'carModel.car_marka_id',
    'url' => '/polzovatelCar/updateZnachenie',
    'ajax' => array(
        'type' => 'POST',
        'url' => '/carModel/',
        'data' => array('car_marka' => 'js:this.value'),
        'update' => '#city_name'
    ),
    'source' => CHtml::listData(CarMarka::model()->findAll(), 'id', 'nazvanie')
));
/*
$this->widget('TbEditableField', array(
    'type' => 'select2',
    'id' => 'carModel22',
    'model' => $model,
    'attribute' => 'car_model_id',
    'url' => '',
    'source' => CHtml::listData(CarModel::model()->findAll(), 'id', 'nazvanie')
));
*/
?>
<?php echo $form->textFieldRow($model, 'gos_nomer', array('class' => 'span5', 'maxlength' => 30)); ?>

<hr/>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
)); ?>

<?php $this->endWidget(); ?>
