<?php
$this->breadcrumbs = array(
    'Мои автомобили',
);
?>
<?
if (!isset($onlyBody)) {
    ?>
    <div class="page-header">
        <h1> Мои автомобили
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'link',
                    'type' => 'success',
                    'icon' => 'plus',
                    'size' => 'mini',
                    'url' => '/polzovatelCar/create',
                    'htmlOptions' => array(
                        'data-toggle' => "tooltip",
                        'data-original-title' => "Добавить новый автомобиль"
                    )
                )
            );
            ?>
        </h1>
    </div>
<?
}
?>

<?
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'polzovatel-car-grid',
    'dataProvider' => $model->search(),
    'type' => 'striped',
    'columns' => array(
        array(
            'header' => 'Автомобиль',
            'type' => 'raw',
            'value' => '$data->carModel->carMarka->nazvanie." ".$data->carModel->nazvanie."<br/> ".$data->gos_nomer',
        ),
        array(
            'header' => 'Ценовая группа',
            'value' => '$data->carModel->carGruppa->nazvanie',
            'htmlOptions' => array('style' => 'vertical-align:middle'),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{delete}',
            'htmlOptions' => array('style' => 'vertical-align:middle'),
            'buttons' => array
            (
                'update' => array
                (
                    'url' => 'Yii::app()->createUrl("/polzovatelCar/update", array("id"=>$data->id))',
                    'options' => array(
                        'style' => 'padding:2px'
                    ),
                    'htmlOptions' => array('style' => 'padding-right:10px'),
                ),
                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("/polzovatelCar/delete", array("id"=>$data->id))',
                    'options' => array(
                        'style' => 'padding:2px'
                    ),
                ),
            ),
        ),
    ),
));
?>