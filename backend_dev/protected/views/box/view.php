<?php
$this->breadcrumbs=array(
	'Boxes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Box','url'=>array('index')),
	array('label'=>'Create Box','url'=>array('create')),
	array('label'=>'Update Box','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Box','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Box','url'=>array('admin')),
);
?>

<h1>View Box #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nazvanie',
		'opisanie',
		'object_id',
		'status_id',
		'data_sozdaniya',
		'avtor_zapisi_id',
		'data_zapisi',
	),
)); ?>
