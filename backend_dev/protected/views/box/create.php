<?php
$model->object_id = $_GET['id'];
$this->breadcrumbs = array(
    'Контрагенты' => '/kontragent/',
    $model->object->kontragenty->nazvanie => array('/kontragent/' . $model->object->kontragenty->id),
    'Объекты' => array("/kontragent/{$model->object->kontragenty->id}?tab=3"),
    $model->object->nazvanie => array("/object/view/" . $model->object->id),
    'Боксы' => array("/object/view/" . $model->object->id . "?tab=6"),
    'Новый'
);
?>

    <div class="page-header">
        <h1>Новый бокс</h1>
    </div>

<?
$model->object_id = $_GET['id'];
$model->avtor_zapisi_id = Yii::app()->user->id;
?>
<?php echo $this->renderPartial('_form', array('model' => $model)); ?>