<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('box-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
$url = $this->createUrl('box/ChangeStatus');
?>
<script>
    $(document).ready(function () {
        $('.box_status').change(function () {

            $.post('<?php echo $url ?>',
                {
                    id_box: $(this).attr('id'),
                    id_stat: $(this).val()
                });
        });
    });
</script>
<div class="page-header">
    <h1>Боксы
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons' => array(
                array('buttonType' => 'link', 'type' => 'success', 'icon' => 'plus white', 'url' => '/box/create/'.$model->object_id),
                array('buttonType' => 'link', 'type' => 'primary', 'icon' => 'search white', 'url' => '#', 'htmlOptions' => array('class' => 'search-button')),
            )));
        ?>
    </h1>
</div>

<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'box-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'nazvanie',
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'editable' => array(
                'url' => '/Box/UpdateZnachenie',
            ),
        ),
        array(
            'name' => 'opisanie',
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'editable' => array(
                'url' => '/Box/UpdateZnachenie',
            ),
        ),
        array(
            'name' => 'status_id',
            'class' => 'bootstrap.widgets.TbEditableColumn',
            'filter' => CHtml::listData(BoxStatus::model()->findAll(), "id", "nazvanie"),
            'value' => '$data->status->nazvanie',
            'editable' => array(
                'type' => 'select',
                'source' => CHtml::listData(BoxStatus::model()->findAll(), "id", "nazvanie"),
                'url' => '/Box/UpdateZnachenie',
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons' => array
            (
                'delete' => array
                (
                    'url' => 'Yii::app()->createUrl("/box/delete", array("id"=>$data->id))',
                    'options' => array(
                        'class' => 'btn-column',
                    ),
                ),
            ),
        ),
    ),
)); ?>
