<?php
$this->breadcrumbs = array(
    'Контрагенты' => '/kontragent/',
    $model->object->kontragenty->nazvanie => array('/kontragent/' . $model->object->kontragenty->id),
    'Объекты' => array("/kontragent/{$model->object->kontragenty->id}?tab=3"),
    $model->object->nazvanie => array("/object/view/" . $model->object->id),
    'Боксы' => array("/object/view/" . $model->object->id . "?tab=6"),
    $model->nazvanie
);
?>

    <div class="page-header">
        <h1>Редактор бокса &laquo;<?=$model->nazvanie?>&raquo;</h1>
    </div>


<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>