<div class="alert alert-info glyphicon-info-sign">Поля, отмеченные <span class="required">*</span> обязательны к
    заполнению.
</div>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'box-form',
    'htmlOptions' => array('class' => 'well'),
    'enableAjaxValidation' => false,
    'focus' => array($model, 'nazvanie'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'nazvanie', array('class' => 'span5', 'maxlength' => 30)); ?>

<?php echo $form->textAreaRow($model, 'opisanie', array('rows' => 6, 'cols' => 50, 'class' => 'span8')); ?>

<?php echo $form->hiddenField($model, 'object_id'); ?>

<div class="row2">
    <?php echo $form->labelEx($model, 'Статус'); ?>
    <?php echo $form->dropDownList($model, 'status_id', BoxStatus::model()->getForFilter()); ?>
    <?php echo $form->error($model, 'status_id'); ?>
</div>

<?php echo $form->hiddenField($model, 'avtor_zapisi_id'); ?>

<hr/>
<?php $this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'submit',
    'icon' => 'check white',
    'type' => 'primary',
    'label' => $model->isNewRecord ? 'Создать' : 'Сохранить',
)); ?>

<?
$this->widget('bootstrap.widgets.TbButton', array(
    'buttonType' => 'button',
    'type' => 'danger',
    'icon' => 'remove white',
    'label' => 'Отмена',
    'htmlOptions' => array(
        'onClick' => "window.location.href = '/object/view/" . $model->object_id . "?tab=6'",
    )
));
?>

<?php $this->endWidget(); ?>
