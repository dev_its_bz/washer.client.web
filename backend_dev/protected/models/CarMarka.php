<?php

/**
 * This is the model class for table "car_marka".
 *
 * The followings are the available columns in table 'car_marka':
 * @property integer $id
 * @property string $nazvanie
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi_id
 * @property string $data_zapisi
 */
class CarMarka extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'car_marka';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //      array('data_sozdaniya', 'required'),
            array('avtor_zapisi_id', 'numerical', 'integerOnly' => true),
            array('nazvanie', 'length', 'max' => 30),
            array('nazvanie', 'required'),
            array('nazvanie', 'compare', 'compareValue' => '---', 'operator' => '!='),
            //array('nazvanie', 'exist', 'attributeName' => 'nazvanie', 'className' => 'CarMarka'),
            array('nazvanie', 'filter', 'filter' => array($this, 'checkName')),
            array('data_zapisi', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nazvanie,  data_sozdaniya, avtor_zapisi_id, data_zapisi', 'safe', 'on' => 'search'),
        );
    }

    public function checkName($value)
    {
        if ($this->exists('nazvanie=:id', array(':id' => $value)))
            $this->addError('nazvanie', 'Такая марка уже есть!');
        return $value;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'carModel' => array(self::HAS_MANY, 'CarModel', 'car_marka_id'),
          
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nazvanie' => 'Марка',
            'data_sozdaniya' => 'Data Sozdaniya',
            'avtor_zapisi_id' => 'Avtor Zapisi',
            'data_zapisi' => 'Data Zapisi',
            'carModel' => 'Модели',
        );
    }

    function getModelNames()
    {
        $ARs = $this->carModel; //массив AR записей актеров
        $arr = CHtml::listData($ARs, 'id', 'nazvanie'); //преобразуем в обычный массив id=>name (если имя актера хранится не в поле name то поправь)
        return implode(", ", $arr); //склеиваем имена через запятую
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nazvanie', $this->nazvanie, true);
        $criteria->compare('data_sozdaniya', $this->data_sozdaniya, true);
        $criteria->compare('avtor_zapisi_id', $this->avtor_zapisi_id);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 15),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CarMarka the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getName()
    {
        $model = CarMarka::model()->findAll();
        return $model;
    }

}
