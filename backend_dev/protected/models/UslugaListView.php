<?php

/**
 * This is the model class for table "usluga_list_view".
 *
 * The followings are the available columns in table 'usluga_list_view':
 * @property integer $id_usluga
 * @property integer $id_object
 * @property integer $car_gruppa_id
 * @property string $nazvanie
 * @property double $cena
 * @property integer $dlitelnost
 */
class UslugaListView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usluga_list_view';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_usluga, id_object, car_gruppa_id', 'required'),
			array('id_usluga, id_object, car_gruppa_id, dlitelnost', 'numerical', 'integerOnly'=>true),
			array('cena', 'numerical'),
			array('nazvanie', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_usluga, id_object, car_gruppa_id, nazvanie, cena, dlitelnost', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_usluga' => 'Id Usluga',
			'id_object' => 'Id Object',
			'car_gruppa_id' => 'Car Gruppa',
			'nazvanie' => 'Nazvanie',
			'cena' => 'Cena',
			'dlitelnost' => 'Dlitelnost',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_usluga',$this->id_usluga);
		$criteria->compare('id_object',$this->id_object);
		$criteria->compare('car_gruppa_id',$this->car_gruppa_id);
		$criteria->compare('nazvanie',$this->nazvanie,true);
		$criteria->compare('cena',$this->cena);
		$criteria->compare('dlitelnost',$this->dlitelnost);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UslugaListView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
