<?php

/**
 * This is the model class for table "review_list_view".
 *
 * The followings are the available columns in table 'review_list_view':
 * @property integer $id
 * @property integer $object_id
 * @property string $fio
 * @property integer $likes
 * @property integer $dislikes
 * @property string $review
 * @property string $data_sozdaniya
 */
class ReviewListView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'review_list_view';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('object_id', 'required'),
			array('id, object_id, likes, dislikes', 'numerical', 'integerOnly'=>true),
			array('fio', 'length', 'max'=>100),
			array('review', 'length', 'max'=>200),
			array('data_sozdaniya', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, object_id, fio, likes, dislikes, review, data_sozdaniya', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'object_id' => 'Object',
			'fio' => 'Fio',
			'likes' => 'Likes',
			'dislikes' => 'Dislikes',
			'review' => 'Review',
			'data_sozdaniya' => 'Data Sozdaniya',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('object_id',$this->object_id);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('likes',$this->likes);
		$criteria->compare('dislikes',$this->dislikes);
		$criteria->compare('review',$this->review,true);
		$criteria->compare('data_sozdaniya',$this->data_sozdaniya,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReviewListView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
