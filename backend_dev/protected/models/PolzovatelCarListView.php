<?php

/**
 * This is the model class for table "polzovatel_car_list_view".
 *
 * The followings are the available columns in table 'polzovatel_car_list_view':
 * @property integer $car_id
 * @property string $car_marka
 * @property string $car_model
 * @property integer $car_model_id
 * @property string $gos_nomer
 * @property string $car_photo
 * @property integer $polzovatel_id
 * @property integer $last_wash
 */
class PolzovatelCarListView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'polzovatel_car_list_view';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('car_model_id, polzovatel_id', 'required'),
			array('car_id, car_model_id, polzovatel_id, last_wash', 'numerical', 'integerOnly'=>true),
			array('car_marka, car_model, gos_nomer', 'length', 'max'=>30),
			array('car_photo', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('car_id, car_marka, car_model, car_model_id, gos_nomer, car_photo, polzovatel_id, last_wash', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'car_id' => 'Car',
			'car_marka' => 'Марка название',
			'car_model' => 'Марка название',
			'car_model_id' => 'Car Model',
			'gos_nomer' => 'Gos Nomer',
			'car_photo' => 'Car Photo',
			'polzovatel_id' => 'Polzovatel',
			'last_wash' => 'Last Wash',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('car_id',$this->car_id);
		$criteria->compare('car_marka',$this->car_marka,true);
		$criteria->compare('car_model',$this->car_model,true);
		$criteria->compare('car_model_id',$this->car_model_id);
		$criteria->compare('gos_nomer',$this->gos_nomer,true);
		$criteria->compare('car_photo',$this->car_photo,true);
		$criteria->compare('polzovatel_id',$this->polzovatel_id);
		$criteria->compare('last_wash',$this->last_wash);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PolzovatelCarListView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
