<?php

/**
 * This is the model class for table "kontragent".
 *
 * The followings are the available columns in table 'kontragent':
 * @property integer $id
 * @property string $nazvanie
 * @property string $nazvanie_ur
 * @property string $nazvanie_ur_kr
 * @property string $inn
 * @property string $kpp
 * @property string $okpo
 * @property string $ogrn
 * @property string $bank
 * @property string $bik
 * @property string $ks
 * @property string $rs
 * @property string $site
 * @property integer $status_id
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi
 * @property string $data_zapisi
 */
class Kontragent extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'kontragent';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('data_sozdaniya', 'required'),
            array('status_id, avtor_zapisi', 'numerical', 'integerOnly' => true),
            array('nazvanie, nazvanie_ur_kr, bank, site', 'length', 'max' => 100),
            array('nazvanie_ur', 'length', 'max' => 255),
            array('inn, kpp, okpo, ogrn, bik, ks, rs', 'length', 'max' => 30),
            array('data_zapisi', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_stat, id, kontragent.nazvanie, nazvanie_ur, nazvanie_ur_kr, inn, kpp, okpo, ogrn, bank, bik, ks, rs, site, status_id, data_sozdaniya, avtor_zapisi, data_zapisi', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array('status' => array(self::BELONGS_TO, 'KontragentStatus', 'status_id'),
            'polzovatel' => array(self::HAS_MANY, 'Polzovatel', 'kontragent_id'),
            'objects' => array(self::HAS_MANY, 'Object', 'kontragent_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nazvanie' => 'Название',
            'nazvanie_ur' => 'Юр. наименование',
            'nazvanie_ur_kr' => 'Юр. наименование (краткое)',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'okpo' => 'ОКПО',
            'ogrn' => 'ОГРН',
            'bank' => 'Банк',
            'bik' => 'БИК',
            'ks' => 'К/С',
            'rs' => 'Р/С',
            'site' => 'Сайт',
            'status_id' => 'Статус',
            'data_sozdaniya' => 'Дата создания',
            'avtor_zapisi' => 'Автор записи',
            'data_zapisi' => 'Дата записи',
            'status.nazvanie' => 'Статус',
            'id_stat' => 'Cтатус',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    //    public $status_nazvanie='';
    public $id_stat = '';

    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('status');
        $criteria->together = true;

        $criteria->compare('id', $this->id);
        $criteria->compare('t.nazvanie', $this->nazvanie, true);
        $criteria->compare('nazvanie_ur', $this->nazvanie_ur, true);
        $criteria->compare('nazvanie_ur_kr', $this->nazvanie_ur_kr, true);
        $criteria->compare('inn', $this->inn, true);
        $criteria->compare('okpo', $this->okpo, true);
        $criteria->compare('ogrn', $this->ogrn, true);
        $criteria->compare('bank', $this->bank, true);
        $criteria->compare('bik', $this->bik, true);
        $criteria->compare('ks', $this->ks, true);
        $criteria->compare('rs', $this->rs, true);
        $criteria->compare('site', $this->site, true);
        $criteria->compare('data_sozdaniya', $this->data_sozdaniya, true);
        $criteria->compare('avtor_zapisi', $this->avtor_zapisi);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);
        $criteria->compare('status_id', '=' . $this->id_stat, true);
        //   $criteria->compare('status.nazvanie',$this->status_nazvanie,true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 15),
            'sort' => array(
                'attributes' => array(
                    '*',
                    'id_stat' => array(
                        'asc' => 'status_id ASC',
                        'desc' => 'status_id DESC',
                    ),
                ),
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Kontragent the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
