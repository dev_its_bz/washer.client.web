<?php

/**
 * This is the model class for table "object".
 *
 * The followings are the available columns in table 'object':
 * @property integer $id
 * @property string $nazvanie
 * @property string $adres
 * @property string $telefon
 * @property integer $grafik_raboty_id
 * @property integer $zarplata
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi_id
 * @property string $data_zapisi
 */
class Object extends CActiveRecord {

    public $columGorod;
    public $k_status;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'object';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //	array('data_sozdaniya, data_zapisi', 'required'),
            array('grafik_raboty_id, zarplata, avtor_zapisi_id', 'numerical', 'integerOnly' => true),
            //array('grafik_raboty_id'),
            array('gorod_id', 'required'),
            array('nazvanie', 'safe'),
            array('telefon', 'length', 'max' => 30),
            array('adres', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, k_status, columGorod, name, kontragent_id, nazvanie, adres, telefon, grafik_raboty_id, zarplata, data_sozdaniya, avtor_zapisi_id, data_zapisi', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function behaviors() {
        return array(
            'activerecord-relation' => array(
                'class' => 'ext.yiiext.behaviors.activerecord-relation.EActiveRecordRelationBehavior',
        ));
    }

    public function getByServices($serviceList) {
       // return $model = UslugaObject::model()->findAllByAttributes(array('id' => $serviceList, 'distinct' => true));
        $dataProvider = new CActiveDataProvider('UslugaObject');
         $dataProvider->criteria->select = 'id_object';
        $dataProvider->criteria->addInCondition('id_usluga', array(26,14));
        $dataProvider->criteria->distinct = true;
       
        var_dump($dataProvider->getItemCount());
        
        return $dataProvider;
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'uslugi' => array(self::HAS_MANY, 'Services', 'object_id'),
            'polzovateli' => array(self::MANY_MANY, 'Polzovatel', 'polzovatel__object(object_id, polzovatel_id)'),
            'kontragenty' => array(self::BELONGS_TO, 'Kontragent', 'kontragent_id'),
            'grafik' => array(self::HAS_ONE, 'GrafikRaboty', array('id' => 'grafik_raboty_id', 'kontragent_id' => 'kontragent_id')),
            'gorod' => array(self::BELONGS_TO, 'Gorod', 'gorod_id'),
            'zapisi' => array(self::HAS_MANY, 'Zapis', 'object_id'),
        );
    }

    public function getForFilter() { //список для фильтра
        return CHtml::listData(
                        self::model()->findAll(array(
                            'select' => array('id', 'nazvanie')
                        )), 'id', 'nazvanie'
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nazvanie' => 'Название',
            'adres' => 'Адрес',
            'telefon' => 'Телефон',
            'grafik_raboty_id' => 'График работы',
            'zarplata' => '% от суммы работы рабочему',
            'data_sozdaniya' => 'Data Sozdaniya',
            'avtor_zapisi_id' => 'Avtor Zapisi',
            'data_zapisi' => 'Data Zapisi',
            'kontragent_id' => 'kontragent id',
            'grafik.nazvanie' => 'Название графика работы',
            'gorod_id' => 'Город',
            'columGorod' => 'Город',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public $k_id = '';
    public $kontragent_id = '';
    public $gorod_name = '';

    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->with = array('gorod');
        $criteria->together = true;
        $criteria->addSearchCondition('gorod.id', $this->columGorod);
        // $criteria->compare('gorod.nazvanie_r', $this->gorod_name, true);
        $criteria->with = array('kontragenty');
        $criteria->together = true;
        $criteria->addSearchCondition('kontragenty.status_id', $this->k_status);

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.nazvanie', $this->nazvanie, true);
        $criteria->compare('adres', $this->adres, true);
        $criteria->compare('telefon', $this->telefon, true);
        $criteria->compare('grafik_raboty_id', $this->grafik_raboty_id);
        $criteria->compare('zarplata', $this->zarplata);
        $criteria->compare('data_sozdaniya', $this->data_sozdaniya, true);
        $criteria->compare('avtor_zapisi_id', $this->avtor_zapisi_id);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);
        $criteria->compare('kontragent_id', $this->kontragent_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchWithFilter($type) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;




        $criteria->with = array('zapisi', 'kontragenty');
        $criteria->together = true;
        $criteria->addSearchCondition('kontragenty.status_id', $this->k_status);

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.nazvanie', $this->nazvanie, true);
        $criteria->compare('adres', $this->adres, true);
        $criteria->compare('telefon', $this->telefon, true);
        $criteria->compare('grafik_raboty_id', $this->grafik_raboty_id);
        $criteria->compare('zarplata', $this->zarplata);
        $criteria->compare('data_sozdaniya', $this->data_sozdaniya, true);
        $criteria->compare('avtor_zapisi_id', $this->avtor_zapisi_id);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);
        $criteria->compare('kontragent_id', $this->kontragent_id, true);
        if ($type == 1) {
            $date = date('Y-m-d');
            $time = '16:45:00'; // date('H-i-s');
            // $criteria->order= "telefon ASC"; else $criteria->order= "telefon DESC";
            $criteria->addCondition("t.id NOT IN (SELECT  object.id AS `_id` FROM object INNER JOIN zapis ON object.id=zapis.object_id "
                    . "WHERE (zapis.time_begin<'$time') AND (zapis.time_end>'$time') AND (zapis.zapis_date='$date') GROUP BY object.id "
                    . "HAVING COUNT(object.id)=(SELECT COUNT(box.id) FROM box WHERE (box.`object_id`=`_id`) AND (box.`status_id`=1)))");
        }


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 15),
        ));
    }

    public function searchById($id) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('nazvanie', $this->nazvanie, true);
        $criteria->compare('adres', $this->adres, true);
        $criteria->compare('telefon', $this->telefon, true);
        $criteria->compare('zarplata', $this->zarplata);
        $criteria->compare('kontragent_id', $id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return object the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
