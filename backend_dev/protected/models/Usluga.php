<?php

/**
 * This is the model class for table "usluga".
 *
 * The followings are the available columns in table 'usluga':
 * @property integer $id
 * @property string $nazvanie
 * @property string $opisanie
 * @property integer $usluga_tip_id
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi_id
 * @property string $data_zapisi
 *
 * The followings are the available model relations:
 * @property UslugaTip $uslugaTip
 * @property UslugaCarGruppa[] $uslugaCarGruppas
 * @property UslugaObject[] $uslugaObjects
 */
class Usluga extends CActiveRecord
{
    public $type_name;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'usluga';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(

            array('usluga_tip_id, avtor_zapisi_id', 'numerical', 'integerOnly' => true),
            array('nazvanie', 'length', 'max' => 255),
            array('opisanie', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nazvanie, opisanie, usluga_tip_id, data_sozdaniya, avtor_zapisi_id, data_zapisi', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'uslugaTip' => array(self::BELONGS_TO, 'UslugaTip', 'usluga_tip_id'),
            'uslugi' => array(self::HAS_MANY, 'UslugaObject', 'id_usluga')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nazvanie' => 'Наименование',
            'opisanie' => 'Описание',
            'usluga_tip_id' => 'Тип',
            'data_sozdaniya' => 'Data Sozdaniya',
            'avtor_zapisi_id' => 'Avtor Zapisi',
            'data_zapisi' => 'Data Zapisi',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('uslugaTip');
        // $criteria->together= true;
        $criteria->compare('uslugaTip.nazvanie', $this->type_name, true);
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.nazvanie', $this->nazvanie, true);
        $criteria->compare('opisanie', $this->opisanie, true);
        $criteria->compare('usluga_tip_id', $this->usluga_tip_id);
        $criteria->compare('t.data_sozdaniya', $this->data_sozdaniya, true);
        $criteria->compare('t.avtor_zapisi_id', $this->avtor_zapisi_id);
        $criteria->compare('t.data_zapisi', $this->data_zapisi, true);
        $criteria->order = "usluga_tip_id ASC, t.nazvanie ASC";

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public $inObj = 0;

    public function getForCheckBox($obj_id, $group)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->select = 't.*,if(b.cena,1,0) as inObj';
        $criteria->join = 'LEFT JOIN usluga_object as b ON b.id_usluga=t.id and b.car_gruppa_id=:group and b.id_object=:obj_id';
        // $criteria->condition = 'b.id_object=:obj_id';
        $criteria->params += array(':obj_id' => $obj_id, ':group' => $group);

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.nazvanie', $this->nazvanie, true);
        $criteria->compare('t.opisanie', $this->opisanie, true);
        $criteria->compare('usluga_tip_id', $this->usluga_tip_id);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Usluga the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
