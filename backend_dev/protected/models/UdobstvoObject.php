<?php

/**
 * This is the model class for table "udobstvo_object".
 *
 * The followings are the available columns in table 'udobstvo_object':
 * @property integer $id
 * @property integer $udobstvo_id
 * @property integer $object_id
 * @property integer $avtor_zapisi_id
 * @property string $data_sozdaniya
 * @property string $data_zapisi
 *
 * The followings are the available model relations:
 * @property Object $object
 * @property Udobstvo $udobstvo
 */
class UdobstvoObject extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'udobstvo_object';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('udobstvo_id, object_id', 'required'),
			array('udobstvo_id, object_id, avtor_zapisi_id', 'numerical', 'integerOnly'=>true),
			array('data_zapisi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, udobstvo_id, object_id, avtor_zapisi_id, data_sozdaniya, data_zapisi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'object' => array(self::BELONGS_TO, 'Object', 'object_id'),
			'udobstvo' => array(self::BELONGS_TO, 'Udobstvo', 'udobstvo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'udobstvo_id' => 'Udobstvo',
			'object_id' => 'Object',
			'avtor_zapisi_id' => 'Avtor Zapisi',
			'data_sozdaniya' => 'Data Sozdaniya',
			'data_zapisi' => 'Data Zapisi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('udobstvo_id',$this->udobstvo_id);
		$criteria->compare('object_id',$this->object_id);
		$criteria->compare('avtor_zapisi_id',$this->avtor_zapisi_id);
		$criteria->compare('data_sozdaniya',$this->data_sozdaniya,true);
		$criteria->compare('data_zapisi',$this->data_zapisi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UdobstvoObject the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
