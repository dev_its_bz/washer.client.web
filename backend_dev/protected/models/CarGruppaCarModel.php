<?php

/**
 * This is the model class for table "car_gruppa__car_model".
 *
 * The followings are the available columns in table 'car_gruppa__car_model':
 * @property integer $id
 * @property integer $car_gruppa_id
 * @property integer $car_model_id
 * @property string $data_sozdaniya
 * @property string $avtor_zapisi_id
 * @property string $data_zapisi
 */
class CarGruppaCarModel extends CActiveRecord {

    public $searchModel;
    public $columMarka;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'car_gruppa__car_model';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('data_sozdaniya, data_zapisi', 'required'),
            array('car_gruppa_id, car_model_id', 'numerical', 'integerOnly' => true),
            array('avtor_zapisi_id', 'length', 'max' => 11),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('columMarka,searchModel,id, car_gruppa_id, car_model_id, data_sozdaniya, avtor_zapisi_id, data_zapisi', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'carmodels' => array(self::BELONGS_TO, 'CarModel', array('car_model_id' => 'id')),
            'cargruppa' => array(self::BELONGS_TO, 'CarGruppa', array('car_gruppa_id' => 'id')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'car_gruppa_id' => 'Car Gruppa',
            'car_model_id' => 'Car Model',
            'data_sozdaniya' => 'Data Sozdaniya',
            'avtor_zapisi_id' => 'Avtor Zapisi',
            'data_zapisi' => 'Data Zapisi',
            'searchModel' => 'Название модели',
            'columMarka' => 'Название марки',
            'cargruppa.nazvanie' => 'Название группы',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($id) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->with = array('carmodels');
        $criteria->together = true;
        $criteria->addSearchCondition('carmodels.nazvanie', $this->searchModel);

        $criteria->with = array('carmodels.carmarks');
        $criteria->together = true;
        $criteria->addSearchCondition('carmarks.id', $this->columMarka,false);

        $criteria->compare('car_gruppa_id', $id);

        $criteria->compare('id', $this->id);
        $criteria->compare('car_gruppa_id', $this->car_gruppa_id);
        //$criteria->compare('car_model_id',$this->car_model_id);
        $criteria->compare('data_sozdaniya', $this->data_sozdaniya, true);
        $criteria->compare('avtor_zapisi_id', $this->avtor_zapisi_id, true);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 20),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CarGruppaCarModel the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
