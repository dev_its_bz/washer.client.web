<?php

/**
 * This is the model class for table "car_model_list_view".
 *
 * The followings are the available columns in table 'car_model_list_view':
 * @property integer $car_model_id
 * @property string $car_model_nazvanie
 * @property integer $car_marka_id
 */
class CarModelListView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_model_list_view';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('car_model_id, car_marka_id', 'numerical', 'integerOnly'=>true),
			array('car_model_nazvanie', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('car_model_id, car_model_nazvanie, car_marka_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'car_model_id' => 'Car Model',
			'car_model_nazvanie' => 'Марка название',
			'car_marka_id' => 'Марка ид',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('car_model_id',$this->car_model_id);
		$criteria->compare('car_model_nazvanie',$this->car_model_nazvanie,true);
		$criteria->compare('car_marka_id',$this->car_marka_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CarModelListView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
