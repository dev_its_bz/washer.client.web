<?php

/**
 * This is the model class for table "box".
 *
 * The followings are the available columns in table 'box':
 * @property integer $id
 * @property string $nazvanie
 * @property string $opisanie
 * @property integer $object_id
 * @property integer $status_id
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi_id
 * @property string $data_zapisi
 */
class Box extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'box';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('data_sozdaniya, data_zapisi', 'required'),
			array('object_id, status_id, avtor_zapisi_id', 'numerical', 'integerOnly'=>true),
			array('nazvanie', 'length', 'max'=>30),
			array('opisanie', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nazvanie, opisanie, object_id, status_id, data_sozdaniya, avtor_zapisi_id, data_zapisi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'object' => array(self::BELONGS_TO, 'Object', 'object_id'),
		    'status' => array(self::BELONGS_TO, 'BoxStatus', 'status_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nazvanie' => 'Название',
			'opisanie' => 'Описание',
			'object_id' => 'Object',
			'status_id' => 'Статус',
			'data_sozdaniya' => 'Data Sozdaniya',
			'avtor_zapisi_id' => 'Avtor Zapisi',
			'data_zapisi' => 'Data Zapisi',
                        'status.nazvanie'=>'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
        
     //   public $status_id='';
        
        public function getDayDropDown()
            {
          
           return CHtml::dropDownList('box_status',$this->status_id,BoxStatus::model()->getForFilter()
               ,array('class'=>'box_status', 'id'=>$this->id)
            );
       //         return BoxStatus::model()->getDropDown();
            }

        
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
               // $criteria->with = array('status');
               // $criteria->together = true;
                
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.nazvanie',$this->nazvanie,true);
		$criteria->compare('t.opisanie',$this->opisanie,true);
		$criteria->compare('t.object_id',$this->object_id);
		$criteria->compare('t.status_id',$this->status_id);
		$criteria->compare('t.data_sozdaniya',$this->data_sozdaniya,true);
		$criteria->compare('t.avtor_zapisi_id',$this->avtor_zapisi_id);
		$criteria->compare('t.data_zapisi',$this->data_zapisi,true);
              //  $criteria->compare('status.id',$this->status,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Box the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
