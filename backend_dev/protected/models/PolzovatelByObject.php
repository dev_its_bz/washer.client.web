<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PolzovatelByObject extends Polzovatel {

    public function search($obj_id, $kont_id) {
        $criteria = new CDbCriteria;

        $criteria->select = 't.*,if(b.avtor_zapisis_id,1,0) as inObj';
        $criteria->join = 'LEFT JOIN polzovatel__object as b ON b.polzovatel_id=t.id and b.object_id=:id';
        $criteria->condition = 't.kontragent_id=:kontr_id';
        $criteria->params+=array(':id' => $obj_id, ':kontr_id' => $kont_id);

        $criteria->compare('id', $this->id);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('parol', $this->parol, true);
        $criteria->compare('fio', $this->fio, true);
        $criteria->compare('dolzhnost', $this->dolzhnost, true);



        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 15),
        ));
    }

}
