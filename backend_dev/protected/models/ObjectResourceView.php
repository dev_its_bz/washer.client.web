<?php

/**
 * This is the model class for table "object_resource_view".
 *
 * The followings are the available columns in table 'object_resource_view':
 * @property integer $object_id
 * @property string $vremya
 * @property integer $resource
 * @property string $data
 */
class ObjectResourceView extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'object_resource_view';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('object_id, vremya, data', 'required'),
            array('object_id, resource', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('object_id, vremya, resource, data', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'object_id' => 'Object',
            'vremya' => 'Vremya',
            'resource' => 'Resource',
            'data' => 'Data',
        );
    }

    public function getTimeList($obj_id, $date, $dlitelnost) {
        $sql = "SELECT  id, SUBSTR(vremya,1,5) AS vremya"
                . " FROM object_resource_view"
                . " WHERE object_id={$obj_id} AND `data`=$date AND resource>=$dlitelnost";
          
        if ($date == "CURRENT_DATE()") $sql .= " AND vremya > IF(TIME(NOW() + INTERVAL 30 MINUTE)<TIME(NOW()), '23:59:59', TIME(NOW() + INTERVAL 30 MINUTE))";

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('object_id', $this->object_id);
        $criteria->compare('vremya', $this->vremya, true);
        $criteria->compare('resource', $this->resource);
        $criteria->compare('data', $this->data, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ObjectResourceView the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
