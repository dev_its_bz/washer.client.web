<?php

/**
 * This is the model class for table "usluga__car_gruppa".
 *
 * The followings are the available columns in table 'usluga__car_gruppa':
 * @property integer $id
 * @property integer $object_id
 * @property integer $usluga_id
 * @property integer $car_gruppa_id
 * @property string $cena
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi_id
 * @property string $data_zapisi
 */
class UslugaCarGruppa extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'usluga__car_gruppa';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('object_id, usluga_id, car_gruppa_id, avtor_zapisi_id', 'numerical', 'integerOnly' => true),
            array('cena', 'length', 'max' => 30),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, object_id, usluga_id, car_gruppa_id, cena, data_sozdaniya, avtor_zapisi_id, data_zapisi', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array('gruppa' => array(self::BELONGS_TO, 'CarGruppa', 'car_gruppa_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'object_id' => 'Object',
            'usluga_id' => 'Usluga',
            'car_gruppa_id' => 'Car Gruppa',
            'cena' => 'Cena',
            'data_sozdaniya' => 'Data Sozdaniya',
            'avtor_zapisi_id' => 'Avtor Zapisi',
            'data_zapisi' => 'Data Zapisi',
            'gruppa.nazvanie' => 'Тип автомобиля',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('object_id', $this->object_id);
        $criteria->compare('usluga_id', $this->usluga_id);
        $criteria->compare('car_gruppa_id', $this->car_gruppa_id);
        $criteria->compare('cena', $this->cena, true);
        $criteria->compare('data_sozdaniya', $this->data_sozdaniya, true);
        $criteria->compare('avtor_zapisi_id', $this->avtor_zapisi_id);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UslugaCarGruppa the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
