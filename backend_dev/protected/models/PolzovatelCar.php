<?php

/**
 * This is the model class for table "polzovatel_car".
 *
 * The followings are the available columns in table 'polzovatel_car':
 * @property integer $id
 * @property integer $car_model_id
 * @property integer $polzovatel_id
 * @property string $gos_nomer
 * @property string $foto
 * @property string $data_moiki
 * @property integer $deleted
 */
class PolzovatelCar extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'polzovatel_car';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('car_model_id, polzovatel_id', 'required'),
            array('car_model_id, polzovatel_id, deleted', 'numerical', 'integerOnly' => true),
            array('gos_nomer', 'length', 'max' => 30),
            array('foto', 'length', 'max' => 100),
            array('data_moiki', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, car_model_id, polzovatel_id, gos_nomer, foto, data_moiki, deleted', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'carModel' => array(self::BELONGS_TO, 'CarModel', 'car_model_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'car_model_id' => 'Car Model',
            'polzovatel_id' => 'Polzovatel',
            'gos_nomer' => 'Гос. номер',
            'foto' => 'Foto',
            'data_moiki' => 'Data Moiki',
            'deleted' => 'Deleted',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function getUserCar($u_id)
    {
        return Yii::app()->db->createCommand("SELECT t.id,t.gos_nomer,t.foto,b.nazvanie,c.nazvanie as 'marka' "
            . "FROM polzovatel_car as t inner join (car_model as b inner join car_marka as c on b.car_marka_id=c.id) "
            . "on t.car_model_id=b.id "
            . "where t.polzovatel_id='$u_id'")->queryAll();
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('car_model_id', $this->car_model_id);
        $criteria->compare('polzovatel_id', $this->polzovatel_id);
        $criteria->compare('gos_nomer', $this->gos_nomer, true);
        $criteria->compare('foto', $this->foto, true);
        $criteria->compare('data_moiki', $this->data_moiki, true);
        $criteria->compare('deleted', $this->deleted);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PolzovatelCar the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
