<?php

/**
 * This is the model class for table "polzovatel".
 *
 * The followings are the available columns in table 'polzovatel':
 * @property integer $id
 * @property string $login
 * @property string $parol
 * @property string $fio
 * @property string $dolzhnost
 * @property string $telefon
 * @property string $email
 * @property integer $gruppa_id
 * @property integer $kontragent_id
 * @property integer $status_id
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi_id
 * @property string $data_zapisi
 */
class Polzovatel extends CActiveRecord
{
    /*
     * @return string the associated database table name
     */
    const ROLE_ADMIN = 'administrator';
    const ROLE_OWNER = 'owner';
    const ROLE_MANAGER = 'manager';
    const ROLE_GUSET = 'guest';

    public $activationCode = "";

    public function tableName()
    {
        return 'polzovatel';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('login, parol', 'required'),
            array('gruppa_id, kontragent_id, status_id, avtor_zapisi_id, object_id', 'numerical', 'integerOnly' => true),
            array('login, telefon, email', 'length', 'max' => 30),
            array('parol', 'length', 'max' => 255),
            array('fio, dolzhnost', 'length', 'max' => 100),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, login, parol, fio, dolzhnost, telefon, email, gruppa_id, kontragent_id, status_id, data_sozdaniya, avtor_zapisi_id, data_zapisi', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function behaviors()
    {
        return array(
            'activerecord-relation' => array(
                'class' => 'ext.yiiext.behaviors.activerecord-relation.EActiveRecordRelationBehavior',
            ));
    }

    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'status' => array(self::BELONGS_TO, 'PolzovatelStatus', 'status_id'),
            'gruppa' => array(self::BELONGS_TO, 'PolzovatelGruppa', 'gruppa_id'),
            'kontragent' => array(self::BELONGS_TO, 'Kontragent', 'kontragent_id'),
            'objects' => array(self::MANY_MANY, 'Object', 'polzovatel__object(polzovatel_id, object_id)'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'login' => 'Имя пользователя',
            'parol' => 'Пароль',
            'fio' => 'Отображаемое имя',
            'dolzhnost' => 'Должность',
            'telefon' => 'Телефон',
            'email' => 'Email',
            'gruppa_id' => 'Группа прав',
            'kontragent_id' => 'Kontragent',
            'status_id' => 'Статус',
            'data_sozdaniya' => 'Data Sozdaniya',
            'avtor_zapisi_id' => 'Avtor Zapisi',
            'data_zapisi' => 'Data Zapisi',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public $object_id;

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->with = array('objects');
        $criteria->together = true;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('login', $this->login, true);
        $criteria->compare('parol', $this->parol, true);
        $criteria->compare('fio', $this->fio, true);
        $criteria->compare('dolzhnost', $this->dolzhnost, true);
        $criteria->compare('telefon', $this->telefon, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('gruppa_id', $this->gruppa_id);
        $criteria->compare('t.kontragent_id', $this->kontragent_id);
        $criteria->compare('status_id', $this->status_id);
        $criteria->compare('data_sozdaniya', $this->data_sozdaniya, true);
        $criteria->compare('avtor_zapisi_id', $this->avtor_zapisi_id);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);
        $criteria->compare('objects.id', $this->object_id, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 15,
            )
        ));
    }

    public $inObj = 0;

    public function getForCheckBox($obj_id, $kont_id)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->select = 't.*,if(b.avtor_zapisis_id,1,0) as inObj';
        $criteria->join = 'LEFT JOIN polzovatel__object as b ON b.polzovatel_id=t.id and b.object_id=:id';
        $criteria->condition = 't.kontragent_id=:kontr_id';
        $criteria->params += array(':id' => $obj_id, ':kontr_id' => $kont_id);


        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.login', $this->login, true);
        $criteria->compare('t.parol', $this->parol, true);
        $criteria->compare('t.fio', $this->fio, true);
        $criteria->compare('t.dolzhnost', $this->dolzhnost, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Polzovatel the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
