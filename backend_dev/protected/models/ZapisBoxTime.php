<?php

/**
 * This is the model class for table "zapis_box_time".
 *
 * The followings are the available columns in table 'zapis_box_time':
 * @property integer $id
 * @property integer $box_id
 * @property integer $interval
 * @property string $data_zapisi
 * @property integer $avtor_zapisi
 */
class ZapisBoxTime extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'zapis_box_time';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_zapisi', 'required'),
			array('box_id, interval, avtor_zapisi', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, box_id, interval, data_zapisi, avtor_zapisi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'box_id' => 'Box',
			'interval' => 'Interval',
			'data_zapisi' => 'Data Zapisi',
			'avtor_zapisi' => 'Avtor Zapisi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('box_id',$this->box_id);
		$criteria->compare('interval',$this->interval);
		$criteria->compare('data_zapisi',$this->data_zapisi,true);
		$criteria->compare('avtor_zapisi',$this->avtor_zapisi);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ZapisBoxTime the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
