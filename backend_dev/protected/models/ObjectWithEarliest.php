<?php

/**
 * This is the model class for table "object_with_earliest".
 *
 * The followings are the available columns in table 'object_with_earliest':
 * @property integer $id
 * @property string $nazvanie
 * @property string $adres
 * @property string $telefon
 * @property integer $grafik_raboty_id
 * @property integer $zarplata
 * @property integer $gorod_id
 * @property integer $kontragent_id
 * @property double $lat
 * @property double $lon
 * @property integer $havemap
 * @property string $foto
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi_id
 * @property string $data_zapisi
 * @property string $vremya
 * @property integer $resource
 * @property integer $box
 */
class ObjectWithEarliest extends CActiveRecord
{

    public function primaryKey()
    {
        return 'id';
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'object_with_earliest';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, grafik_raboty_id, zarplata, gorod_id, kontragent_id, havemap, avtor_zapisi_id, resource, box', 'numerical', 'integerOnly'=>true),
			array('lat, lon', 'numerical'),
			array('nazvanie', 'length', 'max'=>100),
			array('telefon', 'length', 'max'=>30),
			array('foto', 'length', 'max'=>255),
			array('adres, data_sozdaniya, data_zapisi, vremya', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nazvanie, adres, telefon, grafik_raboty_id, zarplata, gorod_id, kontragent_id, lat, lon, havemap, foto, data_sozdaniya, avtor_zapisi_id, data_zapisi, vremya, resource, box', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nazvanie' => 'Nazvanie',
			'adres' => 'Adres',
			'telefon' => 'Telefon',
			'grafik_raboty_id' => 'Grafik Raboty',
			'zarplata' => '% от суммы работы рабочему',
			'gorod_id' => 'Gorod',
			'kontragent_id' => 'Kontragent',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'havemap' => 'Havemap',
			'foto' => 'Foto',
			'data_sozdaniya' => 'Data Sozdaniya',
			'avtor_zapisi_id' => 'Avtor Zapisi',
			'data_zapisi' => 'Data Zapisi',
			'vremya' => 'Vremya',
			'resource' => 'Resource',
			'box' => 'Box',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nazvanie',$this->nazvanie,true);
		$criteria->compare('adres',$this->adres,true);
		$criteria->compare('telefon',$this->telefon,true);
		$criteria->compare('grafik_raboty_id',$this->grafik_raboty_id);
		$criteria->compare('zarplata',$this->zarplata);
		$criteria->compare('gorod_id',$this->gorod_id);
		$criteria->compare('kontragent_id',$this->kontragent_id);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lon',$this->lon);
		$criteria->compare('havemap',$this->havemap);
		$criteria->compare('foto',$this->foto,true);
		$criteria->compare('data_sozdaniya',$this->data_sozdaniya,true);
		$criteria->compare('avtor_zapisi_id',$this->avtor_zapisi_id);
		$criteria->compare('data_zapisi',$this->data_zapisi,true);
		$criteria->compare('vremya',$this->vremya,true);
		$criteria->compare('resource',$this->resource);
		$criteria->compare('box',$this->box);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ObjectWithEarliest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
