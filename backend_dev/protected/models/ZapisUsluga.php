<?php

/**
 * This is the model class for table "zapis__usluga".
 *
 * The followings are the available columns in table 'zapis__usluga':
 * @property integer $id
 * @property integer $zapis_id
 * @property integer $usluga_id
 * @property string $data_zapisi
 * @property integer $avtor_zapisi
 */
class ZapisUsluga extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'zapis__usluga';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(

            array('zapis_id, cena, dlitelnost, usluga_id, avtor_zapisi', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, cena, dlitelnost, zapis_id, usluga_id, data_zapisi, avtor_zapisi', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'usluga' => array(self::BELONGS_TO, 'UslugaObject', 'usluga_id'),
            'zapisi' => array(self::BELONGS_TO, 'Zapis', 'zapis_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'zapis_id' => 'Zapis',
            'usluga_id' => 'Usluga',
            'data_zapisi' => 'Data Zapisi',
            'avtor_zapisi' => 'Avtor Zapisi',
            'dlitelnost' => 'dlit',
            'cena' => 'цена',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public $serv_name;

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->with = array('usluga');
        $criteria->compare('usluga->nazvanie', $this->serv_name);

        $criteria->compare('id', $this->id);
        $criteria->compare('zapis_id', $this->zapis_id);
        $criteria->compare('usluga_id', $this->usluga_id);
        $criteria->compare('cena', $this->cena);
        $criteria->compare('dlitelnost', $this->dlitelnost);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);
        $criteria->compare('avtor_zapisi', $this->avtor_zapisi);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ZapisUsluga the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
