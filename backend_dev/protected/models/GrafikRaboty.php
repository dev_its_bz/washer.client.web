<?php

/**
 * This is the model class for table "grafik_raboty".
 *
 * The followings are the available columns in table 'grafik_raboty':
 * @property integer $id
 * @property string $nazvanie
 * @property integer $kontragent_id
 * @property string $pn_start
 * @property string $pn_stop
 * @property string $vt_start
 * @property string $vt_stop
 * @property string $sr_start
 * @property string $sr_stop
 * @property string $cht_start
 * @property string $cht_stop
 * @property string $pt_start
 * @property string $pt_stop
 * @property string $sb_start
 * @property string $sb_stop
 * @property string $vs_start
 * @property string $vs_stop
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi_id
 * @property string $data_zapisi
 */
class GrafikRaboty extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'grafik_raboty';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('data_sozdaniya, data_zapisi', 'required'),
            array('kontragent_id, avtor_zapisi_id', 'numerical', 'integerOnly' => true),
            array('nazvanie', 'compare', 'compareValue' => '---', 'operator' => '!='),
            array('nazvanie', 'length', 'max' => 100),
            array('pn_start, pn_stop, vt_start, vt_stop, sr_start, sr_stop, cht_start, cht_stop, pt_start, pt_stop, sb_start, sb_stop, vs_start, vs_stop', 'date', 'format' => 'H:m'),
            array('pn_start, pn_stop, vt_start, vt_stop, sr_start, sr_stop, cht_start, cht_stop, pt_start, pt_stop, sb_start, sb_stop, vs_start, vs_stop', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nazvanie, kontragent_id, pn_start, pn_stop, vt_start, vt_stop, sr_start, sr_stop, cht_start, cht_stop, pt_start, pt_stop, sb_start, sb_stop, vs_start, vs_stop, data_sozdaniya, avtor_zapisi_id, data_zapisi', 'safe', 'on' => 'search'),
        );
    }
    
    public function actionUpdateZnachenie() {
        $es = new EditableSaver('GrafikRaboty');  //'User' is name of model to be updated
        $es->update();
        // var_dump($_POST);
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'kontragent' => array(self::BELONGS_TO, 'Kontragent', 'kontragent_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nazvanie' => 'Название',
            'kontragent_id' => 'Контрагент',
            'pn_start' => 'Понедельник с',
            'pn_stop' => 'Понедельник по',
            'vt_start' => 'Вторник с',
            'vt_stop' => 'Вторник по',
            'sr_start' => 'Среда с',
            'sr_stop' => 'Среда по',
            'cht_start' => 'Четверг с',
            'cht_stop' => 'Четверг по',
            'pt_start' => 'Пятница с',
            'pt_stop' => 'Пятница по',
            'sb_start' => 'Суббота с',
            'sb_stop' => 'Суббота по',
            'vs_start' => 'Воскресенье с',
            'vs_stop' => 'Воскресенье по',
            'data_sozdaniya' => 'Data Sozdaniya',
            'avtor_zapisi_id' => 'Avtor Zapisi',
            'data_zapisi' => 'Data Zapisi',
        );
    }

    public function getForFilter($kontragent_id) { //список для фильтра
        return CHtml::listData(
                        self::model()->findAll('kontragent_id=:id', array(':id' => $kontragent_id)), 'id', 'nazvanie'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nazvanie', $this->nazvanie, true);
        $criteria->compare('kontragent_id', $this->kontragent_id);
        $criteria->compare('pn_start', $this->pn_start, true);
        $criteria->compare('pn_stop', $this->pn_stop, true);
        $criteria->compare('vt_start', $this->vt_start, true);
        $criteria->compare('vt_stop', $this->vt_stop, true);
        $criteria->compare('sr_start', $this->sr_start, true);
        $criteria->compare('sr_stop', $this->sr_stop, true);
        $criteria->compare('cht_start', $this->cht_start, true);
        $criteria->compare('cht_stop', $this->cht_stop, true);
        $criteria->compare('pt_start', $this->pt_start, true);
        $criteria->compare('pt_stop', $this->pt_stop, true);
        $criteria->compare('sb_start', $this->sb_start, true);
        $criteria->compare('sb_stop', $this->sb_stop, true);
        $criteria->compare('vs_start', $this->vs_start, true);
        $criteria->compare('vs_stop', $this->vs_stop, true);
        $criteria->compare('data_sozdaniya', $this->data_sozdaniya, true);
        $criteria->compare('avtor_zapisi_id', $this->avtor_zapisi_id);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return GrafikRaboty the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
