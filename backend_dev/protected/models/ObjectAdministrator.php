<?php

/**
 * This is the model class for table "object__administrator".
 *
 * The followings are the available columns in table 'object__administrator':
 * @property integer $id
 * @property integer $polzovatel_id
 * @property integer $object_id
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisis_id
 * @property string $data_zapisi
 */
class ObjectAdministrator extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'object__administrator';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		//	array('data_sozdaniya', 'required'),
			array('polzovatel_id, object_id, avtor_zapisis_id', 'numerical', 'integerOnly'=>true),
			array('data_zapisi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, polzovatel_id, object_id, data_sozdaniya, avtor_zapisis_id, data_zapisi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'polzovatel_id' => 'Polzovatel',
			'object_id' => 'Object',
			'data_sozdaniya' => 'Data Sozdaniya',
			'avtor_zapisis_id' => 'Avtor Zapisis',
			'data_zapisi' => 'Data Zapisi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('polzovatel_id',$this->polzovatel_id);
		$criteria->compare('object_id',$this->object_id);
		$criteria->compare('data_sozdaniya',$this->data_sozdaniya,true);
		$criteria->compare('avtor_zapisis_id',$this->avtor_zapisis_id);
		$criteria->compare('data_zapisi',$this->data_zapisi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ObjectAdministrator the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
