<?php

/**
 * This is the model class for table "gorod".
 *
 * The followings are the available columns in table 'gorod':
 * @property integer $id
 * @property double $lat
 * @property double $lon
 * @property string $nazvanie_r
 * @property string $nazvanie_pr
 * @property string $nazvanie
 */
class Gorod extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gorod';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nazvanie', 'required'),
            array('lat, lon', 'numerical'),
            array('nazvanie_r, nazvanie_pr, nazvanie', 'length', 'max' => 50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, lat, lon, nazvanie_r, nazvanie_pr, nazvanie', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'lat' => 'широта',
            'lon' => 'долгота',
            'nazvanie_r' => 'Название города в родительном',
            'nazvanie_pr' => 'Название города в предложном',
            'nazvanie' => 'Название города',
            'object_count' => 'Объектов',
        );
    }

    public function getForFilter() { //список для фильтра
        return CHtml::listData(
                        self::model()->findAll(), 'id', 'nazvanie'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('lat', $this->lat);
        $criteria->compare('lon', $this->lon);
        $criteria->compare('nazvanie_r', $this->nazvanie_r, true);
        $criteria->compare('nazvanie_pr', $this->nazvanie_pr, true);
        $criteria->compare('nazvanie', $this->nazvanie, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Gorod the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
