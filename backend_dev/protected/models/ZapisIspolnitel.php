<?php

/**
 * This is the model class for table "zapis_ispolnitel".
 *
 * The followings are the available columns in table 'zapis_ispolnitel':
 * @property integer $id
 * @property integer $ispolnitel_id
 * @property string $time_begin
 * @property string $time_end
 * @property integer $zapis_id
 */
class ZapisIspolnitel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'zapis_ispolnitel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ispolnitel_id, zapis_id', 'numerical', 'integerOnly'=>true),
			array('time_begin, time_end', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, ispolnitel_id, time_begin, time_end, zapis_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'user' => array(self::BELONGS_TO, 'Polzovatel', 'ispolnitel_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ispolnitel_id' => 'Ispolnitel',
			'time_begin' => 'Time Begin',
			'time_end' => 'Time End',
			'zapis_id' => 'Zapis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ispolnitel_id',$this->ispolnitel_id);
		$criteria->compare('time_begin',$this->time_begin,true);
		$criteria->compare('time_end',$this->time_end,true);
		$criteria->compare('zapis_id',$this->zapis_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ZapisIspolnitel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
