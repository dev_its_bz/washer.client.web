<?php

/**
 * This is the model class for table "usluga_object".
 *
 * The followings are the available columns in table 'usluga_object':
 * @property integer $id
 * @property integer $id_usluga
 * @property integer $id_object
 * @property integer $car_gruppa_id
 * @property double $cena
 * @property integer $dlitelnost
 * @property double $zarplata
 *
 * The followings are the available model relations:
 * @property Usluga $idUsluga
 * @property Object $idObject
 * @property CarGruppa $carGruppa
 */
class UslugaObject extends CActiveRecord {

    public $usl_name;
    public $usl_type;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'usluga_object';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cena', 'implodeParams'),
            array('id_usluga, id_object, car_gruppa_id', 'required'),
            array('id_usluga, id_object, car_gruppa_id, dlitelnost', 'numerical', 'integerOnly' => true),
            array('cena, zarplata', 'numerical'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, usl_type, usl_name, id_usluga, id_object, car_gruppa_id, cena, dlitelnost, zarplata', 'safe', 'on' => 'search'),
        );
    }

    public function implodeParams($attribute) {
        if (is_array($this->$attribute)) {
            $this->$attribute = implode(',', $this->$attribute);  //in db it is stored as string e.g. '1,3,4'
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'car_gruppa' => array(self::BELONGS_TO, 'CarGruppa', 'car_gruppa_id'),
            'object' => array(self::BELONGS_TO, 'Object', 'id_object'),
            'uslugi' => array(self::BELONGS_TO, 'Usluga', 'id_usluga'),
                //	'carGruppa' => array(self::BELONGS_TO, 'CarGruppa', 'car_gruppa_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'id_usluga' => 'Услуга',
            'id_object' => 'Объект',
            'car_gruppa_id' => 'Группа авто',
            'cena' => 'Цена, руб.',
            'dlitelnost' => 'Длит. (мин)',
            'zarplata' => 'Зарплата (%)',
            'uslugi.nazvanie' => 'Название',
            'usl_name' => 'Название',
            'usl_type' => 'Тип',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->with = array('uslugi');
        // $criteria->together= true;
        $criteria->compare('uslugi.nazvanie', $this->usl_name, true);
        $criteria->compare('uslugi.usluga_tip_id', $this->usl_type, true);

        $criteria->compare('t.id', $this->id);
        $criteria->compare('id_usluga', $this->id_usluga);
        $criteria->compare('id_object', $this->id_object);
        $criteria->compare('car_gruppa_id', $this->car_gruppa_id);
        $criteria->compare('cena', $this->cena);
        $criteria->compare('dlitelnost', $this->dlitelnost);
        $criteria->compare('zarplata', $this->zarplata);
        //$criteria->compare('name', $this->uslugi.nazvanie, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => array(
                    'usl_name' => array(
                        'asc' => 'uslugi.nazvanie',
                        'desc' => 'uslugi.nazvanie DESC',
                    ),
                    'usl_type' => array(
                        'asc' => 'uslugi.usluga_tip_id',
                        'desc' => 'uslugi.usluga_tip_id DESC',
                    ),
                    '*',
                ),
            ),
        ));
    }

    public function getcost() {
        return CActiveForm::textField($this, "rank", array("cost" => "form_name[rank][" . $this->cena . "]"));
    }

    public function getDuration($services, $car_id, $object_id) {
        $query =  Yii::app()->db->createCommand("SELECT sum(dlitelnost)"
                        . " FROM `usluga_object`"
                        . " WHERE id_usluga IN (" . implode(",", $services) . ")"
                        . " AND car_gruppa_id = '$car_id' AND id_object= '$object_id'")
                ->queryAll();
        return $query[0]["sum(dlitelnost)"];
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UslugaObject the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
