<?php

/**
 * This is the model class for table "udobstvo".
 *
 * The followings are the available columns in table 'udobstvo':
 * @property integer $id
 * @property string $nazvanie
 * @property string $drawable
 */
class Udobstvo extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'udobstvo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nazvanie', 'required'),
            array('nazvanie', 'length', 'max' => 255),
            array('drawable', 'length', 'max' => 50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nazvanie, drawable', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nazvanie' => 'Название',
            'drawable' => 'Drawable',
        );
    }

    public $inObj = 0;

    public function getByObj($id) {
        $criteria = new CDbCriteria;
        $criteria->select = 't.*,if(b.avtor_zapisi_id,1,0) as inObj';
        $criteria->join = 'LEFT JOIN `udobstvo_object` as b ON `b`.`udobstvo_id`=`t`.`id` AND `b`.`object_id` = :o_id';
       // $criteria->condition = '`b`.`object_id` = :o_id';
        $criteria->params+=array(':o_id' => $id);

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.nazvanie', $this->nazvanie, true);
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nazvanie', $this->nazvanie, true);
        $criteria->compare('drawable', $this->drawable, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Udobstvo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
