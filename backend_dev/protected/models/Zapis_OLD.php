<?php

/**
 * This is the model class for table "zapis".
 *
 * The followings are the available columns in table 'zapis':
 * @property integer $id
 * @property integer $polzovatel_id
 * @property integer $car_group_id
 * @property integer $object_id
 * @property integer $summa
 * @property integer $full_time_temp
 * @property integer $full_time_fakt
 * @property integer $box_id
 * @property string $time_begin
 * @property string $time_end
 * @property string $time_zapisi
 * @property string $data_zapisi
 * @property integer $avtor_zapisi
 * @property string $zapis_date
 */
class Zapis extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'zapis';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('polzovatel_id, car_group_id, object_id, summa, full_time_temp, full_time_fakt, box_id, avtor_zapisi', 'numerical', 'integerOnly' => true),
            array('time_begin, time_end, time_zapisi, zapis_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, polzovatel_id, car_group_id, object_id, summa, full_time_temp, full_time_fakt, box_id, time_begin, time_end, time_zapisi, data_zapisi, avtor_zapisi, zapis_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'avto_type' => array(self::BELONGS_TO, 'CarGruppa', 'car_group_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'polzovatel_id' => 'Polzovatel',
            'car_group_id' => 'Car Group',
            'object_id' => 'Object',
            'summa' => 'Summa',
            'full_time_temp' => 'Full Time Temp',
            'full_time_fakt' => 'Full Time Fakt',
            'box_id' => 'Box',
            'time_begin' => 'Time Begin',
            'time_end' => 'Time End',
            'time_zapisi' => 'Time Zapisi',
            'data_zapisi' => 'Data Zapisi',
            'avtor_zapisi' => 'Avtor Zapisi',
            'zapis_date' => 'Zapis Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('polzovatel_id', $this->polzovatel_id);
        $criteria->compare('car_group_id', $this->car_group_id);
        $criteria->compare('object_id', $this->object_id);
        $criteria->compare('summa', $this->summa);
        $criteria->compare('full_time_temp', $this->full_time_temp);
        $criteria->compare('full_time_fakt', $this->full_time_fakt);
        $criteria->compare('box_id', $this->box_id);
        $criteria->compare('time_begin', $this->time_begin, true);
        $criteria->compare('time_end', $this->time_end, true);
        $criteria->compare('time_zapisi', $this->time_zapisi, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('oplacheno', $this->oplacheno, true);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);
        $criteria->compare('avtor_zapisi', $this->avtor_zapisi);
        $criteria->compare('zapis_date', $this->zapis_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getFasterWashers()
    {
        $date = date('Y-m-d');
        $date = '2014-03-21'; // заглушка

        $q1 = Yii::app()->db->createCommand("SELECT DISTINCT t.id,`t`.`nazvanie`,`t`.`adres`,`t`.`telefon` FROM object AS t INNER JOIN box ON t.`id` = box.`object_id`"
            . " WHERE box.`id` NOT IN (SELECT  zapis.`box_id` FROM box INNER JOIN zapis ON zapis.`box_id`=box.`id`"
            . " WHERE zapis.`time_begin`>NOW() AND zapis.`zapis_date`='2014-03-21')")
            ->queryAll();

        $q2 = Yii::app()->db->createCommand("SELECT  object.id,object.`nazvanie`,object.`adres`,object.`telefon` FROM zapis INNER JOIN object ON zapis.`object_id`=object.`id`"
            . " WHERE zapis.`time_begin`>NOW() AND zapis.`zapis_date`='$date'"
            . " GROUP BY object.`id`"
            . " HAVING MIN(zapis.`time_begin`)"
            . " ORDER BY zapis.`time_begin` DESC ")
            ->queryAll();

        $q = array_merge($q1, $q2); // объеденить выборки

        for ($i = 0; $i < count($q) - 2; $i++) { // удалить повторения (оставляется 1 вхождение)
            $this->delete_repit($q, $q[$i], $i);
        }

        $q = array_values($q); // обновить индексы массива (для json)

        return $q;
    }

    private function delete_repit(&$q, $el, $num)
    {
        for ($j = $num + 1; $j < count($q); $j++) {
            if ($q[$j] == $el) {
                unset($q[$j]);
            }
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Zapis the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
