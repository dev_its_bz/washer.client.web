<?php

/**
 * This is the model class for table "car_gruppa".
 *
 * The followings are the available columns in table 'car_gruppa':
 * @property integer $id
 * @property string $nazvanie
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi_id
 * @property string $data_zapisi
 *
 * The followings are the available model relations:
 * @property CarModel[] $carModels
 * @property UslugaCarGruppa[] $uslugaCarGruppas
 * @property UslugaObject[] $uslugaObjects
 * @property Zapis[] $zapises
 */
class CarGruppa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'car_gruppa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_sozdaniya', 'required'),
			array('avtor_zapisi_id', 'numerical', 'integerOnly'=>true),
			array('nazvanie', 'length', 'max'=>255),
			array('data_zapisi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nazvanie, data_sozdaniya, avtor_zapisi_id, data_zapisi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'carModels' => array(self::HAS_MANY, 'CarModel', 'car_gruppa_id'),
			'uslugaCarGruppas' => array(self::HAS_MANY, 'UslugaCarGruppa', 'car_gruppa_id'),
			'uslugaObjects' => array(self::HAS_MANY, 'UslugaObject', 'car_gruppa_id'),
			'zapises' => array(self::HAS_MANY, 'Zapis', 'car_group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nazvanie' => 'Название',
			'data_sozdaniya' => 'Data Sozdaniya',
			'avtor_zapisi_id' => 'Avtor Zapisi',
			'data_zapisi' => 'Data Zapisi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nazvanie',$this->nazvanie,true);
		$criteria->compare('data_sozdaniya',$this->data_sozdaniya,true);
		$criteria->compare('avtor_zapisi_id',$this->avtor_zapisi_id);
		$criteria->compare('data_zapisi',$this->data_zapisi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CarGruppa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
