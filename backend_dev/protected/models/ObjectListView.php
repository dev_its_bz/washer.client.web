<?php

/**
 * This is the model class for table "object_list_view".
 *
 * The followings are the available columns in table 'object_list_view':
 * @property integer $id
 * @property string $nazvanie
 * @property string $adres
 * @property string $foto
 * @property double $lat
 * @property double $lon
 * @property string $like
 * @property string $dislike
 * @property string $summary
 * @property string $distance
 */
class ObjectListView extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'object_list_view';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id', 'numerical', 'integerOnly' => true),
            array('lat, lon', 'numerical'),
            array('nazvanie', 'length', 'max' => 100),
            array('foto', 'length', 'max' => 255),
            array('like, dislike', 'length', 'max' => 21),
            array('summary', 'length', 'max' => 22),
            array('distance', 'length', 'max' => 1),
            array('adres', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nazvanie, adres, foto, lat, lon, like, dislike, summary, distance, favorite', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    public function getByCoords($myLat, $myLon, $town_id, $user_id)
    {

        $models = ObjectListView::model()->findAllBySql("
            SELECT *, geodist('$myLat', '$myLon', lat, lon) AS distance, isFavorite(id, '$user_id') AS favorite
            FROM object_list_view
            WHERE gorod_id='$town_id' AND status='1'
            ORDER BY distance ASC");
        $rows = array();
        foreach ($models as $model)
            $rows[] = $model->attributes;
        return $rows;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ObjectListView the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getByServices($serviceList, $car_group_id = 18, $myLat, $myLon, $town_id, $limit = '20', $user_id)
    {
        $servicesList = @implode(",", $serviceList);
        if ($servicesList == "") $servicesList = "''";
        $servicesCount = @count($serviceList);
        if ($limit == '') $limit = '20';
        return Yii::app()->db->createCommand("
            SELECT a.*, geodist('$myLat', '$myLon', lat, lon) AS distance, isFavorite(a.id, '$user_id') AS favorite
            FROM object_list_view a INNER JOIN usluga_object b ON a.id = b.id_object
            WHERE
                b.id_usluga IN ($servicesList) AND
                car_gruppa_id = '$car_group_id' AND
                a.gorod_id='$town_id' AND
                status='1'
            GROUP BY b.id_object
            HAVING COUNT(DISTINCT b.id_object, b.id_usluga)>=$servicesCount
            LIMIT $limit
            ")->queryAll();
    }

    public function getFirstX($limit, $myLat, $myLon, $town_id, $user_id)
    {
        if ($limit == '') $limit = '20';
        return Yii::app()->db->createCommand("
            SELECT a.*, geodist('$myLat', '$myLon', lat, lon) AS distance, isFavorite(a.id, '$user_id') AS favorite
            FROM object_list_view a
            WHERE a.gorod_id='$town_id' AND status='1'
            LIMIT $limit")->queryAll();
    }

    public function getByCost($serviceList, $car_group_id = 18, $myLat, $myLon, $town_id, $user_id)
    {
        $servicesList = @implode(",", $serviceList);
        if ($servicesList == "") $servicesList = "''";
        $servicesCount = @count($serviceList);
        return Yii::app()->db->createCommand("
            SELECT a.*, geodist('$myLat', '$myLon', lat, lon) AS distance, isFavorite(a.id, '$user_id') AS favorite
            FROM object_list_view a INNER JOIN usluga_object b ON a.id = b.id_object
            WHERE
                b.id_usluga IN ($servicesList) AND
                car_gruppa_id = '$car_group_id' AND
                a.gorod_id='$town_id'
                AND status='1'
            GROUP BY b.id_object
            HAVING COUNT(DISTINCT b.id_object, b.id_usluga)>=$servicesCount
            ORDER BY SUM(cena) ASC
            ")->queryAll();
    }

    public function getByRating($myLat, $myLon, $town_id, $user_id)
    {
        return Yii::app()->db->createCommand("
            SELECT a.*, geodist('$myLat', '$myLon', lat, lon) AS distance, isFavorite(a.id, '$user_id') AS favorite
            FROM object_list_view as a
            WHERE
                gorod_id='$town_id' AND
                status='1'
            ORDER BY summary DESC
            ")->queryAll();
    }

    public function getFromFavorites($myLat, $myLon, $user_id)
    {
        return Yii::app()->db->createCommand("
            SELECT a.*, geodist('$myLat', '$myLon', lat, lon) AS distance, isFavorite(a.id, '$user_id') AS favorite
            FROM object_list_view AS a INNER JOIN favorites AS b ON a.id=b.object_id
            WHERE b.user_id='$user_id'
            ")->queryAll();
    }

    public function getViewWithFavorites($user_id, $myLat = 0, $myLon = 0)
    {
        return Yii::app()->db->createCommand("
            SELECT a.*, geodist('$myLat', '$myLon', lat, lon) AS distance, isFavorite(a.id, '$user_id') AS favorite
            FROM object_list_view AS a INNER JOIN favorites AS b ON b.object_id=a.id AND b.user_id='$user_id'
            ")->queryAll();
    }

    //public function getBest() {
    //    return Yii::app()->db->createCommand("SELECT a.*"
    //                            . " FROM `object_list_view` a"
    //                            . " ORDER BY like-dislike")
    //                    ->queryAll();
    //}

    public function getFasters($myLat, $myLon, $town_id, $user_id)
    {
        return Yii::app()->db->createCommand("
            SELECT a.*, geodist('$myLat', '$myLon', a.lat, a.lon) AS distance, isFavorite(a.id, '$user_id') AS favorite
            FROM object_list_view a INNER JOIN object_with_earliest b ON a.id = b.id
            WHERE
                a.gorod_id='$town_id' AND
                status='1'
            ORDER BY b.vremya ASC
            ")->queryAll();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nazvanie' => 'Nazvanie',
            'adres' => 'Adres',
            'foto' => 'Foto',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'like' => 'Like',
            'dislike' => 'Dislike',
            'summary' => 'Summary',
            'distance' => 'Distance',
            'favorite' => 'Favorite',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nazvanie', $this->nazvanie, true);
        $criteria->compare('adres', $this->adres, true);
        $criteria->compare('foto', $this->foto, true);
        $criteria->compare('lat', $this->lat);
        $criteria->compare('lon', $this->lon);
        $criteria->compare('like', $this->like, true);
        $criteria->compare('dislike', $this->dislike, true);
        $criteria->compare('summary', $this->summary, true);
        $criteria->compare('distance', $this->distance, true);
        $criteria->compare('favorite', $this->favorite, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
