<?php

/**
 * This is the model class for table "zapis".
 *
 * The followings are the available columns in table 'zapis':
 * @property integer $id
 * @property integer $polzovatel_id
 * @property integer $car_id
 * @property integer $object_id
 * @property integer $summa
 * @property integer $full_time_temp
 * @property integer $full_time_fakt
 * @property integer $box_id
 * @property string $time_begin
 * @property string $time_end
 * @property string $data_sozdaniya
 * @property string $data_zapisi
 * @property integer $status_id
 * @property integer $oplacheno
 * @property integer $avtor_zapisi_id
 * @property string $zapis_date
 *
 * The followings are the available model relations:
 * @property Box $box
 * @property Object $object
 * @property Polzovatel $polzovatel
 * @property ZapisStatus $status
 * @property ZapisUsluga[] $zapisUslugas
 * @property ZapisIspolnitel[] $zapisIspolnitels
 */
class Zapis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'zapis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('box_id, data_sozdaniya', 'required'),
			array('polzovatel_id, car_id, object_id, summa, full_time_temp, full_time_fakt, box_id, status_id, oplacheno, avtor_zapisi_id', 'numerical', 'integerOnly'=>true),
			array('time_begin, time_end, data_zapisi, zapis_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, polzovatel_id, car_id, object_id, summa, full_time_temp, full_time_fakt, box_id, time_begin, time_end, data_sozdaniya, data_zapisi, status_id, oplacheno, avtor_zapisi_id, zapis_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'box' => array(self::BELONGS_TO, 'Box', 'box_id'),
			'object' => array(self::BELONGS_TO, 'Object', 'object_id'),
			'polzovatel' => array(self::BELONGS_TO, 'Polzovatel', 'polzovatel_id'),
            'polzovatelCar' => array(self::BELONGS_TO, 'PolzovatelCar', 'car_id'),
			'status' => array(self::BELONGS_TO, 'ZapisStatus', 'status_id'),
			'zapisUslugas' => array(self::HAS_MANY, 'ZapisUsluga', 'zapis_id'),
			'zapisIspolnitels' => array(self::HAS_MANY, 'ZapisIspolnitel', 'zapis_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'polzovatel_id' => 'Polzovatel',
			'car_id' => 'Car',
			'object_id' => 'Автомойка',
			'summa' => 'Сумма',
			'full_time_temp' => 'Расчётное время',
			'full_time_fakt' => 'Фактическое время',
			'box_id' => '№ бокса',
			'time_begin' => 'Начало',
			'time_end' => 'Окончание',
			'data_sozdaniya' => 'Data Sozdaniya',
			'data_zapisi' => 'Data Zapisi',
			'status_id' => 'Статус',
			'oplacheno' => 'Оплачено',
			'avtor_zapisi_id' => 'Avtor Zapisi',
			'zapis_date' => 'Дата записи',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('polzovatel_id',$this->polzovatel_id);
		$criteria->compare('car_id',$this->car_id);
		$criteria->compare('object_id',$this->object_id);
		$criteria->compare('summa',$this->summa);
		$criteria->compare('full_time_temp',$this->full_time_temp);
		$criteria->compare('full_time_fakt',$this->full_time_fakt);
		$criteria->compare('box_id',$this->box_id);
		$criteria->compare('time_begin',$this->time_begin,true);
		$criteria->compare('time_end',$this->time_end,true);
		$criteria->compare('data_sozdaniya',$this->data_sozdaniya,true);
		$criteria->compare('data_zapisi',$this->data_zapisi,true);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('oplacheno',$this->oplacheno);
		$criteria->compare('avtor_zapisi_id',$this->avtor_zapisi_id);
		$criteria->compare('zapis_date',$this->zapis_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Zapis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
