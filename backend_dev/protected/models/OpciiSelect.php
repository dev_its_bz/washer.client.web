<?php

/**
 * This is the model class for table "opcii_select".
 *
 * The followings are the available columns in table 'opcii_select':
 * @property integer $id
 * @property integer $object_id
 * @property integer $opcii_id
 * @property string $znachenie
 */
class OpciiSelect extends CActiveRecord
{

    public $inObject = 1;
    public $opciiname;
    public $temp = 0;
    public $opc_id;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'opcii_select';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('object_id, opcii_id', 'required'),
            // array('inObject', 'implodeParams'),
            array('object_id, opcii_id', 'numerical', 'integerOnly' => true),
            array('znachenie', 'length', 'max' => 50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('inObject,id, object_id, opcii_id, znachenie', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'opcii' => array(self::BELONGS_TO, 'Opcii', array('opcii_id' => 'id')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'object_id' => 'Объект',
            'opcii_id' => 'Опция',
            'znachenie' => 'Значение',
            'opciiname' => 'Настройка',
            'opcii.nazvanie' => 'Опция',
            'inObject' => 'Вкл/Выкл'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;


        // $criteria->with = array('opcii');
        // $criteria->together = true;
        // $criteria->addSearchCondition('opcii.id', $this->opc_id);

        $criteria->compare('id', $this->id);
        $criteria->compare('object_id', $this->object_id);
        $criteria->compare('opcii_id', $this->opcii_id);
        $criteria->compare('znachenie', $this->znachenie, true);


        $criteria->select = 't.*, if(t.id,1,0) as inObject, b.nazvanie as opciiname, b.id as opc_id';
        $criteria->join = 'right join opcii as b on b.id=t.opcii_id and t.object_id=:id';
        $criteria->condition = '';
        $criteria->order = 'inObject DESC';
        //$criteria->limit='0';
        $criteria->params = array(':id' => $this->object_id);
        // var_dump($criteria);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OpciiSelect the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getStatusDropdown()
    {
        $stats = array(
            0 => 'Выкл',
            1 => 'Вкл',
        );
        return CHtml::dropDownlist('inObject', $this->inObject, $stats, array(
            'class' => 'status',
            'data-id' => $this->id,
            'data-opcii' => $this->opc_id,
        ));
    }

}
