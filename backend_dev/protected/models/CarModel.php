<?php

/**
 * This is the model class for table "car_model".
 *
 * The followings are the available columns in table 'car_model':
 * @property integer $id
 * @property integer $car_marka_id
 * @property string $nazvanie
 * @property integer $car_gruppa_id
 * @property string $data_sozdaniya
 * @property integer $avtor_zapisi_id
 * @property string $data_zapisi
 *
 * The followings are the available model relations:
 * @property Polzovatel $avtorZapisi
 * @property CarMarka $carMarka
 */
class CarModel extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'car_model';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('data_sozdaniya, data_zapisi', 'required'),
            array('car_marka_id, car_gruppa_id, avtor_zapisi_id', 'numerical', 'integerOnly' => true),
            array('nazvanie', 'length', 'max' => 30),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, car_marka_id, nazvanie, car_gruppa_id, data_sozdaniya, avtor_zapisi_id, data_zapisi', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'avtorZapisi' => array(self::BELONGS_TO, 'Polzovatel', 'avtor_zapisi_id'),
            'carMarka' => array(self::BELONGS_TO, 'CarMarka', 'car_marka_id'),
            'carGruppa' => array(self::BELONGS_TO, 'CarGruppa', 'car_gruppa_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'car_marka_id' => 'ID',
            'nazvanie' => 'Модель',
            'car_gruppa_id' => 'Группа автомобилей',
            'data_sozdaniya' => 'Data Sozdaniya',
            'avtor_zapisi_id' => 'Avtor Zapisi',
            'data_zapisi' => 'Data Zapisi',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('car_marka_id', $this->car_marka_id);
        $criteria->compare('nazvanie', $this->nazvanie, true);
        $criteria->compare('car_gruppa_id', $this->car_gruppa_id);
        $criteria->compare('data_sozdaniya', $this->data_sozdaniya, true);
        $criteria->compare('avtor_zapisi_id', $this->avtor_zapisi_id);
        $criteria->compare('data_zapisi', $this->data_zapisi, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public $inObj = 0;
    public $marka_name = '';

    public function getForCheckBox($group)
    {
        $criteria = new CDbCriteria;

        $criteria->select = 't.*,CASE WHEN t.car_gruppa_id=:group
            THEN 1 
            ELSE 0 
       END AS inObj ';
        //   $criteria->join = 'LEFT JOIN car_gruppa as b ON b.id=:group';
        // $criteria->condition = 'b.id_object=:obj_id';
        $criteria->params += array(':group' => $group);

        $criteria->with = array('carMarka');
        $criteria->compare('carMarka.nazvanie', $this->marka_name, true);
        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.nazvanie', $this->nazvanie, true);
        $criteria->compare('t.car_marka_id', $this->car_marka_id, true);
        $criteria->order = 'inObj DESC';


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function getCarType($user_car_id){
     
        $model_id = PolzovatelCar::model()->findByPk($user_car_id)->car_model_id;
        return $this->findByPk($model_id)->car_gruppa_id;
     
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CarModel the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
