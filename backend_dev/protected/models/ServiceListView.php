<?php

/**
 * This is the model class for table "service_list_view".
 *
 * The followings are the available columns in table 'service_list_view':
 * @property string $nazvanie
 * @property integer $car_gruppa_id
 * @property double $cena
 * @property integer $dlitelnost
 * @property double $zarplata
 * @property integer $object_id
 */
class ServiceListView extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'service_list_view';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('car_gruppa_id, object_id', 'required'),
			array('car_gruppa_id, dlitelnost, object_id', 'numerical', 'integerOnly'=>true),
			array('cena, zarplata', 'numerical'),
			array('nazvanie', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('nazvanie, car_gruppa_id, cena, dlitelnost, zarplata, object_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nazvanie' => 'Nazvanie',
			'car_gruppa_id' => 'Car Gruppa',
			'cena' => 'Cena',
			'dlitelnost' => 'Dlitelnost',
			'zarplata' => 'Zarplata',
			'object_id' => 'Object',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nazvanie',$this->nazvanie,true);
		$criteria->compare('car_gruppa_id',$this->car_gruppa_id);
		$criteria->compare('cena',$this->cena);
		$criteria->compare('dlitelnost',$this->dlitelnost);
		$criteria->compare('zarplata',$this->zarplata);
		$criteria->compare('object_id',$this->object_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ServiceListView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
