<?php

class UslugaCarGruppaController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $title = '';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'updateZnachenie', 'createNew','admin'),
                'roles' => array('manager'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update','delete'),
                'roles' => array('administrator'),
            ),
      
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionUpdateZnachenie()
    {
        $es = new EditableSaver('UslugaCarGruppa');
        $es->update();
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->buildBreadcrumbs($this->loadModel($id));
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($object_id, $usluga_id)
    {
        $model = new UslugaCarGruppa;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $model->object_id = $object_id;
        $model->usluga_id = $usluga_id;

        $this->buildBreadcrumbs($model);

        if (isset($_POST['UslugaCarGruppa'])) {
            $model->attributes = $_POST['UslugaCarGruppa'];
            if ($model->save())
                $this->redirect(array('/usluga/view', 'id' => $usluga_id));
        }

        $carGruppa = CarGruppa::model()->getForFilter();

        $this->render('create', array(
            'model' => $model,
            'carGruppa' => $carGruppa,
        ));
    }

    public function actionCreateNew($object_id, $usluga_id)
    {
        $model = new UslugaCarGruppa;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $model->object_id = $object_id;
        $model->usluga_id = $usluga_id;
        $model->car_gruppa_id = '18';
        $model->cena = '0';


        $this->buildBreadcrumbs($model);

        if ($model->save())
            $this->redirect(array('/usluga/view', 'id' => $usluga_id));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->buildBreadcrumbs($model);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['UslugaCarGruppa'])) {
            $model->attributes = $_POST['UslugaCarGruppa'];
            if ($model->save())
                $this->redirect(array('/usluga/view', 'id' => $model->usluga_id));
        }

        $carGruppa = CarGruppa::model()->getForFilter();

        $this->render('update', array(
            'model' => $model,
            'carGruppa' => $carGruppa,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('UslugaCarGruppa');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new UslugaCarGruppa('search');
        $this->buildBreadcrumbs($model);
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['UslugaCarGruppa']))
            $model->attributes = $_GET['UslugaCarGruppa'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = UslugaCarGruppa::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usluga-car-gruppa-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    protected function buildBreadcrumbs($model)
    {
        $usluga = Usluga::model()->findByPk($model->usluga_id);
        $object = Object::model()->findByPk($model->object_id);
        $kontragent = Kontragent::model()->findByPk($object->kontragent_id);

        $this->breadcrumbs = array(
            'Контрагенты' => array('/kontragent/admin/'),
            $kontragent->nazvanie => array('/kontragent/view/' . $kontragent->id),
            'Объекты' => array('/object/admin/' . $kontragent->id),
            $object->nazvanie => array('/object/view/' . $object->id),
            'Услуги и цены' => array('/usluga/admin/' . $model->object_id),
            $usluga->nazvanie => array('/usluga/view/' . $model->usluga_id)
        );
        $this->title = $usluga->nazvanie;
    }
}
