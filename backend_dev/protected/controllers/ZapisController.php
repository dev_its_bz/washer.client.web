<?php

class ZapisController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'NewZapis'),
                'roles' => array('client'),
            ),
            array('allow',
                'actions' => array('admin', 'update'),
                'roles' => array('manager'),
            ),
            array('deny',
                'roles' => array('*'),
            ),
        );
    }

    public function actionNewZapis($polzovatel_id, $car_id, $services, $box_resource_id)
    {
        //Проверяем, не заняли ли уже наш бокс на это время
        $box_resource = BoxResource::model()->findByPk($box_resource_id);
        if ($box_resource->resource == 0) {
            //Бокс уже заняли, ищем другой свободный на жто же время на этом же объекте
            $box_resource = BoxResource::model()->findByAttributes(
                array(
                    'vremya' => $box_resource->vremya,
                    'object_id' => $box_resource->object_id,
                    'data' => $box_resource->data,),
                array(
                    'condition' => 'resource <> :resource',
                    'params' => array(':resource' => 0)
                )
            );
            if ($box_resource == null) {
                echo 'Error|К сожелению на это время уже всё занято. Попробуйте другое время или другую мойку.';
                return;
            }
        }

        $zapis = Zapis::model()->findAllByAttributes(array(
                'polzovatel_id' => Yii::app()->user->id,
                'zapis_date' => $box_resource->data,
                'car_id' => $car_id,
                'status_id' => array(0, 1, 2)
            )
        );

        if (sizeof($zapis) > 2) {
            echo 'Error|К сожалению вы исчерпали количество допустимых записей этого авто на этот день. Выберите другой день или отмените запись.';
            return;
        }

        $zapis = new Zapis();
        $zapis->polzovatel_id = $polzovatel_id;
        $zapis->avtor_zapisi_id = $polzovatel_id;
        $zapis->object_id = $box_resource->object_id;
        $zapis->box_id = $box_resource->box_id;
        $zapis->car_id = $car_id;


        $polzovatelCar = PolzovatelCar::model()->findByPk($car_id);
        $car_gruppa_id = $polzovatelCar->carModel->car_gruppa_id;


        $services = $services == null ? -1 : $services;
        $sql = "
        SELECT SUM(cena) AS cena, SUM(dlitelnost) AS dlitelnost
        FROM usluga_object
        WHERE id_usluga IN ($services) AND car_gruppa_id = $car_gruppa_id
        ";
        $services = UslugaObject::model()->findBySql($sql);
        $zapis->summa = $services->cena;
        if ($services->dlitelnost == null) {
            $object_id = $box_resource->object_id;
            $dlitelnost = @OpciiSelect::model()->findByAttributes(array('object_id' => $object_id, 'opcii_id' => 2))->znachenie;
            if ($dlitelnost != null)
                $services->dlitelnost = $dlitelnost;
            else
                $services->dlitelnost = '30';
        }
        $zapis->full_time_temp = $services->dlitelnost;

        $zapis->time_begin = $box_resource->vremya;
        $zapis->time_end = new CDbExpression("TIME('2014-01-01 " . $box_resource->vremya . "' + INTERVAL " . $services->dlitelnost . " MINUTE)");
        $zapis->data_zapisi = new CDbExpression('NOW()');
        $zapis->data_sozdaniya = new CDbExpression('NOW()');
        $zapis->status_id = '0';
        $zapis->zapis_date = $box_resource->data;
        $zapis->oplacheno = '0';

        if ($zapis->save())
            echo "OK|Запись №" . $zapis->getPrimaryKey() . " успешно создана. Ждём вас.";
        else
            echo "Error|Ошибка записи. Сообщите о ней администратору: [" . $zapis->getErrors() . "]";
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Zapis;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Zapis'])) {
            $model->attributes = $_POST['Zapis'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

        if (isset($_POST['Zapis'])) {
            $model->attributes = $_POST['Zapis'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $model = $this->loadModel($id);
            $model->status_id = '4';
            $model->save();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new Zapis('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Zapis']))
            $model->attributes = $_GET['Zapis'];

        $model->status_id = array('0,1,2');

        if (Yii::app()->user->isGuest)
            $this->redirect("/");

        if (Yii::app()->user->role == 'client')
            $model->polzovatel_id = Yii::app()->user->id;

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Zapis('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Zapis']))
            $model->attributes = $_GET['Zapis'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Zapis::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'zapis-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
