<?php

class SiteController extends Controller
{

    /**
     * Declares class-based actions.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'sendPass', 'login', 'CheckCode', 'New_pass'),
                'users' => array('*'),
            ),
        );
    }

    public function actions()
    {
        return array(
// captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
// They can be accessed via: index.php?r=site/page&view=FileName
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionPage()
    {
        $view = $_GET['view'];

        $detect = Yii::app()->mobileDetect;
        if ($detect->isMobile())
            $this->layout = "//layouts/mobile_layout";

        if (file_exists("protected/views/site/pages/$view.php"))
            $this->render('/site/pages/' . $view);
        else
            $this->render('/site/pages/404');
    }

    public function actionIndex()
    {
        $detect = Yii::app()->mobileDetect;
// call methods
        if ($detect->isAndroidOS()) {
            $this->render('mobileAndroid');
            exit;
        }
        if ($detect->isiOS()) {
            $this->render('mobileIos');
            exit;
        }

        if ($detect->isWindowsMobileOS() || $detect->isWindowsPhoneOS()) {
            $this->render('mobileWindows');
            exit;
        }

        if ($detect->isMobile()) {
            $this->render('mobileUnsupported');
            exit;
        }

        if (Yii::app()->user->isGuest) {
            $model = new Object('search');
            $model->unsetAttributes();
            if (isset($_GET['Object']))
                $model->attributes = $_GET['Object'];

            $this->render('index', array('model' => $model));
        } else
            $this->redirect('/polzovatel/dashboard');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionSendPass()
    {
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
            if (count(Polzovatel::model()->find('email=:email', array(':email' => $email))) > 0) {
                $login = Polzovatel::model()->find('email=:email', array(':email' => $email))->login;
                $passwd = Polzovatel::model()->find('email=:email', array(':email' => $email))->parol;
                $phone = Polzovatel::model()->find('email=:email', array(':email' => $email))->telefon;
                Yii::app()->sms->send($phone, "Логин: $login\nПароль: $passwd");
                echo json_encode('SMS с логином и паролем отправлен на телефон пользователя в SMS.');
            } else
                echo json_encode('Пользователь с такими данными не обнаружен в системе!');
            return;
        }

        if (isset($_POST['username'])) {
            $phone = $_POST['username'];
            require_once('/var/www/washer.its.bz/backend_dev/protected/views/site/recaptchalib.php');
            $privatekey = "6LfNWvESAAAAAGHXbgOlz8UhYEzZiM18Et2KZjR3";
            $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
            // var_dump($resp);
            if ($resp->is_valid == true) {
                $pw = @Polzovatel::model()->find('login=:tel', array(':tel' => $phone))->parol;
                if ($pw) {
                    Yii::app()->sms->send($phone, 'Код для смены пароля: ' . substr($pw, 0, 4));
                    echo json_encode('Sms на номер ' . $phone . ' отправлено');
                } else
                    echo json_encode('Такого телефона нет в базе');
            } else
                echo json_encode('ERROR');
        }
    }

    public function actionCheckCode()
    {
        $phone = $_POST['username'];
        $code = $_POST['code'];

        $pw = @Polzovatel::model()->find('login=:tel', array(':tel' => $phone))->parol;
        if ($code == substr($pw, 0, 4))
            echo json_encode('NO_ERROR');
        else
            echo json_encode('ERROR');
    }

    public function actionNew_pass()
    {
        $phone = $_POST['username'];
        $pw = $_POST['new_pass'];
        $rec = @Polzovatel::model()->find('login=:tel', array(':tel' => $phone));
        $rec->parol = md5(md5($pw));
        $rec->save();
    }

    public function actionLogin()
    {
        if (!Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->user->returnUrl);

        $model = new LoginForm();

// if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

// collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if (isset($_POST['LoginForm']['username']) && $_POST['LoginForm']['username'] != '') {
                if (Polzovatel::model()->find('login=:login', array(':login' => $_POST['LoginForm']['username'])))
                    $model->username = Polzovatel::model()->find('login=:login', array(':login' => $_POST['LoginForm']['username']))->login;
                else
                    $model->username = '---';
            }


// validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }

// display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionShowCity()
    {
        $gorod = Gorod::model()->findByPk((int)$_POST['city_id']);
        $object = Object::model()->findAll();
        $gMap = $this->prepareGmap($object, $gorod->id);
        $this->actionshowMap();
    }

    protected function prepareGmap($models, $town_id = 1)
    {

        Yii::import('ext.EGMap.*');

        $gMap = new EGMap();
        $gMap->setWidth('100%');
        $gMap->setHeight('100%');
        $gMap->zoom = 10;
        $clusterer = new EGMapMarkerClusterer();
        $clusterer->setOptions(array('enableRetinaIcons' => true, 'maxZoom'=>11));
        $gMap->enableMarkerClusterer($clusterer);
        $mapTypeControlOptions = array(
            'position' => EGMapControlPosition::RIGHT_TOP,
        );

        $gMap->mapTypeId = EGMap::TYPE_ROADMAP;
        $gMap->mapTypeControlOptions = $mapTypeControlOptions;

        foreach ($models as $model) {
            if ($model->havemap != 0) {
                $content = "";
                $content = "
                        <div class='gmaps-label'>
                        {$model->nazvanie}<br/>
                        {$model->adres}<br/>";
                if ($model->kontragenty->site != null)
                    $content .= "<a  target='_blank' href='{$model->kontragenty->site}'>Сайт</a><br/>";
                if ($model->kontragenty->status_id == "2")
                    $content .= "<a  target='_parent' href='/zapisUsluga/fastWash?id={$model->id}'>Записаться</a>";
                $content .= "</div>";

                $info_window_a = new EGMapInfoWindow($content);

                if ($model->kontragenty->status_id == 2)
                    $icon = new EGMapMarkerImage("http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Marker-Outside-Chartreuse-icon.png");
                else
                    $icon = new EGMapMarkerImage("http://icons.iconarchive.com/icons/icons-land/vista-map-markers/32/Map-Marker-Marker-Outside-Azure-icon.png");

                $icon->setSize(32, 37);
                $icon->setAnchor(16, 16.5);
                $icon->setOrigin(0, 0);

                $marker = new EGMapMarkerWithLabel($model->lat, $model->lon, array('title' => Yii::t('object', $model->nazvanie),
                    'icon' => $icon), 'marker');
                $marker->addHtmlInfoWindow($info_window_a);

                /*
                  $label_options = array(
                  'backgroundColor' => 'black',
                  'opacity' => '0.45',
                  'width' => 'auto',
                  'padding-left' => '5px',
                  'padding-right' => '5px',
                  'color' => 'white'
                  );
                  $marker->labelContent = $model->adres;
                  $marker->labelStyle = $label_options;
                 */
                $marker->setLabelAnchor(new EGMapPoint(-15, 0));
                $gMap->addMarker($marker);
            }
        }
        $city = Gorod::model()->findByPk($town_id);

        $gMap->setCenter($city->lat, $city->lon);
        $gMap->zoom = 13;

        return $gMap;
    }

    public function actionShowMap($id = 0)
    {
        if ($id != 0) {
            $object = Object::model()->findAll('gorod_id=:id', array(':id' => $id));
        } else {
            $object = Object::model()->findAll();
            $id = 1;
        }
        $this->layout = '_ajaxMap';
        $this->render('_ajaxMap', array(
                'gMap' => $this->prepareGmap($object, $id),
            )
        );
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'object-grid') {
            echo TbActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
