<?php

define('THIS_DAY', date('Y-m-d'));

class ZapisUslugaController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }


    public function init()
    {
        parent::init();
        if (isset($_GET["id"])) {
            Yii::app()->session['object_id'] = $_GET["id"];
            unset(Yii::app()->session["options"]);
            unset(Yii::app()->session['summa']);
            unset(Yii::app()->session['full_time_temp']);
            unset(Yii::app()->session['services']);

            $this->getOptions();
        }
    }

    private function getOptions()
    {
        $options = OpciiSelect::model()->findAll('object_id=:obj', array(':obj' => Yii::app()->session['object_id']));

        $ar["day_count"] = 5;
        $ar["select_serv"] = null;
        $ar["block_length"] = 30;
        foreach ($options as $opt) {
            switch ($opt["opcii_id"]) {
                case 5:
                    $ar["day_count"] = $opt["znachenie"];
                    break;
                case 1:
                    $ar["select_serv"] = $opt["znachenie"];
                    break;
                case 2:
                    $ar["block_length"] = $opt["znachenie"];
                    break;
            }
        }

        Yii::app()->session["options"] = $ar;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('AddObjectId', 'getGrid', 'fast_wash_Steps', 'fastWash', 'Record', 'Finnal', 'AddUslugi', 'AddCarGroup', 'ProcessSteps', 'recordWizard', 'index', 'view', 'zapis', 'UpdateTimeLine', 'GetCars', 'loaduslugi', 'admin', 'AddService', 'SaveM', 'ProcessStepsNoServices'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('deleteRecord', 'updateBoxesManager', 'recordPaid', 'ViewRecord', 'deleteRecordData', 'saveRecordData', 'Changebox', 'Object_manage', 'create', 'update', 'loadBox', 'Save', "addNewService"),
                'roles' => array('manager'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionRecordWizard($id)
    {

        if (isset(Yii::app()->session["options"]["select_serv"]))
            $this->render('zapis_wizard');
        else
            $this->render('zapis_wizard_bez_uslug');
    }

    public function actionfastWash()
    {
        if (!isset(Yii::app()->user->id))
            $this->redirect('/site/login');
        else
            $this->render('fast_wash_wizard');
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionCreate()
    {
        $model = new ZapisUsluga;

        if (isset($_POST['ZapisUsluga'])) {
            $model->attributes = $_POST['ZapisUsluga'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['ZapisUsluga'])) {
            $model->attributes = $_POST['ZapisUsluga'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionProcessSteps()
    {

        switch ($_REQUEST["step_number"]) {
            case 1:
                $this->actionZapis();
                break;
            case 2:
                $this->actionLoadUslugi();
                break;
            case 3:
                $this->actionIndex($this->getBlockCount());
                break;
        }
    }

    public function actionfast_wash_Steps()
    {

        switch ($_REQUEST["step_number"]) {
            case 1:
                $this->actionZapis();
                break;
            case 2:
            {
                $model = new Object('search');
                $model->unsetAttributes(); // clear any default values
                if (isset($_GET['Object']))
                    $model->attributes = $_GET['Object'];
                $model->k_status = 2;

                $this->renderPartial('vibor_moika', array('model' => $model), false, true);
            }
                break;
            case 3:
                $this->actionIndex($this->getBlockCount());
                break;
        }
    }

    public function actiongetGrid()
    {
        $date = date('Y-m-d');
        $time = '16:45:00'; // date('H-i-s');
        $Path_to_post = Yii::app()->request;
        if ($Path_to_post->getPost('faster') == 1) {
            $q = Zapis::model()->getFasterWashers();

        } else
            $q = Yii::app()->db->createCommand("SELECT DISTINCT `t`.`id`,`t`.`nazvanie`,`t`.`adres`,`t`.`telefon` FROM `object` `t` "
                . "LEFT OUTER JOIN `kontragent` `kontragenty` ON (`t`.`kontragent_id`=`kontragenty`.`id`) "
                . "WHERE (kontragenty.status_id LIKE 2) ")->queryAll();


        $this->renderPartial('choose_grid_view', array('query' => $q), false, true);
    }

    public function actionAddObjectId()
    {
        $Path_to_post = Yii::app()->request;
        Yii::app()->session['object_id'] = $Path_to_post->getPost('id');
        unset(Yii::app()->session["options"]);
        unset(Yii::app()->session['summa']);
        unset(Yii::app()->session['full_time_temp']);
        unset(Yii::app()->session['services']);
        $this->getOptions();
    }

    public function actionProcessStepsNoServices()
    {
        //  if (isset(Yii::app()->session["options"]["select_serv"]))
        switch ($_REQUEST["step_number"]) {
            case 1:
                $this->actionZapis();
                break;
            case 2:
                $this->actionIndex($this->getBlockCount());
                break;
        }
    }

    public function actionFinnal($rec_id)
    {
        $mod = Zapis::model()->findByPk($rec_id);
        $this->render('finnaly_very', array('data' => $mod));
    }

    public function actionLoadUslugi()
    {
        $uslugi = Yii::app()->db->createCommand("
          Select
          t.cena as cena, t.dlitelnost as dlitelnost,
          t.id as idu, usluga.nazvanie as nazvanie, usluga_tip.nazvanie as tip
          from usluga_object as t inner join (usluga inner join usluga_tip on
          usluga.usluga_tip_id=usluga_tip.id) on
          usluga.id = t.id_usluga
          WHERE t.id_object=" . Yii::app()->session['object_id'] . " AND t.car_gruppa_id=" . Yii::app()->session['car_group_id'] .
            "ORDER BY tip")->queryAll();

        if (isset($uslugi['0'])) {
            $current_type = $uslugi['0']['tip'];
            $tags[] = '<optgroup label="' . $current_type . '">';
            foreach ($uslugi as $rec) {
                if ($rec['tip'] <> $current_type) {
                    $current_type = $rec['tip'];
                    $tags[] = '</optgroup>';
                    $tags[] = '<optgroup label="' . $current_type . '">';
                }
                $str = $this->makeHTMLstring($rec['nazvanie'], $rec['dlitelnost'], $rec['cena']);
                $tags[] = CHtml::tag('option', array('value' => $rec['idu'], 'data-cost' =>
                    $rec['cena'], 'data-duration' => $rec['dlitelnost']), CHtml::encode($str), true);
            }
            $tags[] = '</optgroup>';
        } else {
            $tags = 'Нет результатов';
        }

        $this->renderPartial('uslugi_select', array('tags' => $tags
        ), false, true);
    }

    public function actionAddUslugi()
    {
        if (isset($_POST["cost"])) {
            Yii::app()->session['summa'] = $_POST["cost"];
            Yii::app()->session['full_time_temp'] = $_POST["duration"];
        } elseif (isset($_POST["services"]))
            Yii::app()->session['services'] = $_POST["services"];
    }

    public function actionAddCarGroup()
    {
        if (isset($_POST['car_id']))
            $group = CarModel::model()->findbyPk($_POST['car_id'])->car_gruppa_id;
        else
            $group = $_POST['group_id'];

        Yii::app()->session['car_group_id'] = $group;
    }

    private function addZapis($count, $begin_t, $begin_d)
    {
        $mod = new Zapis;
        $mod->polzovatel_id = '1';
        $mod->car_group_id = Yii::app()->session['car_group_id'];
        $mod->object_id = Yii::app()->session['object_id'];
        $mod->full_time_temp = $count * Yii::app()->session['options']['block_length'];
        $mod->summa = Yii::app()->session['summa'];
        $block_begin = $this->getBlockNum(Yii::app()->session['options']['block_length'], $begin_t);
        $id = $this->getBoxId($block_begin, $count, $this->getBoxes(), $begin_d);
        if ($id <> 'error') {
            $mod->box_id = $id;
        }
        $mod->time_begin = $begin_t;
        $mod->status = '1';
        $mod->zapis_date = $begin_d;
        $mod->time_end = $this->plusTime($begin_t, $count * Yii::app()->session['options']['block_length']);
        $mod->save();
        $id_rec = $mod->id;
        if (isset(Yii::app()->session['services'])) {
            $services = Yii::app()->session['services'];

            foreach ($services as $service) {
                $rec = new ZapisUsluga;
                $rec->zapis_id = $id_rec;
                $rec->usluga_id = $service;
                $rec->save();
            }
        }
        return $id_rec;
    }


    public function actionZapis()
    {


        $u_id = Yii::app()->user->id;
        $q = Yii::app()->db->createCommand("SELECT t.car_model_id,b.nazvanie "
            . "FROM polzovatel_car as t inner join car_model as b "
            . "on t.car_model_id=b.id "
            . "where t.polzovatel_id='$u_id'")->queryAll();

        $cars = CHtml::listData($q, 'car_model_id', 'nazvanie');

        $mark_data = CHtml::listData(CarMarka::model()->findAll(), 'id', 'nazvanie');
        $this->renderPartial('zapis_na_uslugu', array('model_list' => $cars
        ), false, true);


    }

    public function actionUpdateVar()
    {
        $es = new EditableSaver('UslugaObject'); //'User' is name of model to be updated
        $es->update();
    }

    private function makeHTMLstring($name, $duration, $cost)
    {
        return $name . '<br>' . $cost . ' рублей<br>' . $duration . ' минут';
    }

    public function actionGetCars()
    {
        $model = CarModel::model()->findAll('t.car_marka_id=:idc', array(':idc' => (int)$_POST['marka_id']));
        foreach ($model as $val) {
            echo CHtml::tag('option', array('value' => $val->id), CHtml::encode($val->nazvanie), true);
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
// we only allow deletion via POST request
            $this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    private function getBoxId($start_block, $count, $box_id, $begin_d)
    {
        $boxes = $this->fillArrs($count, Yii::app()->session['options']['block_length'], $box_id, $begin_d);
        foreach ($box_id as $id) {
            if ($boxes[$id][$start_block] == 0) {
                $j = $start_block;

                while (($j < $start_block + $count) && ($boxes[$id][$j] == 0))
                    $j++;

                if ($j == ($start_block + $count))
                    return $id;
            }
        }
        return 'error';
    }

    public function actionSaveM()
    {
        $block_count = $this->getBlockCount(); //количество блоков для записи

        $date = $this->convertData($_POST['zap_date']); // дату в формат БД
        $boxMass = $this->fillBoxes($block_count, $date); // получить таймлайн
        $index = $_POST['index'];

        if ($boxMass[$index] == 0) { // возможна ли запись в выбранное время
            $log = true;
            $id_rec = $this->addZapis($block_count, $_POST['begin_time'], $date);
        } else {
            $log = false;
            $id_rec = -1;
        }
        $msg = array('msg' => $log, 'data' => $this->fillBoxes($block_count, $date), 'id_rec' => $id_rec);
        echo CJSON::encode($msg);
    }

    private function convertData($dt)
    {
        return date('Y-m-d', strtotime($dt));
    }

    public function actionUpdateTimeLine()
    {
        $Path_to_post = Yii::app()->request;
        $date = $this->convertData($Path_to_post->getPost('date'));

        $msg = array(
            'data' => $this->fillBoxes($block_count = $this->getBlockCount(), $date));
        echo CJSON::encode($msg);
    }

    private function plusTime($time_start, $min_add)
    {
        $format = '%H:%M:%S';
        $ar = strptime($time_start, $format);

        $min_full = $ar['tm_min'] + $min_add;
        $hour = floor($min_full / 60);
        $min = $min_full % 60;

        return date("H:i:s", mktime($ar['tm_hour'] + $hour, $min, 0)); // - потом добавить сюда дату
    }

    private function getBlockNum($b_time, $time)
    {
        $format = '%H:%M:%S';
        $ar = strptime($time, $format);
        $time_in_min = $ar['tm_hour'] * 60 + $ar['tm_min'];

        $block_num = floor($time_in_min / $b_time);
        return $block_num;
    }

    private function getDay($num)
    {
        switch ($num) {
            case 1:
                $day = 'pn';
                break;
            case 2:
                $day = 'vt';
                break;
            case 3:
                $day = 'sr';
                break;
            case 4:
                $day = 'cht';
                break;
            case 5:
                $day = 'pt';
                break;
            case 6:
                $day = 'sb';
                break;
            case 7:
                $day = 'vs';
                break;
            case 8:
                $day = 'pn';
                break;
        }
        return $day;
    }

    private function getToodayGraph($id_graph, $zap_date)
    {

        $dt = date('l', strtotime($zap_date));
        switch ($dt) {
            case 'Monday':
                $day = 1;
                break;
            case 'Tuesday':
                $day = 2;
                break;
            case 'Wednesday':
                $day = 3;
                break;
            case 'Thursday':
                $day = 4;
                break;
            case 'Friday':
                $day = 5;
                break;
            case 'Saturday':
                $day = 6;
                break;
            case 'Sunday':
                $day = 7;
                break;
        }

        $today = $this->getDay($day);
        $tomorrow = $this->getDay($day + 1);

        $rec = GrafikRaboty::model()->findByPk($id_graph);

        $graph = array('start' => $rec[$today . '_start'], 'stop' => $rec[$today . '_stop'], 'tomorrow_start' => $rec[$tomorrow . '_start']);

        return $graph;
    }

    private function fillTimeOfWork(&$boxMass, $box_id, $block_time, $block_count, $zap_date)
    { //пока заглушка
        $grafik_id = Object::model()->findByPk(Yii::app()->session['object_id'])->grafik_raboty_id;
        $tooday_graph = $this->getToodayGraph($grafik_id, $zap_date);

        $start_block = $this->getBlockNum($block_time, $tooday_graph['start']);
        $end_block = $this->getBlockNum($block_time, $tooday_graph['stop']);
        $end_block = ($end_block == 0) ? $end_block = (60 / $block_time) * 24 : $end_block;
        $tommorow_start = $this->getBlockNum($block_time, $tooday_graph['tomorrow_start']);
        foreach ($boxMass as &$box)
            for ($i = $start_block; $i <= $end_block - 1; $i++)
                $box[$i] = 0;
        unset($box);

        if ($tommorow_start < $block_count)
            foreach ($boxMass as &$bx)
                for ($i = $end_block; $i < $end_block - 1 + $block_count; $i++)
                    $bx[$i] = 0;
        unset($bx);
    }

    private function fillArrs($block_count, $block_time, $box_id, $zap_date)
    {

        $time_line = (60 / $block_time) * 24; // - длина тайм лайна
        $zapisi = Zapis::model()->findAll('zapis_date=:dt AND object_id=:id AND status<>4', array(':dt' => $zap_date, ':id' => Yii::app()->session['object_id']));
        for ($i = 0; $i < $time_line + $block_count - 1; $i++) // инициализируем массив боксов еденицами, 
            //добавим кусок следуещего дня чтобы знать инфо для последних блоков
            foreach ($box_id as $id)
                $boxMas[$id][$i] = 1;

        $this->fillTimeOfWork($boxMas, $box_id, $block_time, $block_count, $zap_date); // добавим условия времени работы боксов


        foreach ($zapisi as $zapis) {
            $block_begin = $this->getBlockNum($block_time, $zapis['time_begin']);
            $lenght = ceil($zapis['full_time_temp'] / $block_time);
            for ($i = $block_begin; $i < $block_begin + $lenght; $i++)
                $boxMas[$zapis['box_id']][$i] = 1;
        }

        // инфо о начале след дня (интересуют только заявки для первых $block_count блоков)
        $usl_time = $block_count * $block_time;
        $time = $this->plusTime('00:00:00', $usl_time);
        $nextDay = new DateTime($zap_date);
        $nextDay->modify('+1 day');

        $zapisi_tail = Zapis::model()->findAll('zapis_date=:dt AND time_end<=:time AND object_id=:id AND status<>4', array(':dt' => $nextDay->format('Y-m-d'), ':id' => Yii::app()->session['object_id'],
            ':time' => $time));

        foreach ($zapisi_tail as $zapis) {
            $block_begin = $time_line + $this->getBlockNum($block_time, $zapis['time_begin']);
            $lenght = ceil($zapis['full_time_temp'] / $block_time);
            for ($i = $block_begin; $i < $block_begin + $lenght; $i++)
                $boxMas[$zapis['box_id']][$i] = 1;
        }


        return $boxMas;
    }

    private function getBoxes()
    {
        $boxes_rec = Box::model()->findAll('(object_id=:id) AND (status_id=1)', array(':id' => Yii::app()->session['object_id']));

        foreach ($boxes_rec as $box)
            $boxes[] = $box['id'];

        return $boxes;
    }

    private function fillBoxes($block_count, $date_zap = THIS_DAY)
    {
        $time_line = (60 / Yii::app()->session['options']['block_length']) * 24;

        $boxMass = $this->fillArrs($block_count, Yii::app()->session['options']['block_length'], $this->getBoxes(), $date_zap);


        for ($j = 0; $j < $time_line; $j++)
            $finnalArr[] = 1;
        foreach ($boxMass as $box)
            for ($j = 0; $j < $time_line; $j++) {
                if ($box[$j] == 0) {
                    $n = 0;
                    $k = $j;
                    while (($k <> ($j + $block_count)) && ($box[$k] == 0)) {
                        $n++;
                        $k++;
                    }
                    if ($n == $block_count)
                        $finnalArr[$j] = 0;
                }
            };
        return $finnalArr;
    }

    private function getBlockCount()
    {
        if (isset(Yii::app()->session["options"]["select_serv"]))
            return ceil(Yii::app()->session['full_time_temp'] / Yii::app()->session['options']['block_length']);
        else
            return 1;
    }

    public function actionIndex($block_count)
    { //id - количество блоков
        Yii::app()->session['object_id'] = 6;
        $model = new Zapis('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Zapis']))
            $model->attributes = $_GET['Zapis'];


        $finnalArr = $this->fillBoxes($block_count);
        $this->renderPartial('index', array(
            'model' => $model, 'farr' => json_encode($finnalArr),
            'day_count' => Yii::app()->session["options"]["day_count"],
            'block_count' => (60 / Yii::app()->session['options']['block_length']) * 24,
            'block_time' => Yii::app()->session['options']['block_length'],
        ), false, true);
    }

    /**
     * Manages all models.
     */
    public function actionObject_manage()
    {
        $date_zap = THIS_DAY;
        $boxes = $this->getBoxesArray(Yii::app()->session['options']['block_length'], $this->getBoxes(), $date_zap);

        $this->render('boxes_view', array('boxes' => json_encode($boxes),
            'boxes_id' => array_keys($boxes),
            'block_count' => (60 / Yii::app()->session['options']['block_length']) * 24,
            'block_time' => Yii::app()->session['options']['block_length'],
        ));
    }

    public function actionupdateBoxesManager()
    {
        $Path_to_post = Yii::app()->request;
        $date = $this->convertData($Path_to_post->getPost('date'));

        $msg = array(
            'data' => $this->getBoxesArray(Yii::app()->session['options']['block_length'], $this->getBoxes(), $date));
        echo CJSON::encode($msg);
    }

    public function actionChangebox()
    {

        $boxes = $this->getBoxesArray(Yii::app()->session['options']['block_length'], $this->getBoxes(), THIS_DAY);

        $valid_box = $this->getValidBox($_POST["box"], $_POST["block"], $boxes, $_POST["type"]);
        if ($valid_box != false)
            $this->moveRequest($valid_box, $_POST["box"], $_POST["block"], THIS_DAY);


        $boxes_upd = $this->getBoxesArray(Yii::app()->session['options']['block_length'], $this->getBoxes(), THIS_DAY);
        $msg = array('data' => $boxes_upd, 'block_time' => Yii::app()->session['options']['block_length']);
        echo CJSON::encode($msg);
    }

    private function getValidBox($box, $block, $boxes, $type)
    {

        $block_lenght = 1; // количество требуемых свободных блоков
        $i = $block + 1;
        $valid_box = false;
        while ($i < count($boxes[$box]) && $boxes[$box][$i] == -1) {
            $block_lenght++;
            $i++;
        }

        $boxes_id = $this->createByPassArray(array_keys($boxes), $type, $box); //массив обхода содержащий id боксов
        for ($i = 0; $i < count($boxes_id); $i++) {
            $j = $block;
            if ($box != $boxes_id[$i]) {
                while ($j < $block + $block_lenght && $boxes[$boxes_id[$i]][$j] == 0)
                    $j++;
            }
            if ($j == $block_lenght + $block) {
                $valid_box = $boxes_id[$i];
                break;
            }
        }
        return $valid_box;
    }

    private function createByPassArray($boxes_id, $type, $box)
    {
        $current_index = 0;
        while ($boxes_id[$current_index] != $box)
            $current_index++;

        if ($type == 'down') {
            $temp1 = array_slice($boxes_id, 0, $current_index);
            $temp2 = array_slice($boxes_id, $current_index + 1);
            return array_merge($temp2, $temp1);
        } elseif ($type == 'up') {
            $temp1 = array_slice($boxes_id, 0, $current_index);
            $temp2 = array_slice($boxes_id, $current_index + 1);
            $finnalarr = array_merge($temp2, $temp1);
            return array_reverse($finnalarr);
        }
    }

    private function moveRequest($valid_box, $box, $block, $date)
    {
        $time = $block * Yii::app()->session['options']['block_length'];
        $time_begin = $this->plusTime('00:00:00', $time);
        $request = new Zapis;
        $request = Zapis::model()->find("time_begin=:time AND zapis_date=:date AND box_id=:box", array(':time' => $time_begin, ':date' => $date, ':box' => $box));
        $request->box_id = $valid_box;
        $request->save();
    }

    private function getBoxesArray($block_time, $box_id, $zap_date)
    {
        $time_line = (60 / $block_time) * 24; // - длина тайм лайна
        $zapisi = Zapis::model()->findAll('zapis_date=:dt AND object_id=:id AND status<>4', array(':dt' => $zap_date, ':id' => Yii::app()->session['object_id']));
        for ($i = 0; $i < $time_line; $i++) // инициализируем массив боксов еденицами, 
            foreach ($box_id as $id)
                $boxMas[$id][$i] = 1;

        $this->fillTimeOfWork($boxMas, $box_id, $block_time, 1, $zap_date); // добавим условия времени работы боксов

        foreach ($zapisi as $zapis) {
            $block_begin = $this->getBlockNum($block_time, $zapis['time_begin']);
            $lenght = ceil($zapis['full_time_temp'] / $block_time);
            $boxMas[$zapis['box_id']][$block_begin] = $zapis['id'];
            for ($i = $block_begin + 1; $i < $block_begin + $lenght; $i++)
                $boxMas[$zapis['box_id']][$i] = -1;
        }

        return $boxMas;
    }

    public function actionViewRecord($rec_id)
    {
        $record = Zapis::model()->findByPk($rec_id);

        $z = ZapisIspolnitel::model()->findAll("zapis_id=:zap", array(':zap' => $rec_id));
        if ($z)
            foreach ($z as $isp) {
                $executor_index[] = $isp->ispolnitel_id;
            } else
            $executor_index = '';


        $executors_array = PolzovatelObject::model()->findAll("object_id=:obj", array(':obj' => $record['object_id']));

        foreach ($executors_array as $ex) {
            $executors_all[$ex->polzovatel->id] = $ex->polzovatel->fio;
        }

        $serv = ZapisUsluga::model()->findAll("zapis_id=:zap", array(':zap' => $rec_id));
        if (count($serv) !== 0)
            foreach ($serv as $s) {
                $services_choosen[] = array('name' => $s->usluga->uslugi->nazvanie, 'id' => $s->usluga_id,
                    'cost' => $s->usluga->cena, 'duration' => $s->usluga->dlitelnost, 'relation_id' => $s->id);
            }

        if (!isset($services_choosen)) {
            $services_choosen = null;
        } else {
            foreach ($services_choosen as $s) {
                $helpers_rec = Pomoshniki::model()->findAll("id_zapis_usluga=:id_r", array(':id_r' => $s['relation_id']));
                foreach ($helpers_rec as $helper) {
                    $name = $executors_all[$helper->id_pomoshnik];
                    $helpers[] = array($helper->idZapisUsluga->usluga_id, $helper->id_pomoshnik, $name);
                }
            }
        }
        if (!isset($helpers)) {
            $helpers = null;
        }

        $services = UslugaObject::model()->findAll("id_object=:obj AND car_gruppa_id=:gr", array(':obj' => $record['object_id'], ':gr' => $record['car_group_id']));
        foreach ($services as $service) {
            $valid_services[$service->id_usluga] = $service->uslugi->nazvanie;
        }


        $this->render('ViewRecord', array('services' => json_encode($services_choosen
        ), 'record' => ($record),
            'executor_index' => $executor_index,
            'executors_all' => $executors_all,
            'services_all' => $valid_services,
            'helpers' => $helpers,
        ));
    }

    private function deleteAllRecordData($rec_id)
    {
        $record_services = ZapisUsluga::model()->findAll("zapis_id=:z_id", array(":z_id" => $rec_id));
        if ($record_services)
            foreach ($record_services as $row) {
                Pomoshniki::model()->deleteAll('id_zapis_usluga=:id_z', array(":id_z" => $row->id));
                $row->delete();
            }
    }

    private function deleteServices($services, $record)
    {
        foreach ($services as $service)
            $all_id[] = $service["id"];
        $record_services = ZapisUsluga::model()->findAll("zapis_id=:z_id", array(":z_id" => $record));
        foreach ($record_services as $row) {
            if (!in_array($row->usluga_id, $all_id)) {
                Pomoshniki::model()->deleteAll('id_zapis_usluga=:id_z', array(":id_z" => $row->id));
                ZapisUsluga::model()->deleteByPk($row->id);
            }
        }
    }

    public function actionrecordPaid()
    {
        $Path_to_post = Yii::app()->request;
        $rec_id = $Path_to_post->getPost('record_id');
        $record = Zapis::model()->findByPk($rec_id);
        $record->oplacheno = 1;
        $record->save();
    }

    public function actiondeleteRecord()
    {
        $Path_to_post = Yii::app()->request;
        $rec_id = $Path_to_post->getPost('record_id');
        $record = Zapis::model()->findByPk($rec_id);
        $record->status = 4;
        $record->save();
    }

    public function actionsaveRecordData()
    {
        $Path_to_post = Yii::app()->request;
        $services = $Path_to_post->getPost('services');
        $helpers = $Path_to_post->getPost('helpers');
        $executors = $Path_to_post->getPost('executors');
        $rec_id = $Path_to_post->getPost('record_id');

        if (!isset($services))
            $this->deleteAllRecordData($rec_id);
        else {
            foreach ($executors as $executor) {

                $chk = ZapisIspolnitel::model()->find("zapis_id=:z_id AND ispolnitel_id=:i_id", array(":z_id" => $rec_id,
                    ":i_id" => $executor));
                if ($chk)
                    $zapis_isp = $chk;
                else
                    $zapis_isp = new ZapisIspolnitel;
                $zapis_isp->zapis_id = $rec_id;
                $zapis_isp->ispolnitel_id = $executor;
                $zapis_isp->save();
            }
            foreach ($services as $service) {

                $chk = ZapisUsluga::model()->find("zapis_id=:z_id AND usluga_id=:u_id", array(":z_id" => $rec_id,
                    ":u_id" => $service["id"]));
                if ($chk) {
                    $zap_usl = $chk;
                } else {
                    $zap_usl = new ZapisUsluga;
                }
                $zap_usl->zapis_id = $rec_id;
                $zap_usl->usluga_id = $service["id"];
                $zap_usl->cena = $service["cost"];
                $zap_usl->dlitelnost = $service["duration"];
                $zap_usl->save();
            }
            $this->deleteServices($services, $rec_id);
            if (isset($helpers))
                foreach ($helpers as $helper) {
                    $id_z_u = ZapisUsluga::model()->
                        find("zapis_id=:r_id AND usluga_id=:u_id", array(":r_id" => $rec_id, ":u_id" => $helper["service"]))->id;

                    $chk = Pomoshniki::model()->find("id_pomoshnik=:p_id AND id_zapis_usluga=:z_id", array(":p_id" => $helper["id"],
                        ":z_id" => $id_z_u));

                    if ($chk) {
                        $helper_db = $chk;
                    } else {
                        $helper_db = new Pomoshniki;
                    }
                    $helper_db->id_pomoshnik = $helper["id"];
                    $helper_db->id_zapis_usluga = $id_z_u;
                    $helper_db->save();
                }
        }
    }

    public function actionaddNewService()
    {
        $Path_to_post = Yii::app()->request;
        $service_id = $Path_to_post->getPost('service_id');
        $avto_type = $Path_to_post->getPost('avto_type');
        //    echo $service_id;
        //   exit;
        $serv = UslugaObject::model()->find("id_usluga=:id AND id_object=:obj AND car_gruppa_id=:gr", array(':id' => $service_id, ':obj' => Yii::app()->session['object_id'], ':gr' => $avto_type));

        $service_to_add = array('name' => $serv->uslugi->nazvanie, 'id' => $serv->id,
            'cost' => $serv->cena, 'duration' => $serv->dlitelnost);
        echo json_encode($service_to_add);
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'zapis-usluga-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
