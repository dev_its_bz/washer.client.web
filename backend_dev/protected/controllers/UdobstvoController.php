<?php

class UdobstvoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','SaveAndAdd'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete', 'updateZnachenie'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
        
        public function actionSaveAndAdd($id){
        
                if (isset($_POST['model_temp']))
                $this->InstertLink(array_diff($_POST['model_to_insert'], $_POST['model_temp']), $id); //массив на добавление
            else
                $this->InstertLink($_POST['model_to_insert'], $id);

        if (isset($_POST['model_temp']))
            if (isset($_POST['model_to_insert']))
//массив на удаление
                $this->DeleteLink(array_diff($_POST['model_temp'], $_POST['model_to_insert']), $id);
            else
                $this->DeleteLink($_POST['model_temp'], $id);

        $this->redirect(array('/object/view', 'id' => $id, 'tab' => 8));
        }
        
    private function InstertLink($IdArr, $idObj) {
        foreach ($IdArr as $IdUd) {
            $link = new UdobstvoObject;
            $link->udobstvo_id = $IdUd;
            $link->object_id = $idObj;
            $link->avtor_zapisi_id = Yii::app()->user->id;
            $link->save();
        }
    }
    
    private function DeleteLink($IdArr, $idObj) {
        $criteria = new CDbCriteria;
        $criteria->addInCondition('udobstvo_id', $IdArr);
        $criteria->addCondition('object_id=:idObj', 'AND');
        $criteria->params += array(':idObj' => $idObj);
        UdobstvoObject::model()->deleteAll($criteria);
    }
        
	public function actionCreate()
	{
		$model=new Udobstvo;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Udobstvo']))
		{
			$model->attributes=$_POST['Udobstvo'];
			if($model->save())
                $this->redirect(array('/Udobstvo/admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Udobstvo']))
		{
			$model->attributes=$_POST['Udobstvo'];
			if($model->save())
				$this->redirect(array('/Udobstvo/admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

    public function actionUpdateZnachenie()
    {
        $es = new TbEditableSaver('Udobstvo');
        $es->update();
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Udobstvo');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Udobstvo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Udobstvo']))
			$model->attributes=$_GET['Udobstvo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Udobstvo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='udobstvo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
