<?php

class CarModelController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'ModelByGroup', 'ComboModelByMarka'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'SaveGruppaAndAdd', 'delete', 'UpdateZnachenie'),
                'roles' => array('administrator'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin'),
                'roles' => array('manager'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id)
    {
        $model = new CarModel;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['CarModel'])) {
            $model->attributes = $_POST['CarModel'];
            if ($model->save())
                $this->redirect(array('/CarMarka/update/', 'id' => $model->car_marka_id));
        }

        $this->render('create', array(
            'model' => $model, 'id' => $id
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['CarModel'])) {
            $model->attributes = $_POST['CarModel'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionModelByGroup($id)
    {

        $model = new CarModel('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['CarModel']))
            $model->attributes = $_GET['CarModel'];

        $model->car_gruppa_id = $id;
        $this->render('Models_Groups', array(
            'model' => $model, 'group' => $id,
        ));
    }

    public function actionUpdateZnachenie()
    {
        $es = new TbEditableSaver('CarModel');
        $es->update();
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('CarModel');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionComboModelByMarka()
    {
        $data = CarModel::model()->findAllByAttributes(array('car_marka_id' => $_POST['car_marka_id']), array('order' => 'nazvanie ASC'));
        $data = CHtml::listData($data, 'id', 'nazvanie');

        echo "<option value=''>Выберите марку</option>";
        foreach ($data as $value => $name)
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($name), true);
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new CarModel('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['CarModel']))
            $model->attributes = $_GET['CarModel'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionSaveGruppaAndAdd($group)
    {

        if (isset($_POST['model_to_insert']))
            if (isset($_POST['model_temp']))
                $this->UpdateLink(array_diff($_POST['model_to_insert'], $_POST['model_temp']), $group); //массив на добавление
            else
                $this->UpdateLink($_POST['model_to_insert'], $group);

        if (isset($_POST['model_temp']))
            if (isset($_POST['model_to_insert']))
                //массив на удаление
                $this->UpdateLink(array_diff($_POST['model_temp'], $_POST['model_to_insert']), 0);
            else
                $this->UpdateLink($_POST['model_temp'], 0);

        $this->redirect(array('/carGruppa/index/18'));

    }

    private function UpdateLink($IdArr, $id_group)
    {
        foreach ($IdArr as $IdCar) {
            //   $link = new CarModel;
            $link = $this->loadModel($IdCar);

            $link->car_gruppa_id = $id_group;

            $link->save();
        }

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = CarModel::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'car-model-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
