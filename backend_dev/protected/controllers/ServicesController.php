<?php

class ServicesController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'admin'),
                'roles' => array('manager'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('ViewByTip','delete', 'create', 'update'),
                'roles' => array('administrator'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {

        $dataProvider = new CActiveDataProvider('UslugaCarGruppa');
        $dataProvider->setData(UslugaCarGruppa::model()->findAll('usluga_id=:u_id', array(':u_id' => $id)));

        $this->render('view', array(
            'model' => $this->loadModel($id), 'prise' => $dataProvider
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new services;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['services'])) {
            $model->attributes = $_POST['services'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }


        $moiki = object::model()->findAll();
        $moiki_data = CHtml::listData($moiki, 'id', 'nazvanie');

        $uslugi = UslugaGruppa::model()->findAll();
        $uslugi_data = CHtml::listData($uslugi, 'id', 'nazvanie');

        $this->render('create', array(
            'model' => $model, 'moiki_data' => $moiki_data, 'uslugi_data' => $uslugi_data
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['services'])) {
            $model->attributes = $_POST['services'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $moiki = object::model()->findAll();
        $moiki_data = CHtml::listData($moiki, 'id', 'nazvanie');

        $uslugi = UslugaGruppa::model()->findAll();
        $uslugi_data = CHtml::listData($uslugi, 'id', 'nazvanie');

        $this->render('update', array(
            'model' => $model, 'moiki_data' => $moiki_data, 'uslugi_data' => $uslugi_data
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        //$dataProvider=new CActiveDataProvider('services');
        // $model=new services;
        // $dataProvider = $model->search(null);
        //   print_r($dataProvider);
        //    exit;
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($id = 0) {

        $model = new Usluga('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Usluga']))
            $model->attributes = $_GET['Usluga'];

        $this->render('admin', array(
            'model' => $model,
            'id' => $id,
        ));
    }

    public function actionViewByTip($id) {
        $model = new Usluga('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Usluga']))
            $model->attributes = $_GET['Usluga'];
        $model->usluga_tip_id = $id;
        $this->render('usluga/view_by_tip', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return services the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = services::model()->findByPk($id);

        //  $model['moika'] = $model->moika->nazvanie;
        //  $model['gruppa'] = $model->gruppa->nazvanie;

        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');

        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param services $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'services-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
