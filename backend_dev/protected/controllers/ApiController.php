<?php

class ApiController extends Controller
{

    Const APPLICATION_ID = 'washer.its.bz';

    private $format = 'json';

    //===================================== ACTION BEFORE ACTION ======================================================
    public function beforeAction($action)
    {
        return true;
        //DEBUG
        if ($action->id == "login" || $action->id == "verify")
            return true;

        if (isset($_REQUEST['auth_polzovatel_id']) && isset($_REQUEST['auth_polzovatel_parol'])) {
            $polzovatel = Polzovatel::model()->findByPk($_REQUEST['auth_polzovatel_id']);
            if (count($polzovatel) == 1)
                if ($polzovatel->parol == $_REQUEST['auth_polzovatel_parol'])
                    return true;
        }
        $this->_sendResponse(401, sprintf('"Auth error."'));
        return false;
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array();
    }

    public function actionGiveMeServices()
    {
        $services = Usluga::model()->findAll();
        $rows_x = array();
        foreach ($services as $model) {
            $temp["name"] = $model->nazvanie;
            $temp["id"] = $model->id;
            $temp["id_group"] = $model->usluga_tip_id;
            $rows_x[] = $temp;
        }
        $uslugi = UslugaTip::model()->findAll();
        $rows_y = array();
        foreach ($uslugi as $model) {
            $temp_1["name"] = $model->nazvanie;
            $temp_1["id"] = $model->id;
            $rows_y[] = $temp_1;
        }
        $rows = array();
        array_push($rows, $rows_x, $rows_y);
//        $rows['services'] = $rows_x;
        //      $rows['groups'] = $rows_y;
        //              $rows['1'] = 'a';
        $this->_sendResponse(200, CJSON::encode($rows));
    }

    //===================================== ACTION LOGIN VERIFY =======================================================

    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if ($body != '') {
            // send the body
            echo '{"data":' . $body . '}';
        } // we need to create the body if none is passed
        else {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch ($status) {
                case 401:
                    $message = '"You must be authorized to view this page."';
                    break;
                case 404:
                    $message = '"The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found."';
                    break;
                case 500:
                    $message = '"The server encountered an error processing your request."';
                    break;
                case 501:
                    $message = '"The requested method is not implemented."';
                    break;
            }

            // servers don't always have a signature turned on
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
</head>
<body>
    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
    <p>' . $message . '</p>
    <hr />
    <address>' . $signature . '</address>
</body>
</html>';

            echo $body;
        }
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    public function actionLogin()
    {
        $telefon = @$_REQUEST['telefon'];
        $password = @$_REQUEST['password'];

        $models = Polzovatel::model()->findByAttributes(array("telefon" => $telefon));
        if (count($models) == 1) {
            if (md5(md5($password)) == $models->parol) {
                $rows = array();
                $rows[] = $models->attributes;
                $this->_sendResponse(200, CJSON::encode($rows));
            } else
                $this->_sendResponse(200, '[{"error":"Password incorrect"}]');
            //Existing user
            //Send id, password, name and email in JSON
            //On client filles data from JSON
        } else {
            $models = new Polzovatel;
            $models->telefon = $telefon;
            $models->login = $telefon;
            $models->parol = md5(md5($password));
            $models->gruppa_id = "3";
            $models->status_id = "2";
            $models->save();
            $id = $models->getPrimaryKey();
            $models->fio = "Пользователь " . $id;
            $models->save();

            $models = Polzovatel::model()->findAllByPk($id);
            $rows = array();
            foreach ($models as $model)
                $rows[] = $model->attributes;
            $this->_sendResponse(200, CJSON::encode($rows));
            //NewUser
            //Create new user with paswd and telephone
            //Send id, password, name and email in JSON
            //On client request filling user data
            //Edit profile action fired
        }
    }

    public function actionUserStatus()
    {
        $telefon = @$_REQUEST['telefon'];
        //   $password = @$_REQUEST['password'];

        $models = Polzovatel::model()->findByAttributes(array("telefon" => $telefon));
        if (count($models) == 1) {
            $rows['isNew'] = false;
        } else {
            $rows['isNew'] = true;
        }
        $this->_sendResponse(200, CJSON::encode($rows));
    }

    public function actionSetCgmId($user_id, $reg_id)
    {
        $record = new CGMID();
        $record->user_id = $user_id;
        $record->registration_id = $reg_id;
        $record->save();
    }

    public function actionSendPushMessage($user_id, $action)
    {

        $url = 'https://android.googleapis.com/gcm/send';

        $cgm_info = CGMID::model()->find("user_id=:id", array(":id" => $user_id));

        $reg_ids = array();
        array_push($reg_ids, $cgm_info->registration_id);

        $message = array();
        // action {wash_ended, stadtus_changed, simple_message, need_update}
        //TODO добавить параметры когда будет интерфейс

        switch ($action) {
            case "wash_ended":
                $message["Notice"] = "Окончание мойки";
                $message["action"] = "1";
                $object_id = "63";
                $message["params"] = array("object_id" => $object_id, "test_cl" => "kfsd");
                break;
            case "status_changed":
                $message["Notice"] = "Статус заказа изменен";
                $message["action"] = "2";
                break;
            case "simple_message":
                $message["Notice"] = "Оповещение";
                $message["action"] = "3";
                break;
            case "need_update":
                $message["Notice"] = "Требуется обновление приложения";
                $message["action"] = "4";
                break;
        }

        $fields = array(
            'registration_ids' => $reg_ids,
            'data' => $message,
        );

        //  var_dump($fields);

        $headers = array(
            'Authorization: key=AIzaSyDTplj4DjIffvr0sIV6KdKheiIwWQmFn5A',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        echo $result;
    }

    public function actionChangePassword()
    {
        $telefon = @$_REQUEST['telefon'];
        $password = @$_REQUEST['password'];
        $models = Polzovatel::model()->findByAttributes(array("telefon" => $telefon));
        $models->parol = $password;
        $models->save();
        $this->_sendResponse(200, sprintf('"Request OK."'));
    }

    public function actionVerify()
    {
        $telefon = @$_REQUEST['telefon'];
        //Generate uniq verify code
        //Send SMS with uniq verify code
        //Send JSON with uniq verify code
        //On client check SMS<>JSON.
        //If equals then actionLogin called
        $hash = rand(1000, 9999);
        $ch = curl_init("http://sms.ru/sms/send");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            "api_id" => "2ec5637c-0b81-acd4-d128-45d2c7f57780 ",
            "to" => "$telefon",
            "from" => "WashMeUpCom",
            "text" => iconv("windows-1251", "utf-8", "$hash")
        ));
        if (false === ($Result = curl_exec($ch))) {
            throw new Exception('Http request failed');
        }
        curl_close($ch);

        $res = md5("$hash");
        $data = new stdClass();
        $data->data = new stdClass();
        $data->data->verifyCode = $res;
        echo CJSON::encode($data);
    }

    public function actionDelete()
    {
        $model = @$_REQUEST['model'];
        switch ($model) {
            case 'polzovatel_car':
                $car = PolzovatelCar::model()->findByPk($_REQUEST['car_id']);
                $car->deleted = "1";
                $car->save();
                break;
            default:
                $this->_sendResponse(200, sprintf('"Request OK."'));
        }
        $this->_sendResponse(200, sprintf('"Request OK."'));
    }

    public function actionAddReview()
    {
        $review = new Reviews();
        $review->polzovatel_id = @$_POST["user_id"];
        $review->object_id = @$_POST["object_id"];
        $review->grade = @$_POST["grade"];
        $review->review = @$_POST["comment"];
        $review->save();
    }

    //===================================== ACTION EDIT ===============================================================

    public function actionAddDeveloperAppeal()
    {
        $appeal = new RazrabotchikJaloba();
        $appeal->user_id = @$_POST["user_id"];
        $appeal->appeal = @$_POST["appeal"];
        $appeal->save();
    }

    //===================================== ACTION VIEW================================================================

    public function actionAddObjectAppeal()
    {
        $appeal = new ObjectJaloba();
        $appeal->user_id = @$_POST["user_id"];
        $appeal->appeal = @$_POST["appeal"];
        $appeal->object_id = @$_POST["object_id"];
        $appeal->save();
    }

    public function actionEdit()
    {
        $model = @$_REQUEST['model'];
        switch ($model) {
            case 'polzovatel_car':
                if (@$_REQUEST["is_new"] == "1")
                    $car = new PolzovatelCar;
                else
                    $car = PolzovatelCar::model()->findByPk(@$_REQUEST['car_id']);

                $car->id = @$_REQUEST['car_id'];
                $car->polzovatel_id = @$_REQUEST['polzovatel_id'];
                $car->car_model_id = @$_REQUEST['car_model_id'];
                $car->gos_nomer = @$_REQUEST['gos_nomer'];
                $car->save();
                print_r($car);
                die();
                break;

            case 'polzovatel':
                $polzovatel = Polzovatel::model()->findByPk(@$_REQUEST['polzovatel_id']);
                $polzovatel->email = @$_REQUEST['email'];
                $polzovatel->fio = @$_REQUEST['fio'];
                $polzovatel->save();
                break;
            default:
                $this->_sendResponse(200, sprintf('"Request OK."'));
        }
        $this->_sendResponse(200, sprintf('"Request OK."'));
    }

    public function actionGetRecordData()
    {
        $record_id = @$_REQUEST['record_id'];
        $data = Zapis::model()->getRecordData($record_id);
        $this->_sendResponse(200, CJSON::encode($data));
    }

    public function actionView()
    {
        $model = @$_REQUEST['model'];
        switch ($model) {
            case 'polzovatel':
                $models = Polzovatel::model()->findAllByPk($_REQUEST['polzovatel_id']);
                break;

            case 'object':
                $object_id = $_REQUEST['object_id'];
                $user_id = @$_POST["user_id"];
                if (!isset($_REQUEST['myLat']) || !isset($_REQUEST['myLon']) || $_REQUEST['myLat'] == "" || $_REQUEST['myLat'] == "")
                    $models = ObjectListView::model()->findAllBySql("SELECT a.*, '?' AS distance, IF(b.data_sozdaniya,1,0) AS favorite"
                        . " FROM object_list_view AS a LEFT JOIN favorites AS b ON b.object_id=a.id AND b.user_id=$user_id WHERE a.id=$object_id");

                //     $models = ObjectListView::model()->findAllBySql("SELECT *, '?' AS distance FROM object_list_view WHERE id=$object_id");
                else {
                    $myLat = @$_REQUEST['myLat'];
                    $myLon = @$_REQUEST['myLon'];
                    $models = ObjectListView::model()->findAllBySql("SELECT a.*, geodist($myLat, $myLon, lat, lon) AS distance, IF(b.data_sozdaniya,1,0) AS favorite"
                        . " FROM object_list_view AS a LEFT JOIN favorites AS b ON b.object_id=a.id AND b.user_id=$user_id WHERE a.id=$object_id");

                    //  $models = ObjectListView::model()->findAllBySql("SELECT *, geodist($myLat, $myLon, lat, lon) AS distance FROM object_list_view  WHERE id=$object_id");
                }
                break;

            case 'polzovatel_car':
                $models = PolzovatelCarListView::model()->findAllByAttributes(array("id" => $_REQUEST['car_id']));
                break;

            default:
                $this->_sendResponse(501, sprintf(
                    '"Error: Mode <b>list</b> is not implemented for model <b>%s</b>"', $model));
                Yii::app()->end();
        }

        if (empty($models)) {
            $this->_sendResponse(200, sprintf('"No items where found for model <b>%s</b>"', $model));
        } else {
            $rows = array();
            foreach ($models as $model)
                $rows[] = $model->attributes;
            $this->_sendResponse(200, CJSON::encode($rows));
        }
    }

    public function actiongetTimeList()
    {


        $date = @$_POST["date"];
        if ($date == "0")
            $date = 'CURRENT_DATE()';
        else
            $date = "CURRENT_DATE() + INTERVAL $date DAY";


        $obj_id = @$_POST["object_id"];
        if (isset($_POST["duration"]))
            $dlitelnost = @$_POST["duration"];
        else
            $dlitelnost = UslugaObject::model()->getDuration(@$_POST["services"], CarModel::model()->getCarType(@$_POST["car_id"]), $obj_id);

        $time_array = ObjectResourceView::model()->getTimeList($obj_id, $date, $dlitelnost);

        $this->_sendResponse(200, CJSON::encode($time_array));
    }

    //===================================== ACTION LIST ===============================================================

    public function actionAddRecord()
    {
        //    $polzovatel_id, $car_id, $services, $box_resource_id
        $polzovatel_id = @$_POST["user_id"];
        $car_id = @$_POST["car_id"];
        $box_resource_id = @$_POST["resource_id"];
        if (isset($_POST["services"]))
            $service_list = @$_POST["services"];
        else
            $service_list = null;

        //Проверяем, не заняли ли уже наш бокс на это время
        $box_resource = BoxResource::model()->findByPk($box_resource_id);

        if ($box_resource->resource == 0) { //TODO реусрс должен сравниваться с длительностью мойки
            //Бокс уже заняли, ищем другой свободный на жто же время на этом же объекте
            $box_resource = BoxResource::model()->findByAttributes(
                array(
                    'vremya' => $box_resource->vremya,
                    'object_id' => $box_resource->object_id,
                    'data' => $box_resource->data,), array(
                    'condition' => 'resource <> :resource',
                    'params' => array(':resource' => 0)
                )
            );
            if ($box_resource == null) {
                $message = '1'; // занятно
                $this->_sendResponse(200, CJSON::encode($message));
                return;
            }
        }

        $zapis = Zapis::model()->findAllByAttributes(array(
                'polzovatel_id' => $polzovatel_id,
                'zapis_date' => $box_resource->data,
                'car_id' => $car_id,
                'status_id' => array(0, 1, 2)
            )
        );


        if (sizeof($zapis) > 2) {
            $message = '2'; //К сожалению вы исчерпали количество допустимых записей этого авто на этот день. Выберите другой день или отмените запись.

            $this->_sendResponse(200, CJSON::encode($message));
            return;
        }


        $zapis = new Zapis();
        $zapis->polzovatel_id = $polzovatel_id;
        $zapis->avtor_zapisi_id = $polzovatel_id;
        $zapis->object_id = $box_resource->object_id;
        $zapis->box_id = $box_resource->box_id;
        $zapis->car_id = $car_id;


        $polzovatelCar = PolzovatelCar::model()->findByPk($car_id);
        $car_gruppa_id = $polzovatelCar->carModel->car_gruppa_id;
        $object_id = $box_resource->object_id;

        if ($service_list != null) {
            $sql = "
        SELECT SUM(cena) AS cena, SUM(dlitelnost) AS dlitelnost
        FROM usluga_object
        WHERE id_usluga IN (" . implode(",", $service_list) . ") AND car_gruppa_id = $car_gruppa_id AND id_object = $object_id
        ";
        } else {
            $sql = "
        SELECT SUM(cena) AS cena, SUM(dlitelnost) AS dlitelnost
        FROM usluga_object
        WHERE id_usluga IN (-1) AND car_gruppa_id = $car_gruppa_id AND id_object = $object_id
        ";
        }
        $services = UslugaObject::model()->findBySql($sql);
        $zapis->summa = $services->cena;
        if ($services->dlitelnost == null) {

            $dlitelnost = @OpciiSelect::model()->findByAttributes(array('object_id' => $object_id, 'opcii_id' => 2))->znachenie;
            if ($dlitelnost != null)
                $services->dlitelnost = $dlitelnost;
            else
                $services->dlitelnost = '30';
        }
        $zapis->full_time_temp = $services->dlitelnost;

        $zapis->time_begin = $box_resource->vremya;
        $zapis->time_end = new CDbExpression("TIME('2014-01-01 " . $box_resource->vremya . "' + INTERVAL " . $services->dlitelnost . " MINUTE)");
        $zapis->data_zapisi = new CDbExpression('NOW()');
        $zapis->data_sozdaniya = new CDbExpression('NOW()');
        $zapis->status_id = '0';
        $zapis->zapis_date = $box_resource->data;
        $zapis->oplacheno = '0';

        if ($zapis->save()) {
            if ($service_list != null) {

                $sql = "
        SELECT *
        FROM usluga_object
        WHERE id_usluga IN (" . implode(",", $service_list) . ") AND car_gruppa_id = $car_gruppa_id AND id_object = $object_id
        ";
                $services_data_list = UslugaObject::model()->findAllBySql($sql);

                foreach ($services_data_list as $service) {
                    $zapis_service = new ZapisUsluga;
                    $zapis_service->usluga_id = $service->id;
                    $zapis_service->cena = $service->cena;
                    $zapis_service->dlitelnost = $service->dlitelnost;
                    $zapis_service->zapis_id = $zapis->id;
                    $zapis_service->save();
                }
            }
            $message = $zapis->getPrimaryKey(); // запись создана
        } else
            $message = "3"; //Ошибка записи. Сообщите о ней администратору
        $this->_sendResponse(200, CJSON::encode($message));
    }

    public function actionGetSettings()
    {
        if ($record = PolzovatelNastroiki::model()->findByAttributes(array("polzovatel_id" => @$_POST["user_id"], "opcii_id" => 1))) {
            $row["service_enable"] = $record->value;
            $record = PolzovatelNastroiki::model()->findByAttributes(array("polzovatel_id" => @$_POST["user_id"], "opcii_id" => 2));
            $row["filter_count"] = $record->value;
        } else {
            $row["service_enable"] = '1';
            $row["filter_count"] = '20';
        }
        $this->_sendResponse(200, CJSON::encode($row));
    }

    public function actionChangeSettings()
    {
        if (isset($_POST["service_enable"])) {
            $this->AddSetting(@$_POST["user_id"], @$_POST["service_enable"] == "false" ? 0 : 1, 1);
        }
        if (isset($_POST["filter_count"])) {
            $this->AddSetting(@$_POST["user_id"], @$_POST["filter_count"], 2);
        }
        $this->_sendResponse(200, sprintf('"Request OK."'));
    }

    private function AddSetting($user_id, $value, $setting_id)
    {
        $record = PolzovatelNastroiki::model()->findByAttributes(array("polzovatel_id" => $user_id, "opcii_id" => $setting_id));
        if (count($record) == 1) {
            $record->value = $value;
            $record->save();
        } else {
            $record = new PolzovatelNastroiki;
            $record->value = $value;
            $record->opcii_id = $setting_id;
            $record->polzovatel_id = $user_id;
            $record->save();
        }
    }

    public function actionAddToFavorite()
    {
        $rec = new Favorites();
        $rec->user_id = @$_POST["user_id"];
        $rec->object_id = @$_POST["object_id"];
        $rec->save();
    }

    public function actionRemoveFromFavorite()
    {
        $rec = Favorites::model()->find("user_id = :u_id AND object_id=:o_id", array(":u_id" => @$_POST["user_id"], ":o_id" => @$_POST["object_id"]));
        $rec->delete();
    }

    //===================================== SEND RESPONSE =============================================================

    public function actionSaveUserTown()
    {
        $user_id = @$_POST['user_id'];
        $town_id = @$_POST['town_id'];
        $rec = PolzovatelGorod::model()->find("polzovatel_id = :u_id", array(":u_id" => $user_id));
        if ($rec) {
            $rec->polzovatel_id = $user_id;
            $rec->gorod_id = $town_id;
            $rec->save();
        } else {
            $rec = new PolzovatelGorod();
            $rec->polzovatel_id = $user_id;
            $rec->gorod_id = $town_id;
            $rec->save();
        }
    }

    public function actionList()
    {
        $model = @$_REQUEST['model'];
        $message_post = false;
        switch ($model) {
            case 'object':
            {
                if (@$_POST['car_id'] != null) {
                    $polzovatelCar = PolzovatelCar::model()->findByPk(@$_POST['car_id']);
                    $car_gruppa_id = $polzovatelCar->carModel->car_gruppa_id;
                } else
                    $car_gruppa_id = 18;
                $this->ObjByfilters(@$_POST['filter'], @$_POST['services'], $car_gruppa_id, @$_POST['town_id'], @$_POST['limit'], @$_POST['user_id']);

                $message_post = true;
                break;
            }

            case 'object_favorites':
            {
                $myLat = @$_REQUEST['myLat'];
                $myLon = @$_REQUEST['myLon'];
                $user_id = @$_POST['user_id'];

                $models = ObjectListView::model()->getViewWithFavorites($user_id, $myLat, $myLon);
                $this->addUdobstva($models);
                $this->_sendResponse(200, CJSON::encode($models));
                $message_post = true;

                break;
            }

            case 'polzovatel_car':
                $models = PolzovatelCarListView::model()->findAllByAttributes(array("polzovatel_id" => $_REQUEST['polzovatel_id']));
                break;

            case 'gorod':
            {
                $myLat = @$_REQUEST['myLat'];
                $myLon = @$_REQUEST['myLon'];
                $models = Gorod::model()->findAllBySql("SELECT a.id, a.nazvanie, geodist($myLat, $myLon, lat, lon) AS distance, a.lat, a.lon"
                    . " FROM gorod AS a "
                    . " ORDER BY distance ASC");
                break;
            }
            case 'car_marka':
                $models = CarMarkaListView::model()->findAll();
                break;

            case 'car_model':
                $carMarkaId = @$_REQUEST['car_marka_id'];
                $models = CarModelListView::model()->findAllByAttributes(array("car_marka_id" => "$carMarkaId"));
                break;

            case 'usluga':
                $objectId = @$_REQUEST['object_id'];
                $models = UslugaListView::model()->findAllByAttributes(array("id_object" => "$objectId"));
                break;

            case 'review':
                $objectId = @$_REQUEST['object_id'];
                $models = ReviewListView::model()->findAllByAttributes(array("object_id" => "$objectId"));
                break;

            case 'service':
                $objectId = @$_REQUEST['object_id'];
                $carId = @$_REQUEST['car_id'];
                $models = ServiceListView::model()->findAllBySql("SELECT * FROM service_list_view WHERE object_id='$objectId' AND car_gruppa_id=getCarGruppa('$carId')");
                break;

            case 'comfort':
                $objectId = @$_REQUEST['object_id'];
                $models = UdobstvoListView::model()->findAllByAttributes(array("object_id" => "$objectId"));
                break;

            case 'records':
                $user_id = @$_REQUEST['user_id'];
                // $models = Zapis::model()->findAllByAttributes(array("polzovatel_id"=>"$user_id"));
                //        $models = Zapis::model()->findAllBySql("SELECT zapis.*, object.nazvanie as `object_name` FROM zapis inner join object on zapis.object_id=object.id WHERE polzovatel_id='$user_id' ORDER BY data_zapisi DESC");
                $models = UserRecordList::model()->findAllByAttributes(array("user_id" => "$user_id"));
                break;
            default:
                $this->_sendResponse(501, sprintf(
                    '"Error: Mode <b>list</b> is not implemented for model <b>%s</b>"', $model));
                Yii::app()->end();
        }
        if (!$message_post)
            if (empty($models)) {
                $this->_sendResponse(200, sprintf('"No items where found for model <b>%s</b>"', $model));
            } else {
                $rows = array();
                foreach ($models as $model)
                    $rows[] = $model->attributes;
                $this->_sendResponse(200, CJSON::encode($rows));
            }
    }

    public function ObjByfilters($arr = null, $services = null, $car_gruppa_id = 18, $town_id, $limit = 10000, $user_id)
    {
        $filter_result = array();
        $washer_list = array();
        $fromFavorite = false;
        if ($arr) {
            foreach ($arr as $request) {
                switch ($request) {
                    case "Самая ранняя":
                        $objects = ObjectListView::model()->getFasters(@$_REQUEST['myLat'], @$_REQUEST['myLon'], $town_id, $user_id);
                        break;
                    case "Самая близкая":
                        $objects = ObjectListView::model()->getByCoords(@$_REQUEST['myLat'], @$_REQUEST['myLon'], $town_id, $user_id);
                        break;
                    case "Самая дешёвая":
                        $objects = ObjectListView::model()->getByCost($services, $car_gruppa_id, @$_REQUEST['myLat'], @$_REQUEST['myLon'], $town_id, $user_id);
                        break;
                    case "Самая лучшая":
                        $objects = ObjectListView::model()->getByRating(@$_REQUEST['myLat'], @$_REQUEST['myLon'], $town_id, $user_id);
                        break;
                    case "Из любимых":
                        $fromFavorite = true;
                        break;
                    default:
                        $objects = ObjectListView::model()->getFirstX('999999', @$_REQUEST['myLat'], @$_REQUEST['myLon'], $town_id, $user_id);
                }
                if ($request <> "Из любимых") {
                    array_push($filter_result, $objects);
                    $washer_list = array_merge($washer_list, $objects);
                }
            }

            $res = $this->mergeArrays($washer_list, $filter_result);
            $res1 = $res;
            if ($fromFavorite) {
                $favorites = ObjectListView::model()->getFromFavorites(@$_REQUEST['myLat'], @$_REQUEST['myLon'], $user_id);
                $res = $this->arrayLogicAnd($res1, $favorites);
            }
            if ($services) {
                $ObjectsWithService = ObjectListView::model()->getByServices($services, $car_gruppa_id, @$_REQUEST['myLat'], @$_REQUEST['myLon'], $town_id, $limit, $user_id);
                $finnal = $this->arrayLogicAnd($res, $ObjectsWithService);
                $this->addFullCostAndTime($finnal, $services, $car_gruppa_id);
            } else
                $finnal = $res;
        } elseif ($services) {
            $finnal = ObjectListView::model()->getByServices($services, $car_gruppa_id, @$_REQUEST['myLat'], @$_REQUEST['myLon'], $town_id, $limit, $user_id);
            $this->addFullCostAndTime($finnal, $services, $car_gruppa_id);
        } else {
            $finnal = ObjectListView::model()->getFirstX($limit, @$_REQUEST['myLat'], @$_REQUEST['myLon'], $town_id, $user_id);
        }


        $this->addUdobstva($finnal);

        if (count($finnal) > $limit)
            array_splice($finnal, $limit);
        $this->_sendResponse(200, CJSON::encode($finnal));
    }

    private function mergeArrays($washers_list, $washers_filters)
    {
        $washers_list = array_map("unserialize", array_unique(array_map("serialize", $washers_list)));
        $weis = array();
        define("N", 11);

        foreach ($washers_list as $washer) {
            $weis[$washer['id']] = 0;
            $i = 0;

            foreach ($washers_filters as $filter) {
                if (array_search($washer, $filter) !== false) {
                    $weis[$washer['id']] += (N - array_search($washer, $filter)) * (6 - $i);
                }
                $i++;
            }
        }

        $result_array = array();

        arsort($weis);
        $id_array = array_keys($weis);
        $i = 0;

        $result_array = array();
        foreach ($id_array as $id) {
            $result_array[$i] = $this->getWasherById($washers_list, $id);
            $i++;
        }

        return $result_array;
    }

    private function getWasherById($washers, $id)
    {
        foreach ($washers as $washer) {
            if ($washer["id"] == $id)
                return $washer;
        }
    }

    private function arrayLogicAnd($arr1, $arr2)
    { // сортировка сохраняется по arr1
        $result = array();
        foreach ($arr1 as $el) {
            if (array_search($el, $arr2) !== false) {
                array_push($result, $el);
            }
        }
        if (empty($result))
            $result = $arr2; // если в фильтрах нет требуемых моек то берем только требуемые
        return $result;
    }

    private function addFullCostAndTime(&$ObjectList, $services, $car_id)
    {
        foreach ($ObjectList as &$object) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'id_object = :object AND car_gruppa_id=:car_id';
            $criteria->params = array(
                ':object' => $object["id"],
                ':car_id' => $car_id,
            );

            $criteria->addInCondition("id_usluga", $services);

            $services_data = UslugaObject::model()->findAll($criteria);
            $full_cost = 0;
            $full_time = 0;
            foreach ($services_data as $s_data) {
                $full_cost += $s_data["cena"];
                $full_time += $s_data["dlitelnost"];
            }
            $object["full_cost"] = $full_cost;
            $object["full_time"] = $full_time;
        }
    }

    private function addUdobstva(&$ObjectList)
    {
        foreach ($ObjectList as &$object) {
            $udobstvoList = UdobstvoObject::model()->findAll('object_id = :obj', array(":obj" => $object["id"]));
            $temparray = array();
            foreach ($udobstvoList as $udobstvo) {
                $temparray[$udobstvo->udobstvo->drawable] = $udobstvo->udobstvo->drawable;
            }
            $object["udobstvo"] = $temparray;
        }
    }

    public function actiontest()
    {
        Zapis::model()->getRecordData(203);
        //    $data = 1;
    }

}
