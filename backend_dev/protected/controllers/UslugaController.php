<?php

class UslugaController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'SaveUslugiAndAdd'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'ViewByTip', 'UpdateVar'),
                'roles' => array('administrator'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionUpdateVar()
    {
        $es = new TbEditableSaver('Usluga'); //'User' is name of model to be updated
        $es->update();
    }

    public function actionSaveUslugiAndAdd($id_obj, $id_group)
    {

        if (isset($_POST['model_to_insert']))
            if (isset($_POST['model_temp']))
                $this->InstertLink(array_diff($_POST['model_to_insert'], $_POST['model_temp']), $id_obj, $id_group); //массив на добавление
            else
                $this->InstertLink($_POST['model_to_insert'], $id_obj, $id_group);

        if (isset($_POST['model_temp']))
            if (isset($_POST['model_to_insert']))
                //массив на удаление
                $this->DeleteLink(array_diff($_POST['model_temp'], $_POST['model_to_insert']), $id_obj, $id_group);
            else
                $this->DeleteLink($_POST['model_temp'], $id_obj, $id_group);
        $tab = $this->getTabName($id_group);

        $this->redirect(array('/Object/View', 'id' => $id_obj, 'tab' => $id_group));
    }

    private function getTabName($id)
    {
        switch ($id) {
            case 18:
                return 'Легковая';
            case 19:
                return 'Минивен';
            case 20:
                return 'Джип';
            case 21:
                return 'Микроавтобус';
            case 22:
                return 'Газель';
            default:
                return 'Общая информация';
        }
    }

    private function InstertLink($IdArr, $idObj, $id_group)
    {
        foreach ($IdArr as $IdUsluga) {
            $link = new UslugaObject;
            $link->id_object = $idObj;
            $link->car_gruppa_id = $id_group;
            $link->id_usluga = $IdUsluga;
            $link->cena = '1';
            $link->dlitelnost = '0';
            $link->zarplata = '0';
            $link->save();
        }
    }

    private function DeleteLink($IdArr, $idObj, $id_group)
    {
        $criteria = new CDbCriteria;
        $criteria->addInCondition('id_usluga', $IdArr);
        $criteria->addCondition('id_object=:idObj', 'AND');
        $criteria->addCondition('car_gruppa_id=:idGroup', 'AND');
        $criteria->params += array(':idObj' => $idObj);
        $criteria->params += array(':idGroup' => $id_group);
        UslugaObject::model()->deleteAll($criteria);
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id)
    {
        $model = new Usluga;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Usluga'])) {
            $model->attributes = $_POST['Usluga'];
            if ($model->save())
                $this->redirect(array('ViewByTip', 'id' => $model->usluga_tip_id));
        }

        $this->render('create', array(
            'model' => $model,
            'tip' => $id,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Usluga'])) {
            $model->attributes = $_POST['Usluga'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Usluga');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Usluga('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Usluga']))
            $model->attributes = $_GET['Usluga'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionViewByTip($id)
    {
        $model = new Usluga('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Usluga']))
            $model->attributes = $_GET['Usluga'];
        $model->usluga_tip_id = $id;
        $this->render('view_by_tip', array(
            'model' => $model,
            'tip_id' => $id,
            'tip_name' => UslugaTip::model()->findByPk($id)->nazvanie,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Usluga the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Usluga::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Usluga $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usluga-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
