<?php

class SystemLogController extends Controller {

    public function actionClear() {
        file_put_contents(Yii::app()->basePath.'/runtime/application.log', '');
        $this->render('index');
    }

    public function actionIndex() {
        $this->render('index');
    }

}
