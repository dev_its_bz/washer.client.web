<?php

class ObjectController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('ViewForClient', 'getVremyaCombo'),
                'roles' => array('client'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'ObjectByAgent', 'savecoords', 'sampleOfObj', 'androidGetList', 'admin'),
                'roles' => array('manager'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'UpdateZnachenie', 'delete'),
                'roles' => array('owner'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionGetVremyaCombo($object_id, $resource, $interval)
    {
        $data = "CURRENT_DATE() + INTERVAL $interval DAY";
        $sql = "
SELECT id, box_id, SUBSTR(vremya,1,5) AS vremya
FROM object_resource_view
WHERE object_id=$object_id AND `data`=$data AND resource>=$resource
";
        $mdl = ObjectResourceView::model()->findAllBySql($sql);
        $list = '[';
        foreach ($mdl AS $row) {
            $list .= '{"id":"' . $row->id . '","text":"' . $row->vremya . '"},';
        }
        $list .= '{"id":"0", "text":"----"}]';
        echo $list;
    }

    public function actionUpdateZnachenie()
    {
        $es = new TbEditableSaver('Object');
        $es->update();
    }

    public function actionGetFreeTime($objectId, $data, $resource)
    {

    }

    public function actionViewForClient($object_id = null, $uslugi = null, $car_id = null, $data = null)
    {
        if ($object_id == null)
            return;
        $model = $this->loadModel($object_id);
        $this->renderPartial('viewForClient', array('model' => $model, 'uslugi' => $uslugi, 'car_id' => $car_id, 'data' => $data));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Object::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {

        $model = $this->loadModel($id);
        $this->render('view', array(
            'model' => $model,
            'gMap' => $this->prepareGmap($this->loadModel($id)),
            'kontragent' => Kontragent::model()->findByPk($model->kontragent_id),
        ));
    }

    protected function prepareGmap($model)
    {
//
// ext is your protected.extensions folder
// gmaps means the subfolder name under your protected.extensions folder
//
        Yii::import('ext.EGMap.*');

        $gMap = new EGMap();
        $gMap->setWidth('100%');
        $gMap->setHeight(550);
        $gMap->zoom = 10;
        $mapTypeControlOptions = array(
            'position' => EGMapControlPosition::RIGHT_TOP,
            'style' => EGMap::MAPTYPECONTROL_STYLE_HORIZONTAL_BAR
        );

        $gMap->mapTypeId = EGMap::TYPE_ROADMAP;
        $gMap->mapTypeControlOptions = $mapTypeControlOptions;

// Preparing InfoWindow with information about our marker.
        $info_window_a = new EGMapInfoWindow("<div class='gmaps-label' style='color: #000;'>{$model->nazvanie}</div>");

// Setting up an icon for marker.
        $icon = new EGMapMarkerImage("http://google-maps-icons.googlecode.com/files/car.png");

        $icon->setSize(32, 37);
        $icon->setAnchor(16, 16.5);
        $icon->setOrigin(0, 0);

// Saving coordinates after user dragged our marker.
        $dragevent = new EGMapEvent('dragend', "function (event) { $.ajax({
                                            'type':'POST',
                                            'url':'" . $this->createUrl('object/saveCoords') . '/' . $model->id . "',
                                            'data':({'lat': event.latLng.lat(), 'lng': event.latLng.lng()}),
                                            'cache':false,
                                        });}", false, EGMapEvent::TYPE_EVENT_DEFAULT);


// If we have already created marker - show it
        if ($model->havemap != 0) {
            $sample_address = $model->gorod->nazvanie . ', ' . $model->adres;
            $geocoded_address = new EGMapGeocodedAddress($sample_address);
            $geocoded_address->geocode($gMap->getGMapClient());

            if ($geocoded_address->getLat() != NULL && $geocoded_address->getLng() != NULL) {
                $gMap->addMarker(
                    new EGMapMarker($geocoded_address->getLat(), $geocoded_address->getLng(), array())
                );
            }

            $marker = new EGMapMarkerWithLabel($model->lat, $model->lon, array('title' => Yii::t('object', $model->nazvanie),
                'icon' => $icon, 'draggable' => true), 'marker', array('dragevent' => $dragevent));
            $marker->addHtmlInfoWindow($info_window_a);

            $label_options = array(
                'backgroundColor' => 'black',
                'padding' => '5px',
                'opacity' => '0.65',
                'width' => 'auto',
                'color' => 'white'
            );
            $marker->labelContent = "<strong>{$model->nazvanie}</strong><br/>$model->adres";
            $marker->labelStyle = $label_options;

            $marker->setLabelAnchor(new EGMapPoint(-15, 15));


            $gMap->addMarker($marker);
            $gMap->setCenter($model->lat, $model->lon);
            $gMap->zoom = 16;

// If we don't have marker in database - make sure user can create one
        } else {
            $gorod = Gorod::model()->findByPk($model->gorod_id);
            $gMap->setCenter($gorod->lat, $gorod->lon);
            $gMap->zoom = 16;
            // Setting up new event for user click on map, so marker will be created on place and respectful event added.
            $gMap->addEvent(new EGMapEvent('click', 'function (event) {var marker = new google.maps.Marker({position: event.latLng, map: ' . $gMap->getJsName() .
                ', draggable: true, icon: ' . $icon->toJs() . '}); ' . $gMap->getJsName() .
                '.setCenter(event.latLng); var dragevent = ' . $dragevent->toJs('marker') .
                '; $.ajax({' .
                '"type":"POST",' .
                '"url":"' . $this->createUrl('object/savecoords') . "/" . $model->id . '",' .
                '"data":({"lat": event.latLng.lat(), "lng": event.latLng.lng()}),' .
                '"cache":false,' .
                '}); }', false, EGMapEvent::TYPE_EVENT_DEFAULT_ONCE));
        }
        return $gMap;
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id)
    {
        $model = new Object;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Object'])) {
            $model->attributes = $_POST['Object'];
            $model->avtor_zapisi_id = Yii::app()->user->id;
            $model->kontragent_id = $id;
            if ($model->save())
                $this->redirect(array('/object/view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
            'grafik' => GrafikRaboty::model()->getForFilter($id),
            'gorod' => Gorod::model()->getForFilter(),
            'kontragent' => Kontragent::model()->findByPk($id),
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Object'])) {
            $model->attributes = $_POST['Object'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
            'grafik' => GrafikRaboty::model()->getForFilter($model->kontragent_id),
            'gorod' => Gorod::model()->getForFilter(),
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new Object('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Object']))
            $model->attributes = $_GET['Object'];
        if (Yii::app()->user->role == 'owner')
            $model->kontragent_id = Yii::app()->user->kontragent_id;
        else
            if (Yii::app()->user->role == 'manager')
                $model->kontragent_id = Yii::app()->user->kontragent_id;
        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionObjectByAgent($id)
    {
        $model = new Object('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Object']))
            $model->attributes = $_GET['Object'];

        $model->kontragent_id = $id;

        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionSampleOfObj()
    {
        $model = new Object('search');
        $model->unsetAttributes(); // clear any default values

        if (isset($_GET['Object']))
            $model->attributes = $_GET['Object'];

        $criteria = new CDbCriteria();
        $criteria->addInCondition("t.id", Yii::app()->user->objects);
        $dp = new CActiveDataProvider('Object', array(
            'criteria' => $criteria,
        ));

        $newDataProvider = $model->search();
        $dp->criteria->mergeWith($newDataProvider->criteria);


        $this->render('admin', array(
            'model' => $model, 'dp' => $dp,
        ));
    }

    public function actionAdmin()
    {
        $model = new Object('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Object']))
            $model->attributes = $_GET['Object'];
        //$dp = new CActiveDataProvider('Object');
        $this->render('admin', array(
            'model' => $model, 'dp' => $model->search(),
        ));
    }

    public function viewObjectByKontragent($id)
    {
        $model = new Object('search');
        $model->unsetAttributes(); // clear any default values
        $model->kontragent_id = $id;
        $this->renderPartial('admin', array(
            'model' => $model,
        ));
    }

    public function actionSaveCoords($id)
    {
        $model = $this->loadModel($id);
        $model->lat = $_POST['lat'];
        $model->lon = $_POST['lng'];
        $model->havemap = 1;
        $model->save();
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'object-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
