<?php

class PolzovatelController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('Registration', 'CodeAgain', 'checkCode', 'AddToDb', 'PrivateOffice', 'CheckUnique'),
                'users' => array('?'),
            ),
            array('allow',
                'actions' => array('dashboard', 'UpdateZnachenie', 'update', 'view'),
                'users' => array('@')),

            array('allow',
                'actions' => array('addNomer', 'deleteCar', 'AddCar', 'GetCars'),
                'users' => array('client')),

            array('allow',
                'actions' => array('ExecutorSalary', 'index', 'view', 'LinkToObject', 'UserByObject', 'admin'),
                'roles' => array('manager'),
            ),
            array('allow',
                'actions' => array('delete', 'SaveUsersAndAdd', 'UpdateZnachenie', 'create', 'update'),
                'roles' => array('owner'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionDashboard()
    {
        switch (Yii::app()->user->role) {
            case 'administrator':
                $this->render('administrator/dashboard');
                break;
            case 'client':
                $this->render('client/dashboard');
                break;
            case 'owner':
                $this->render('owner/dashboard');
                break;
            case 'manager':
                $this->render('manager/dashboard');
                break;
            case 'buh':
                $this->render('buh/dashboard');
                break;
            default:
                $this->redirect('/');
                break;
        }
    }

    public function actionUpdateZnachenie()
    {
        $es = new TbEditableSaver('Polzovatel');
        $es->update();
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id = null)
    {
        if (!isset($id))
            $id = Yii::app()->user->id;
        $model = $this->loadModel($id);

        if (Yii::app()->user->role == 'client') {
            $this->render('client/view', array(
                'model' => $model,
            ));
        } else
            $this->redirect("/polzovatel/update/$id");
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model = Polzovatel::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionCheckUnique()
    {
        $polzovatel = Polzovatel::model()->findAllByAttributes(array('login' => $_POST['login']));
        if (sizeof($polzovatel) == '0') {
            $code = rand(1000, 9999);
            Yii::app()->session->add("code", $code);
            Yii::app()->sms->send($_POST['login'], $code);
            echo '1';
        } else {
            echo 'Пользователь с таким номером уже зарегистрирован в системе.';
        }
    }

    public function actionPrivateOffice()
    {
        /*
        $u_id = Yii::app()->user->id;
        $mark_data = CHtml::listData(CarMarka::model()->findAll(), 'id', 'nazvanie');
        $q = PolzovatelCar::model()->getUserCar($u_id);
        $this->render('privateOffice', array('marka_list' => $mark_data, 'query' => $q));
        */
        $model = new PolzovatelCar();
        $model->unsetAttributes(); // clear any default values
        $model->polzovatel_id = Yii::app()->user->id;
        $model->deleted = '0';
        $this->render('privateOffice', array('model' => $model));
    }

    public function actionGetCars()
    {
        $model = CarModel::model()->findAll('t.car_marka_id=:idc', array(':idc' => (int)$_POST['marka_id']));
        foreach ($model as $val) {
            echo CHtml::tag('option', array('value' => $val->id), CHtml::encode($val->nazvanie), true);
        }
    }

    public function actionaddNomer()
    {
        $model = PolzovatelCar::model()->findByPk($_POST['id']);
        $model->gos_nomer = $_POST['nomer'];
        $model->save();
    }

    public function actionAddCar()
    {
        $u_id = Yii::app()->user->id;
        $model = new PolzovatelCar;
        $model->car_model_id = $_POST['car_id'];
        $model->polzovatel_id = $u_id;
        $model->save();
        $q = PolzovatelCar::model()->getUserCar($u_id);
        echo json_encode($q);
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id = 0)
    {
        $model = new Polzovatel;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Polzovatel'])) {
            $model->attributes = $_POST['Polzovatel'];
            $model->avtor_zapisi_id = Yii::app()->user->id;
            $model->kontragent_id = $id;
            if ($model->save())
                $this->redirect(array('/kontragent/view/', 'id' => $model->kontragent_id, 'tab' => '2'));
        }

        $this->render('create', array(
            'model' => $model, 'groups' => PolzovatelGruppa::model()->getForFilter(),
            'status' => PolzovatelStatus::model()->getForFilter()
        ));
    }

    public function actionCheckCode()
    {
        if ($_POST['activationCode'] == Yii::app()->session->get("code")) {
            $model = new Polzovatel;
            $model->login = $_POST['login'];
            $model->telefon = $_POST['login'];
            $model->parol = md5(md5($_POST['parol']));
            $model->gruppa_id = $_POST['tip'] == 0 ? 3 : 4; //3 - пользак, 4 - контрагент
            $model->status_id = 2;
            if ($_POST['tip'] == 1) {
                $kontragent = new Kontragent();
                $kontragent->nazvanie = "Новый контрагент";
                $kontragent->status_id = "2";
                $kontragent->save();
                $model->kontragent_id = $kontragent->getPrimaryKey();
            } else
                $model->kontragent_id = 1;

            if ($model->save()) {
                $identity = new UserIdentity($model->login, $_POST['parol']);
                $identity->authenticate();
                Yii::app()->user->login($identity);
                echo '1';
            } else
                echo "Ошибка сохранения данных.";
        } else
            echo 'Проверочный код указан не верно.';
    }

    public
    function actionCodeAgain()
    {

        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            $user = $_POST['Polzovatel_reg'];

            Yii::app()->sms->send($user['telefon'], $this->getCode($user['telefon']));
            echo 'пароль отправлен повторно';
        }
    }

    private
    function getCode($phone)
    {
        $str = crypt($phone, $this->generateSalt());
        $str_short = substr($str, 0, 2);
        $code = bin2hex($str_short);
        Yii::app()->session->add("code", $code);
        return $code;
    }

    private
    function generateSalt($length = 2)
    {
        $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }

    public
    function actionAddToDb()
    {
        $model = new Polzovatel_reg;
        if (isset($_POST['Polzovatel_reg'])) {
            $model->attributes = $_POST['Polzovatel_reg'];
            $model->login = $_POST['Polzovatel_reg']['telefon'];
            $model->status = 2;
            $model->gruppa_id = 3;
            $model->save();
        };

        $_model = new LoginForm;
        $_model->attributes = $_POST['Polzovatel_reg'];
        $_model->username = $_POST['Polzovatel_reg']['telefon'];
        $_model->password = $_POST['Polzovatel_reg']['parol'];
        $_model->login();
    }

    public
    function actionRegistration()
    {
        if (Yii::app()->user->isGuest) {
            $user = new Polzovatel();
            $this->performAjaxValidation($user);
            if (empty($_POST['Polzovatel'])) {
                /*
                * Если форма не отправленна, то выводим форму
                */
                $this->render('/polzovatel/registration', array('model' => $user));

            } else {
                /*
                * Форма получена
                */
                $user->attributes = $_POST['Polzovatel'];

                //Проверяем на уникальность имя пользователя(телефон)
                if ($user->model()->count("login = :username",
                    array(':username' => $user->login))
                ) {

                    $user->addError('login', 'Имя пользователя уже занято');
                    $this->render("registration", array('model' => $user));

                } else {
                    /*
                    * Если проверки пройдены шифруем пароль, генерируем код
                    * активации аккаунта, а также устанавливаем время регистрации
                    * и роль по умолчанию для пользователя
                    */
                    $user->parol = md5(md5($user->parol));
                    $code = rand(1000, 9999);
                    Yii::app()->session->add("code", $code);
                    Yii::app()->sms->send($user->login, $code);

                    /*
                    * Проверяем если добавление пользователя прошло успешно
                    * устанавливаем ему права.
                    */

                    $this->render("activation", array('model' => $user));
                }
            }
        } else {
            /*
            * Если пользователь залогинен редиректим обратно
            */
            $this->redirect(Yii::app()->user->returnUrl);
        }
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected
    function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'polzovatel-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public
    function actionUpdate($id = null)
    {
        if (!isset($id))
            $id = Yii::app()->user->id;

        $model = $this->loadModel($id);
        $this->performAjaxValidation($model);

        if (isset($_POST['Polzovatel'])) {
            $model->attributes = $_POST['Polzovatel'];
            if ($model->save())
                $this->redirect(array('/kontragent/view/', 'id' => $model->kontragent_id, 'tab' => '2'));
        }

        $this->render('update', array(
            'model' => $model, 'groups' => PolzovatelGruppa::model()->getForFilter(),
            'status' => PolzovatelStatus::model()->getForFilter()
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public
    function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();

            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Просмотр сотрудников в объекте.
     */
    public
    function viewPolzovatel($id)
    {
        $model = new Polzovatel('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Polzovatel']))
            $model->attributes = $_GET['Polzovatel'];
        $model->object_id = $id;
        $this->renderPartial('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Lists all models.
     */
    public
    function actionIndex()
    {
        $model = new Polzovatel('search');
        $model->unsetAttributes(); // clear any default values
        $model->kontragent_id = '1';
        $kontragent = $object = null;
        if (isset($_GET['kontragent_id'])) {
            $model->kontragent_id = $_GET['kontragent_id'];
            $kontragent = new Kontragent('search');
            $kontragent = $kontragent->findByPk($model->kontragent_id);
        }

        if (isset($_GET['object_id'])) {
            $model->object_id = $_GET['object_id'];
            $object = new Object('search');
            $object = $object->findByPk($model->object_id);
        }

        if (isset($_GET['Polzovatel']))
            $model->attributes = $_GET['Polzovatel'];

        if (Yii::app()->user->role == 'owner')
            $model->kontragent_id = Yii::app()->user->kontragent_id;
        else
            if (Yii::app()->user->role == 'manager')
                $model->kontragent_id = Yii::app()->user->kontragent_id;

        $this->render('index', array(
            'model' => $model, 'kontragent' => $kontragent, 'object' => $object
        ));
    }

    /**
     * Manages all models.
     */
    public
    function actionAdmin()
    {
        $model = new Polzovatel('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Polzovatel']))
            $model->attributes = $_GET['Polzovatel'];

        if (isset($_GET['kontragent_id']))
            $model->kontragent_id = $_GET['kontragent_id'];
        elseif (isset($_GET['object_id']))
            $model->object_id = $_GET['object_id'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public
    function actionSaveUsersAndAdd($id, $idK)
    {

        if (isset($_POST['model_to_insert']))
            if (isset($_POST['model_temp']))
                $this->InstertLink(array_diff($_POST['model_to_insert'], $_POST['model_temp']), $id); //массив на добавление
            else
                $this->InstertLink($_POST['model_to_insert'], $id);

        if (isset($_POST['model_temp']))
            if (isset($_POST['model_to_insert']))
//массив на удаление
                $this->DeleteLink(array_diff($_POST['model_temp'], $_POST['model_to_insert']), $id);
            else
                $this->DeleteLink($_POST['model_temp'], $id);

        $this->redirect(array('/object/view', 'id' => $id, 'tab' => 3));
    }

    private
    function InstertLink($IdArr, $idObj)
    {
        foreach ($IdArr as $IdUser) {
            $link = new PolzovatelObject;
            $link->object_id = $idObj;
            $link->polzovatel_id = $IdUser;
            $link->avtor_zapisis_id = Yii::app()->user->id;
            $link->save();
        }
    }

    private
    function DeleteLink($IdArr, $idObj)
    {
        $criteria = new CDbCriteria;
        $criteria->addInCondition('polzovatel_id', $IdArr);
        $criteria->addCondition('object_id=:idObj', 'AND');
        $criteria->params += array(':idObj' => $idObj);
        PolzovatelObject::model()->deleteAll($criteria);
    }

    public
    function actionExecutorSalary($id)
    {
        $connection = Yii::app()->db;
        $sql = "SELECT polzovatel.id, polzovatel.fio "
            . "FROM polzovatel INNER JOIN polzovatel__object "
            . "on polzovatel.id = polzovatel__object.polzovatel_id "
            . "WHERE dolzhnost = 'Мойщик' AND "
            . "polzovatel__object.object_id = " . $id;
        $dataReader = $connection->createCommand($sql)->query();
        $rows = $dataReader->readAll();
        foreach ($rows as &$row) { //шагаем по рабочим
            $works = ZapisIspolnitel::model()->findAll('ispolnitel_id = :id_ex', array(':id_ex' => $row['id']));
            $salary = 0;
            foreach ($works as $work) { //шагаем по списку выполненых записей
                $executor_count = ZapisIspolnitel::model()->count('zapis_id=:id_z', array(':id_z' => $work->zapis_id));
                // количество людей обрабатывавших запись
                $services = ZapisUsluga::model()->findAll('zapis_id = :id_z', array(':id_z' => $work->zapis_id));

                foreach ($services as $service) { //шагаем по списку выполненых услуг
                    $temp_sapalary = $service->cena * (UslugaObject::model()->findByPk($service->usluga_id)->zarplata / 100);
                    $helper_count = Pomoshniki::model()->count('id_zapis_usluga=:id_z_u', array(':id_z_u' => $service->id));
                    $temp_sapalary /= $helper_count + $executor_count; // вычислили долю
                    $salary += $temp_sapalary;
                }
            }

            $helps = Pomoshniki::model()->findAll('id_pomoshnik = :id_p', array(':id_p' => $row['id']));
            foreach ($helps as $help) {

                $service = ZapisUsluga::model()->findByPk($help->id_zapis_usluga); //нашли запись
                $executor_count = ZapisIspolnitel::model()->count('zapis_id=:id_z', array(':id_z' => $service->zapis_id));
                $helper_count = Pomoshniki::model()->count('id_zapis_usluga=:id_z_u', array(':id_z_u' => $help->id_zapis_usluga));
                $full_count = $helper_count + $executor_count;
                // узнали сколько людей работало над услугой
                $temp_sapalary = $service->cena * (UslugaObject::model()->findByPk($service->usluga_id)->zarplata / 100);
                $temp_sapalary /= $full_count; // вычислили долю
                $salary += $temp_sapalary;
            }


            $row["salary"] = $salary;
        }

        $this->render('zarplata_rabochie', array(
            'data' => $rows,
        ));
    }

    public
    function actionLinkToObject($id)
    {
        $model = new Polzovatel('search');

        $model->unsetAttributes();

        if (isset($_GET['Polzovatel']))
            $model->attributes = $_GET['Polzovatel'];

        $this->render('linkToObject', array(
            'model' => $model,
            'object_id' => $id,
            'kontragent_id' => Object::model()->find('id=:idObj', array('idObj' => $id))->kontragent_id,
        ));
    }

}
