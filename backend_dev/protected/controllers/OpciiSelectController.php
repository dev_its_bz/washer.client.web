<?php

class OpciiSelectController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function actions()
    {
        return array(
            'toggle' => array(
                'class'=>'bootstrap.actions.TbToggleAction',
                'modelName' => 'OpciiSelect',
            )
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
       
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('UpdateZnachenie', 'viewOpcii', 'index', 'view', 'UpdateStatus'),
                'roles' => array('manager'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete','create', 'update', 'UpdateStatus'),
                'roles' => array('administrator'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new OpciiSelect;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['OpciiSelect'])) {
            $model->attributes = $_POST['OpciiSelect'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdateZnachenie() {
        $es = new TbEditableSaver('OpciiSelect');  //'User' is name of model to be updated
        $es->update();
        // var_dump($_POST);
    }

    public function actionUpdateStatus() {
        if ($_POST['status'] != 0) {
            $model = new OpciiSelect;
            $model->opcii_id = (int) $_POST['opcii_id'];
            $model->object_id = (int) $_POST['object_id'];
            $model->save();
        } else {
            $model = OpciiSelect::model()->findByPk($_POST['id']);
            $model->delete();
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['OpciiSelect'])) {
            $model->attributes = $_POST['OpciiSelect'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex($id = 0) {
        $model = new OpciiSelect('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['OpciiSelect']))
            $model->attributes = $_GET['OpciiSelect'];
        if ($id != 0) {
            $model->object_id = $id;
        }
        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function viewOpcii($id) {
        $model = new OpciiSelect('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['OpciiSelect']))
            $model->attributes = $_GET['OpciiSelect'];
        if ($id != 0) {
            $model->object_id = $id;
        }
        $this->renderPartial('index', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new OpciiSelect('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['OpciiSelect']))
            $model->attributes = $_GET['OpciiSelect'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = OpciiSelect::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'opcii-select-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
