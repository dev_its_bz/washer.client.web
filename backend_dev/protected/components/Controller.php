<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/guest_layout';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function init()
    {
        if (isset(Yii::app()->user->role)) {
            switch (Yii::app()->user->role) {
                case 'administrator':
                    $this->layout = '//layouts/admin_layout';
                    break;
                case 'client':
                    $this->layout = '//layouts/client_layout';
                    break;
                case 'owner':
                    $this->layout = '//layouts/owner_layout';
                    break;
                case 'manager':
                    $this->layout = '//layouts/manager_layout';
                    break;
                case 'buh':
                    $this->layout = '//layouts/buh_layout';
                    break;
                default:
                    $this->layout = '//layouts/guest_layout';
                    break;
            }
        }
    }
}