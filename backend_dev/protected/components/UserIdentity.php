<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    private function getUserData($user_name) {
    $users = Yii::app()->db->createCommand()
        ->select('parol, id, gruppa_id, status_id, kontragent_id')
        ->from('washer.polzovatel')
        ->where('login=:log', array(':log' => $user_name))
        ->queryRow();
    return $users;
}
protected $_id;
    public function authenticate() {
        $user_data = $this->getUserData($this->username);

        if (!isset($this->username))
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        elseif ($user_data['parol'] !== $this->password)
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        elseif ($user_data['status_id'] != '2')
            $this->errorCode = self::STATUS_ERROR;
        else {
            $this->errorCode = self::ERROR_NONE;
            $this->_id= $user_data['id'];
            $this->setState('id', $user_data['id']);
            $this->setState('gruppa_id', $user_data['gruppa_id']);
            $this->setState('role', $this->getRole($user_data['gruppa_id']));
            $this->setState('kontragent_id', $user_data['kontragent_id']);
            if ($user_data['gruppa_id']=='5') $this->setState ('objects', $this->getObjId($user_data['id']));
        }
        
        return !$this->errorCode;
    }
    private function getRole($id){
      switch ($id){
      case 2: return 'administrator';
      case 4: return 'owner';
      case 5: return 'manager';
      default: return 'guest';
      }
        
        
        
        
        /*$role = Yii::app()->db->createCommand()
        ->select('nazvanie')
        ->from('washer.polzovatel_gruppa')
        ->where('id=:idr', array(':idr' => $id))
        ->queryRow();

         return $role['nazvanie'];
    */
    }
    private function getObjId($iduser) {
        $objects = Yii::app()->db->createCommand()
        ->select('object_id')
        ->from('washer.polzovatel__object')
        ->where('polzovatel_id=:idr', array(':idr' => $iduser))
        ->queryAll();
        $mass;
        foreach ($objects as $id){
            $mass[]=$id['object_id'];
        }  
      
        return $mass;
    }
    public function getId(){
        return $this->_id;
    }

}
