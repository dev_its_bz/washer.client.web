<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

// change the following paths if necessary
$yii = dirname(__FILE__) . '/../framework/YiiBase.php';
$config = dirname(__FILE__) . '/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once($yii);

class Yii extends YiiBase {
    /**
     * @static
     * @return WebApplication
     */
    public static function app() {
        return parent::app();
    }
}

class WebApplication extends CWebApplication {
    /**
     * @return TWebUser
     */
    public function getUser() {
        return parent::getUser();
    }
}

Yii::createApplication('WebApplication',$config)->run();
//Yii::createWebApplication($config)->run();
?>
